import { BaseUrl } from '../constants/api';
import { Alert } from 'react-native';

export const SuggestionBynamesuggestionApi = (token, search_value) => {
  var formData = new FormData();
  formData.append('search_value', search_value);
  return async (dispatch) => {
    dispatch({ type: 'Request_Suggestion_By_Name' });
    fetch(`${BaseUrl}/user/getItemSuggestion`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('sugesstion', res);
        if (res.status == 'success') {
          dispatch({ type: 'Suggestion_By_Name', payload: res });
        } else {
          // Alert.alert(res.message)
          dispatch({ type: 'Suggestion_By_Name', payload: res });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_Suggestion_By_Name' });
      });
  };
};
