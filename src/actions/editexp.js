import { Actions } from 'react-native-router-flux';
import { BaseUrl } from '../constants/api';

export const editexpApi = (token, date, feeling_description, feeling_status, items, id) => {
  var formData = new FormData();
  formData.append('date', date);
  formData.append('feeling_description', feeling_description);
  formData.append('feeling_status', feeling_status);
  formData.append('items', JSON.stringify(String((items))));
  formData.append('id', Number(id));
  return async (dispatch) => {
    dispatch({ type: 'Edit_Exp_Request' });
    fetch(`${BaseUrl}/user/editExperience`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token

      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('editexp', res);
        if (res.status == 'success') {
          dispatch({ type: 'Editexp', payload: res });
          Actions.Calendar()
        } else {
          dispatch({ type: 'Edit_Exp_Failure' });
          // Alert.alert('Something went wrong');
        }
      })
      .catch((e) => {
        dispatch({ type: 'Edit_Exp_Failure' });
        console.log('kjfgdghdf', e);
      });
  };
};
