import { Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { BaseUrl } from '../constants/api';

export const GetbarcodeDataApi = (token, barcode_value , type) => {
  var formData = new FormData();
  formData.append('barcode_value', barcode_value);
  formData.append('type', type);

  console.log('formdata', formData)
  return async (dispatch) => {
    const modelCheck = await AsyncStorage.getItem('Model')
    dispatch({ type: 'Request_BarcodeData' });
    fetch(`${BaseUrl}/user/getBarcodeData`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('BarcodeData', res);
        if (res.status == 'success') {
          if (modelCheck === '1') {
            Actions.ScannedScreen(res);
          }
          dispatch({ type: 'BarcodeData', payload: res });
          AsyncStorage.setItem('Exit', '1')
        } else {
          dispatch({ type: 'Failure_BarcodeData' });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_BarcodeData' });
        console.log('Error', e);
      });
  };
};
