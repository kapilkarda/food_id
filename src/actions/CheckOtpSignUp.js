import { BaseUrl } from '../constants/api';
import {Actions} from 'react-native-router-flux';

export const checkOtpsignUpApi = (mobile, otp , passwordChange) => {
  console.log('passwordChange' , passwordChange)
  var formData = new FormData();
  formData.append('mobile', mobile);
  formData.append('otp', otp);
  return async (dispatch) => {
    dispatch({ type: 'Check_OtpSignup', payload: '' });
    dispatch({ type: 'Request_Check_OtpSignup' });
    fetch(`${BaseUrl}/user/otpCheck`, {
      method: 'POST', 
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('checkOtp res', res);
        if (res.status == 'success') {
          dispatch({ type: 'Check_OtpSignup', payload: res });
          if(passwordChange === 'passwordChange')
          {
          Actions.passwordChange(mobile)
          }
        } else {
          dispatch({ type: 'Check_OtpSignup', payload: res });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_Check_OtpSignup' });
      });
  };
};
