import {BaseUrl} from '../constants/api';

export const SearchFood = (token, search_value) => {
  console.log('search_value', search_value);
  var formData = new FormData();
  formData.append('search_value', search_value);
  return async (dispatch) => {
    dispatch({type: 'Request_Search_Food'});
    fetch(`${BaseUrl}/user/searchFoodForWatchout`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token,
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('Foods Res', res);
        if (res.status == 'success') {
          let apiData = res.data;
          for (let item of apiData) {
            if (item.type == 'category') {
              const firstName = item.name.toString().slice(0, 1);
              item.first_name = firstName;
            }
            console.log('firstName', item.first_name)
          }
        
          // let apiData = res.data;
          // console.log("apiData",apiData);
          // for(let item of apiData){
          //     item.isSelected = false
          // }
          dispatch({type: 'Search_Food', payload: res});
        } else {
          dispatch({type: 'Search_Food', payload: res});
        }
      })
      .catch((e) => {
        dispatch({type: 'Failure_Search_Food'});
      });
  };
};
