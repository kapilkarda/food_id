import { BaseUrl } from '../constants/api';

export const getFoodCategoryApi = (token) => {
  return async (dispatch) => {
    dispatch({type: 'Get_Food_SubCategory', payload: ''});
    dispatch({ type: 'Request_Get_Food_Category' });
    fetch(`${BaseUrl}/user/getFoodCategory`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token
      },
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('food Category', res);
        if (res.status == 'success') {
          dispatch({ type: 'Get_Food_Category', payload: res.data });
        } else {
          dispatch({ type: 'Failure_Get_Food_Category' });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_Get_Food_Category' });
      });
  };
};
