import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import { BaseUrl } from '../constants/api';

export const RgistrationCheckApi = (email, mobile, username) => {
  let data = {
    email: email,
    mobile: mobile,
    username: username,
  };
  var formData = new FormData();
  formData.append('email', email);
  formData.append('mobile', mobile);
  console.log('data', formData);
  return async (dispatch) => {
    dispatch({ type: 'Check', payload: '' });
    dispatch({ type: 'Request_Check' });
    fetch(`${BaseUrl}/user/registrationCheck`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('check res', res);
        if (res.status === 'success') {
          AsyncStorage.setItem('status', res.status);
          AsyncStorage.removeItem('message')
          dispatch({ type: 'Check', payload: res.data });
          Actions.join3(data, username);
        } else if (res.status === 'fail') {
          AsyncStorage.setItem('message', res.message);
          dispatch({ type: 'Check', payload: res.message });
          AsyncStorage.setItem('message', res.message);
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_Check' });
      });
  };
};
