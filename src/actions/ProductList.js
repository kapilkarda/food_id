import { BaseUrl } from '../constants/api';

export const getProductList = (token) => {
  return async (dispatch) => {
    dispatch({ type: 'Request_ProductList' });
    fetch(`${BaseUrl}/user/subscriptionPlanList`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token
      },
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('Products', res);
        if (res.status == 'success') {
          dispatch({ type: 'ProductList', payload: res.data });
        } else {
          dispatch({ type: 'Failure_ProductList' });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_ProductList' });
      });
  };
};
