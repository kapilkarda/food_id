import { BaseUrl } from '../constants/api';

export const brainNomsApi = (token) => {
  return async (dispatch) => {
    fetch(`${BaseUrl}/user/brainnorms`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token
      },
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('Brain', res);
        if (res.status == 'success') {
          dispatch({ type: 'Brain', payload: res.data });
        } else {
        }
      })
      .catch((e) => {
        console.log('Error', e);
      });
  };
};
