// import * as ActionTypes from '../constants/ActionTypes';
// export function checkOfflineRes(data) {
//     return {
//         type: ActionTypes.CHECK_OFFLINE,
//         data
//     }
// };
export const checkOfflineRes = (data) => {
    return async (dispatch) => {
        dispatch({ type: 'CHECK_OFFLINE', payload: data });
    };
};