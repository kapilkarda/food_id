import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import { BaseUrl } from '../constants/api';

export const UserDetailApi = (token , navigation) => {

  return async (dispatch) => {
    dispatch({ type: 'Request_User_Detail' });
    fetch(`${BaseUrl}/user/profile`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token 
      },
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('UserDetailres', res);
        if(res.message === "Invalid token")
        {
          navigation.navigate('Login')
          AsyncStorage.setItem('toggle', JSON.stringify(false));
          AsyncStorage.setItem('loginStatus', '0');
          //Actions.Login()
        }
        if (res.status == 'success') {
          dispatch({ type: 'User_Detail', payload: res.data });
          // dispatch({type:'User_Detail_Message' , payload:res.message})
          AsyncStorage.setItem('Username', res.data.name);
          AsyncStorage.setItem('UserNumber', res.data.mobile);
          AsyncStorage.setItem('DOB', res.data.date_of_birth);
          AsyncStorage.setItem('LoginBy', res.data.login_by);
          
        } else {
          dispatch({ type: 'Failure_User_Detail' });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_User_Detail' });
      });
  };
};
