import AsyncStorage from '@react-native-community/async-storage';
import { BaseUrl } from '../constants/api';

export const forgetPasswordApi = (mobile) => {
 
  var formData = new FormData();
  formData.append('mobile', mobile);
  return async (dispatch) => {
    dispatch({ type: 'Request_Forget' });
    fetch(`${BaseUrl}/user/forgotPassword`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('forgetPassword res', res);
        if (res.status == 'success') {
          AsyncStorage.setItem('mobile' , mobile)
          // AsyncStorage.setItem('forgetNumber', res.status)
          AsyncStorage.setItem('ForgeetOtpCheck', JSON.stringify(res.opt.opt))
          dispatch({ type: 'Forget', payload: res });
          AsyncStorage.setItem('idf', res.opt.user_info.id)
        } else {
          dispatch({ type: 'Forget', payload: res });
          // AsyncStorage.setItem('forgetNumber', res.status)
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_Forget' });
      });
  };
};
