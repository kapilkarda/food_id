import { Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { BaseUrl } from '../constants/api';

export const UserDetailChangeApi = (name, mobile, date, token) => {
  const date_of_birth = date
  var formData = new FormData();
  formData.append('name', name);
  formData.append('mobile', mobile);
  formData.append('date_of_birth', date_of_birth);
  return async (dispatch) => {
    dispatch({ type: 'Request_User_Detail_Change' });
    fetch(`${BaseUrl}/user/changeprofile`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('UserDetailChangeres', res);
        if (res.status == 'success') {
          dispatch({ type: 'User_Detail_Change', payload: res.data });
          Actions.User_Details()
        } else {
          Alert.alert('Please change your info');
          dispatch({ type: 'Failure_User_Detail_Change' });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_User_Detail_Change' });
      });
  };
};
