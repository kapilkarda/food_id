import { Platform } from "react-native";

class RestClient {

static imageUpload(url, params) {
  console.log('RestClient params',params)

    let data = new FormData();
    if (Platform.OS === "android") {
      params.forEach((item, i) => {
        data.append("image", {
          uri: item.path,
          type: item.mime, 
          name: item.name 
        });
      });
    } else {
      params.forEach((item, i) => {
        data.append("image", {
          uri: item.path,
          type: item.mime, 
          name: item.name 
        });
      });
    }

    return new Promise(function(fulfill, reject) {
          fetch(url, {
            method: "POST",
            timeout:1000,
            headers: {
              Accept: "application/json",
              "Content-Type": "multipart/form-data"
            },
            body: data
          })
            .then(response => {
              return response.text();
            })
            .then(responseText => {
              fulfill(JSON.parse(responseText));
            })
            .catch(error => {
              fulfill({
                message: ''
              });
              console.warn("eroro", error);
            });
        })
  }
}
export default RestClient;