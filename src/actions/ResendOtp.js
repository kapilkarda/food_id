import AsyncStorage from '@react-native-community/async-storage';
import { BaseUrl } from '../constants/api';

export const resendOtpApi = (mobile) => {
  var formData = new FormData();
  formData.append('mobile', mobile);
  return async (dispatch) => {
    dispatch({ type: 'Resend_Otp', payload: '' });
    dispatch({ type: 'Request_Resend_Otp' });
    fetch(`${BaseUrl}/user/forgotPassword`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('ResendOtp res', res);
        if (res.status == 'success') {
          // AsyncStorage.setItem('forgetNumber', res.status)
          AsyncStorage.setItem('ForgeetOtpCheck', JSON.stringify(res.opt.opt))
          dispatch({ type: 'Resend_Otp', payload: res });
          AsyncStorage.setItem('idf', res.opt.user_info.id)
        } else {
          dispatch({ type: 'Resend_Otp', payload: res });
          // AsyncStorage.setItem('forgetNumber', res.status)
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_Resend_Otp' });
      });
  };
};
