import { Actions } from 'react-native-router-flux';
import { BaseUrl } from '../constants/api';

export const OtpApi = (email, mobile, password, confirm_password, name) => {
  let data2 = {
    email: email,
    mobile: mobile,
    password: password,
    password2: confirm_password,
    UserName: name
  };
  var formData = new FormData();
  formData.append('email', email);
  formData.append('mobile', mobile);
  formData.append('password', password);
  formData.append('confirm_password', confirm_password);
  formData.append('name', name)
  return async (dispatch) => {
    dispatch({ type: 'Request_Otp' });
    fetch(`${BaseUrl}/user/sendotpforregistration`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('otp res', res);
        if (res.status == 'success') {
          dispatch({ type: 'Otp', payload: res.data });
          Actions.Otp(data2);
        } else {
          // Alert.alert('Something went wrong');
          dispatch({ type: 'Failure_Otp' });
        }
      })
      .catch((e) => {
        console.log('Error', e)
        dispatch({ type: 'Failure_Otp' });
      });
  };
};
