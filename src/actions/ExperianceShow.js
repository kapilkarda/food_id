import AsyncStorage from '@react-native-community/async-storage';
import { BaseUrl } from '../constants/api';

export const getExperianceApi = (token, format) => {
  return async (dispatch) => {
    dispatch({ type: 'Request_experiance_show' });
    var formData = new FormData();
    formData.append('search_date', format)
    fetch(`${BaseUrl}/user/experienceList`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('experiance show', res);
        if (res.status == 'success') {
          let apiData = res.data;
          for (let item of apiData) {
            item.isSelected = false;
            item.Expand = false;
          }
          dispatch({ type: 'experiance_show', payload: res.data });
        } else {
          dispatch({ type: 'Failure_experiance_show' });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_experiance_show' });
      });
  };
};
