import { BaseUrl } from '../constants/api';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';

export const Subscription = (token, device_type, product_id , navigation) => {
  var formData = new FormData();
  formData.append('device_type', device_type);
  formData.append('product_id', product_id);
  return async (dispatch) => {
    dispatch({ type: 'subcriptionPlan', payload: '' });
    dispatch({ type: 'Request_subcriptionPlan' });
    fetch(`${BaseUrl}/user/purchasePlan`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token,
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('Subscription Res', res);
        if (res.status == 'success') {
          AsyncStorage.setItem('toggle', JSON.stringify(true));
          // Actions.tabbar()
          // Actions.Dashboard();
          navigation.navigate('Dashboard')
          dispatch({ type: 'subcriptionPlan', payload: res });
        } else {
          dispatch({ type: 'subcriptionPlan', payload: res });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_subcriptionPlan' });
      });
  };
};
