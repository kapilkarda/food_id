import { BaseUrl } from '../constants/api';

export const DictionaryApi = (token, search_value) => {
  return async (dispatch) => {
    dispatch({ type: 'Request_Dictionary' });
    fetch(`${BaseUrl}/googleWord?word=${search_value}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token
      },
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('Dicres', res.message);
        if (res.success) {
          dispatch({ type: 'Dictionary', payload: res });
        } else {
          dispatch({ type: 'Dictionary', payload: res });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_Dictionary' });
      });
  };
};
