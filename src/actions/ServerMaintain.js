import { BaseUrl } from '../constants/api';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';

export const getServerStatus = () => {
    return async (dispatch) => {
        dispatch({ type: 'Request_SERVER_STATUS' });
        fetch(`${BaseUrl}/user/maintanance`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
        })
            .then((res) => res.json())
            .then(async (res) => {
                console.log('getServerStatus', res);
                if (res.status == 'success') {
                    dispatch({ type: 'SERVER_STATUS', payload: res });
                } else {
                    dispatch({ type: 'Failure_SERVER_STATUS' });
                }
            })
            .catch((e) => {
                dispatch({ type: 'Failure_SERVER_STATUS' });
            });
    };
};
