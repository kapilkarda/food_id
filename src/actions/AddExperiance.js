
import { Actions } from 'react-native-router-flux';
import { BaseUrl } from '../constants/api';


export const addExpApi = (token, date, feeling_description, feeling_status, items , navigation) => {

  var formData = new FormData();
  formData.append('date', date);
  formData.append('feeling_description', feeling_description);
  formData.append('feeling_status', feeling_status);
  formData.append('items', JSON.stringify(String((items))));
 
  return async (dispatch) => {
    dispatch({ type: 'Add_Exx_Request' });
    fetch(`${BaseUrl}/user/addExperience`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('addExp', res);
        if (res.status == 'success') {
          dispatch({ type: 'addExp', payload: res });
          // Actions.pop();
          // navigation.navigate('Calendar')
          Actions.Calendar()
        } else {
          dispatch({ type: 'Add_Exx_Faliure' });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Add_Exx_Faliure' });
      });
  };
};
