import { BaseUrl } from '../constants/api';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';

export const FreeSubscription = (token, device_type, product_id , navigation) => {
    var formData = new FormData();
    formData.append('device_type', device_type);
    formData.append('product_id', product_id);
    return async (dispatch) => {
        dispatch({ type: 'Free_SubcriptionPlan', payload: '' });
        dispatch({ type: 'Request_Free_SubcriptionPlan' });
        fetch(`${BaseUrl}/user/purchasePlan`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                auth_token: token,
            },
            body: formData,
        })
            .then((res) => res.json())
            .then((res) => {
                console.log('FreeSubscription Res', res);
                if (res.status == 'success') {
                    dispatch({ type: 'Free_SubcriptionPlan', payload: res });
                    AsyncStorage.setItem('toggle', JSON.stringify(true));
                    // Actions.TrialStart();
                    navigation.navigate('TrialStart')
                } else {
                    dispatch({ type: 'Failure_Free_SubcriptionPlan' });
                }
            })
            .catch((e) => {
                dispatch({ type: 'Failure_Free_SubcriptionPlan' });
            });
    };
};
