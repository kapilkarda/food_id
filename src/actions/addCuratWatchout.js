import AsyncStorage from '@react-native-community/async-storage';
import { getFoodSubCategoryApi } from './getSubcategory';
import { BaseUrl } from '../constants/api';
import { SearchFood } from '../actions/SearchFoodForWatchout'
export const addWatchOutApi = (item_id, token, is_api, type, isremoved, catadd , search_value) => {

  var formData = new FormData();
  formData.append('item_id', item_id);
  formData.append('is_api', is_api);
  formData.append('type', type);
  formData.append('isremoved', isremoved);
  console.log('DataApi',formData)
  return async (dispatch) => {
    dispatch({ type: 'Request_Add_Curat_Watchout' });
    fetch(`${BaseUrl}/user/addCuratWatchout`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('addCuratWatchout', res);
        AsyncStorage.setItem('status', res.status)
        if (res.status == 'success') {
          dispatch({ type: 'Add_Curat_Watchout', payload: res });
          setTimeout(() => {
            // dispatch(getFoodSubCategoryApi(token, catadd, isremoved));
            // if (search_value!=''){
              //   dispatch(SearchFood(token, search_value));
            // }
          }, 200);
        } else {
          dispatch({ type: 'Add_Curat_Watchout', payload: res });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_Add_Curat_Watchout' });
      });
  };
};
