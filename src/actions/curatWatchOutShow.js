import {BaseUrl} from '../constants/api';

export const curatWatchoutShowApi = (
  token,
  search_value,
  RemoveCurateData,
  tempSelectedItems,
) => {
  console.log('search_value', search_value);
  return async (dispatch) => {
    dispatch({type: 'Request_Curat_List_Show'});
    fetch(`${BaseUrl}/user/getCuratWatchout?search_value=${search_value}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token,
      },
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('curatlistShow', res);
        if (res.status == 'success') {
          let apiData = res.data;
          for (let item of apiData) {
            const firstName = item.name.toString().slice(0, 1);
            item.first_name = firstName;
            item.Expand = false;
          }
          // let tempArray = [...res.data]
          if (RemoveCurateData) {
            for (let items of apiData) {
              for (let subCatItem of items.subcategory) {
                if (subCatItem.item_id == RemoveCurateData[0].item_id) {
                  items.Expand = true;
                  break;
                }
              }
            }
          }
          if (tempSelectedItems && tempSelectedItems.length > 0)
            for (let itemss of apiData) {
              for (let itemsss of tempSelectedItems) {
                if (itemss.item_id === itemsss.item_id) {
                  itemss.Expand = itemsss.Expand;
                }
              }
            }
          console.log('apiData', res);
          dispatch({type: 'Curat_List_Show', payload: res});
        } else {
          dispatch({type: 'Failure_Curat_List_Show'});
        }
      })
      .catch((e) => {
        dispatch({type: 'Failure_Curat_List_Show'});
      });
  };
};
