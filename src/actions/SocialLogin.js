import { Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { BaseUrl } from '../constants/api';

export const SocialLoginApi = (data, navigation) => {
  const email = data.email
  const mobile = data.mobile
  const provider = data.provider
  const name = data.name
  const social_token = data.deviceToken

  var formData = new FormData();
  formData.append('email', email);
  formData.append('mobile', mobile);
  formData.append('provider', provider);
  formData.append('name', name);
  formData.append('social_token', social_token);

  return async (dispatch) => {
    dispatch({ type: 'Request_Social' });
    fetch(`${BaseUrl}/user/sociallogin`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('Social res', res);
        if (res.status == 'success') {
          // AsyncStorage.setItem('token', res.data.auth_token);
          // AsyncStorage.setItem('toggle', JSON.stringify(true));
          // AsyncStorage.setItem('is_expired', JSON.stringify(res.data.is_expired))
          // dispatch({ type: 'Social', payload: res.status });
          // if (from === 'join' || from === 'join2' || from === 'join3') {
          //   AsyncStorage.setItem('learningscreen', '0')
          //   AsyncStorage.setItem('Model', '0')
          //   Actions.Instruction();
          // } else {
          //   Actions.tabbar();
          //   Actions.Dashboard();
          // }
          dispatch({ type: 'Social', payload: res.status });
          if (res.data.is_expired == 1) {
            if (res.data.is_first_time == '0') {
              AsyncStorage.setItem('token', res.data.auth_token);
              // AsyncStorage.setItem('toggle', JSON.stringify(true));
              AsyncStorage.setItem('is_expired', JSON.stringify(res.data.is_expired));
              // if (from === 'join' || from === 'join2' || from === 'join3') {
              AsyncStorage.setItem('learningscreen', '0')
              AsyncStorage.setItem('Model', '0')
              // Actions.Instruction();
              navigation.navigate('Instruction')
              // } else {
              //   Actions.tabbar();
              //   Actions.Dashboard();
              // }
            } else {
              AsyncStorage.setItem('token', res.data.auth_token);
              AsyncStorage.setItem('toggle', JSON.stringify(true));
              AsyncStorage.setItem('is_expired', JSON.stringify(res.data.is_expired));
              // Actions.tabbar();
              // Actions.Dashboard();
              navigation.navigate('Dashboard')
            }
            
          } 
          else if (res.data.is_expired == 0)
            {
              if (res.data.is_first_time == '0') {
                AsyncStorage.setItem('token', res.data.auth_token);
                // AsyncStorage.setItem('toggle', JSON.stringify(true));
                AsyncStorage.setItem('is_expired', JSON.stringify(res.data.is_expired));
                // if (from === 'join' || from === 'join2' || from === 'join3') {
                AsyncStorage.setItem('learningscreen', '0')
                AsyncStorage.setItem('Model', '0')
                navigation.navigate('Instruction')
                // Actions.Instruction();
                // } else {
                //   Actions.tabbar();
                //   Actions.Dashboard();
                // }
              } else {
                AsyncStorage.setItem('token', res.data.auth_token);
                // AsyncStorage.setItem('toggle', JSON.stringify(true));
                AsyncStorage.setItem('is_expired', JSON.stringify(res.data.is_expired));
                // Actions.FreeTrialPayment();
                navigation.navigate('FreeTrialPayment')
              }

            }
          else if (res.data.is_expired == 2 ) {
            AsyncStorage.setItem('token', res.data.auth_token);
            // AsyncStorage.setItem('toggle', JSON.stringify(true));
            AsyncStorage.setItem('is_expired', JSON.stringify(res.data.is_expired))
            // Actions.FreeTrialPayment();
            navigation.navigate('FreeTrialPayment')
          }

        } else {
          Alert.alert('Something went wrong');
          dispatch({ type: 'Failure_Social' });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_Social' });
      });
  };

};

