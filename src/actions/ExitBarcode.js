import AsyncStorage from '@react-native-community/async-storage';
import {Alert} from 'react-native';
import {Actions} from 'react-native-router-flux';

export const exitBarcode = () => {
  return async (dispatch) => {
    dispatch({type: 'ScannerOpenOnExit', payload: false});
    dispatch({type: 'ScannerHideOnExit', payload: true});
  };
};

export const OpenBarcode = () => {
  return async (dispatch) => {
    dispatch({type: 'ScannerOpenOnExit', payload: true});
    dispatch({type: 'ScannerHideOnExit', payload: false});
  };
};


