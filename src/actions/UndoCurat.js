import { BaseUrl } from '../constants/api';
import { showFavList } from './ShowFavList';
import {curatWatchoutShowApi} from './curatWatchOutShow'
export const UndoCurat = (token, RemoveCurateData, search_value , tempSelectedItems) => {
  return async (dispatch) => {
    dispatch({ type: 'Request_Undo_Curat' });
    fetch(`${BaseUrl}/user/undoCuratWatchout`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        auth_token: token
      },
      body: JSON.stringify(RemoveCurateData),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('UndoCurat', res);
        if (res.status == 'success') {
          dispatch(curatWatchoutShowApi(token, search_value ,RemoveCurateData , tempSelectedItems));

          dispatch(showFavList(token, search_value));
          dispatch({ type: 'Undo_Curat', payload: res.data });
        } else {
          dispatch({ type: 'Failure_Undo_Curat' });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_Undo_Curat' });
      });
  };
};
