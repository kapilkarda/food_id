import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { BaseUrl } from '../constants/api';

export const UserRagistrationAPI = (
  email,
  mobile,
  password, 
  confirm_password,
  name,
  navigation
) => {
  var formData = new FormData();
  formData.append('email', email);
  formData.append('mobile', mobile);
  formData.append('password', password);
  formData.append('confirm_password', confirm_password);
  formData.append('name', name);
  return async (dispatch) => {
    dispatch({ type: 'Request_Ragistration' });
    fetch(`${BaseUrl}/user/registration`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('ragistration res', res);
        if (res.status == 'success') {
          AsyncStorage.setItem('token', res.data.auth_token);
          dispatch({ type: 'Ragistration', payload: res.status });
          AsyncStorage.setItem('ids', res.data.id);
          AsyncStorage.setItem('learningscreen', '0')
          AsyncStorage.setItem('Model', '0')
          AsyncStorage.setItem('is_expired', JSON.stringify(res.data.is_expired));
          // Actions.Instruction();
          navigation.navigate('Instruction')
        } else {
          dispatch({ type: 'Failure_Ragistration' });
          // Alert.alert('Something went wrong');
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_Ragistration' });
      });
  };
};
