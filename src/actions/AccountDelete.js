import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { BaseUrl } from '../constants/api';

export const AccoundDeleteApi = (token) => {
  return async (dispatch) => {
    dispatch({ type: 'Request_Account_Delete' });
    fetch(`${BaseUrl}/user/deleteAccount`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token,
      },
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('Account Delete', res);
        if (res.status == 'success') {
          dispatch({ type: 'Account_Delete', payload: res });
          AsyncStorage.setItem('learningscreen', '0');
          AsyncStorage.setItem('Model', '0')
          AsyncStorage.removeItem('res.path');
          AsyncStorage.removeItem('convertedFilePath');
          AsyncStorage.removeItem('token');
          AsyncStorage.setItem('toggle', JSON.stringify(false));
          Actions.join();
        } else {
          dispatch({ type: 'Failure_Account_Delete' });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_Account_Delete' });
        console.log('Error', e);
      });
  };
};
