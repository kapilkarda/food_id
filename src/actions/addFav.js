import AsyncStorage from '@react-native-community/async-storage';
import { BaseUrl } from '../constants/api';

export const addFavApi = (item_id, token) => {
  var formData = new FormData();
  formData.append('item_id', item_id);
  return async (dispatch) => {
    dispatch({ type: 'Request_Fav_List' });
    fetch(`${BaseUrl}/user/addFavorite`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('addFavres', res);
        if (res.status == 'success') {
          AsyncStorage.setItem('favorite_id', JSON.stringify(res.data.favorite_id))
          dispatch({ type: 'Fav_List', payload: res.data });
        } else {
          dispatch({ type: 'Failure_Fav_List' });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_Fav_List' });
      });
  };
};
