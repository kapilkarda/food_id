import AsyncStorage from '@react-native-community/async-storage';
import { getFoodSubCategoryApi } from './getSubcategory';
import { BaseUrl } from '../constants/api';
import { SearchFood } from '../actions/SearchFoodForWatchout'

export const removeCuratAPI = (token, type, item_id, isremoved, catadd , search_value) => {
  var formData = new FormData();
  formData.append('type', type);
  formData.append('item_id', item_id);
  return (dispatch) => {
    dispatch({ type: 'Request_Remove_Curat' });
    fetch(`${BaseUrl}/user/removeCuratWatchout`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('RemoveCurat', res);
        if (res.status == 'success') {
          AsyncStorage.setItem('item_id', item_id)
          AsyncStorage.setItem('type', type)
          // dispatch(getFoodSubCategoryApi(token, catadd, isremoved));
          dispatch({ type: 'Remove_Curat', payload: res });
          // dispatch(SearchFood(token, search_value));
        } else {
          dispatch({ type: 'Remove_Curat', payload: res });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_Remove_Curat' });
      });
  };
};
