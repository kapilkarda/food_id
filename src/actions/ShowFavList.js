import { BaseUrl } from '../constants/api';

export const showFavList = (token, search_value) => {
  return async (dispatch) => {
    dispatch({ type: 'Request_Show_Fav_List' });
    fetch(`${BaseUrl}/user/favoriteList?search_value=${search_value}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token
      },
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('Fav List', res);
        if (res.status == 'success') {
          let apiData = res.data;
          for (let item of apiData) {
            const firstName = item.item_data.name.toString().slice(0, 1);
            item.first_name = firstName;
          }
          dispatch({ type: 'Show_Fav_List', payload: res.data });
        } else {
          dispatch({ type: 'Failure_Show_Fav_List' });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_Show_Fav_List' });
      });
  };
};
