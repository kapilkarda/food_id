import { Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { BaseUrl } from '../constants/api';

export const LoginAPI = (email, password , navigation) => {
  console.log('email', email);
  console.log('password', password);
  var formData = new FormData();
  formData.append('email', email);
  formData.append('password', password);
  return (dispatch) => {
    dispatch({ type: 'Error', payload: '' });
    dispatch({ type: 'Request_Login' });
    fetch(`${BaseUrl}/user/login`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('Login response', res);
        if (res.status == 'success') {
          dispatch({ type: 'Success_Login' });
          // AsyncStorage.setItem('email', email);
          // AsyncStorage.setItem('password', password);
          // AsyncStorage.setItem('loginStatus', '1');
          // AsyncStorage.setItem('token', res.data.auth_token);
          // AsyncStorage.setItem('toggle', JSON.stringify(true));
          // Actions.tabbar();
          // Actions.Dashboard();
          if (res.data.is_expired == 1) {
            AsyncStorage.setItem('email', email);
            AsyncStorage.setItem('password', password);
            AsyncStorage.setItem('loginStatus', '1');
            AsyncStorage.setItem('token', res.data.auth_token);
            AsyncStorage.setItem('toggle', JSON.stringify(true));
            AsyncStorage.setItem('is_expired', JSON.stringify(res.data.is_expired));
            // Actions.tabbar();
            // Actions.Dashboard();
            navigation.navigate('Dashboard')
          } else if (res.data.is_expired == 2 || res.data.is_expired == 0) {
            AsyncStorage.setItem('loginStatus', '1');
            // AsyncStorage.setItem('toggle', JSON.stringify(true));
            AsyncStorage.setItem('token', res.data.auth_token);
            AsyncStorage.setItem('is_expired', JSON.stringify(res.data.is_expired));
            // Actions.FreeTrialPayment();
            navigation.navigate('FreeTrialPayment')
          }
        } else {
          dispatch({ type: 'Error', payload: res });
          // Alert.alert(res.message)
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_Login' });
        console.warn(e);
      });
  };
};
