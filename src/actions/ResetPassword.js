import {BaseUrl} from '../constants/api';
import {Actions} from 'react-native-router-flux';

export const ResetPasswordApi = (password, confirm_password, user_id) => {

  var formData = new FormData();
  // formData.append('password', password);
  // formData.append('confirm_password', confirm_password);
  // formData.append('user_id', user_id);
  // console.log('formdata', formData);
  let data = {
    user_id : user_id,
    password : password,
    confirm_password : confirm_password,
  }
  console.log('data' , data)
  return async (dispatch) => {
    dispatch({type: 'Request_Reset'});
    fetch(`${BaseUrl}/user/resetpassword`, {
      method: 'POST',
      headers: { 
        Accept: 'application/json',
        'Content-Type': 'application/json'
          
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('resetpass res', res);
        if (res.status == 'success') {
          dispatch({type: 'Reset', payload: res.status});
          Actions.Login();
        } else {
          // Alert.alert('Something went wrong');
          dispatch({type: 'Failure_Reset'});
        }
      })
      .catch((e) => {
        console.log("error : " +e);

        dispatch({type: 'Failure_Reset'});
      });
  };
};
