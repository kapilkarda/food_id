import { BaseUrl } from '../constants/api';

export const SearchBynameApi = (token, search_value) => {
  var formData = new FormData();
  formData.append('search_value', search_value);
  return async (dispatch) => {
    dispatch({ type: 'Request_SearchByName' });
    fetch(`${BaseUrl}/user/searchByName`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('SearchName res', res);
        if (res.status == 'success') {
          dispatch({ type: 'SearchByName', payload: res });
        } else {
          dispatch({ type: 'Failure_SearchByName' });
          // Alert.alert('Something went wrong');
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_SearchByName' });
      });
  };
};
