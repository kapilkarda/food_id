import { BaseUrl } from '../constants/api';
import { showFavList } from './ShowFavList';

export const Undo = (favorite_id, token, search_value) => {
  var formData = new FormData()
  formData.append('favorite_id', favorite_id);
  return async (dispatch) => {
    dispatch({ type: 'Request_Undo_Food' });
    fetch(`${BaseUrl}/user/undoFavoriteApi`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('Undo', res);
        if (res.status == 'success') {
          dispatch(showFavList(token, search_value));
          dispatch({ type: 'Undo_Food', payload: res.data });
        } else {
          dispatch({ type: 'Failure_Undo_Food' });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_Undo_Food' });
      });
  };
};
