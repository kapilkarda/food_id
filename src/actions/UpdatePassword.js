import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import { BaseUrl } from '../constants/api';

export const UpdatePasswordApi = (old_password, new_password, confirm_password, token) => {
  var formData = new FormData();
  formData.append('old_password', old_password);
  formData.append('new_password', new_password);
  formData.append('confirm_password', confirm_password);
  return async (dispatch) => {
    dispatch({ type: 'Request_Password_Change' });
    fetch(`${BaseUrl}/user/updatePassword`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('UserUpdatePassword', res);
        if (res.status == 'success') {
          // AsyncStorage.setItem(res.status)
          dispatch({ type: 'Password_Change', payload: res.data });
          Actions.User_Details()
        } else {
          dispatch({ type: 'Failure_Password_Change' });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_Password_Change' });
      });
  };
};
