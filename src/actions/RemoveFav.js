import AsyncStorage from '@react-native-community/async-storage';
import { BaseUrl } from '../constants/api';
import { showFavList } from './ShowFavList';

export const RemoveFavApi = (favorite_id, token, search_value) => {
  var formData = new FormData();
  formData.append('favorite_id', favorite_id);
  return async (dispatch) => {
    dispatch({ type: 'Request_Remove_Fav' });
    fetch(`${BaseUrl}/user/removeFavorite`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('RemoveList', res);
        if (res.status == 'success') {
          AsyncStorage.setItem('favorite_id', JSON.stringify(res.data.favorite_id))
          dispatch({ type: 'Remove_Fav', payload: res });
          dispatch(showFavList(token, search_value));
        } else {
          dispatch({ type: 'Failure_Remove_Fav' });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_Remove_Fav' });
      });
  };
};
