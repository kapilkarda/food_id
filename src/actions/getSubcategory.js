import {BaseUrl} from '../constants/api';

export const getFoodSubCategoryApi = (token, cat_id, isremoved) => {
  var formData = new FormData();
  formData.append('cat_id', cat_id);
  formData.append('isremoved', isremoved);
  return async (dispatch) => {
    dispatch({type: 'Request_Get_Food_SubCategory'});
    fetch(`${BaseUrl}/user/getFoodSubCategory`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token,
      },
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('food Sub Category', res);
        if (res.status == 'success') {
          dispatch({type: 'Get_Food_SubCategory', payload: res.data});
          let apiData = res.data;
          for (let item of apiData) {
            if (item.type == 'category') {
              const firstName = item.name.toString().slice(0, 1);
              item.first_name = firstName;
            }
          }
        } else {
          dispatch({type: 'Failure_Get_Food_SubCategory'});
        }
      })
      .catch((e) => {
        dispatch({type: 'Failure_Get_Food_SubCategory'});
      });
  };
};
