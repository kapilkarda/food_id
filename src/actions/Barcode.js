import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { BaseUrl } from '../constants/api';

export const BarCodeApi = (token, barcode_value , navigation , type) => {
  var formData = new FormData();
  formData.append('barcode_value',barcode_value);
  formData.append('type',type)
  return async (dispatch) => {
    dispatch({ type: 'Request_barCode' });
    const modelCheck = await AsyncStorage.getItem('Model')
    fetch(`${BaseUrl}/user/getBarcodeData`, {
      method: 'POST',
      headers: {
        Accept: 'application/json', 
        'Content-Type': 'multipart/form-data',
        auth_token: token
      }, 
      body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('Barcode', res);
        if (res.status == 'success') {
          console.log('model check action', modelCheck)
          if (modelCheck === '1') {
            // Actions.ScannedScreen(res);
            navigation.navigate('ScannedScreen',res)
          }
          AsyncStorage.setItem('Exit', '0')
          dispatch({ type: 'barCode', payload: res });
        } else {
          dispatch({ type: 'ErrorBarcode', payload: res.status });
        }
      })
      .catch((e) => {
        console.log('e',e)
        dispatch({ type: 'Failure_barCode' });
      });
  };
};
