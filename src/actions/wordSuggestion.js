import { BaseUrl } from '../constants/api';

export const SuggestionApi = (token, search_value) => {
  return async (dispatch) => {
    dispatch({ type: 'Request_Suggestion' });
    fetch(`${BaseUrl}/googleWordSuggestion?word=${search_value}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token
      },
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('sugesstion', res);
        if (res.success) {
          dispatch({ type: 'Suggestion', payload: res });
        } else {
          dispatch({ type: 'Failure_Suggestion' });
        }
      })
      .catch((e) => {
        dispatch({ type: 'Failure_Suggestion' });
      });
  };
};
