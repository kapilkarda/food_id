import {Alert} from 'react-native';
import {Actions} from 'react-native-router-flux';
import RestClient from './Helper/RestClient'
// import notateeApi from '../../api/notatee';
//  import types from '../types'
export function LoginRes(data) {
  // return {
  //   type: types.DELETE,
  //   data,
}

export const PhotoUploadApi = (token ,img ) => {

  var formData = new FormData();
  formData.append('img', img);
  
  return async (dispatch) => {
    
   try {
      RestClient.imageUpload(
        `http://3.23.99.232/user/profileImageUpload`,{
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
          auth_token : token
        },
        
        body: formData,
      })
      
        .then(response => {
          console.log("uploadProfileImage response", response);
            dispatch(uploadProfileImageSuccess(response));
        })
        .catch(error => {
            uploadProfileImageFailure('Image upload failed')
        });
    } catch (error) {
        uploadProfileImageFailure('Image upload failed')
    }
  };
};
