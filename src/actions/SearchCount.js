import { Actions } from 'react-native-router-flux';
import { BaseUrl } from '../constants/api';

export const SearchCountApi = (token, type , cameraNav, navigation) => {
    var formData = new FormData();
    formData.append('type', type);
    return async (dispatch) => {
        dispatch({ type: 'Request_Search_Count' });
        fetch(`${BaseUrl}/user/checkSearchCountNew`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                auth_token: token
            },
            body: formData,
        })
            .then((res) => res.json())
            .then((res) => {
                console.log('SearchCount res', res);
                if (res.status == 'success') {
                    console.log('success')
                    dispatch({ type: 'Search_Count', payload: res });
                    if (type == 'search') {
                        Actions.SearchByname();
                    } else if (type == 'scan' && cameraNav != 'tab') {
                        navigation.navigate('Scan')
                    }
                } else {
                    console.log('fail')

                    Actions.PlanChooseScreen();
                    dispatch({ type: 'Failure_Search_Count' });
                }
            })
            .catch((e) => {
                console.log('e', e)
                dispatch({ type: 'Failure_Search_Count' });
            });
    };
};
