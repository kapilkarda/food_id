import { StyleSheet, Dimensions } from 'react-native';
import { w, h } from '../../utils/Dimensions';
import { TextSize } from '../../theme/TextSize';
import { TextColor, UiColor } from '../../theme';

export default StyleSheet.create({
 
  CurateView:
  {flexDirection:'row'},

  IconView:
  {
    height:h(2),
    width:h(2),
    marginTop:h(9),
    marginLeft:w(8)
   
  },
    IconButton:
    {alignSelf:'center',
    marginTop:h(8),
    marginLeft:w(5),
    // right:w(1)
  },

    LineView:
    {width : '75%', 
    height: 1, 
    backgroundColor: '#9a9a9a',
    marginLeft:w(15),
    marginTop:h(2),
    alignSelf:'center'},
    IconTxt:
    {fontSize:16,
      color:"#383B3F",
      top:h(0.5),
      fontWeight:'500'

    }

});
