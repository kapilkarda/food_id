import React from 'react';
import {
  View,
  Image,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
// import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {h, w} from '../../utils/Dimensions';
import Styles from './Styles';

class DrawerBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    };
  }

  componentWillMount() {}

  goMainHome = () => {
    this.toggleDrawer();
    Actions.NovusHomePage();
  };

  toggleDrawer = () => {
    Actions.drawerToggle();
  };

  render() {
    return (
      <KeyboardAvoidingView style={{flex: 1}} behavior="padding" enabled>
        <View>
          <ScrollView>
            <View style={Styles.CurateView}>
              <Image
                style={Styles.IconView}
                source={require('../../assets/icon/search22.png')}
              />
              <TouchableOpacity
              onPress ={Actions.Addingra}
              style={Styles.IconButton}>
                <Text style={Styles.IconTxt}>Curate Your Watchouts</Text>
              </TouchableOpacity>
            </View>
            <View style={Styles.LineView}></View>

            <View style={Styles.CurateView}>
              <Image
                style={Styles.IconView}
                source={require('../../assets/icon/EditYourWatchouts_Hamburger(1).png')}
              />
              <TouchableOpacity
                onPress={Actions.Watchout}
                style={Styles.IconButton}>
                <Text style={Styles.IconTxt}>Edit Your Watchouts</Text>
              </TouchableOpacity>
            </View>
            <View style={Styles.LineView}></View>

            <View style={Styles.CurateView}>
              <Image
                style={Styles.IconView}
                source={require('../../assets/icon/Dictionary_Icon_Hamburger.png')}
              />
              <TouchableOpacity
                onPress={Actions.Dictionary}
                style={Styles.IconButton}>
                <Text style={Styles.IconTxt}>Ingredient Dictionary</Text>
              </TouchableOpacity>
            </View>
            <View style={Styles.LineView}></View>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

DrawerBar.propTypes = {};

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(DrawerBar);
