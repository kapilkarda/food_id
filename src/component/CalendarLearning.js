import React, { useEffect, useState ,useCallback} from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  Modal,
  ImageBackground,
  ScrollView,
  Keyboard,
  TouchableWithoutFeedback, 
  KeyboardAvoidingView,
  Platform, 
} from 'react-native';
import { Actions } from 'react-native-router-flux';
//import Styles from './Style';
import { Pages } from 'react-native-pages';

import AsyncStorage from '@react-native-community/async-storage';
import { useSelector, useDispatch } from 'react-redux';
import CalendarAdd from './../container/Calendar/Add/CalendarAdd'
import Calendars from './../container/Calendar/Calendar'
import { useFocusEffect } from '@react-navigation/native';
const br = `\n`;

function CalendarLearning ({props})  { 
  // const isFocused = useIsFocused();
  
  useEffect(() => {
   learning()
  }, [props]);
  
    
    const learning = async () => {
      const learning = await AsyncStorage.getItem('learningscreen')
      if (learning === '0') {
        Actions.CalendarAdd()
      }
      else {
        Actions.Calendar()
      }
    }

    return (
      <CalendarAdd/>
    )
};
export default CalendarLearning;
