import React from 'react';
import { Scene, Router, Stack, Drawer, Tabs } from 'react-native-router-flux';
import { Dimensions, Platform, Image, View, ImageBackground } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
// import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import DrawerBar from './component/Drawer/Drawer';
import Splash from './container/Splash/Splash';
import Instruction from './container/Instruction/instruction';
import join from './container/Join In/join';
import join2 from './container/Join In/join2';
import join3 from './container/Join In/join3';
import Otp from './container/Otp Verification/Otp';
import Login from './container/login/Login';
import { connect } from 'react-redux';
import passwordChange from './container/passwordChange/passwordChange';
import Dashboard from './container/Dashboard/Dashboard';
import Addingra from './container/Addingra/Addingra';
import Search from './container/Search/Search';
import Watchout from './container/WatchOuts/Watchout';
import { height } from './utils/Dimensions';
import Dictionary from './container/Dictionary/Dictionary';
import User_Details from './container/User_Details/User_Details';
import Calendar from './container/Calendar/Calendar';
import Scan from './container/Scan/Scan';
import Caseines from './container/Caseines/Caseines';
import Faves from './container/Faves/Faves';
import { h, w } from '../src/utils/Dimensions';
import Changeinfo from './container/User_Details/ChangeInfo/Changeinfo';
import PasswordUpdate from './container/User_Details/PasswordUpdate/PasswordUpdate';
import LegalStuff from './container/User_Details/Legat Stuff/LegalStuff';
import ReachOut from './container/User_Details/Reach Out/ReachOut';
import Add from './container/Calendar/Add/CalendarAdd';
import CalendarAdd from './container/Calendar/Add/CalendarAdd';
import SelectedDate from './container/Calendar/SelectedDate/SelectedDate';
import ScannedScreen from './container/ScannedScreen/ScannedScreen';
import OtpLogin from './container/Otp Verification/otpLogin';
import { Text } from 'native-base';
import FreeTrialPayment from './container/PyamentScreens/FreeTrialPayment'
import TrialStart from './container/PyamentScreens/TrialStart'
import PlanChooseScreen from './container/PyamentScreens/PlanChooseScreen'
import SearchByname from './container/SearchByname/SearchByname'
import ForgetScreen from './container/ForgetScreen'
import StartSearchingscreen from './container/Addingra/StartSearchingscreen'
import AsyncStorage from '@react-native-community/async-storage';
import CalendarLearning from './component/CalendarLearning'
import { exitBarcode, OpenBarcode } from './actions/ExitBarcode';
import { SearchCountApi} from './actions/SearchCount';

var width = Dimensions.get('window').width;

const RouterWithRedux = connect()(Router);
var image;
var imgBack
var tintcolor;
const TabIcon = ({

  selected,
  title,
  img,
  focused,
  heightimg,
  widthimg,
  imgBackColor,
  imhTop,
  marginLeftimg
}) => {

  switch (title) {
    case 'Dashboard':
      image = focused
        ? require('./assets/icon/Dashboard_Filled.png')
        : require('./assets/icon/ghar.png');
      tintcolor = focused ? '#FE6F69' : '#c3c3c3';
      heightimg = h(3);
      widthimg = h(3);
      imgBackColor = null;
      imhTop = h(-2);
      break;

    case 'Addingra':
      image = focused
        ? require('./assets/icon/Currate_Bottom_Nav_Selected.png')
        : require('./assets/icon/Currate_Bottom_Nav_NotSelected.png');
      tintcolor = focused ? '#FE6F69' : '#c3c3c3';
      heightimg = h(3);
      widthimg = h(3);
      imgBackColor = null;
      imhTop = h(-2);
      marginLeftimg = (-13)
      break;

    case 'Scan':
      image = focused
        ? require('./assets/icon/bigscan.png')
        : require('./assets/icon/bigscan.png');
      tintcolor = focused ? 'ebebebeb' : 'ebebebeb';
      heightimg = h(12);
      widthimg = h(12);
      imgBackColor = 'red';
      imgBack = require('./assets/icon/Vector.png')
      imhTop = h(-8);
      // marginLeftimg = h(5)
      break;

    case 'Calendar':

      image = focused
        ? require('./assets/icon/FoodJournal_Bottom_Nav_Selected.png')
        : require('./assets/icon/FoodJournal_Bottom_Nav_NotSelected.png');
      tintcolor = focused ? '#FE6F69' : '#c3c3c3';
      heightimg = h(3);
      widthimg = h(3);
      imgBackColor = null;
      imhTop = h(-2);
      break;
    case 'User_Details':
      image = focused
        ? require('./assets/icon/Profilel_Bottom_Nav_Selected.png')
        : require('./assets/icon/Profile_Bottom_Nav_NotSelected.png');
      tintcolor = focused ? '#FE6F69' : '#c3c3c3';
      heightimg = h(3);
      widthimg = h(3);
      imgBackColor = null;
      imhTop = h(-2);
      break;
  }
  return (

    <View
      style={{
        height: h(12.5),
        backgroundColor: '#EBEBEB',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        // paddingTop:50
      }}>
      <Image
        source={image}
        style={{
          width: widthimg,
          height: heightimg,
          tintColor: tintcolor,
          // backgroundColor: imgBackColor,
          marginTop: imhTop,
          marginLeft: marginLeftimg
        }}
        resizeMode="contain"></Image>
    </View>
  );

};

class Root extends React.Component {

  constructor(props) {
    super(props);
    console.log('calling', props)
  }

  onExitScanner = () => {
    this.props.exitBarcodeCall();
  }

  OnOpenScanner = async () => {
    const token = await AsyncStorage.getItem('token');
    this.props.OpenBarcodeCall();
    this.props.SearchCountApi(token, 'scan' , 'tab')
  }

  render() {
    return (

      <RouterWithRedux navigationBarStyle={{ backgroundColor: '#5dca67' }}>
        <Scene key="root" hideNavBar hideTabBar>
          <Stack key="app">
            <Scene hideNavBar panHandlers={null}>
              <Scene>
                <Scene
                  component={Splash}
                  hideNavBar={true}
                  key="Splash"
                  title="Splash"
                />
                <Scene
                  component={Instruction} //classname
                  hideNavBar={true}
                  wrap={false}
                  key="Instruction"
                  title="Instruction"
                />
                <Scene
                  component={join} //classname
                  hideNavBar={true}
                  wrap={false}
                  key="join"
                  title="join"
                />
                <Scene
                  component={join2} //classname
                  hideNavBar={true}
                  wrap={false}
                  key="join2"
                  title="join2"
                />

                <Scene
                  component={join3} //classname
                  hideNavBar={true}
                  wrap={false}
                  key="join3"
                  title="join3"
                />
                <Scene
                  component={Otp} //classname
                  hideNavBar={true}
                  wrap={false}
                  key="Otp"
                  title="Otp"
                />
                <Scene
                  component={OtpLogin} //classname
                  hideNavBar={true}
                  wrap={false}
                  key="OtpLogin"
                  title="OtpLogin"
                />

                <Scene
                  component={Login} //classname
                  hideNavBar={true}
                  wrap={false}
                  key="Login"
                  title="Login"
                />
                <Scene
                  component={passwordChange} //classname
                  hideNavBar={true}
                  wrap={false}
                  key="passwordChange"
                  title="passwordChange"
                />
                <Scene
                  component={FreeTrialPayment} //classname
                  hideNavBar={true}
                  wrap={false}
                  key="FreeTrialPayment"
                  title="FreeTrialPayment"
                />
                <Scene
                  component={TrialStart} //classname
                  hideNavBar={true}
                  wrap={false}
                  key="TrialStart"
                  title="TrialStart"
                />
                <Scene
                  component={ForgetScreen} //classname
                  hideNavBar={true}
                  wrap={false}
                  key="ForgetScreen"
                  title="ForgetScreen"
                />
                <Scene
                  component={ReachOut} //classname
                  hideNavBar={true}
                  wrap={false}
                  key="ReachOut"
                  title="ReachOut"
                />

                <Drawer
                  hideNavBar
                  key="drawer"
                  contentComponent={DrawerBar}
                  drawerWidth={width - 100}>
                  <Scene
                    key="tabbar"
                    tabs
                    showLabel={false}
                    // initial={true}
                    tabBarStyle={
                      {
                        // backgroundColor: '#c4c4c4',
                        // height: h(8.5)
                        // borderBottomLeftRadius: 15,
                        // borderBottomRightRadius: 15,
                        // paddingVertical: h(0.5),
                      }
                    }>
                    <Scene title="Dashboard" icon={TabIcon} img={image} >
                      <Scene
                        component={Dashboard} //classname
                        initial={true}
                        hideNavBar={true}
                        wrap={false}
                        key="Dashboard"
                        title="Dashboard"
                        showLabel={false}
                      />
                      <Scene
                        component={Watchout} //classname
                        hideNavBar={true}
                        wrap={false}
                        key="Watchout"
                        title="Watchout"
                      />
                      <Scene
                        component={Dictionary} //classname
                        hideNavBar={true}
                        wrap={false}
                        key="Dictionary"
                        title="Dictionary"
                      />
                      <Scene
                        component={Caseines} //classname
                        hideNavBar={true}
                        wrap={false}
                        key="Caseines"
                        title="Caseines"
                      />
                      <Scene
                        component={Faves} //classname
                        hideNavBar={true}
                        wrap={false}
                        key="Faves"
                        title="Faves"
                      />
                      <Scene
                        component={SearchByname} //classname
                        hideNavBar={true}
                        wrap={false}
                        key="SearchByname"
                        title="SearchByname"
                      />
                      <Scene
                        component={ScannedScreen} //classname
                        hideNavBar={true}
                        wrap={false}
                        key="ScannedScreen"
                        title="ScannedScreen"
                      />
                      <Scene
                        component={PlanChooseScreen} //classname
                        hideNavBar={true}
                        wrap={false}
                        key="PlanChooseScreen"
                        title="PlanChooseScreen"
                      />
                    </Scene>

                    <Scene title="Addingra" icon={TabIcon} img={image}>
                      <Scene
                        component={Addingra} //classname
                        hideNavBar={true}
                        wrap={false}
                        key="Addingra"
                        title="Addingra"
                      />
                      <Scene
                        component={StartSearchingscreen} //classname
                        hideNavBar={true}
                        wrap={false}
                        key="StartSearchingscreen"
                        title="StartSearchingscreen"
                      />
                      <Scene
                        component={Search} //classname
                        hideNavBar={true}
                        wrap={false}
                        key="Search"
                        title="Search"
                      />
                    </Scene>

                    <Scene title="Scan" icon={TabIcon} img={image}>
                      <Scene
                        component={Scan} //classname
                        hideNavBar={true}
                        onEnter={() => this.OnOpenScanner()}
                        onExit={() => this.onExitScanner()}
                        key="Scan"
                      />
                    </Scene>

                    <Scene title="Calendar" icon={TabIcon} img={image}>
                      <Scene
                        component={Calendar} //classname
                        hideNavBar={true}
                        wrap={false}
                        key="Calendar"
                        title="Calendar"
                      />
                      <Scene
                        component={CalendarAdd} //classname
                        hideNavBar={true}
                        wrap={false}
                        key="CalendarAdd"
                        title="CalendarAdd"
                      />
                      <Scene
                        component={SelectedDate} //classname
                        hideNavBar={true}
                        wrap={false}
                        key="SelectedDate"
                        title="SelectedDate"
                      />
                    </Scene>

                    <Scene title="User_Details" icon={TabIcon} img={image}>
                      <Scene
                        component={User_Details} //classname
                        hideNavBar={true}
                        wrap={false}
                        key="User_Details"
                        title="User_Details"
                      />
                      <Scene
                        component={Changeinfo} //classname
                        hideNavBar={true}
                        wrap={false}
                        key="Changeinfo"
                        title="Changeinfo"
                      />
                      <Scene
                        component={PasswordUpdate} //classname
                        hideNavBar={true}
                        wrap={false}
                        key="PasswordUpdate"
                        title="PasswordUpdate"
                      />
                      <Scene
                        component={LegalStuff} //classname
                        hideNavBar={true}
                        wrap={false}
                        key="LegalStuff"
                        title="LegalStuff"
                      />
                    </Scene>

                  </Scene>

                  <Scene
                    component={Search} //classname
                    hideNavBar={true}
                    wrap={false}
                    key="Search"
                    title="Search"
                  />
                  <Scene
                    component={Watchout} //classname
                    hideNavBar={true}
                    wrap={false}
                    key="Watchout"
                    title="Watchout"
                  />
                  <Scene
                    component={Dictionary} //classname
                    hideNavBar={true}
                    wrap={false}
                    key="Dictionary"
                    title="Dictionary"
                  />
                  <Scene
                    component={Caseines} //classname
                    hideNavBar={true}
                    wrap={false}
                    key="Caseines"
                    title="Caseines"
                  />
                  <Scene
                    component={Faves} //classname
                    hideNavBar={true}
                    wrap={false}
                    key="Faves"
                    title="Faves"
                  />

                </Drawer>
              </Scene>
            </Scene>
          </Stack>
        </Scene>
      </RouterWithRedux >
    );
  }
}

//export default Root;
// const Tab = createBottomTabNavigator();
const mapDispatchToProps = (dispatch) => {
  return {
    exitBarcodeCall: () => dispatch(exitBarcode()),
    OpenBarcodeCall: () => dispatch(OpenBarcode()),
    SearchCountApi: (token, type , cameraNav) => dispatch(SearchCountApi(token, type , cameraNav)),
  };
};

const mapStateToProps = (state) => {
  return {
    //scanonhide: state.scanonhide,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Root);

// const mapDispatchToPropsOpen = (dispatch) => {
//   return {
//     OpenBarcodeCall: () => dispatch(OpenBarcode())
//   }; 
// };
// // const mapStateToProps = (state) => {
// //   return { 
// //     //scanonhide: state.scanonhide,
// //   };
// // };
//  connect(mapStateToProps, mapDispatchToPropsOpen)(Root);
console.disableYellowBox = true;
