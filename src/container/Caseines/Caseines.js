import React, { useEffect, useState } from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  Modal,
  ImageBackground,
  ScrollView,
  Keyboard,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Platform,
  BackHandler,
  ActivityIndicator,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { IconAsset, Strings, UiColor } from '../../theme';
import { h, w } from '../../utils/Dimensions';
//import Styles from './Style'; 
import { Pages } from 'react-native-pages';
import styles from '../Splash/styles';
import Styles from './Style';
import AsyncStorage from '@react-native-community/async-storage';
import { DictionaryApi } from './../../actions/Dictionary'
import { useSelector, useDispatch } from 'react-redux';

const br = `\n`;

const Caseines = ({ ...props }) => {

  const dispatch = useDispatch();
  const [search_value, setsearch_value] = useState('')
  const [Token, setToken] = useState('');
  const [values, setvalues] = useState('')
  const [uiRender, setUiRender] = useState('')
  // const Food_Serach = useSelector((state) => console.log('Search', state));
  const DictionaryDAta = useSelector((state) => state.dictionary.DICTIONARY ? state.dictionary.DICTIONARY.meaning : null);
  const DictionaryError = useSelector((state) => state.dictionary.DICTIONARY ? state.dictionary.DICTIONARY.success : null);
  const DictionaryLoading = useSelector((state) => state.dictionary.isLoading);

  useEffect(() => {
    const value = props.data
    setsearch_value(value)
    callApiFirsttime(value);
  }, [])

  useEffect(() => {
    tokenGet()
  }, [])

  const callApiFirsttime = async (value) => {
    const text = value;
    const token = await AsyncStorage.getItem('token');
    dispatch(DictionaryApi(token, text));
  }

  const dectionaryApiFunc = async (text) => {
    setsearch_value(text);
    if (text.length === 0) {
      dispatch(DictionaryApi(Token, text));
    } else if (text.length > 0) {
      dispatch(DictionaryApi(Token, text));
    }
    setUiRender(!uiRender);
  };

  const tokenGet = async () => {
    const token = await AsyncStorage.getItem('token');
    setToken(token)
  }

  useEffect(() => {
    const backAction = () => {
      Actions.Dictionary()
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  }, []);
const clearData = (search_value) =>
{
  setsearch_value(search_value)
  dispatch({ type: 'Dictionary', payload: '' });


}
  return (
    <View style = {{backgroundColor:'#ffff', height:'100%'}}>
    <ScrollView style={{ backgroundColor: '#ffffff', marginBottom:h(7) }}>
      <TouchableWithoutFeedback
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <KeyboardAvoidingView style={{ flex: 1 }}
          behavior={Platform.OS === 'ios' ? 'position' : 'position'}
          // padding={10}
          keyboardVerticalOffset={Platform.OS === 'ios' ? -100 : -100}>
          <View style={Styles.MainContainer}>
            <Image
              style={Styles.yellowimage}
              source={require('../../assets/icon/Yelloround.png')}
            />
            <Image
              style={Styles.milkimage}
              source={require('../../assets/icon/Definition_Artichoke.png')}
            />
            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity onPress={Actions.drawerOpen}>
                <Image
                  style={Styles.DrawerIcon}
                  source={require('../../assets/image/Hamburger_Nav.png')}
                />
              </TouchableOpacity>
            </View>

            <View>
              <TextInput
                style={[{ fontSize: 25, top: h(10), alignSelf: 'center', width: w(50) }]}
                placeholderTextColor="#000"
                underlineColorAndroid="transparent"
                placeholder="Search"
                autoCapitalize="none"
                underlineColorAndroid="transparent"
                onChangeText={(search_value) => clearData(search_value)}
                onSubmitEditing={() => dectionaryApiFunc(search_value)}
                value={search_value}
              />

              {search_value !== undefined && search_value.length !== 0 ?
                <Text
                  style={{
                    alignSelf: 'center',
                    marginTop: h(15),
                    fontSize: 20,
                    color: '#383B3F',
                    fontSize: 16,
                    width: w(50)
                  }}>
                  {DictionaryDAta}
                </Text> : null}
              {DictionaryError === false && search_value !== undefined && search_value.length !== 0 ?
                <Text style={{ color: '#FE6F69', fontSize: 10, alignSelf: 'center' }}>This word can’t be found. Try something different!</Text> : null
              }
            </View>
            <View style={{ height: h(10), width: h(10) }}>
            </View>

          </View>
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
      {
        (DictionaryLoading) ?
          <View
            style={{
              position: 'absolute', left: 0, right: 0, top: 5, bottom: 0, alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: "transparent"
            }}
          >
            <View>
              <ActivityIndicator size="large" color="#000" />
            </View>
          </View> : null
      }
    </ScrollView>
    </View>
  );
};
export default Caseines;
