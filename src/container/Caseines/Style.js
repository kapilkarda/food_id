import {StyleSheet} from 'react-native';
import {h, w} from '../../utils/Dimensions';

export default StyleSheet.create({
  MainContainer: {
  },
  yellowimage: {
    width: h(68),
    height:h(42),
    position: 'absolute',
    resizeMode: 'stretch',
    top:h(-17),
    alignSelf:'center'
  },
  milkimage: {
    alignSelf: 'center',
    marginTop: h(15),
    width:h(18),
    height:h(22),
    resizeMode:'contain'
  },
  DrawerIcon:
{
  width: h(2.3),
  height: h(2.3),
  marginLeft : w(8),
  resizeMode:'contain',
  // marginTop:h(5)
  // top:h(2),
  // position:'absolute',
  tintColor:'#383B3F'

},
Searchtxt:
  {
alignSelf:'center',
marginTop:h(-19)

  },
  TouchTxt:
  {
fontSize:30,
fontWeight:'bold',
color :'#9a9a9a',
marginRight:w(5), 

  },
  Text:
  {

    fontSize:30,
    alignSelf:'center',
    marginTop:h(8),
    color:'#383B3F'
    
  }
})