import {StyleSheet} from 'react-native';
import {h, w} from '../../../utils/Dimensions';

export default StyleSheet.create({
  MainContainer: {
    flex: 1,
  },
  inputFieldContainer: {
    alignSelf: 'center',
    borderColor: '#000',
    borderWidth: w(0.28),
    width: w(75),
    height: h(5),
    borderRadius: w(10),
    flexDirection: 'row',
    marginTop: h(5),
    paddingLeft: w(5),
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  UploadView: {
    width: h(18),
    height: h(18),
    borderRadius: 80,
    alignSelf: 'center',
    backgroundColor: '#EBEBEB',
    marginTop: h(2),
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: '#fa8072',
    width: w(85),
    height: h(8),
    borderRadius: w(10),
    marginTop: h(4),
  },
  AndText: {
    fontSize: 16,
    alignSelf: 'center',
    color: '#FBFAFA',
    // marginTop: h(1.5),
    // paddingTop:h(1.4),

    fontWeight: '500',
  },
  Text: {
    fontSize: 20,
    color: '#383B3F',
    // top:h(4.5),
    alignSelf: 'center',
    // marginTop:h(4.5),
    fontWeight: 'bold',
    // marginRight:w(30.5)
  },
  HeaderStyle: {
    flexDirection: 'row',
    marginLeft: w(10),
    marginTop: h(8),
    justifyContent: 'space-between',
  },
  errmsg: {
    color: '#ff0000',
    // marginBottom: h(),
    marginLeft: w(12),
  },
});
