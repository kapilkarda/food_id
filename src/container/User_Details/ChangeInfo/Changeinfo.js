import React, { useState, useEffect } from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  Modal,
  ScrollView,
  ImageBackground,
  BackHandler,
  ActivityIndicator,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { h, w } from '../../../utils/Dimensions';
import Styles from './Style';
import { Pages } from 'react-native-pages';
import { connect } from 'react-redux';
import { UserDetailChangeApi } from './../../../actions/ChangeInfo';
import AsyncStorage from '@react-native-community/async-storage';
// import DatePicker from 'react-native-datepicker'
// import { baseProps } from 'react-native-gesture-handler/dist/src/handlers/gestureHandlers';
import { useSelector, useDispatch } from 'react-redux';
// import DateTimePickerModal from "react-native-modal-datetime-picker";
import DateTimePickerModal from 'react-native-modal-datetime-picker';
// import Style from './Style'
import moment from 'moment';
import { UserDetailApi } from '../../../actions/USerProfile';
import { withNavigationFocus } from 'react-navigation';

const Changeinfo = ({ navigation, ...props }) => {

  const dispatch = useDispatch();
  const screenStatus = navigation.isFocused();

  const showImage = props.data;

  const userDetail = useSelector((state) => state.userdetail.USERDETAIL);
  const userDetailChangeLoading = useSelector(
    (state) => state.chnageuserdetail.isLoading,
  );
  const userDetailLoading = useSelector((state) => state.userdetail.isLoading);

  // const Image = props.data

  const [name, setname] = useState('');
  const [number, setnumber] = useState('');
  const [errors, seterrors] = useState(false);
  const [date_of_birth, setdate_of_birth] = useState('');
  const [showButton, setshowButton] = useState(false);
  const [date, setdate] = useState('');
  const [image, setimage] = useState('');
  const [isDatePickerVisible, setIsDatePickerVisible] = useState(false);
  const [fbPic, setFbPic] = useState('');
  const [LoginStatus, setLoginStatus] = useState(false);


  const changedetails = async () => {
    let mobile = number;
    let token = await AsyncStorage.getItem('token');
    dispatch(UserDetailChangeApi(name, mobile, date, token)); //Call api for change userInfo
    return errors;
  };
  useEffect(() => {
 
    if (userDetail) {
      if (
        userDetail.login_by === 'facebook' ||
        userDetail.login_by === 'google' ||
        userDetail.login_by === 'apple'
      ) {
        setLoginStatus(true);
      } else {
        setLoginStatus(false);
      }
    }
  }, [userDetail]);
  useEffect(() => {
    userDetails();
    Deatails();
  }, []);

  useEffect(()=>{
    if(userDetail)
    {
      const userName = userDetail.name
      const PhoneNumber = userDetail.mobile
      const DOB = userDetail.date_of_birth
      setname(userName);
      setnumber(PhoneNumber);
      setdate(DOB);

    }
  },[userDetail])

  const colorChange = () => {
    setshowButton(true);
    changedetails();
  };

  const Deatails = async () => {
    // const PhoneNumber = await AsyncStorage.getItem('UserNumber');
    // const userName = await AsyncStorage.getItem('Username');
    // const DOB = await AsyncStorage.getItem('DOB');
    // console.log('DOBsss',DOB)
    // const fbProfileImage = await AsyncStorage.getItem('ProfilePicture');
    // const loginStatus = await AsyncStorage.getItem('LoginBy');
    // if (loginStatus == 'facebook') {
    //   setFbPic(fbProfileImage);
    // }
    // setname(userName);
    // setnumber(PhoneNumber);
    // setdate(DOB);
  };

  useEffect(() => {
    imageshow();
  }, []);

  const imageshow = async () => {   //Show Image function
    const imageset = await AsyncStorage.getItem('res.path');
    console.log('imageset', imageset);
    setimage(imageset);
  };

  const userDetails = async () => {
    console.log('calling_userDetails');
    let token = await AsyncStorage.getItem('token');
    console.log('token', token);
    dispatch(UserDetailApi(token));
  };

  const hideDatePicker = () => {
    setIsDatePickerVisible(!isDatePickerVisible);
  };

  const handleConfirm = (date) => {
    const DATE = moment(date).format('L');
    console.log('DATE', DATE);
    setdate(DATE);
    hideDatePicker();
  };

  useEffect(() => {
    const backAction = () => {
      Actions.pop();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, [screenStatus]);

  return (
    <ScrollView style={{ backgroundColor: '#fff' }}>
      <View style={{}}>
        {/* <View style={Styles.HeaderStyle}>
          <TouchableOpacity onPress={Actions.User_Details}>
            <Image
              style={{width: w(2), height: h(2), marginTop: h(5), opacity: 1}}
              source={require('../../../assets/icon/BackB.png')}
            />
          </TouchableOpacity>
          <Text style={Styles.Text}>Change Your Info</Text>
        </View> */}
        <View style={{ flexDirection: 'row', height: h(7), marginTop: h(8) }}>
          <TouchableOpacity onPress={Actions.User_Details} style={{ flex: 0.6, justifyContent: 'center', alignItems: 'center' }}>
            <View>
              <Image
                style={{ width: w(2), height: h(2), opacity: 1 }}
                source={require('../../../assets/icon/BackB.png')}
              />
            </View>
          </TouchableOpacity>
          <View style={{ flex: 3, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={Styles.Text}>Change Your Info</Text>
          </View>
          <View style={{ flex: 0.4, }}>

          </View>
        </View>
        <ImageBackground
          imageStyle={{ borderRadius: h(10) }}
          style={Styles.UploadView}
          source={{
            uri: fbPic ? fbPic : userDetail ? userDetail.profile_photo : '',
          }}></ImageBackground>

        <TextInput
          maxLength={20}
          placeholderTextColor="#383B3F"
          style={[Styles.inputFieldContainer, { fontSize: 16 }]}
          underlineColorAndroid="transparent"
          placeholder="Nom De Plume"
          autoCapitalize="none"
          underlineColorAndroid="transparent"
          onChangeText={(name) => setname(name)}
          value={name}
        />
        <TextInput
          placeholderTextColor="#383B3F"
          style={{
            alignSelf: 'center',
            borderColor: '#000',
            borderWidth: w(0.28),
            width: w(75),
            height: h(5),
            borderRadius: w(10),
            flexDirection: 'row',
            marginTop: h(5.5),
            paddingLeft: w(5),
            alignItems: 'center',
            justifyContent: 'space-between',
            fontSize: 16,
            backgroundColor: LoginStatus ? '#d9d9d9' : '#fff',
          }}
          underlineColorAndroid="transparent"
          keyboardType="name-phone-pad"
          placeholder="Phone Number"
          autoCapitalize="none"
          maxLength={13}
          underlineColorAndroid="transparent"
          onChangeText={(number) => setnumber(number)}
          value={number}
          editable={LoginStatus ? false : true}
        />
        <View style={Styles.inputFieldContainer}>
          <Text style={{ alignSelf: 'center', fontSize: 16 }}>
            {date ? date : 'Birthday'}
          </Text>
          <TouchableOpacity
            onPress={() => setIsDatePickerVisible(!isDatePickerVisible)}>
            <Image
              source={require('../../../assets/icon/calendaricon.png')}
              style={{ height: 17, width: 17, marginRight: w(4) }}
            />
          </TouchableOpacity>
        </View>

        <DateTimePickerModal
          style={{}}
          time={date}
          isVisible={isDatePickerVisible}
          mode="date"
          maximumDate={moment().toDate()}
          onConfirm={handleConfirm}
          onCancel={hideDatePicker}
        />
        <TouchableOpacity
          // disabled={name == null || name == ''}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            backgroundColor: showButton ? '#3d9390' : '#fe6f69',
            width: w(75),
            height: h(5),
            borderRadius: w(10),
            marginTop: h(7.5),
            // opacity: name == null || name == '' ? 0.6 : 1,
          }}
          onPress={colorChange}>
          <Text style={Styles.AndText}>Remember Me</Text>
        </TouchableOpacity>
      </View>
      {userDetailChangeLoading || userDetailLoading ? (
        <View
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'transparent',
          }}>
          <View>
            <ActivityIndicator size="large" color="#000" />
          </View>
        </View>
      ) : null}
    </ScrollView>
  );
};
export default withNavigationFocus(Changeinfo);