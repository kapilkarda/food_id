import {StyleSheet} from 'react-native';
import {h, w, width} from '../../../utils/Dimensions';

export default StyleSheet.create({
  MainContainer: {
    flex: 1,
  },
  HeaderStyle:
      
        {
            flexDirection:'row',
            marginTop:h(3),
            justifyContent:'space-between',
            width: w(90),
            alignSelf:'center',
        },
        Text:
      {
        fontSize:20,
        color:'#383B3F',
        // top:h(4.5), 
        alignSelf:'center',
        marginTop:h(4.5),
        fontWeight:'bold',
        marginLeft:w(5),
        // marginRight:w(23.5),
        textAlign:'center'

      },
})