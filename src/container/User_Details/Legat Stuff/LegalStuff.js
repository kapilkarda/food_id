import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  Modal,
  BackHandler,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {h, w} from '../../../utils/Dimensions';
import Styles from './Style';
import {Pages} from 'react-native-pages';
import {connect} from 'react-redux';
import {ScrollView} from 'react-native-gesture-handler';
import {withNavigationFocus} from 'react-navigation';
import {useSelector, useDispatch} from 'react-redux';
import {termsandconditions} from '../../../actions/termAndconditions';
import { BaseUrl } from '../../../constants/api';
import HTML from "react-native-render-html";

// import Style from './Style'

const LegalStuff = ({navigation}) => {
  const screenStatus = navigation.isFocused();
  const dispatch = useDispatch();

  const [data, setdata] = useState('');


  useEffect(() => {
    console.log('dispatch');
    fetch(`${BaseUrl}/user/termsandcondition`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        // auth_token: token,
      },
      // body: formData,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('TermAndCondition Res', res);
        if (res.status == 'success') {
          console.log('res.data',res.data)
          if(res)
          {
          setdata(res.data)
          }
        } else {
        }
      })
      .catch((e) => {
        console.log('error', e);
      });
  }, []);

  useEffect(() => {
    const backAction = () => {
      Actions.User_Details();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, [screenStatus]);
  console.log('data',data)
  return (
    <View style={{backgroundColor: '#ffff'}}>
      <ScrollView style={{backgroundColor: '#fff', marginBottom: h(7)}}>
        <View style={Styles.HeaderStyle}>
          <TouchableOpacity onPress={Actions.User_Details}>
            <Image
              style={{
                width: h(2),
                height: h(2),
                marginTop: h(5.5),
                resizeMode: 'contain',
              }}
              source={require('../../../assets/icon/BackB.png')}
            />
          </TouchableOpacity>
          <Text style={Styles.Text}>
            Terms and Conditions{'\n'}of Use Agreement
          </Text>
          <View style={{width: 30}} />

          
        </View>
        <View
          style={{width: w(75), alignSelf: 'center', justifyContent: 'center',marginTop:h(2)}}>
           <HTML source={{ html: data}}  />
        
        </View>
        <View style={{height: h(10), width: h(10)}}></View>
      </ScrollView>
    </View>
  );
};
export default withNavigationFocus(LegalStuff);
