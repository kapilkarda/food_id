import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  Modal,
  ImageBackground,
  ScrollView,
  Platform,
  Linking,
  ActivityIndicator,
  BackHandler,
  PermissionsAndroid,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {IconAsset, Strings, UiColor} from '../../theme';
import {h, w} from '../../utils/Dimensions';
import Styles from './Style';
import {Pages} from 'react-native-pages';
import {connect} from 'react-redux';
import {useSelector, useDispatch} from 'react-redux';
import {UserDetailApi} from './../../actions/USerProfile';
// import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
import RNFetchBlob from 'rn-fetch-blob';
import {PhotoUploadApi} from './../../actions/PhotoUpload';
import {Form} from 'native-base';
import Mailer from 'react-native-mail';
import * as ImagePicker from 'react-native-image-picker';
import {AccoundDeleteApi} from './../../actions/AccountDelete';
import {withNavigationFocus} from 'react-navigation';
import {checkMultiple, PERMISSIONS, RESULTS} from 'react-native-permissions';
import {BaseUrl} from '../../constants/api';

const br = `\n`;
const User_Details = ({navigation}) => {
  const dispatch = useDispatch();

  const screenStatus = navigation.isFocused();
  const [shareVisible, shareSetVisible] = useState(false);
  const [shareVisible2, shareSetVisible2] = useState(false);
  const [image, setimage] = useState('');
  const [profileImage, setprofileImage] = useState('');
  const [defaultImage, setdefaultImage] = useState('');
  const [convertedimage, setconvertedimage] = useState('');
  const [deleteaacount, setdeleteaccount] = useState(false);
  const [profilePic, setProfilePic] = useState('');
  const [pickerModelVisible, setPickerModelVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [fbPic, setFbPic] = useState('');
  const [LoginStatus, setLoginStatus] = useState(false);
  const [permissionStorage, setPermissionStorage] = useState(false);
  const [uiRender, setUiRender] = useState(false);
  const [userPlan, setUserPlan] = useState(false);
  const [logout, setlogout] = useState(false);

  const getUserDetail = useSelector((state) => state.userdetail.USERDETAIL);
  const deleteAccountLoading = useSelector(
    (state) => state.deleteaccount.isLoading,
  );
  const userDetailLoading = useSelector((state) => state.userdetail.isLoading);

  const username = getUserDetail ? getUserDetail.name : '';

  const openShareModal = () => {
    setPickerModelVisible(false);
    shareSetVisible(true);
  };

  const closeModal = () => {
    shareSetVisible(false);
  };

  const openShareModal2 = () => {
    shareSetVisible2(true);
    shareSetVisible(false);
  };

  const closeModal2 = () => {
    shareSetVisible2(false);
    closeModal();
  };

  useEffect(() => {
    userDetails();
  }, [screenStatus]);

  useEffect(() => {
    profile();
  }, [screenStatus]);

  useEffect(() => {
    imageshow();
  }, [screenStatus]);

  useEffect(() => {
    setdefaultImage(getUserDetail ? getUserDetail.profile_photo : '');
    setprofileImage(getUserDetail ? getUserDetail.profile_photo : '');
    setUserPlan(getUserDetail ? getUserDetail.plan_id : 0);
    if (getUserDetail) {
      if (
        getUserDetail.login_by === 'facebook' ||
        getUserDetail.login_by === 'google' ||
        getUserDetail.login_by === 'apple'
      ) {
        setLoginStatus(true);
      } else {
        setLoginStatus(false);
      }
    }
  }, [getUserDetail]);

  const profile = async () => {
    const imageset = await AsyncStorage.getItem('res.path');
    const fbProfileImage = await AsyncStorage.getItem('ProfilePicture');
    const loginStatus = await AsyncStorage.getItem('LoginBy');
    if (loginStatus == 'facebook') {
      setFbPic(fbProfileImage);
    }
    // setLoginStatus(loginStatus);
    // setdefaultImage(getUserDetail.profile_photo);
    // setprofileImage(getUserDetail.profile_photo);
  };

  useEffect(() => {
    ButtonDisable();
    checkPermission();
  }, [screenStatus]);

  const ButtonDisable = async () => {
    // const loginStatus = await AsyncStorage.getItem('LoginBy');
    // console.log('loginStatus', loginStatus)
    // if (loginStatus) {
    //   if (loginStatus === 'facebook' || loginStatus === 'google' || loginStatus === 'apple') {
    //     setLoginStatus(true)
    //   } else {
    //     setLoginStatus(false)
    //   }
    // }
    // setTimeout(async () => {
    //   const loginStatus = await AsyncStorage.getItem('LoginBy');
    //   console.log('loginStatus', loginStatus)
    //   if (loginStatus === 'facebook' || loginStatus === 'google' || loginStatus === 'apple') {
    //     setLoginStatus(true)
    //   } else {
    //     setLoginStatus(false)
    //   }
    // }, 200);
  };

  const checkPermission = async () => {
    if (Platform.OS === 'android') {
      checkMultiple([PERMISSIONS.ANDROID.CAMERA]).then((statuses) => {
        if (statuses[PERMISSIONS.ANDROID.CAMERA] == 'granted') {
          setPermissionStorage(true);
        } else {
          setPermissionStorage(false);
        }
      });
    }
  };

  const userDetails = async () => { 
    let token = await AsyncStorage.getItem('token');
    dispatch(UserDetailApi(token, navigation)); //call userDetail Api 
  };

  const imageshow = async () => {
    const showImage = await AsyncStorage.getItem('convertedFilePath');
    setimage(showImage);
  };

  const chooseFile = (type) => {    //Photo upload function
    setPickerModelVisible(false);
    setTimeout(async () => {
      if (Platform.OS === 'ios') {
        if (type === 'Gallary') {
          ImagePicker.launchImageLibrary(
            {
              mediaType: 'photo',
              includeBase64: false,
              maxHeight: 200,
              maxWidth: 200,
            },
            (response) => {
              setProfilePic(response.uri);
              shareSetVisible(false);
              uploadImageApi(response);
              if (response.uri) {
                setIsLoading(!isLoading);
              }
            },
          );
        } else if (type === 'Camera') {
          ImagePicker.launchCamera(
            {
              mediaType: 'photo',
              includeBase64: false,
              maxHeight: 200,
              maxWidth: 200,
            },
            (response) => {
              setProfilePic(response.uri);
              shareSetVisible(false);
              uploadImageApi(response);
              if (response.uri) {
                setIsLoading(!isLoading);
              }
            },
          );
        }
      } else {
        if (type === 'Gallary') {
          ImagePicker.launchImageLibrary(
            {
              mediaType: 'photo',
              includeBase64: false,
              maxHeight: 200,
              maxWidth: 200,
            },
            (response) => {
              console.log('response', response);
              const photo = response.uri;
              console.log('photo', photo);
              setProfilePic(response.uri);

              shareSetVisible(false);
              uploadImageApi(response);
              setIsLoading(!isLoading);
            },
          );
        } else if (type === 'Camera') {
          if (permissionStorage == true) {
            const options = {
              mediaType: 'photo',
              includeBase64: false,
              maxHeight: 200,
              maxWidth: 200,
            };
            ImagePicker.launchCamera(options, (response) => {
              if (response.didCancel) {
                console.log('User cancelled image picker');
              } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
              } else if (response.customButton) {
                console.log(
                  'User tapped custom button: ',
                  response.customButton,
                );
              } else {
                console.log('cameraResopnse', response);
                setProfilePic(response.uri);
                shareSetVisible(false);
                uploadImageApi(response);
                if (response.uri) {
                  setIsLoading(!isLoading);
                }
              }
            });
          } else {
            try {
              const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                  title: 'Food ID app Camera permission',
                  message: 'Food ID App needs access to storage',
                  buttonNegative: '',
                  buttonPositive: 'Okay',
                },
              );
              if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                setPermissionStorage(true);
                setUiRender(!uiRender);
                const options = {
                  mediaType: 'photo',
                  includeBase64: false,
                  maxHeight: 200,
                  maxWidth: 200,
                };
                ImagePicker.launchCamera(options, (response) => {
                  if (response.didCancel) {
                    console.log('User cancelled image picker');
                  } else if (response.error) {
                    console.log('ImagePicker Error: ', response.error);
                  } else if (response.customButton) {
                    console.log(
                      'User tapped custom button: ',
                      response.customButton,
                    );
                  } else {
                    console.log('cameraResopnse', response);
                    setProfilePic(response.uri);
                    shareSetVisible(false);
                    uploadImageApi(response);
                    if (response.uri) {
                      setIsLoading(!isLoading);
                    }
                  }
                });
              } else {
                setPermissionStorage(false);
                setUiRender(!uiRender);
              }
            } catch (err) {
              console.warn(err);
            }
          }
        }
      }
    }, 300);
  };

  const uploadImageApi = async (response) => {
    console.log('imageValue', response);
    let token = await AsyncStorage.getItem('token');
    var data = new FormData();
    data.append('img', {
      uri: response.uri,
      type: response.type,
      name: response.fileName,
    });
    fetch(`${BaseUrl}/user/profileImageUpload`, {  //Call Photo upload Api
      method: 'POST',
      body: data,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        auth_token: token,
      },
    })
      .then((res) => res.json())
      .then(async (res) => {
        console.log('uploadImage res', res);
        if (res.status === 'success') {
          AsyncStorage.setItem('res.path', res.path);
          setprofileImage(res.path);
          setIsLoading(false);
          setFbPic('');
          AsyncStorage.setItem('LoginBy', '');
        } else {
          setIsLoading(false);
          console.log('res,.message', res.message);
        }
      })
      .catch((e) => {
        setIsLoading(false);
        console.log(e);
      });
  };

  const loggedOut = async () => {  //LogOut Function
    AsyncStorage.removeItem('res.path');
    AsyncStorage.removeItem('token');
    AsyncStorage.setItem('toggle', JSON.stringify(false));
    AsyncStorage.setItem('loginStatus', '0');
    navigation.navigate('Login');
    // Actions.Login();
  };

  const accounntDeleteModel = () => {
    setdeleteaccount(true);
  };

  const accounntDeleteModelOnly = () => {
    setdeleteaccount(false);
    DeleteAccount();
    // closeModal();
  };

  const accounntDeleteModelclose = () => {
    setdeleteaccount(false);
  };

  const DeleteAccount = async () => {
    let token = await AsyncStorage.getItem('token');
    dispatch(AccoundDeleteApi(token));
  };

  const opencamera = () => {
    setPickerModelVisible(false);
    chooseFile('Camera');
  };

  useEffect(() => {  //Back Hundler for Android Devices
    const backAction = () => {
      Actions.pop();
      return true;
    };
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );
    return () => backHandler.remove();
  }, [screenStatus]);
// User Detail Ui
  return (
    <ScrollView
      bounces={false}
      style={{
        backgroundColor:
          deleteaacount || shareVisible || shareVisible2 || pickerModelVisible
            ? '#000'
            : '#fff',
        opacity:
          deleteaacount || shareVisible || shareVisible2 || pickerModelVisible
            ? 0.5
            : 1,
      }}>
      <View
        style={[
          Styles.MainContainer,
          {
            backgroundColor:
              deleteaacount ||
              shareVisible ||
              shareVisible2 ||
              pickerModelVisible
                ? '#000'
                : '#fff',
            opacity:
              deleteaacount ||
              shareVisible ||
              shareVisible2 ||
              pickerModelVisible
                ? 0.5
                : 1,
          },
        ]}>
        <ImageBackground
          imageStyle={{borderRadius: h(10)}}
          style={Styles.UploadView}
          source={{
            uri: fbPic ? fbPic : profileImage,
          }}>
          <View
            style={{
              height: 15,
              width: 15,
              borderRadius: 15,
              alignSelf: 'center',
              position: 'absolute',
            }}>
            {isLoading ? (
              <ActivityIndicator
                visible={isLoading}
                size="large"
                color="#000"
              />
            ) : null}
          </View>
          <TouchableOpacity
            onPress={() => setPickerModelVisible(!pickerModelVisible)}>
            <Image
              style={Styles.UploadImage}
              source={require('../../assets/icon/Upload_Profile_Image.png')}
            />
          </TouchableOpacity>
        </ImageBackground>

        <View style={{marginTop: h(4)}}>
          <TouchableOpacity
            style={Styles.buttonContainer}
            onPress={() => Actions.Changeinfo(image)}>
            <Text style={Styles.AndText}>Change My Info</Text>
          </TouchableOpacity>
          {LoginStatus === false ? (
            <TouchableOpacity
              style={Styles.buttonContainer}
              onPress={() => Actions.PasswordUpdate(image)}>
              <Text style={Styles.AndText}>Update My Secret Word</Text>
            </TouchableOpacity>
          ) : null}
          <TouchableOpacity
            style={Styles.buttonContainer}
            onPress={Actions.LegalStuff}>
            <Text style={Styles.AndText}>Legal Stuff</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={Styles.buttonContainer}
            onPress={() =>
              Linking.openURL(
                'mailto:service@foodid.biz?subject=Hey Food ID, ' +
                  username +
                  ' here!',
                '&body=Description',
              )
            }
            title="support@example.com">
            <Text style={Styles.AndText}>Reach Out</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={Styles.buttonContainer}
            onPress={() => loggedOut()}>
            <Text style={Styles.AndText}>Log Out</Text>
          </TouchableOpacity>
          <TouchableOpacity
            disabled={userPlan == '1' ? true : false}
            onPress={accounntDeleteModel}
            style={[
              Styles.buttonContainer,
              {backgroundColor: userPlan == '1' ? '#FE6F6990' : '#FE6F69'},
            ]}>
            <Text style={Styles.AndText}>Delete Account</Text>
          </TouchableOpacity>
        </View>
        {deleteAccountLoading || userDetailLoading ? (
          <View
            style={{
              position: 'absolute',
              left: 0,
              right: 0,
              top: 0,
              bottom: 0,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: 'transparent',
            }}>
            <View>
              <ActivityIndicator size="large" color="#000" />
            </View>
          </View>
        ) : null}
      </View>
      <Modal visible={deleteaacount} transparent={true} animationType="slide">
        {/* <View
            style={{
              width: '100%',
              height: '100%',
              backgroundColor: '#000',
              opacity: 0.8,
            }}> */}
        <View style={Styles.modelView}>
          <Text style={[Styles.modelTxT, {marginTop: h(2)}]}>
            You sure, sure you want to {'\n'} delete your account? We won’t save
            a lick of your info. FYI,{'\n'} you’ll still have to go to your
            settings to fully unsubscribe.
          </Text>
          <View style={Styles.modelbuttonContainer}>
            <TouchableOpacity onPress={accounntDeleteModelOnly}>
              <Text style={Styles.modelAndText}>Delete, Please</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={accounntDeleteModelclose}>
            <Text
              style={{
                color: '#fa8072',
                alignSelf: 'center',
                marginTop: h(3.5),
                fontSize: 18,
                fontWeight: '600',
              }}>
              Nvm!
            </Text>
          </TouchableOpacity>
        </View>
        {/* </View> */}
      </Modal>

      {/* <View style={Styles.centeredView}> */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={pickerModelVisible}
        onRequestClose={() => {
          console.log('Model closed');
        }}>
        <View style={Styles.centeredView}>
          <View style={Styles.modalView}>
            <Text
              style={{
                fontSize: 18,
                textAlign: 'center',
                // alignSelf: 'center',
                color: '#fff',
                marginTop: h(5),
              }}>
              How do you want to do this {'\n'} whole profile-picture thing?
            </Text>
            <TouchableOpacity
              onPress={() => chooseFile('Camera')}
              style={{
                height: 40,
                width: '70%',
                backgroundColor: '#FE6F69',
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: 50,
                borderRadius: 20,
                alignSelf: 'center',
              }}>
              <Text style={{fontSize: 16, color: '#fff', fontWeight: '500'}}>
                Camera
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => openShareModal()}
              style={{
                height: 40,
                width: '70%',
                backgroundColor: '#FE6F69',
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: 15,
                borderRadius: 20,
                alignSelf: 'center',
              }}>
              <Text style={{fontSize: 16, color: '#fff', fontWeight: '500'}}>
                Gallery
              </Text>
            </TouchableOpacity>
            <View
              style={{
                position: 'absolute',
                bottom: 15,
                height: 40,
                width: '100%',
              }}>
              <TouchableOpacity
                onPress={() => {
                  setPickerModelVisible(!pickerModelVisible);
                }}
                style={{
                  height: 40,
                  width: '70%',
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: 20,
                  alignSelf: 'center',
                }}>
                <Text
                  style={{fontSize: 16, color: '#fa8072', fontWeight: '500'}}>
                  Cancel
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      {/* </View> */}

      <Modal visible={shareVisible} transparent={true}>
        {/* <View style={{width: '100%',height: '100%',backgroundColor: '#000',opacity: 0.8,}}> */}
        <View style={Styles.modelView}>
          <Text style={Styles.modelTxT}>Can we access your pics?</Text>
          <View style={Styles.modelbuttonContainer}>
            <TouchableOpacity onPress={() => chooseFile('Gallary')}>
              <Text style={Styles.modelAndText}>Go For It!</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={openShareModal2}>
            <Text
              style={{
                color: '#fa8072',
                alignSelf: 'center',
                marginTop: h(5),
                fontSize: 18,
              }}>
              No Thanks
            </Text>
          </TouchableOpacity>
        </View>
        {/* </View> */}
      </Modal>

      <Modal visible={shareVisible2} transparent={true}>
        {/* <View style={{width: '100%',height: '100%',backgroundColor: '#000',opacity: 0.8,}}> */}
        <View style={Styles.modelView}>
          <Text style={Styles.modelTxT}>
            Oh snap! OK, if you change {'\n'}
            your mind you can grant access {'\n'}
            in your phone settings.{' '}
          </Text>
          <View style={Styles.modelbuttonContainer}>
            <TouchableOpacity onPress={closeModal2}>
              <Text style={Styles.modelAndText}>Cool</Text>
            </TouchableOpacity>
          </View>
        </View>
        {/* </View> */}
      </Modal>
    </ScrollView>
  );
};
export default withNavigationFocus(User_Details);
// export default User_Details;
