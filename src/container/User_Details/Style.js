import { Platform } from 'react-native';
import {StyleSheet} from 'react-native';
import {h, w} from '../../utils/Dimensions';

export default StyleSheet.create({
  MainContainer: {
    flex: 1,
  },
  UploadView: {
    width: h(18),
    height: h(18),
    borderRadius: h(10),
    alignSelf: 'center',
    backgroundColor: '#d3d3d3',
    marginTop: h(7.9),
    justifyContent:'center',
    alignItems:'center'
  },
  UploadImage: {
    marginTop: h(12),
    marginLeft: h(15),
    
  },
  buttonContainer: {
    justifyContent:'center',
    alignItems:'center',
    alignSelf: 'center',
    backgroundColor: '#FE6F69',
    width: w(75),
    height: h(5),
    borderRadius: w(10),
    // top: h(10),
    marginTop:h(4),
},
AndText: {
  fontSize:16,
  alignSelf: 'center',
  color: '#FBFAFA',
  // marginTop: h(1.4),
  // marginVertical:h(1.0),
  fontWeight:'500'

},
  modelView:
  {
    backgroundColor:'#000',
    alignSelf:'center',
    // height:h(37),
    width: w(70),
    marginTop:h(17),
    borderRadius:20,
    justifyContent:'center',
    alignItems:'center',
    paddingVertical:h(5)
  },
  modelTxT :
  {
    color:'#fff',
    textAlign:'center',
    // backgroundColor:'red', 
    fontSize:16,
    // marginTop:h(5),
    fontWeight:'400',
    marginLeft:w(5),
    marginRight:w(5),
    // lineHeight:18
  
  
  },modelbuttonContainer: {
      alignSelf: 'center',
      backgroundColor: '#fa8072',
      width: w(50),
      height: h(6),
      borderRadius: w(10),
      marginTop: h(4),
      // justifyContent:'center'
    },
    modelAndText: {
      fontSize: 18,
      textAlign:'center',
      // alignSelf: 'center',
      color: '#fff',
      marginTop: h(1.6),
    },
    centeredView: {
      flex: 1,
      
     
    },
    modalView: {
      backgroundColor:'#000',
      alignSelf:'center',
      height:h(45),
      width:w(80),
      marginTop:h(17),
      borderRadius:20
    },
  
});

