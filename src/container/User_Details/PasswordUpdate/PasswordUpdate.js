import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  Modal,
  ScrollView,
  Keyboard,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  ImageBackground,
  BackHandler,
  ActivityIndicator,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {h, w} from '../../../utils/Dimensions';
import Styles from './Style';
import {Pages} from 'react-native-pages';
import {connect} from 'react-redux';
import {useSelector, useDispatch} from 'react-redux';

import {UpdatePasswordApi} from './../../../actions/UpdatePassword';
import AsyncStorage from '@react-native-community/async-storage';
import {UserDetailApi} from '../../../actions/USerProfile';
import {withNavigationFocus} from 'react-navigation';
import { Platform } from 'react-native';

const PasswordUpdate = ({navigation, ...props}) => {
  const dispatch = useDispatch();
  const screenStatus = navigation.isFocused();

  const showImage = props.data;
  const userDetail = useSelector((state) => state.userdetail.USERDETAIL);
  const userDetailLoading = useSelector((state) => state.userdetail.isLoading);

  const [old_password, setold_password] = useState('');
  const [new_password, setnew_password] = useState('');
  const [confirm_password, setconfirm_password] = useState('');
  const [errors, seterrors] = useState('');
  const [uiRender, setuiRender] = useState(false);
  const [showButton, setshowButton] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showNewPassword, setShowNewPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const [image, setimage] = useState('');
  const [red, setred] = useState(false);

  const Validation = async () => {
    seterrors('');
    setred(false);
    var text = new_password;
    let reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@@#\$%\^&\*])(?=.{8,})/; //Regex for strong password
    if (new_password == '' || new_password == null) {
      seterrors('These new secret words must be the same, silly!');
    } else if (confirm_password == '' || confirm_password == null) {
      seterrors('These new secret words must be the same, silly!');
    } else if (reg.test(text) === false) {
      setred(true);
      // seterrors('These new secret words must be the same, silly!');
      // return false;
    } else if (new_password != confirm_password) {
      seterrors('These new secret words must be the same, silly!');
    } else {
      let token = await AsyncStorage.getItem('token');
      setshowButton(true);
      dispatch(
        UpdatePasswordApi(old_password, new_password, confirm_password, token), //Call Api for Update password
      );

      console.log('hgurhgiurg');
    }
  };

  const colorChange = () => {
    Validation();
  };

  useEffect(() => {
    userDetails();
  }, []);

  const userDetails = async () => {
    let token = await AsyncStorage.getItem('token');
    dispatch(UserDetailApi(token));
  };

  useEffect(() => {
    const backAction = () => {
      Actions.pop();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, [screenStatus]);

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}>
      <ScrollView style={{backgroundColor: '#fff'}}>
        <View style={{flexDirection: 'row', height: h(7), marginTop: h(8)}}>
          <TouchableOpacity
            onPress={Actions.User_Details}
            style={{flex: 0.6, justifyContent: 'center', alignItems: 'center'}}>
            <View>
              <Image
                style={{width: w(2), height: h(2), opacity: 1}}
                source={require('../../../assets/icon/BackB.png')}
              />
            </View>
          </TouchableOpacity>
          <View
            style={{flex: 3, justifyContent: 'center', alignItems: 'center'}}>
            <Text style={Styles.Text}>Update Your Secret Word</Text>
          </View>
          <View style={{flex: 0.4}}></View>
        </View>

        <View style={{}}>
          <ImageBackground
            imageStyle={{borderRadius: h(10)}}
            style={Styles.UploadView}
            source={{
              uri: userDetail ? userDetail.profile_photo : image,
            }}></ImageBackground>
          <View
            style={{
              borderColor: '#000',
              borderWidth: w(0.28),
              width: w(75),
              height: h(5),
              borderRadius: w(10),
              marginTop: h(5.5),
              alignSelf: 'center',
              flexDirection: 'row',
              justifyContent: 'center',
            }}>
            <TextInput
              style={[Styles.inputFieldContainer, {fontSize: 16}]}
              placeholderTextColor="#383B3F"
              underlineColorAndroid="transparent"
              placeholder="Old Secret Word"
              autoCapitalize="none"
              underlineColorAndroid="transparent"
              secureTextEntry={!showPassword}
              onChangeText={(old_password) => setold_password(old_password)}
              value={old_password}
            />
            <TouchableOpacity
              style={Styles.passimage}
              onPress={() => setShowPassword(!showPassword)}>
              {!showPassword ? (
                <Image
                  style={{resizeMode: 'contain', height: 10, width: 20}}
                  source={require('../../../assets/icon/EyeVector.png')}
                />
              ) : (
                <Image
                  style={{resizeMode: 'contain', height: 10, width: 20}}
                  source={require('../../../assets/icon/openEyeVector.png')}
                />
              )}
            </TouchableOpacity>
          </View>
          <View
            style={{
              borderColor:
                new_password.length == 0
                  ? '#000'
                  : '#000' && new_password == confirm_password
                  ? '#5283C8'
                  : '#000' && errors != ''
                  ? 'red'
                  : '#000',
              borderWidth: w(0.28),
              width: w(75),
              height: h(5),
              borderRadius: w(10),
              marginTop: h(6),
              alignSelf: 'center',
              flexDirection: 'row',
              justifyContent: 'center',
            }}>
            <TextInput
              style={[
                Styles.inputFieldContainer,
                {
                  fontSize: 16,
                },
              ]}
              placeholderTextColor="#383B3F"
              underlineColorAndroid="transparent"
              placeholder="New Secret Word"
              autoCapitalize="none"
              underlineColorAndroid="transparent"
              secureTextEntry={!showNewPassword}
              onChangeText={(new_password) => setnew_password(new_password)}
              value={new_password}
            />
            <TouchableOpacity
              style={Styles.passimage}
              onPress={() => setShowNewPassword(!showNewPassword)}>
              {!showNewPassword ? (
                <Image
                  style={{resizeMode: 'contain', height: 10, width: 20}}
                  source={require('../../../assets/icon/EyeVector.png')}
                />
              ) : (
                <Image
                  style={{resizeMode: 'contain', height: 10, width: 20}}
                  source={require('../../../assets/icon/openEyeVector.png')}
                />
              )}
            </TouchableOpacity>
          </View>
          <Text
            style={{
              alignSelf: 'center',
              color: red && errors === '' ? 'red' : '#8C8C8C',
              marginTop: h(1.5),
              fontSize: Platform.OS === 'ios' ? 9 : 10,
            }}>
            Your secret word must be {'>'} 6 characters and contain{'\n'}an
            uppercase letter, a number, and a special character.
          </Text>

          <View
            style={{
              borderColor:
                new_password.length == 0
                  ? '#000'
                  : '#000' && new_password == confirm_password
                  ? '#5283C8'
                  : '#000' && errors != ''
                  ? 'red'
                  : '#000',
              borderWidth: w(0.28),
              width: w(75),
              height: h(5),
              borderRadius: w(10),
              marginTop: h(3),
              alignSelf: 'center',
              flexDirection: 'row',
              justifyContent: 'center',
            }}>
            <TextInput
              style={[
                Styles.inputFieldContainer,
                {
                  borderColor:
                    new_password.length == 0
                      ? '#000'
                      : '#000' && new_password == confirm_password
                      ? '#5283C8'
                      : '#000' && errors != ''
                      ? 'red'
                      : '#000',
                  fontSize: 16,
                },
              ]}
              placeholderTextColor="#383B3F"
              underlineColorAndroid="transparent"
              placeholder="New Secret Word Again"
              autoCapitalize="none"
              underlineColorAndroid="transparent"
              secureTextEntry={!showConfirmPassword}
              onChangeText={(confirm_password) =>
                setconfirm_password(confirm_password)
              }
              value={confirm_password}
            />
            <TouchableOpacity
              style={Styles.passimage}
              onPress={() => setShowConfirmPassword(!showConfirmPassword)}>
              {!showConfirmPassword ? (
                <Image
                  style={{resizeMode: 'contain', height: 10, width: 20}}
                  source={require('../../../assets/icon/EyeVector.png')}
                />
              ) : (
                <Image
                  style={{resizeMode: 'contain', height: 10, width: 20}}
                  source={require('../../../assets/icon/openEyeVector.png')}
                />
              )}
            </TouchableOpacity>
          </View>
          {errors != '' ? <Text style={Styles.errmsg}>{errors}</Text> : null}
          <View>
            <TouchableOpacity
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center',
                backgroundColor: showButton ? '#3d9390' : '#fe6f69',
                width: w(75),
                height: h(5),
                borderRadius: w(10),
                marginTop: h(5),
                opacity:1
      
              }}
              onPress={colorChange}>
              <Text style={Styles.AndText}>Remember Me</Text>
            </TouchableOpacity>
          </View>
          {userDetailLoading ? (
            <View
              style={{
                position: 'absolute',
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'transparent',
              }}>
              <View>
                <ActivityIndicator size="large" color="#000" />
              </View>
            </View>
          ) : null}
        </View>
      </ScrollView>
    </TouchableWithoutFeedback>
  );
};
export default withNavigationFocus(PasswordUpdate);
