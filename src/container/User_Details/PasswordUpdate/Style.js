import {StyleSheet} from 'react-native';
import {h, w} from '../../../utils/Dimensions';
import colorChange from './PasswordUpdate'

export default StyleSheet.create({
  MainContainer: {
    flex: 1,
  },
  inputFieldContainer: {
  //   alignSelf: 'center',
  // borderColor: '#000',
  // borderWidth: w(0.28),
  width: w(58),
  paddingLeft:12
  // height: h(5),
  // borderRadius: w(10),
  // // top: h(21)
  // marginTop:h(5.5),
  // paddingLeft:w(5)
  },

      UploadView: {
        width: h(18),
        height: h(18),
        borderRadius: 80,
        alignSelf: 'center',
        backgroundColor: '#d3d3d3',
        marginTop: h(3),
      },
      buttonContainer: {
        alignSelf: 'center',
        backgroundColor: '#fa8072',
        width: w(85),
        height: h(8),
        borderRadius: w(10),
        marginTop: h(4),
      },
      btnNormal:
      {
        
        alignSelf: 'center',
        backgroundColor: '#000',
        width: w(85),
        height: h(8),
        borderRadius: w(10),
        marginTop: h(4),


      },
      AndText: {
        fontSize: 16,
        alignSelf: 'center',
        color: '#FBFAFA',
        // paddingTop:h(1.4),

        // marginTop: h(1.2),
        fontWeight:'500'
      },
      Text:
      {
        fontSize:20,
        color:'#383B3F',
        // top:h(4.5), 
        alignSelf:'center',
        // marginTop:h(4.5),
        fontWeight:'bold',
        // marginRight:w(16)
      },
      HeaderStyle:
      
        {
          flexDirection:'row',
            marginLeft:w(10),
            marginTop:h(3),
            justifyContent:'space-between',
            
        },
        passimage: {
          alignItems:'center', 
          justifyContent:'center',
          width:w(12),
          // height:h(1.4),
          //   marginLeft: w(75),
          //   marginTop:h(-3)

          },
          errmsg: {
            color: '#FE6F69',
            top:h(1),
            // marginBottom: h(5),
            marginLeft: w(18),
            fontSize:10
          },
          
  
});
