import React, { useEffect } from 'react';
import { View, Image, Text, BackHandler } from 'react-native';
import { h, w } from '../../../utils/Dimensions';
import Styles from './Style';

const ReachOut = () => {

  useEffect(() => {
    const backAction = () => {
      return true;
    };
    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );
    return () => backHandler.remove();
  }, []);

  return (
    <View>
      <View style={Styles.HeaderStyle}>
        <Text style={Styles.noConnectionTexxt}>NO INTERNET CONNECTION</Text>
      </View>
      <Image
        style={{ width: h(30), height: h(28), alignSelf: 'center', marginTop: h(7) }}
        source={require('../../../assets/icon/Strawberry_Icon.png')}
      />
      <Text style={{ fontSize: 20, color: '#383B3F', alignSelf: 'center', marginTop: h(4.5), fontWeight: 'bold' }}>
        Error Bummer{' '}
      </Text>
      <Text style={{ alignSelf: 'center', fontSize: 16, fontWeight: '900', marginTop: h(5), color: '#383B3F' }}>
        Find your illusive internet {'\n'}connection and try again.
        {/* Slow and steady has lost this {'\n'}loading time race. Try again {'\n'}soon now, ya hear? */}
      </Text>
    </View>
  );
};

export default ReachOut;
