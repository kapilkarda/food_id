import { StyleSheet } from 'react-native';
import { h, w } from '../../../utils/Dimensions';

export default StyleSheet.create({
  MainContainer: {
    flex: 1,
  },
  HeaderStyle: {
    marginTop: h(10),
  },
  noConnectionTexxt: {
    textAlign: 'center',
    fontWeight: '400',
    fontSize: 16,
    color: '#8C8C8C',
  },
  Text: {
    fontSize: 20,
    fontWeight: 'bold',
    marginRight: w(24)

  },
})