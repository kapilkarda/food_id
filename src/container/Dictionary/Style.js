import {StyleSheet} from 'react-native';
import {h, w} from '../../utils/Dimensions';

export default StyleSheet.create({
  MainContainer: {
    // flex: 1,
  },
  yellowimage: {
    width: h(68),
    height:h(42),
    position: 'absolute',
    resizeMode: 'stretch',
    top:h(-17),
    alignSelf:'center'
  },
  milkimage: {
    alignSelf: 'center',
    marginTop: h(15),
    width:h(18),
    height:h(22),
    resizeMode:'contain'
  },
  DrawerIcon:
{
  width: w(3),
  height: w(3),
  marginLeft : w(8),
  // marginTop:h(5)
  // top:h(2),
  position:'absolute',
  tintColor:'#383B3F'

},
Searchtxt:
  {
alignSelf:'center',
marginTop:h(-19)

  },
  TouchTxt:
  {
fontSize:36,
fontWeight:'bold',
color :'#383B3F',
marginRight:w(-3), 

  },
  TouchTxt2:
  {
fontSize:36,
fontWeight:'bold',
color :'#383B3F',
// marginRight:w(2),
marginLeft:h(1) 

  }
  
})