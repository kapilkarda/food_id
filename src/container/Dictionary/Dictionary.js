import React, { useState, useEffect } from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  Modal,
  ImageBackground,
  ScrollView,
  Keyboard,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Platform,
  BackHandler,
  ActivityIndicator,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { IconAsset, Strings, UiColor } from '../../theme';
import { h, w } from '../../utils/Dimensions';
//import Styles from './Style';
import { Pages } from 'react-native-pages';
import { connect } from 'react-redux';
import styles from '../Splash/styles';
import Styles from './Style';
import { useSelector, useDispatch } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { DictionaryApi } from './../../actions/Dictionary';
import { SuggestionApi } from './../../actions/wordSuggestion';

const br = `\n`;
const Dictionary = () => {

  const dispatch = useDispatch();

  const [search_value, setsearch_value] = useState(false);
  const [search_values, setsearch_values] = useState(false);
  const [hide, sethide] = useState(false);
  let [uiRender, setUiRender] = useState(false);
  const [Token, setToken] = useState('');
  const [Buttonsearch, setButtonsearch] = useState(false);

  // const Food_Serach = useSelector((state) => console.log('words', state));
  const suggestion = useSelector((state) => state.wordsuggestion.WORDS);
  const suggestionLoading = useSelector((state) => state.wordsuggestion.isLoading);
  // const  words = suggestion.CompleteSuggestion

  useEffect(() => {
    tokenget();
  }, []);

  const tokenget = async () => {
    const token = await AsyncStorage.getItem('token');
    setToken(token);
  };

  
  const onSubmit = () => {
    Actions.Caseines(search_value);
  };

  const input = () => {
    setButtonsearch(true);
  };

  useEffect(() => {
    const backAction = () => {
      Actions.Dashboard();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  return (
    <View style={{ flex: 1, backgroundColor: '#ffff' }}>
      <TouchableWithoutFeedback
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          behavior={Platform.OS === 'ios' ? 'position' : 'position'}
          // padding={10}
          keyboardVerticalOffset={Platform.OS === 'ios' ? -100 : 50}>
          <View style={Styles.MainContainer}>
            <Image
              style={Styles.yellowimage}
              source={require('../../assets/icon/Yelloround.png')}
            />
            <Image
              style={Styles.milkimage}
              source={require('../../assets/icon/Definition_Artichoke.png')}
            />
            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity
                style={Styles.DrawerIcon}
                onPress={Actions.drawerOpen}>
                <Image
                  source={require('../../assets/image/Hamburger_Nav.png')}
                  style={{ height: h(2.3), width: h(2.3), resizeMode: 'contain' }}
                />
              </TouchableOpacity>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
                left: w(15),
                marginTop: h(-2),
              }}>
              <View
                style={{
                  width: w(0.5),
                  height: Buttonsearch ? '25%' : '50%',
                  backgroundColor: '#8c8c8c',
                  // marginLeft: w(21.5),
                  right: w(14),
                  marginTop: h(17),
                }}></View>
              {Buttonsearch ? (
                <TextInput
                  style={[
                    {
                      fontSize: 20,
                      fontWeight: '500',
                      marginTop: h(18),
                      alignSelf: 'center',
                      width: w(55),
                      left: w(-10),
                    },
                  ]}
                  placeholderTextColor="#000"
                  underlineColorAndroid="transparent"
                  placeholder="Explore Food Dictionary"
                  autoCapitalize="none"
                  underlineColorAndroid="transparent"
                  onChangeText={(text) => setsearch_value(text)}
                  value={search_value}
                  onSubmitEditing={(text) => onSubmit(text)}
                  autoFocus={true}
                />
              ) : (
                <TouchableOpacity
                  onPress={input}
                  style={{ marginTop: h(18), left: w(-12) }}>
                  <Text
                    style={{ fontSize: 36, fontWeight: '500', color: '#383B3F' }}>
                    Explore Food {'\n'}Dictionary
                  </Text>
                </TouchableOpacity>
              )}
            </View>

            <View>
              {search_value.length > 0 ? (
                // <FlatList
                // style = {{marginTop:h(1),alignSelf:'center',left:w(1)}}
                //   data={suggestion}
                //   horizontal={false}
                //   renderItem={({item , index}) => (
                //     <View>
                //       <TouchableOpacity onPress = {()=> Actions.Caseines(item.data)}>
                //         <Text style = {{fontSize:16}}>{item.data}</Text>
                //       </TouchableOpacity>
                //     </View>
                //   )}
                // />
                <FlatList
                  style={{ marginTop: h(2) }}
                  data={suggestion ? suggestion.meaning : []}
                  // horizontal={true}
                  renderItem={({ item, index }) => (
                    <TouchableOpacity
                      onPress={() => Actions.Caseines(item.data)}
                      style={{
                        backgroundColor: '#5283C8',
                        width: '100%',
                        height: h(5),
                        marginTop: h(0.5),
                      }}>
                      <Text
                        style={{
                          color: '#fff',
                          fontSize: 13,
                          marginLeft: w(18),
                          marginTop: h(1.5),
                        }}>
                        {item.data}
                      </Text>
                    </TouchableOpacity>
                  )}
                />
              ) : null}
            </View>
          </View>
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
      {
        (suggestionLoading) ?
          <View
            style={{
              position: 'absolute', left: 0, right: 0, top: 5, bottom: 0, alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: "transparent"
            }}
          >
            <View>
              <ActivityIndicator size="large" color="#000" />
            </View>
          </View> : null
      }
    </View>
  );
};
export default Dictionary;
