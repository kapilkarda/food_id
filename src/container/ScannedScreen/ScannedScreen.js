import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  Modal,
  ScrollView,
  SafeAreaView,
  BackHandler,
  ActivityIndicator,
  Platform,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {IconAsset, Strings, UiColor} from '../../theme';
import {h, w} from '../../utils/Dimensions';
// import Styles from './Style';
import {Pages} from 'react-native-pages';
import {connect} from 'react-redux';
import {addFavApi} from './../../actions/addFav';
import AsyncStorage from '@react-native-community/async-storage';
import {useSelector, useDispatch} from 'react-redux';
import {InputGroup, Item} from 'native-base';
import {Undo} from './../../actions/Undo';

let number = 0;
// const empty = []
const ScannedScreen = ({...props}) => {
  // console.log('props', props);
  const navi = props.navigation.state.key;
  // console.log('navi', navi);
  const dispatch = useDispatch();
  const [items, setItems] = useState(false);
  const [cira, setcira] = useState('');
  const [show, setshow] = useState(false);
  const [cateName, setcatName] = useState('');
  const [chnage, setchange] = useState(false);
  const [Navigation, setNavigation] = useState(false);
  const [blankIngrediants, setblankIngrediants] = useState(false);

  const name = props.data.product_name;
  const brand_name = props.data.brand;
  const barcode = props.data.barcode;
  const Ingrediantss = props.data.ingredients;
  // console.log('Ingrediants', Ingrediantss);
  const fat = props.data.fat[0].per_100g;
  const salt = props.data.salt[0].per_100g;
  const sugar = props.data.sugar[0].per_100g;
  const saturate = props.data.carbohydrate[0].per_100g;
  const ciratedItems = props.data.cirateditems;
  const splittedCitraedItems = ciratedItems.split(',');
  const ciretedArray = props.data.cirateditemsCatArray;
  const portion = props.data.totalPortion;

  const df = Ingrediantss.split(',');
  const hj = ciratedItems.trim();

  const favlistLoading = useSelector((state) => state.favlist.isLoading);
  const undoFavLoading = useSelector((state) => state.undo.isLoading);
  const blankIngrediantss = props.data.ingredients;
  console.log('blankIngrediantss', blankIngrediantss);
  const empty = Ingrediantss.split(',');
  console.log('empty',empty)
  useEffect(()=>{
    setblankIngrediants(false);
  },[])
    // setblankIngrediants(false);

  useEffect(() => {
    // setblankIngrediants(false);
    // if (
    //   blankIngrediantss == '' ||
    //   blankIngrediantss == null ||
    //   blankIngrediantss == undefined
    // ) {
    //   console.log('inside if');
    //   setblankIngrediants(true);
    // }
    // else{
    //   setblankIngrediants(false);
    // }
    if(empty)
    {
      if(empty.length === 1)
      {
        setblankIngrediants(true);
      }
      else{
        setblankIngrediants(false);

      }
    }
  }, [empty]);
  
  useEffect(() => {
    number = 0;
  }, []);

  var abc = [];
  for (let fg of df) {
    // console.log('df', df);
    abc.push({name: fg, value: false});
  }

  const fg = ciratedItems.trim();
  for (let item of abc) {
    for (let ctItem of splittedCitraedItems) {
      if (item.name === ctItem) {
        // console.log('matched item', item);
        if (item.value === false) {
          item.value = true;
        } else if (item.value === true) {
          item.value = false;
        }
      }
    }
  }

  if (ciratedItems.length > 0) {
    number = ciratedItems.split(',').length;
  } else {
    number = 0;
  }

  const showDetails = (name) => {
    setcira(name);
    if (show == false) {
      setshow(true);
    }
    for (let matchItem of ciretedArray) {
      if (name == matchItem.item_name) {
        setcatName(matchItem.category_name);
      }
    }
  };

  const addToFav = async () => {
    const token = await AsyncStorage.getItem('token');
    dispatch(addFavApi(barcode, token));
    if (chnage == false) {
      setchange(true);
      // setTimeout(() => setchange(false), 8000);
    }
  };

  const UndoFood = async () => {
    const favorite_id = await AsyncStorage.getItem('favorite_id');
    const token = await AsyncStorage.getItem('token');
    dispatch(Undo(favorite_id, token));
    setchange(false);
  };

  useEffect(() => {
    const backAction = async () => {
      const exit = await AsyncStorage.getItem('Exit');
      if (exit === '1' || exit === 1) {
        Actions.SearchByname();
      } else if (exit === '0' || exit === 0) {
        Actions.Scan();
      }
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  console.log('blank', blankIngrediants);
  return (
    <View style={{backgroundColor: '#ffff'}}>
      <ScrollView style={{marginBottom: h(7)}} bounces={false}>
        <View
          style={{
            backgroundColor: '#383b3f',
            width: '105%',
            height: h(25),
            borderBottomLeftRadius: h(4),
          }}>
          <TouchableOpacity
            onPress={Actions.Dashboard}
            style={{top: h(10.5), left: w(5)}}>
            <Image
              style={{width: h(2), height: h(2), resizeMode: 'contain'}}
              source={require('./../../assets/icon/BackB.png')}
            />
          </TouchableOpacity>

          <Text
            style={{
              color: '#EBEBEB',
              alignSelf: 'center',
              fontSize: 20,
              marginTop: h(8),
            }}>
            {brand_name}
          </Text>
          <Text
            style={{
              color: '#EBEBEB',
              alignSelf: 'center',
              fontSize: 16,
              marginTop: h(1),
            }}>
            {name}
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginTop: h(5.5),
            width: w(85),
            alignSelf: 'center',
          }}>
          <View style={{flexDirection: 'column', width: w(32)}}>
            <View
              style={{
                marginTop: h(2),
                backgroundColor: '#EBEBEB',
                width: h(11),
                height: h(12),
                borderBottomRightRadius: h(5),
                // marginLeft: w(10),
              }}>
              <View
                style={{
                  height: h(3.5),
                  width: h(3.5),
                  backgroundColor: '#FE6F69',
                  alignSelf: 'center',
                  borderRadius: h(10),
                  top: h(2),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{color: '#fff', alignSelf: 'center', fontSize: h(2)}}>
                  {number}
                </Text>
              </View>
              <Text
                style={{
                  color: '#8C8C8C',
                  fontSize: h(1.6),
                  fontWeight: 'bold',
                  alignSelf: 'center',
                  marginTop: h(4),
                }}>
                watchouts
              </Text>
            </View>
            {show ? (
              <View
                style={{
                  marginTop: h(2),
                  borderTopWidth: w(0.1),
                  borderTopColor: '#8c8c8c',
                  borderBottomWidth: w(0.1),
                  // backgroundColor: '#EBEBEB',
                  width: h(13.5),
                  borderBottomColor: '#8c8c8c',
                  // height: h(12),
                  // marginLeft: w(10),
                  // paddingBottom: 5,
                  padding: h(1.5),
                }}>
                <Text
                  style={{
                    // paddingTop: h(2),
                    color: '#FE6F69',
                    fontSize: 13,
                  }}>
                  {cira}:
                </Text>
                <Text style={{color: '#9a9a9a', fontSize: 13}}>
                  part of the {cateName} category
                </Text>
              </View>
            ) : null}
          </View>
          <View
            style={{
              marginTop: h(2),
              backgroundColor: '#fff',
              // width: h(33),
              paddingRight: w(30),

              // marginRight:w(10),
              borderWidth: 0,
              // backgroundColor:'red'
            }}>
            {blankIngrediants === false ? (
              <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                {abc &&
                  abc.length > 0 &&
                  abc.map((item, i) => (
                    <TouchableOpacity
                      onPress={() => {
                        item.value ? showDetails(item.name) : null;
                      }}>
                      <Text
                        style={{
                          color: item.value ? '#FE6F69' : '#9a9a9a',
                          fontSize: 13.5,
                          alignSelf: 'center',
                          marginLeft: w(1),
                        }}
                        key={i}>
                        {abc.length - 1 === i ? item.name : item.name + ','}
                      </Text>
                    </TouchableOpacity>
                  ))}
              </View>
            ) : (
              <View
                style={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 13.5,
                    // alignSelf: 'center',
                    marginLeft: w(10),
                    color: '#9a9a9a',
                    marginTop:h(8)
                  }}>
                  ingredients not found
                </Text>
              </View>
            )}
          </View>
        </View>
        <View
          style={{
            backgroundColor: '#EBEBEB',

            width: w(85),
            height: h(20),
            marginTop: h(5),
            // marginLeft: w(6.8),
            alignSelf: 'center',
            borderBottomRightRadius: w(8),
          }}>
          <Text
            style={{
              color: '#FE6F69',
              alignSelf: 'center',
              fontSize: 14,
              marginTop: h(2),
            }}>
            <Text style={{fontWeight: 'bold'}}>Nutritional values per </Text>
            100g (portion {portion}g)
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              marginLeft: w(2),
              marginTop: h(2),
            }}>
            <Text
              style={{
                color: '#8C8C8C',
                fontSize: 13,
                marginLeft: w(-2),
                top: h(-1),
              }}>
              Fats
            </Text>
            <Text
              style={{color: '#8C8C8C', fontSize: 13, left: w(1), top: h(-1)}}>
              Sugar
            </Text>
            <Text
              style={{color: '#8C8C8C', fontSize: 13, left: w(4), top: h(-1)}}>
              Salt
            </Text>
            <Text
              style={{color: '#8C8C8C', fontSize: 13, left: w(4), top: h(-1)}}>
              Saturates
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              Top: h(2.5),
            }}>
            <View
              style={{
                width: h(6),
                height: h(8),
                backgroundColor: '#383b3f',
                borderRadius: h(3),
              }}>
              <Text
                style={{
                  color: '#fff',
                  alignSelf: 'center',
                  top: h(3),
                  fontSize: 10,
                }}>
                {fat}
              </Text>
            </View>
            <View
              style={{
                width: h(6),
                height: h(8),
                backgroundColor: '#383b3f',
                borderRadius: h(3),
              }}>
              <Text
                style={{
                  color: '#fff',
                  alignSelf: 'center',
                  top: h(3),
                  fontSize: 10,
                }}>
                {sugar}
              </Text>
            </View>
            <View
              style={{
                width: h(6),
                height: h(8),
                backgroundColor: '#383b3f',
                borderRadius: h(3),
              }}>
              <Text
                style={{
                  color: '#fff',
                  alignSelf: 'center',
                  top: h(3),
                  fontSize: 10,
                }}>
                {salt}
              </Text>
            </View>
            <View
              style={{
                width: h(6),
                height: h(8),
                backgroundColor: '#383b3f',
                borderRadius: h(3),
              }}>
              <Text
                style={{
                  color: '#fff',
                  alignSelf: 'center',
                  top: h(3),
                  fontSize: 10,
                }}>
                {saturate}
              </Text>
            </View>
          </View>
        </View>
        {chnage ? (
          <View
            style={{
              alignSelf: 'center',
              backgroundColor: '#000',
              width: h(15),
              height: h(4),
              borderRadius: w(10),
              marginLeft: w(77),
              marginTop: h(3),
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity onPress={UndoFood}>
              <Text
                style={{
                  fontSize: 14,
                  alignSelf: 'center',
                  color: '#F5F4F4',

                  fontWeight: '600',
                  marginRight: w(2),
                }}>
                Undo{' '}
              </Text>
            </TouchableOpacity>
          </View>
        ) : (
          <View
            style={{
              alignSelf: 'center',
              backgroundColor: '#fa8072',
              width: '30%',
              height: h(4),
              borderRadius: w(10),
              marginLeft: w(77),
              marginTop: h(3),
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity onPress={addToFav}>
              <Text
                style={{
                  fontSize: 14,
                  alignSelf: 'center',
                  color: '#F5F4F4',

                  fontWeight: '600',
                  marginRight: w(2),
                }}>
                + Fave Stuff{' '}
              </Text>
            </TouchableOpacity>
          </View>
        )}
        <View style={{height: h(10), width: h(10)}}></View>
        {favlistLoading || undoFavLoading ? (
          <View
            style={{
              position: 'absolute',
              left: 0,
              right: 0,
              top: 5,
              bottom: 0,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: 'transparent',
            }}>
            <View>
              <ActivityIndicator size="large" color="#000" />
            </View>
          </View>
        ) : null}
      </ScrollView>
    </View>
  );
};
export default ScannedScreen;
