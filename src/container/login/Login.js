import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  Modal,
  TouchableWithoutFeedback,
  Keyboard,
  ImageBackground,
  BackHandler,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {withNavigationFocus} from 'react-navigation';
import {Actions} from 'react-native-router-flux';
import {IconAsset, Strings, UiColor} from '../../theme';
import {h, w} from '../../utils/Dimensions';
import Styles from './Style';
import {Pages} from 'react-native-pages';
import {connect} from 'react-redux';
import {LoginAPI} from './../../actions/Login';
import {forgetPasswordApi} from './../../actions/forgetPassword';
import {SocialLoginApi} from './../../actions/SocialLogin';
import AsyncStorage from '@react-native-community/async-storage';
import {
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager,
} from 'react-native-fbsdk';
import Style from './Style';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from 'react-native-google-signin';
import {Platform} from 'react-native';
import {
  AppleButton,
  appleAuth,
} from '@invertase/react-native-apple-authentication';

let checkedServerStatus = true;

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const Login = ({navigation}) => {
  const screenStatus = navigation.isFocused();

  // useEffect(() => {
  //   checktoken();
  //   setCredentials();
  // }, []);

  const dispatch = useDispatch();
  const loginError = useSelector((state) => state.Login.ERROR);
  const loginLoading = useSelector((state) => state.Login.isLoading);
  const socialLoading = useSelector((state) => state.social.isLoading);
  const getMaintainStatus = useSelector(
    (state) => state.getServerMaintainStatus,
  );

  const [Show, setShow] = useState(false);
  const [shareVisible, shareSetVisible] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [hidePassword, sethidePassword] = useState(true);
  const [uiRender, setuiRender] = useState(false);
  const [showButton, setshowButton] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [isEmailError, issetEmailError] = useState('');
  const [isEmailExistError, setEmailExistError] = useState('');
  const [isPasswordError, isSetPasswordError] = useState('');
  const [errors, seterrors] = useState('');
  const [toggle, settoggle] = useState(false);
  const [visibleServerModal, setVisibleServerModal] = useState(false);
  const userInfo = {};
  const white = require(`../../assets/icon/Eye_Closed.png`);
  const black = require(`../../assets/icon/icon-021.png`);

  const passwordSave = async () => {
    settoggle(true);
  };

  useEffect(() => {
    if (getMaintainStatus.serverStatus) {
      if (getMaintainStatus.serverStatus.data) {
        if (getMaintainStatus.serverStatus.data.status == true) {
          console.log('Server');
          if (checkedServerStatus) {
            console.log('ServerIf');
            setVisibleServerModal(true);
            checkedServerStatus = false;
          }
        } else {
          setVisibleServerModal(false);
        }
      }
    }
  }, [getMaintainStatus]);
  useEffect(() => {
    checktoken();
    setCredentials();
    setEmailExistError('');
  }, [screenStatus]);

  useEffect(() => {
    if (loginError && loginError.message) {
      if (
        loginError.message === 'Email Does Not Exist' ||
        loginError.message === 'mobile does not exist'
      ) {
        setEmailExistError(
          'You must be new around here. Please ‘Join In’ instead.',
        );
      } else if (loginError.message === 'Invalid Email or Password') {
        isSetPasswordError(loginError.message);
      } else if (
        loginError.message === 'This Email is already exist in social'
      ) {
        shareSetVisible(true);
      }
    }
  }, [loginError]);

  const checktoken = async () => {
    const imageset = await AsyncStorage.getItem('res.path');
    console.log('imageset444', imageset);
  };

  const fbLogin = async () => {
    // Login By FaceBook Function
    // logout()
    if (Platform.OS === 'android') {
      LoginManager.setLoginBehavior('web_only');
    }

    const result = await LoginManager.logInWithPermissions([
      'public_profile',
      'email',
    ]);

    console.log('result', result);
    if (result.isCancelled) {
      // throw errorConsts.cancelledLogin;
    } else {
      const {accessToken} = await AccessToken.getCurrentAccessToken();
      if (accessToken) {
        const user = await fetchFbProfile(accessToken);
        console.log('user fb', user);
        const ProfilePicture = user.picture.data.url;
        AsyncStorage.setItem('ProfilePicture', ProfilePicture);
        AsyncStorage.setItem('LoginBy', 'facebook');
        try {
          const currentAccessToken = await AccessToken.getCurrentAccessToken();

          const graphRequest = new GraphRequest(
            '/me',
            {
              accessToken: currentAccessToken.accessToken,
              parameters: {
                fields: {
                  string: 'picture.type(large)',
                },
              },
            },
            (error, result) => {
              if (error) {
                console.log(error);
              } else {
                console.log('result fblogin', result);
                let data = {
                  name: user.name,
                  socialId: user.id,
                  email: user.email,
                  mobile: '',
                  provider: 'facebook',
                  social_token: user.id,
                };
                console.log('data', data);
                dispatch(SocialLoginApi(data, navigation));
              }
            },
          );

          new GraphRequestManager().addRequest(graphRequest).start();
        } catch (error) {
          console.error(error);
        }
      }
    }
  };

  //Fetching profile details
  const fetchFbProfile = async (accessToken) => {
    try {
      const response = await fetch(
        `https://graph.facebook.com/v2.5/me?fields=email,first_name,last_name,name,picture&access_token=${accessToken}`,
      );
      return response.json();
    } catch (error) {
      console.log('Unable to login, please retry.');
    }
  };

  useEffect(() => {
    console.log('hello');
    AsyncStorage.getItem('showButtonLogin').then((value) => {
      const newValue = JSON.parse(value);
      // console.log('newValue', newValue);
      setshowButton(newValue);
    });
  }, [screenStatus]);

  const configureGoogle = () => {
    return GoogleSignin.configure({
      iosClientId:
        '911162193902-t727o372o94l32j9gh3q0c73alj5ah73.apps.googleusercontent.com',
      webClientId:
        '911162193902-q1a6lul636onqmhkd19im1kc84of8m64.apps.googleusercontent.com',
      offlineAccess: true,
    });
  };

  const googleLogin = async () => {
    //Login By Google function
    configureGoogle();
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();

      let user = userInfo.user;

      let data = {
        name: user.name,
        socialId: user.id,
        email: user.email,
        mobile: '',
        deviceToken: null,
        loginType: null,
        userType: null,
        provider: 'google',
      };
      console.log('data', data);
      dispatch(SocialLoginApi(data, navigation));
    } catch (error) {
      console.log('error', error);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        //signin cancelled
      } else if (error.code === statusCodes.IN_PROGRESS) {
        //signin inprogress
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        //playservice not available
      }
    }
  };
  const signOutGoogle = async () => {
    console.log('google signout called.');

    console.log('before revokeaccess');
    await GoogleSignin.revokeAccess()
      .then((res) => console.log('revoke access->response->', res))
      .catch((err) => console.log('revoke access error -> ', err));
    console.log('revoked');
    await GoogleSignin.signOut()
      .then((res) => console.log('sign out response->', res))
      .catch((err) => console.log('signout error ->', err));
    console.log('signed out.');
  };
  const setCredentials = async () => {
    console.log('setCredentials');
    if (showButton) {
      console.log('showInsideIf', showButton);
      const EmailSet = await AsyncStorage.getItem('email');
      const PasswordsetSet = await AsyncStorage.getItem('password');
      console.log('EmailSet', EmailSet);
      console.log('PasswordsetSet', PasswordsetSet);
      setEmail(EmailSet);
      setPassword(PasswordsetSet);
    } else {
      setEmail('');
      setPassword('');
    }
  };

  const colorChange = async () => {
    setshowButton(!showButton);
    await AsyncStorage.setItem('showButtonLogin', JSON.stringify(!showButton));
  };

  const doLogin = () => {
    //Login Function with Validation
    issetEmailError('');
    isSetPasswordError('');
    setEmailExistError('');
    AsyncStorage.setItem('LoginBy', '');
    var text = email;
    let reg = /^(?:\d{10}|\w+@\w+\.\w{2,3})$/; // email regx
    if (
      (email === '' || email === null) &&
      (password === '' || password === null)
    ) {
      console.log('inside if');
      issetEmailError('Please Enter Email');
      isSetPasswordError('Please Enter password');
    } else if (email === '' || email === null) {
      issetEmailError('Please Enter Email');
    } else if (password === '' || password === null) {
      isSetPasswordError('Please Enter password');
    } else {
      dispatch(LoginAPI(email, password , navigation)); //Calling Login Api

      passwordSave();
    }
  };

  console.log('showPassword', showPassword);
  // console.log('ierror', isError);
  console.log('showButton', showButton);
  const appleSignIn = (result) => {
    console.log('Resssult', result);
  };

  async function onAppleButtonPress(updateCredentialStateForUser) {
    // Login By Apple function
    console.warn('Beginning Apple Authentication');
    appleAuth.Operation.LOGOUT;

    // start a login request
    try {
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
        requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
      });

      console.log('appleAuthRequestResponse', appleAuthRequestResponse);

      if (
        appleAuthRequestResponse.fullName.givenName !== null &&
        appleAuthRequestResponse.email !== null
      ) {
        let data = {
          name: appleAuthRequestResponse.fullName.givenName,
          socialId: ' ',
          email: appleAuthRequestResponse.email,
          mobile: '',
          deviceToken: appleAuthRequestResponse.identityToken,
          loginType: null,
          userType: null,
          provider: 'apple',
        };
        dispatch(SocialLoginApi(data , navigation));
      } else {
        let data = {
          name: '',
          socialId: ' ',
          email: '',
          mobile: '',
          deviceToken: appleAuthRequestResponse.identityToken,
          provider: 'apple',
        };
        dispatch(SocialLoginApi(data , navigation));
      }

      console.warn(`Apple Authentication Completed, ${user}, ${email}`);
    } catch (error) {
      if (error.code === appleAuth.Error.CANCELED) {
        console.warn('User canceled Apple Sign in.');
      } else {
        console.error(error);
      }
    }
  }
  useEffect(() => {
    const backAction = () => {
      Alert.alert('Hold on!', 'Are you sure you want to Exit the App', [
        {
          text: 'Cancel',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'YES', onPress: () => BackHandler.exitApp()},
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  const checkEmailFunc = () => {
    var text = email;
    let reg = /^(?:\d{10}|\w+@\w+\.\w{2,3})$/;
    if (reg.test(text) === false) {
      console.log('Email is not Correct');
      // issetEmailError("Please enter valid email")
    } else {
      issetEmailError('');
    }
  };

  const closeServerModal = () => {
    BackHandler.exitApp();
    setVisibleServerModal(false);
    checkedServerStatus = true;
  };
  const closeSocialModel = () => {
    shareSetVisible(false);
    dispatch({type: 'Error', payload: ''});
  };

  // Return Ui For Login Page
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}>
      <View
        style={{
          flex: 1,
          backgroundColor: shareVisible ? '#000' : '#fff',
          opacity: shareVisible ? 0.5 : 1,
        }}>
        <View style={{flex: 0.9}}>
          <Text numberOfLines={1} adjustsFontSizeToFit style={Styles.txt}>
            Yay, You’re Back!
          </Text>
        </View>

        <View style={{flex: 1.43}}>
          <View>
            <TextInput
              style={[
                Styles.inputFieldContainer,
                {
                  fontSize: 16,
                  borderColor:
                    isEmailError || isEmailExistError ? 'red' : '#000',
                },
              ]}
              placeholderTextColor="#383B3F"
              underlineColorAndroid="transparent"
              placeholder="@ or #"
              autoCapitalize="none"
              underlineColorAndroid="transparent"
              onChangeText={(email) => setEmail(email)}
              onBlur={() => checkEmailFunc()}
              value={email}
            />
            {isEmailExistError ? (
              <Text
                style={{
                  color: '#FE6F69',
                  fontSize: 10,
                  textAlign: 'center',
                  marginTop: h(1),
                }}>
                {isEmailExistError}
              </Text>
            ) : null}
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              borderColor: isPasswordError ? 'red' : '#000',
              borderWidth: w(0.28),
              width: w(75),
              height: h(5),
              borderRadius: w(10),
              marginTop: h(7),
              paddingLeft: w(5),
            }}>
            <TextInput
              style={[Styles.inputFieldContainer2, {fontSize: 16}]}
              placeholderTextColor="#383B3F"
              underlineColorAndroid="transparent"
              placeholder="Secret Word"
              autoCapitalize="none"
              underlineColorAndroid="transparent"
              secureTextEntry={!showPassword}
              onChangeText={(password) => setPassword(password)}
              value={password}
            />

            <TouchableOpacity
              style={{alignItems: 'center', justifyContent: 'center'}}
              // onPress={PasswordVisibility}
              onPress={() => setShowPassword(!showPassword)}>
              {!showPassword ? (
                <Image
                  source={require('../../assets/icon/EyeVector.png')}
                  style={{resizeMode: 'contain', height: 10, width: 20}}
                />
              ) : (
                <Image
                  source={require('../../assets/icon/openEyeVector.png')}
                  style={{resizeMode: 'contain', height: 10, width: 20}}
                />
              )}
            </TouchableOpacity>
          </View>

          <View
            style={{
              flexDirection: 'row',
              marginTop: h(1),
              alignSelf: 'center',
            }}>
            <TouchableOpacity onPress={() => colorChange()}>
              <ImageBackground
                source={require('../../assets/image/Outline_Button.png')}
                style={{
                  height: 18,
                  width: 18,
                  justifyContent: 'center',
                  alignItems: 'center',
                  top: h(0.2),
                }}>
                {showButton ? (
                  <Image
                    source={require('../../assets/icon/login4eve.png')}
                    style={{height: 12, width: 12, resizeMode: 'contain'}}
                  />
                ) : null}
              </ImageBackground>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => colorChange()}>
              <Text
                style={{
                  color: '#8c8c8c',
                  marginLeft: w(2),
                  fontSize: 12,
                  marginTop: h(0.3),
                }}>
                Sign In 4eva
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={Actions.ForgetScreen}
              style={Styles.forgotButton}>
              <Text style={{color: '#8c8c8c', fontSize: 12}}>
                Forgot Secret Word?
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={doLogin} style={Styles.buttonContainer}>
            <Text style={Styles.AndText}>Get After It</Text>
          </TouchableOpacity>
        </View>
        <View style={{flex: 1.5}}>
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              justifyContent: 'center',
            }}>
            <TouchableOpacity
              onPress={fbLogin}
              style={{
                height: height / 14.5,
                width: height / 14.5,
                borderRadius: h(14.5),
                backgroundColor: '#FE6F69',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Image
                style={{
                  width: h(2.8),
                  height: h(2.8),
                  resizeMode: 'contain',
                }}
                source={require('../../assets/icon/Facebook-glass.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={googleLogin}
              style={{
                height: height / 14.5,
                width: height / 14.5,
                borderRadius: h(14.5),
                backgroundColor: '#FE6F69',
                alignItems: 'center',
                justifyContent: 'center',
                marginLeft: w(5),
              }}>
              <Image
                style={{
                  width: h(2.8),
                  height: h(2.8),
                  resizeMode: 'contain',
                }}
                source={require('../../assets/icon/google-glass-logo.png')}
              />
            </TouchableOpacity>
            {Platform.OS === 'ios' ? (
              <TouchableOpacity
                onPress={onAppleButtonPress}
                style={{
                  height: height / 14.5,
                  width: height / 14.5,
                  borderRadius: h(14.5),
                  backgroundColor: '#FE6F69',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginLeft: w(5),
                }}>
                <Image
                  style={{
                    width: h(3),
                    height: h(3),
                    resizeMode: 'contain',
                    tintColor: '#fff',
                  }}
                  source={require('../../assets/icon/apple-glass-logo.png')}
                />
              </TouchableOpacity>
            ) : null}
          </View>

          <TouchableOpacity
            onPress={Actions.join}
            style={{
              alignSelf: 'center',
              marginTop: h(8),
            }}>
            <Text
              style={{
                color: '#fa8072',
              }}>
              Join In
            </Text>
          </TouchableOpacity>
          {userInfo.name && (
            <Text
              style={{
                fontSize: 14,
                marginVertical: 14,
                alignSelf: 'center',
                color: '#df396b',
                fontSize: h(1.8),
              }}>
              Logged in As {userInfo.name}
            </Text>
          )}
        </View>
        {loginLoading || socialLoading ? (
          <View
            style={{
              position: 'absolute',
              left: 0,
              right: 0,
              top: 5,
              bottom: 0,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: 'transparent',
            }}>
            <View>
              <ActivityIndicator size="large" color="#000" />
            </View>
          </View>
        ) : null}
        <Modal
          visible={visibleServerModal}
          transparent={true}
          animationType="slide">
          <View
            style={{
              height: '100%',
              width: '100%',
              backgroundColor: '#00000090',
            }}>
            <View style={Styles.modelView}>
              <Text style={Styles.modelTxT}>
                Scheduled maintenance is {'\n'}happening. Like right now. {'\n'}
                We’ll try to make it quick {'\n'}and painless.
              </Text>

              <View style={Styles.modelbuttonContainer}>
                <TouchableOpacity
                  onPress={() => {
                    closeServerModal();
                  }}>
                  <Text style={Styles.modelAndText}>Got It!</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
        <Modal visible={shareVisible} transparent={true} animationType="slide">
          <View
            style={{
              backgroundColor: '#121619',
              alignSelf: 'center',
              height: h(40),
              width: w(80),
              marginTop: h(25),
              borderRadius: w(10),
            }}>
            <Text
              style={{
                color: '#fff',
                textAlign: 'center',

                fontSize: 18,
                marginTop: h(10),
                fontWeight: '400',
              }}>
              This email has signed in using{'\n'}social before. Try logging in{'\n'}
              that way instead.
            </Text>

            <TouchableOpacity
              style={{
                alignSelf: 'center',
                backgroundColor: '#fa8072',
                width: w(50),
                height: h(6),
                borderRadius: w(10),
                marginTop: h(4),
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={closeSocialModel}>
              <Text
                style={{
                  fontSize: 18,

                  color: '#fff',
                }}>
                Got It!
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    </TouchableWithoutFeedback>
  );
};
export default withNavigationFocus(Login);
