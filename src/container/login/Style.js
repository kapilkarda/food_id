import { StyleSheet } from 'react-native';
import { h, w } from '../../utils/Dimensions';

export default StyleSheet.create({
  MainContainer: {
    flex: 1,
    backgroundColor: '#fff'
  },
  txt: {
    alignSelf: 'center',
    fontSize: 30,
    fontWeight: 'bold',
    top: h(13),
    color: '#383B3F'
  },
  Otpcontainer: {
  },
  inputFieldContainer: {
    alignSelf: 'center',
    // borderColor: '#000',
    borderWidth: w(0.28),
    width: w(75),
    height: h(5),
    borderRadius: w(10),
    // top: h(21),
    paddingLeft: w(5)
  },
  inputFieldContainer2: {
    // alignSelf: 'center',
    // borderColor: '#000',
    // borderWidth: w(0.28),
    width: w(60),
    height: h(5),
    borderRadius: w(10),
  },
  passimage: {
    top: h(31),
    // marginRight:w(20)
    right: w(7)
  },
  forgotButton: {
    marginLeft: w(11),
    marginTop: h(0.3)
    // top:h(33),
    // marginRight:h(10),
    // marginTop:h(2),
    // marginTop:h(-3)
  },
  buttonContainer: {
    alignSelf: 'center',
    backgroundColor: '#FE6F69',
    width: w(75),
    height: h(5),
    borderRadius: w(10),
    top: h(7),
    justifyContent: 'center',
    alignItems: 'center'
  },
  AndText: {
    fontSize: 16,
    fontWeight: '500',
    // alignSelf: 'center',
    color: '#fff',
    // marginTop: h(1.2),
  },
  socialButton: {
    flexDirection: 'row',
    top: h(14),
    alignSelf: 'center',
    marginLeft: w(7),
  },
  socialImgButton: {
    // width: w(19),
    // alignSelf: 'center',
  },
  img: {
    width: h(7),
    height: h(7.1),
    resizeMode: 'contain',
  },
  imgApple: {
    width: h(7),
    height: h(7.1),
    resizeMode: 'contain',
    borderRadius: w(10),
  },
  modelView: {
    backgroundColor: '#000',
    alignSelf: 'center',
    height: h(36),
    width: w(83),
    marginTop: h(20),
    borderRadius: w(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  modelTxT: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 16,
    fontWeight: '400',
  },
  modelbuttonContainer: {
    alignSelf: 'center',
    backgroundColor: '#fa8072',
    width: w(60),
    height: h(6),
    borderRadius: w(10),
    marginTop: h(4),
    justifyContent: 'center',
  },
  modelAndText: {
    fontSize: 16,
    alignSelf: 'center',
    color: '#fff',
    fontWeight: '400',
  },
  ButtonJoined: {
    alignSelf: 'center',
    top: h(20),
    marginLeft: w(3)
  },
  txtJoined: {
    color: '#fa8072',
  },
})