import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Keyboard,
  TouchableWithoutFeedback,
  Modal,
  ScrollView,
  ActivityIndicator,
  Alert,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {h, w} from '../utils/Dimensions';
import {useSelector, useDispatch} from 'react-redux';
import {forgetPasswordApi, saveForgotNumber} from './../actions/forgetPassword';
import AsyncStorage from '@react-native-community/async-storage';

let openModal = true;

const ForgetScreen = () => {
  
  const dispatch = useDispatch();

  const [number, setnumber] = useState('');
  const [shareVisible, setShareVisible] = useState(false);
  const [error, seterror] = useState(false);

  const forgetLoading = useSelector((state) => state.forget.isLoading);
  const forgetData = useSelector((state) => state.forget.FORGET);

  useEffect(() => {
    seterror(false);
  }, []);

  useEffect(() => {
    if (forgetData) {
      if (forgetData.status == 'success' && openModal) {
        setShareVisible(true);
        openModal = false;
      } else  if (forgetData.status == 'fail') {
        seterror(true);
        openModal = true;
      }
    } else {
      setShareVisible(false);
      openModal = true;
    }
  }, [forgetData]);

  const openShareModal = async () => {
    if (number.length <= 2) {
      seterror(true);
    } else {
      dispatch(forgetPasswordApi(number));
    }
  };

  const closeModal = async() => {
    // setShareVisible(false);
    console.log('number',number)
    dispatch({type: 'Forget', payload: ''});
    setTimeout(async()=>{
      const numberget = await AsyncStorage.getItem('mobile')
      console.log('numberGet',numberget)
      Actions.OtpLogin(numberget);

    },500)
  };


  const ResendOtp = () => {
    // setShareVisible(false);
    dispatch({type: 'Forget', payload: ''});
    dispatch(forgetPasswordApi(number)); // calling get otp api 
  };

  const LoginBack = () => {
    Actions.Login();
    dispatch({type: 'Forget', payload: ''});
  };

  return (
    <ScrollView
      style={{
        backgroundColor: shareVisible ? '#000' : '#fff',
        opacity: shareVisible ? 0.5 : 1,
      }}>
      <View style={{flex: 1}}>
        <View>
          <TouchableOpacity
            onPress={LoginBack}
            style={{marginTop: h(10), marginLeft: w(10)}}>
            <Image
              style={{width: w(2), height: h(2), resizeMode: 'contain'}}
              source={require('./../assets/icon/BackB.png')}
            />
          </TouchableOpacity>
          <Text
            style={{
              color: '#383B3F',
              fontWeight: 'bold',
              alignSelf: 'center',
              fontSize: 30,
              marginTop: h(5),
            }}>
            Confirm Your #
          </Text>
          <TextInput
            style={{
              alignSelf: 'center',
              borderColor: error ? 'red' : '#000',
              borderWidth: w(0.28),
              width: w(75),
              height: h(5),
              borderRadius: w(10),
              paddingLeft: w(5),
              marginTop: h(9),
            }}
            placeholderTextColor="#000"
            underlineColorAndroid="transparent"
            placeholder="Phone"
            keyboardType='name-phone-pad'
            autoCapitalize="none"
            underlineColorAndroid="transparent"
            onChangeText={(number) => setnumber(number)}
            value={number}
            maxLength={13}
          />
          {error ? (
            <Text
              style={{
                color: '#FE6F69',
                top: h(1.5),
                textAlign: 'center',
                fontSize: 10,
              }}>
              This number hasn’t been here before. Join In instead.
            </Text>
          ) : null}

          <TouchableOpacity
            onPress={openShareModal}
            style={{
              alignSelf: 'center',
              backgroundColor: '#FE6F69',
              width: w(75),
              height: h(5),
              borderRadius: w(10),
              marginTop: h(10),
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: 16,
                fontWeight: '500',
                color: '#FBFAFA',
              }}>
              Send Me A Code
            </Text>
          </TouchableOpacity>
        </View>
        {forgetLoading ? (
          <View
            style={{
              position: 'absolute',
              left: 0,
              right: 0,
              top: 5,
              bottom: 0,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: 'transparent',
            }}>
            <View>
              <ActivityIndicator size="large" color="#000" />
            </View>
          </View>
        ) : null}
        <Modal visible={shareVisible} transparent={true} animationType="slide">
          <TouchableWithoutFeedback
            onPress={() => setShareVisible(!shareVisible)}>
            <View
              style={{
                width: '70%',
                height: '30%',
                backgroundColor: '#000',
                alignSelf: 'center',
                marginTop: h(32),
                borderRadius: 20,
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 1,
                shadowRadius: 5,
                elevation: 5,
              }}>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: h(5.5),
                }}>
                <Text style={{color: '#fff', fontSize: 16}}>
                  Sent you an exclusive,
                </Text>
                <Text style={{color: '#fff', fontSize: 16}}>
                  one-time-only password.
                </Text>
              </View>
              <TouchableOpacity
                style={{
                  alignSelf: 'center',
                  backgroundColor: '#fa8072',
                  width: w(50),
                  height: h(4),
                  borderRadius: w(10),
                  marginTop: h(5),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                onPress={closeModal}>
                <Text
                  style={{
                    fontSize: 16,
                    color: '#FBFAFA',
                  }}>
                  Got It!
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={ResendOtp}>
                <Text
                  style={{
                    color: '#fa8072',
                    alignSelf: 'center',
                    marginTop: h(3),
                    fontSize: 16,
                    fontWeight: '500',
                  }}>
                  Resend
                </Text>
              </TouchableOpacity>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    </ScrollView>
  );
};
export default ForgetScreen;
