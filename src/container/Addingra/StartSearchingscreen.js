import React, { useState } from 'react';
import {
    View,
    Image,
    TextInput,
    FlatList,
    Text,
    TouchableOpacity,
    Alert,
    Modal,
    ImageBackground,
    ScrollView,
    TouchableWithoutFeedback,
    Keyboard,
    Platform,
    KeyboardAvoidingView
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { IconAsset, Strings, UiColor } from '../../theme';
import { h, w } from '../../utils/Dimensions';
//import Styles from './Style';
import { Pages } from 'react-native-pages';
import AsyncStorage from '@react-native-community/async-storage';

import { connect } from 'react-redux';
import Styles from './Style';
const br = `\n`;
const StartSearchingscreen = () => { //Learning Show Screen function
    const learning = () => {

        AsyncStorage.setItem('learningscreen', '1')
        Actions.Addingra()
    }
    return (
        <TouchableWithoutFeedback
            onPress={() => {
                Keyboard.dismiss();
            }}>

            <View style={Styles.MainContainer}>
                <Image style={Styles.yellowimage}
                    source={require('../../assets/icon/DragonFruit_IconDragonFruit_Icon_CuratePage.png')}
                />
                <TouchableOpacity
                style={{left:w(5)}}
                    onPress={Actions.Dashboard} >
                    <Image
                        style={{ width: w(2), height: h(2), resizeMode: "contain",marginTop:h(15) }}
                        source={require('./../../assets/icon/BackB.png')}
                    />
                </TouchableOpacity>

                <Text style={{ marginTop: h(37), alignSelf: 'center', color: '#383B3F', fontSize: 16 }}>Search and add the ingredients </Text>
                <Text style={{ alignSelf: 'center', color: '#383B3F', fontSize: 16 }}>you’re avoiding so when you </Text>
                <Text style={{ alignSelf: 'center', color: '#383B3F', fontSize: 16 }}>scan or search a label, Food ID</Text>
                <Text style={{ alignSelf: 'center', color: '#383B3F', fontSize: 16 }}>can flag anything related to</Text>
                <Text style={{ alignSelf: 'center', color: '#383B3F', fontSize: 16 }}>these curated watchouts.</Text>

                <TouchableOpacity
                    onPress={learning}
                    style={{
                        alignSelf: 'center',
                        backgroundColor: '#FE6F69',
                        width: w(75),
                        height: h(5),
                        borderRadius: w(10),
                        marginTop: h(5)
                    }}>
                    <Text style={{
                        fontSize: 16,
                        alignSelf: 'center',
                        color: '#fff',
                        marginTop: h(1.2)
                    }}>Get After It</Text>
                </TouchableOpacity>
            </View>

        </TouchableWithoutFeedback>
    );
};
export default StartSearchingscreen;
