import {StyleSheet} from 'react-native';
import {h, w} from '../../utils/Dimensions';

export default StyleSheet.create({
  MainContainer: {
    // flex: 1,
    padding :h(2),
    backgroundColor:'#fff',
    height:'100%'
    
  },
  yellowimage: {
    width: w(65),
    height: w(65),
    position: 'absolute',
    resizeMode: 'contain',
    alignSelf: 'center',
    top:h(18)
  },
  DrawerIcon: {
    width: w(6),
    height: w(6),
    marginLeft : w(3),
    // marginTop:h(5)
    top:h(12)
  },
  Searchtxt:
  {
alignSelf:'center',
marginTop:h(-19)

  },
  TouchTxt:
  {
fontSize:36,
fontWeight:'bold',
color :'#383B3F',
marginTop:h(2)
  }
});
