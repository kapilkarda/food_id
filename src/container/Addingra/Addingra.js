import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  Modal,
  ImageBackground,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
  KeyboardAvoidingView,
  BackHandler,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {IconAsset, Strings, UiColor} from '../../theme';
import {h, w} from '../../utils/Dimensions';
//import Styles from './Style';
import {Pages} from 'react-native-pages';
import {connect} from 'react-redux';
import styles from '../Splash/styles';
import Styles from './Style';
import {withNavigationFocus} from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';

const br = `\n`;
const Addingra = ({navigation}) => {
  const screenStatus = navigation.isFocused();

  const [value, setvalue] = useState('');
  const [Buttonsearch, setButtonsearch] = useState(false);
  const [screenChange , setscreenChange] = useState(false)
  const valueset = (value) => {
    console.log('value', value);
    if (value === '') {
      Actions.Search();
    } else {
      Actions.Search(value);
    }
  };
  useEffect(() => {
    setvalue('')
    learningscreen();
  }, [screenStatus]);
  useEffect(() => {
    setButtonsearch(false);
  }, [screenStatus]);
  const learningscreen = async () => {
    const learning = await AsyncStorage.getItem('learningscreen');
    console.log('learning', learning);
    if (learning === '0') {
      setscreenChange(true)
      // Actions.StartSearchingscreen();
    }
  };
  const input = () => { 
    setButtonsearch(true);
  };
  useEffect(() => {
    const backAction = () => {
      Actions.Dashboard();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, [screenStatus]);
  const DraweOpen = () => {
    Keyboard.dismiss();
    Actions.drawerOpen();
    
  }; 
  const learning = () => {
  setscreenChange(false)
    AsyncStorage.setItem('learningscreen', '1')
}
const Nav = () =>
{
  navigation.navigate('Dashboard') 
}
 
console.log('screenChange',screenChange)
  if (screenChange === false)
  {
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}>
      <KeyboardAvoidingView
        style={{flex: 1, backgroundColor: '#ffff'}}
        behavior={Platform.OS === 'ios' ? 'position' : 'position'}
        // padding={10}
        keyboardVerticalOffset={Platform.OS === 'ios' ? -100 : -100}>
        <View style={Styles.MainContainer}>
          <Image
            style={Styles.yellowimage}
            source={require('../../assets/icon/DragonFruit_IconDragonFruit_Icon_CuratePage.png')}
          />
          <TouchableOpacity style={Styles.DrawerIcon} onPress={DraweOpen}>
            <Image
              source={require('../../assets/image/Hamburger_Nav.png')}
              style={{height: h(2.3), width: h(2.3), resizeMode: 'contain'}}
            />
          </TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              left: w(6),
              marginTop: h(40),
            }}>
            <View
              style={{
                width: w(0.5),
                height: Buttonsearch ? '30%' : '60%',
                backgroundColor: '#8c8c8c',
                right: w(5),
                marginTop: Buttonsearch ? h(16.5) : h(12),
              }}></View>
            {Buttonsearch ? (
              <TextInput
                style={[
                  {
                    fontSize: 16,
                    marginTop: h(18),
                    alignSelf: 'center',
                    width: w(55),
                  },
                ]}
                placeholderTextColor="#000"
                underlineColorAndroid="transparent"
                placeholder="Search Ingredients Here..."
                autoCapitalize="none"
                underlineColorAndroid="transparent"
                onChangeText={(value) => setvalue(value)}
                value={value}
                onSubmitEditing={() => valueset(value)}
                autoFocus={true}
              />
            ) : (
              <TouchableOpacity onPress={input} style={{marginTop: h(12)}}>
                <Text
                  style={{fontSize: 36, fontWeight: '500', color: '#383B3F'}}>
                  Search {'\n'}Ingredients {'\n'}Here...
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  ); }
  else{
    return (
      <TouchableWithoutFeedback
          onPress={() => {
              Keyboard.dismiss();
          }}>

          <View style={Styles.MainContainer}>
              <Image style={Styles.yellowimage}
                  source={require('../../assets/icon/DragonFruit_IconDragonFruit_Icon_CuratePage.png')}
              />
              <TouchableOpacity
              style={{left:w(5)}}
                  onPress={Nav} >
                  <Image
                      style={{ width: w(2), height: h(2), resizeMode: "contain",marginTop:h(15) }}
                      source={require('./../../assets/icon/BackB.png')}
                  />
              </TouchableOpacity>

              <Text style={{ marginTop: h(37), alignSelf: 'center', color: '#383B3F', fontSize: 16 }}>Search and add the ingredients </Text>
              <Text style={{ alignSelf: 'center', color: '#383B3F', fontSize: 16 }}>you’re avoiding so when you </Text>
              <Text style={{ alignSelf: 'center', color: '#383B3F', fontSize: 16 }}>scan or search a label, Food ID</Text>
              <Text style={{ alignSelf: 'center', color: '#383B3F', fontSize: 16 }}>can flag anything related to</Text>
              <Text style={{ alignSelf: 'center', color: '#383B3F', fontSize: 16 }}>these curated watchouts.</Text>

              <TouchableOpacity
                  onPress={learning}
                  style={{
                      alignSelf: 'center',
                      backgroundColor: '#FE6F69',
                      width: w(75),
                      height: h(5),
                      borderRadius: w(10),
                      marginTop: h(5)
                  }}>
                  <Text style={{
                      fontSize: 16,
                      alignSelf: 'center',
                      color: '#fff',
                      marginTop: h(1.2)
                  }}>Get After It</Text>
              </TouchableOpacity>
          </View>

      </TouchableWithoutFeedback>
  );
  }
};
export default withNavigationFocus(Addingra);
