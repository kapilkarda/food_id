import {StyleSheet} from 'react-native';
import {h, w} from '../../utils/Dimensions';

export default StyleSheet.create({
  MainContainer: {
    flex: 1,
    backgroundColor:'#fff'
  },
  yellowimage: {
    width: w(75),
    height: h(24),
    position: 'absolute',
    resizeMode: 'stretch',
  },
  milkimage: {
    width:h(25),
    height:h(20),
    alignSelf: 'center',
    top: h(8),
    resizeMode:'contain'

  },
  inputFieldContainer: {
    borderWidth: w(0.28),
  width: w(75),
  height: h(5),
  borderRadius: w(10),
  paddingLeft: w(5),
  borderColor:'#383B3F',
  // marginLeft:w(5),
  alignSelf:'center',
  marginTop:h(18),
  marginBottom:h(5)
   
    
  },

})