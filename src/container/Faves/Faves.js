import React, { useState, useEffect } from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  Modal,
  ImageBackground,
  ScrollView,
  Animated,
  Dimensions,
  StyleSheet,
  BackHandler,
  ActivityIndicator,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { IconAsset, Strings, UiColor } from '../../theme';
import { h, w } from '../../utils/Dimensions';
import Styles from './Style';
import { connect } from 'react-redux';
import { SwipeListView } from 'react-native-swipe-list-view';
import { showFavList } from './../../actions/ShowFavList';
import { useSelector, useDispatch } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { RemoveFavApi } from './../../actions/RemoveFav';
import { TouchableOpacityBase } from 'react-native';
import { Undo } from './../../actions/Undo';
import { Item } from 'native-base';
import Swipeable from 'react-native-gesture-handler/Swipeable';
const br = `\n`;

const foodNameArr = [];
const rowTranslateAnimatedValues = {};

const Faves = () => {

  const dispatch = useDispatch();
  const newFavList = useSelector((state) => state.showfavlist.SHOWFAVLIST);
  const showFavlistLoading = useSelector((state) => state.showfavlist.isLoading);
  const removeFavLoading = useSelector((state) => state.removefav.isLoading);
  const undoFavLoading = useSelector((state) => state.undo.isLoading);

  const getFavList = [];
  if (newFavList) {
    let index = 0;
    if (newFavList)
      for (let item of newFavList) {
        getFavList.push({
          created_at: item.created_at,
          first_name: item.first_name,
          id: item.id,
          is_delete: item.is_delete,
          item_data: item.item_data,
          item_id: item.item_id,
          updated_at: item.updated_at,
          user_id: item.user_id,
          key: `${index}`,
        });
        rowTranslateAnimatedValues[`${index}`] = new Animated.Value(1);
        index = index + 1;
      }
  }

  useEffect(() => {
    // callShowFavListApi();
  }, [newFavList]);

  const [chnage, setchange] = useState(false);
  const [search_value, setsearch_value] = useState('');
  const [uiRender, setuiRender] = useState(false);

  useEffect(() => {
    callShowFavListApi();
  }, []);

  const callShowFavListApi = async (search_value) => {
    const token = await AsyncStorage.getItem('token');
    dispatch(showFavList(token, search_value));
  };

  const ShowcallShowFavListApi = async (search_value) => {
    setsearch_value(search_value);
    const token = await AsyncStorage.getItem('token');
    dispatch(showFavList(token, search_value));
  };

  const removeFav = async (data) => {
    const token = await AsyncStorage.getItem('token');
    const favorite_id = data && data.item && data.item.id;
    dispatch(RemoveFavApi(favorite_id, token, search_value));
    if (chnage == false) {
      setchange(true);
      setTimeout(() => setchange(false), 2000);
    }
  };

  const UndoFood = async () => {
    const favorite_id = await AsyncStorage.getItem('favorite_id');
    const token = await AsyncStorage.getItem('token');
    dispatch(Undo(favorite_id, token, search_value));
    setchange(false);
    // callShowFavListApi()
  };

  let animationIsRunning = false;

  const onSwipeValueChange = async (swipeData) => {
    // console.log('swipeData',swipeData);
    const token = await AsyncStorage.getItem('token');
    const { key, value, id } = swipeData;
    if (value < -Dimensions.get('window').width && !animationIsRunning) {
      animationIsRunning = true;
      Animated.timing(rowTranslateAnimatedValues[key], {
        toValue: 0,
        duration: 100,
      }).start(() => {
        if (chnage == false) {
          const id = getFavList[key].id;
          console.log('id', id);
          setchange(true);
          // dispatch(RemoveFavApi(id, token, search_value));
          setTimeout(() => setchange(false), 2000);
        }
        setuiRender(!uiRender);
        animationIsRunning = false;
      });
    }
  };

  const onSwipeRight = async (swipedItem) => {
    console.log('onSwipeRight', swipedItem);
    const token = await AsyncStorage.getItem('token');
    const id = swipedItem.id;
    dispatch(RemoveFavApi(id, token, search_value));
    if (chnage === false) {
      setchange(true);
      setTimeout(() => setchange(false), 2000);
    }
  };

  const RightActions = (progress, dragX, item) => {
    const scale = dragX.interpolate({
      inputRange: [-100, 0],
      outputRange: [0.3, 0],
    });
    return (
      <>
        <Animated.View
          style={{
            backgroundColor: '#FCBE2E',
            height: '100%',
            transform: [{ translateX: scale }],
          }}>
          <Image
            style={{
              height: h(2),
              width: h(2),
              right: w(5),
              marginLeft: w(105),
              tintColor: '#fff',
              marginTop: h(2.4),
            }}
            source={require('./../../assets/icon/dustbin.png')}
          />
        </Animated.View>
      </>
    );
  };
  useEffect(() => {
    const backAction = () => {
      console.log('Running')
      Actions.Dashboard();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  return (
    <View style={Styles.MainContainer}>
      <Image
        style={Styles.yellowimage}
        source={require('../../assets/icon/favlistyellow.png')}
      />
      <Image
        style={Styles.milkimage}
        source={require('../../assets/icon/crabss.png')}
      />
      <View style={Styles.txtInput}>
        <TextInput
          style={Styles.inputFieldContainer}
          underlineColorAndroid="transparent"
          placeholderTextColor="#383B3F"
          placeholder="Search Fave Stuff"
          autoCapitalize="none"
          //keyboardType=""
          underlineColorAndroid="transparent"
          onChangeText={(search_value) => ShowcallShowFavListApi(search_value)}
          value={search_value}
        />
      </View>
    
      <FlatList
        style={{marginBottom:h(7)}}
        data={getFavList}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => (
          <Swipeable
            overshootRight={false}
            onSwipeableRightWillOpen={() => {
              onSwipeRight(item);
            }}
            // renderLeftActions={LeftActions}
            renderRightActions={(progress, dragX) =>
              RightActions(progress, dragX, item)
            }>
            <View
              style={[
                styles.rowFrontContainer,
                {
                  backgroundColor: '#5283c8',
                  height: h(7),
                  borderWidth: w(0.1),
                  borderColor: '#fff',
                  borderTopRightRadius: h(10),
                  borderBottomRightRadius: h(10),
                  width: '110%'
                },
              ]}>
              <View
                style={{
                  height: h(3),
                  width: h(3),
                  backgroundColor: '#fff',
                  borderRadius: h(2),
                  top: h(2),
                  marginLeft: w(5),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: '#000',
                    alignSelf: 'center',
                    fontWeight: '500',
                    fontSize: 16,
                  }}>
                  {item.first_name}
                </Text>
              </View>
              <Text
                style={{
                  fontSize: 16,
                  marginLeft: w(14),
                  fontSize: 16,
                  marginTop: h(-0.6),
                  color: '#fff',
                }}>
                {item.item_data.product_name}
              </Text>
            </View>
          </Swipeable>
        )}
        ItemSeparatorComponent={() => <View style={styles.itemSeparator} />}
      />

      {chnage ? (
        <View
          style={{
            alignSelf: 'center',
            backgroundColor: '#000',
            width: h(15),
            height: h(3),
            borderRadius: w(10),
            marginLeft: w(77),
            justifyContent: 'center',
            alignItems: 'center',
            bottom: h(4),
          }}>
          <TouchableOpacity onPress={UndoFood}>
            <Text
              style={{
                fontSize: 14,
                alignSelf: 'center',
                color: '#F5F4F4',
                fontWeight: '600',
                marginRight: w(2),
              }}>
              Undo{' '}
            </Text>
          </TouchableOpacity>
        </View>
      ) : null}
      {
        (showFavlistLoading || removeFavLoading || undoFavLoading) ?
          <View
            style={{
              position: 'absolute', left: 0, right: 0, top: 5, bottom: 0, alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: "transparent"
            }}
          >
            <View>
              <ActivityIndicator size="large" color="#000" />
            </View>
          </View> : null
      }
    </View>
  );
};
export default Faves;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
  },
  backTextWhite: {
    color: '#FFF',
  },
  rowFront: {
    alignItems: 'center',
    backgroundColor: '#CCC',
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    justifyContent: 'center',
    height: 50,
  },
  rowBack: {
    alignItems: 'center',
    backgroundColor: 'red',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 15,
  },
  backRightBtn: {
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    width: 75,
  },
  backRightBtnRight: {
    backgroundColor: 'red',
    right: 0,
  },
});
