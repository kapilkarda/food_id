import { StyleSheet } from 'react-native';
import { h, w } from '../../utils/Dimensions';

export default StyleSheet.create({
  container: {
   
  },
  splashImg: {
    height: h(100),
    width: h(100),
    alignSelf: 'center',
  },
  text :{
    fontSize:50,
    alignSelf:'center',
    fontWeight:'bold'


  },
  Splash:
  {
    width:h(36.2),
    height:h(7.8),
    alignSelf:'center', 
    marginTop:h(50),
  
    tintColor:'#FE6F69'
  },
  Splashicon:
  {
   
  }
});
