import React from 'react';
import { View, Animated, Image } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { getServerStatus } from '../../actions/ServerMaintain';

class Splash extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      fadeValue: new Animated.Value(0),
      colorchnage: false,
      bottomcolor: false,
    }
  }

  componentWillMount() {
    try {
      setTimeout(async () => {
        const toggle = await AsyncStorage.getItem('toggle');
        console.log('toggle',toggle)
        const login_status = await AsyncStorage.getItem('loginStatus');
        if (toggle == true || toggle == 'true') {
          Actions.tabbar()
        }
        else if (toggle === false || toggle === 'false') {
          Actions.Login()
        }
        else {
          Actions.join()
        }
      }, 6000);
      this.props.getServerStatus();
    } catch (error) {
      console.log('error' + error);
    }
  }

  startAnimation = () => {
    Animated.timing(this.state.fadeValue, {
      toValue: 1,
      duration: 2000,
    }).start();
  };

  render() {

    return (
      <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
        <Image
          style={{ alignSelf: 'center', width: '100%' }}
          source={require("../../assets/image/foodid22.gif")}
        />
      </View>
    );
  }

}

Splash.propTypes = {
};

const mapStateToProps = (state) => {
  return {
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getServerStatus: () => dispatch(getServerStatus()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Splash);