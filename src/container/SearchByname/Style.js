import { StyleSheet } from 'react-native';
import { h, height, w } from '../../utils/Dimensions';

export default StyleSheet.create({
  MainContainer: {
    flex: 1,
  },
  txt: {
    alignSelf: 'center',
    fontSize: 30,
    fontWeight: 'bold',
    top: h(17.5),
    color: '#383B3F',
    // fontFamily:'Roboto'
  },
  BlueImage:
  {
    width: h(30),
    height: h(28),
    alignSelf: 'center',
    marginTop: h(18),
    // resizeMode:'contain'
    // marginLeft:w(3)
  },
  MilkIkmage:
  {
    height:h(22),
    width:h(22),
    alignSelf: 'center',
    marginTop: h(4),
  }

});
