import React, {useState, useEffect, useCallback} from 'react';
import {Alert} from 'react-native';
import {ImageBackground} from 'react-native';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Keyboard,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Linking,
  Modal,
  ActivityIndicator,
  BackHandler,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {IconAsset, Strings, UiColor} from '../../theme';
import {h, w} from '../../utils/Dimensions';
import Styles from './Style';
import {SearchBynameApi} from './../../actions/SearchByName';
import AsyncStorage from '@react-native-community/async-storage';
import {useSelector, useDispatch} from 'react-redux';
import {SuggestionBynamesuggestionApi} from './../../actions/WordSuggestionsSearchByName';
import {GetbarcodeDataApi} from './../../actions/GetBarcodeData';
import {ScrollViewComponent} from 'react-native';
import {UserDetailApi} from '../../actions/USerProfile';

let openModal = true;

const SearchByname = () => {
  const dispatch = useDispatch();

  // const Scanner = useSelector((state) =>
  //   console.log('serachByNameSuggestionState', state),
  // );
  const memoizedHandleClick = useCallback(
    () => {
      suggestion();
    },
    [], // Tells React to memoize regardless of arguments.
  );

  const SearchByNameData = useSelector(
    (state) => state.searchByname.SEARCHBYNAME,
  );

  const SearchByNameSuggestionData = useSelector((state) =>
    state.searchByNameSuggestion.SEARCHBYNAMESUGGESTION
      ? state.searchByNameSuggestion.SEARCHBYNAMESUGGESTION.data
      : null,
  );

  const barCodeData = useSelector((state) => state.barcodeData.BARCODEDATAS);
  // console.log('barCodeData', barCodeData);
  const status = useSelector((state) =>
    state.searchByNameSuggestion.SEARCHBYNAMESUGGESTION
      ? state.searchByNameSuggestion.SEARCHBYNAMESUGGESTION.status
      : false,
  );

  const getUserDetail = useSelector((state) => state.userdetail.USERDETAIL);
  const isBarCodeDataLoading = useSelector(
    (state) => state.barcodeData.isLoading,
  );

  const searchByNameSuggestionLoading = useSelector(
    (state) => state.searchByNameSuggestion.isLoading,
  );
  const username = getUserDetail ? getUserDetail.name : '';

  const [foodName, setfoodName] = useState('');
  let [uiRender, setUiRender] = useState(false);
  const [show, setshow] = useState(false);
  const [Buttonsearch, setButtonsearch] = useState(false);
  const [shareVisible, setShareVisible] = useState(false);
  const [ModelVariable, setModelVariable] = useState('');
  const [tooltipShow, settooltipShow] = useState(false);
  const [typing, settyping] = useState(false);
  const [typingTimeout, settypingTimeout] = useState(0);

  const searchByName = async (item) => {
    const token = await AsyncStorage.getItem('token');
    const barcode_value = item.barcode;
    dispatch(GetbarcodeDataApi(token, barcode_value, 'search'));
  };

  useEffect(() => {
    if (barCodeData && openModal) {
      openShareModal();
      openModal = false;
    }
  }, [barCodeData]);

  useEffect(() => {
    dispatch({type: 'Suggestion_By_Name', payload: null});
    userDetails();
  }, []);

  useEffect(() => {
    checkmodelStatus();
  }, []);

  const suggestion = async (text) => {
    setfoodName(text);
    const token = await AsyncStorage.getItem('token');
    SuggestionApiCalling(text);
    setshow(true);
  };

  const SuggestionApiCalling = async (text) => {
    clearTimeout(typingTimeout);
    settyping(false);
    if (text.length > 2) {
      settypingTimeout(
        setTimeout(async () => {
          const token = await AsyncStorage.getItem('token');

          dispatch(SuggestionBynamesuggestionApi(token, text));
        }, 1000),
      );
    } else {
      dispatch({type: 'Suggestion_By_Name', payload: ''});
    }
  };
  const userDetails = async () => {
    let token = await AsyncStorage.getItem('token');
    dispatch(UserDetailApi(token));
  };

  const input = () => {
    setButtonsearch(true);
  };

  const openShareModal = async () => {
    if (ModelVariable === '1') {
      setShareVisible(false);
    } else {
      setShareVisible(true);
    }
  };

  const closeModal = () => {
    if (
      ModelVariable === '0' ||
      ModelVariable === null ||
      ModelVariable === undefined
    ) {
      if (barCodeData) {
        setShareVisible(false);
        Actions.ScannedScreen(barCodeData);
      } else {
        setShareVisible(false);
      }
    }
    setTimeout(() => {
      dispatch({type: 'BarcodeData', payload: ''});
      openModal = true;
    }, 500);
  };

  const modelClose = () => {
    if (
      ModelVariable === '0' ||
      ModelVariable === null ||
      ModelVariable === undefined
    ) {
      if (barCodeData) {
        Actions.ScannedScreen(barCodeData);
      }
    }
    AsyncStorage.setItem('Model', '1');
    setShareVisible(false);
    dispatch({type: 'BarcodeData', payload: ''});
  };

  const checkmodelStatus = async () => {
    const modelCheck = await AsyncStorage.getItem('Model');
    setModelVariable(modelCheck);
  };

  useEffect(() => {
    const backAction = () => {
      Actions.Dashboard();
      return true;
    };
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );
    return () => backHandler.remove();
  }, []);

  const OpenTooltip = () => {
    settooltipShow(true);
  };

  const CloseToolTip = () => {
    settooltipShow(false);
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: shareVisible || tooltipShow ? '#000' : '#fff',
        opacity: shareVisible || tooltipShow ? 0.5 : 1,
      }}>
      <TouchableWithoutFeedback
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <KeyboardAvoidingView
          style={{flex: 1}}
          behavior={Platform.OS === 'ios' ? 'position' : 'position'}
          // padding={10}
          keyboardVerticalOffset={Platform.OS === 'ios' ? 50 : 50}>
          <TouchableOpacity onPress={Actions.Dashboard} style={{top: h(18)}}>
            <Image
              style={{
                width: w(2),
                height: h(2),
                resizeMode: 'contain',
                marginLeft: w(10),
              }}
              source={require('./../../assets/icon/BackB.png')}
            />
          </TouchableOpacity>
          <ImageBackground
            imageStyle={{resizeMode: 'contain'}}
            style={Styles.BlueImage}
            source={require('./../../assets/icon/SearchBynameNew.png')}>
            <Image
              style={Styles.MilkIkmage}
              source={require('./../../assets/image/MilkDashboard.png')}
            />
          </ImageBackground>
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              left: w(6),
              marginTop: h(-6),
            }}>
            {Buttonsearch ? (
              <TouchableOpacity
                onPress={OpenTooltip}
                style={{
                  marginTop: Platform.OS === 'ios' ? h(17) : h(18.2),
                  marginRight: w(8),
                  top: h(1.3),
                  right: w(2),
                }}>
                <Image
                  style={{width: h(2), height: h(2), resizeMode: 'contain'}}
                  source={require('./../../assets/image/Tooltik.png')}
                />
              </TouchableOpacity>
            ) : null}
            <View
              style={{
                width: w(0.5),
                height: Buttonsearch ? '25%' : '40%',
                backgroundColor: '#8c8c8c',
                // marginLeft: w(21.5),
                right: w(5),
                marginTop: Buttonsearch ? h(17) : h(15),
              }}></View>
            {Buttonsearch ? (
              <TextInput
                // multiline={show ? fals0e : true}
                style={[
                  {
                    fontSize: 18,
                    marginTop: h(18),
                    alignSelf: 'center',
                    width: w(60),
                    fontWeight: '500',
                    marginBottom: Platform.OS === 'android' ? h(5) : null,
                  },
                ]}
                placeholderTextColor="#000"
                placeholder="Type Product Name Here..."
                autoCapitalize="none"
                underlineColorAndroid="transparent"
                onChangeText={(text) => suggestion(text)}
                value={foodName}
                autoFocus={true}
                // onSubmitEditing = {()=>suggestion()}
              />
            ) : (
              <TouchableOpacity onPress={input} style={{marginTop: h(15)}}>
                <Text
                  style={{fontSize: 30, fontWeight: '500', color: '#383B3F'}}>
                  Type Product {'\n'}Name Here...
                </Text>
              </TouchableOpacity>
            )}
          </View>
          {status == 'fail' && foodName.length > 2 ? (
            <View>
              <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                <TouchableOpacity
                  style={{alignSelf: 'center', top: h(3)}}
                  onPress={() =>
                    Linking.openURL(
                      'mailto:service@foodid.biz?subject= ' +
                        username +
                        ' has something to say',
                      '&body=Description',
                    )
                  }
                  title="support@example.com">
                  <Text
                    style={{
                      color: '#FE6F69',
                      fontSize: 10,
                      alignSelf: 'center',
                    }}>
                    This product can’t be found. If you think that’s{'\n'}
                    bogus, ask us to add it by heading over{' '}
                    <Text
                      style={{
                        color: '#FE6F69',
                        textDecorationLine: 'underline',
                      }}>
                      here
                    </Text>
                    .
                  </Text>
                </TouchableOpacity>
              </View>
              {/* <View
                style={{
                  width: '5%',
                  backgroundColor: '#FE6F69',
                  height: h(0.2),
                  top: h(6),
                  alignSelf: 'flex-end',
                  right: Platform.OS === 'android' ? w(25.5) : w(24.5),
                }}></View> */}
            </View>
          ) : null}
          {status == 'success' && foodName.length > 0 ? (
            <View
              style={{
                backgroundColor: '#ffff',
                height: h(22),
                marginTop: h(7),
              }}>
              <FlatList
                style={{}}
                data={SearchByNameSuggestionData}
                // horizontal={true}
                renderItem={({item, index}) => (
                  <TouchableOpacity
                    onPress={() => searchByName(item)}
                    style={{
                      backgroundColor: '#5283C8',
                      width: '100%',
                      height: h(8),
                      marginTop: h(0.25),
                      justifyContent: 'center',
                    }}>
                    <Text
                      style={{color: '#fff', fontSize: 16, marginLeft: w(20)}}>
                      {item.name}
                    </Text>
                  </TouchableOpacity>
                )}
              />
              {Platform.OS === 'android' ? (
                <View style={{height: h(4), width: h(4)}}></View>
              ) : null}
              {/* <View style = {{}}></View> */}
            </View>
          ) : null}

          <Modal visible={isBarCodeDataLoading} transparent={true}>
            <View
              style={{
                alignSelf: 'center',
                height: h(40),
                width: w(80),
                marginTop: h(50),
              }}>
              <ActivityIndicator
                visible={isBarCodeDataLoading}
                size="large"
                color="#000"
              />
            </View>
          </Modal>
          <Modal visible={shareVisible} transparent={true}>
            <View
              style={{
                backgroundColor: '#000',
                alignSelf: 'center',
                height: h(50),
                width: h(45),
                marginTop: h(23),
                borderRadius: 20,
              }}>
              <Text
                style={{
                  color: '#fff',
                  textAlign: 'center',
                  // backgroundColor:'red',
                  fontSize: 16,
                  marginTop: h(10),
                  fontWeight: '400',
                  marginLeft: w(5),
                  marginRight: w(5),
                }}>
                Just a reminder that you should {'\n'} eat at your own
                discretion. We{'\n'}
                do our best to give the most {'\n'} accurate information but{' '}
                {'\n'}nothing is guaranteed in life {'\n'}except death and
                taxes.
              </Text>
              <View
                style={{
                  alignSelf: 'center',
                  backgroundColor: '#fa8072',
                  width: w(50),
                  height: h(6),
                  borderRadius: w(10),
                  marginTop: h(4),
                }}>
                <TouchableOpacity onPress={closeModal}>
                  <Text
                    style={{
                      fontSize: 18,
                      textAlign: 'center',
                      // alignSelf: 'center',
                      color: '#fff',
                      marginTop: h(1.6),
                    }}>
                    Got It!
                  </Text>
                </TouchableOpacity>
              </View>
              <TouchableOpacity onPress={modelClose}>
                <Text
                  style={{
                    color: '#fa8072',
                    alignSelf: 'center',
                    marginTop: h(5),
                    fontSize: 18,
                  }}>
                  Don’t Show Again
                </Text>
              </TouchableOpacity>
            </View>
            {/* </View> */}
          </Modal>
          <Modal visible={tooltipShow} transparent={true}>
            <View
              style={{
                backgroundColor: '#000',
                alignSelf: 'center',
                height: h(35),
                width: h(35),
                marginTop: h(23),
                borderRadius: 20,
              }}>
              <Text
                style={{
                  color: '#fff',
                  textAlign: 'center',
                  // backgroundColor:'red',
                  fontSize: 16,
                  marginTop: h(10),
                  fontWeight: '400',
                  marginLeft: w(5),
                  marginRight: w(5),
                }}>
                Type three or more characters{'\n'}to get our system searching.
              </Text>
              <View
                style={{
                  alignSelf: 'center',
                  backgroundColor: '#fa8072',
                  width: w(50),
                  height: h(6),
                  borderRadius: w(10),
                  marginTop: h(4),
                }}>
                <TouchableOpacity onPress={CloseToolTip}>
                  <Text
                    style={{
                      fontSize: 18,
                      textAlign: 'center',
                      // alignSelf: 'center',
                      color: '#fff',
                      marginTop: h(1.6),
                    }}>
                    Got It!
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            {/* </View> */}
          </Modal>
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
      {searchByNameSuggestionLoading ? (
        <View
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 5,
            bottom: 0,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'transparent',
          }}>
          <View>
            <ActivityIndicator size="large" color="#000" />
          </View>
        </View>
      ) : null}
      {/* {Platform.OS === 'android' ?
      <View style = {{height:10,width:10}}></View>:null} */}
    </View>
  );
};
export default SearchByname;
