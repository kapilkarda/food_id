import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  BackHandler,
  TouchableWithoutFeedback,
  Keyboard,
  ActivityIndicator,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {IconAsset, Strings, UiColor} from '../../theme';
import {h, w} from '../../utils/Dimensions';
import Styles from './Style';
import {Pages} from 'react-native-pages';
import {connect} from 'react-redux';
import Style from './Style';
import {useSelector, useDispatch} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {forgetPasswordApi} from './../../actions/forgetPassword';
import OtpInputs from 'react-native-otp-inputs';
import {UserRagistrationAPI} from './../../actions/UserRagistration';
import {OtpApi} from '../../actions/Otp';
import {checkOtpsignUpApi} from './../../actions/CheckOtpSignUp';

const Otp = ({navigation, ...data2}) => {
  const dispatch = useDispatch();

  const otpLoading = useSelector((state) => state.otp.isLoading);
  const registerLoading = useSelector((state) => state.ragistration.isLoading);
  const otpCorrect = useSelector((state) =>
    state.userOtpChecksignup.otpCheckSignUp
      ? state.userOtpChecksignup.otpCheckSignUp.status
      : null,
  );
  console.log('otpCorrect', otpCorrect);
  const [otp, setotp] = useState('');
  const [seconds, setSeconds] = React.useState(45);
  const [otpGet, setotpGet] = useState('');
  const [otpWrong, setotpWrong] = useState(false);
  const [wrongOtpCheck, setwrongOtpCheck] = useState(false);

  console.log('data2', data2);
  const email = data2.email;
  const mobile = data2.mobile;
  console.log('mobile', mobile);
  const password = data2.password;
  const confirmPassword = data2.password2;
  const name = data2.UserName;

  const realNumber = mobile.replace(/.(?=.{4,}$)/g, '*'); //replace mobile number into star
  // const contactNumber = realNumber.slice(3);

  useEffect(() => {
    //calling function for expiring timer
    if (seconds > 0) {
      setTimeout(() => setSeconds(seconds - 1), 1000);
    } else {
      setSeconds(0);
    }
  });

  useEffect(() => {
    //Check otp is correct or not
    if (otpCorrect) {
      if (otpCorrect === 'fail') {
        setwrongOtpCheck(true);
      } else {
        dispatch(
          UserRagistrationAPI(
            email,
            mobile,
            password,
            confirmPassword,
            name,
            navigation,
          ), //Calling function for user ragistration
        );
      }
    }
  }, [otpCorrect]);
  const userRagistration = async () => {
    dispatch(checkOtpsignUpApi(mobile, otp)); //calling api for check otp
  };

  const resendOtpApi = () => {
    // Resend otp function
    const number = data2.mobile;
    dispatch(OtpApi(email, mobile, password, confirmPassword, name));
  };
  useEffect(() => {
    const backAction = () => {
      Actions.join();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  //Return otp Ui
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}>
      <View style={Styles.MainContainer}>
        <TouchableOpacity
          onPress={Actions.join3}
          style={{left: w(7), top: h(6)}}>
          <Image
            style={{width: w(2), height: h(2), resizeMode: 'contain'}}
            source={require('./../../assets/icon/BackB.png')}
          />
        </TouchableOpacity>
        <Text style={Styles.txt}>Making Sure You’re Real</Text>
        <Text style={Styles.Txt}>Code sent at lightning speed to</Text>
        <Text style={Styles.ContactTxt}>{realNumber}</Text>
        <View style={Styles.OtpView}>
          <OtpInputs
            numberOfInputs={4}
            handleChange={(otp) => setotp(otp)}
            inputStyles={{
              height: h(6.5),
              width: h(6.5),
              borderRadius: h(20),
              borderWidth: 1,
              borderColor: wrongOtpCheck ? 'red' : ' #000',
              top: h(16),
              textAlign: 'center',
              alignSelf: 'center',
            }}
          />
        </View>
        <View>
          <TouchableOpacity
            disabled={otp == ''}
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'center',
              backgroundColor: '#FE6F69',
              width: w(75),
              height: h(5),
              borderRadius: w(10),
              top: h(21),
              opacity: otp == '' ? 0.6 : 1,
            }}
            onPress={userRagistration}>
            <Text style={Styles.AndText}>Thank You, Next</Text>
          </TouchableOpacity>
        </View>

        <Text
          style={{
            top: h(24),
            alignSelf: 'center',
            fontSize: 16,
            color: '#8c8c8c',
          }}>
          0:{seconds < 10 ? '0' + seconds : seconds} till code expires
        </Text>
        <TouchableOpacity
          disabled={seconds != 0 ? true : false}
          onPress={() => resendOtpApi()}
          style={[Styles.resendButton, {opacity: seconds != 0 ? 0.6 : 1}]}>
          <Text style={Styles.resendText}>Resend the Code Now</Text>
        </TouchableOpacity>
        {otpLoading || registerLoading ? (
          <View
            style={{
              position: 'absolute',
              left: 0,
              right: 0,
              top: 5,
              bottom: 0,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: 'transparent',
            }}>
            <View>
              <ActivityIndicator size="large" color="#000" />
            </View>
          </View>
        ) : null}
      </View>
    </TouchableWithoutFeedback>
  );
};
export default Otp;
