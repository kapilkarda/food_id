import React, {useState, useEffect, useRef, useCallback} from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {IconAsset, Strings, UiColor} from '../../theme';
import {h, w} from '../../utils/Dimensions';
import Styles from './Style';
import {Pages} from 'react-native-pages';
import {connect} from 'react-redux';
import Style from './Style';
import {useSelector, useDispatch} from 'react-redux';

import OtpInputs, {OtpInputsRef} from 'react-native-otp-inputs';
import {UserRagistrationAPI} from './../../actions/UserRagistration';
import AsyncStorage from '@react-native-community/async-storage';
import {forgetPasswordApi} from '../../actions/forgetPassword';
import {checkOtpApi} from './../../actions/CheckOtp';
import {resendOtpApi} from './../../actions/ResendOtp';

const OtpLogin = ({...props}) => {
  const dispatch = useDispatch();
  const [seconds, setSeconds] = React.useState(45);
  const otpRef = useRef(null);
  // console.log('prop',props)
  const showNumber = props.data;
  var realNumber = '';
  if (showNumber) {
    const realNumber1 = showNumber.split('+');
    var str = realNumber1[1];
    var res = str.charAt(0);
    if (res == '9') {
      realNumber = showNumber.slice(3);
    } else if (res == '1') {
      realNumber = showNumber.slice(2);
    }
  }
  const contactNumber = realNumber.replace(/\d(?=\d{4})/g, '*');
  // console.log('contactNumber', contactNumber);
  const [otp, setotp] = useState('');
  const [wrongOtpCheck, setwrongOtpCheck] = useState(false);

  const forgetLoading = useSelector((state) => state.forget.isLoading);
  const resentLoading = useSelector((state) => state.resent.isLoading);
  const checkUserOtpLoading = useSelector(
    (state) => state.checkUserOtp.isLoading,
  );
  const otpCorrect = useSelector((state) =>
    state.checkUserOtp.otpCheck ? state.checkUserOtp.otpCheck.status : null,
  );


  useEffect(() => {
    if (otpCorrect) {
      if (otpCorrect === 'fail') {
        setwrongOtpCheck(true);
      }
    }
  }, [otpCorrect]);

  useEffect(() => {
    if (seconds > 0) {
      setTimeout(() => setSeconds(seconds - 1), 1000);
    } else {
      setSeconds(0);
    }
  });

  const resetOTP = useCallback(() => {
    otpRef.current.reset();
  }, []);

  const OtpCheck = async () => {
    const number = props.data;
    dispatch(checkOtpApi(number, otp, 'passwordChange'));
    
  };

  const sendresendOtpApi = () => {
    dispatch({type: 'Check_Otp', payload: ''});
    setwrongOtpCheck(false);
    resetOTP();
    const number = props.data;
    console.log('number', number);
    dispatch(resendOtpApi(number));
    setSeconds(45);
    // console.log('secondsssss', seconds);
  };

  const goBack = () => {
    Actions.ForgetScreen();
    setotp('');
    dispatch({type: 'Check_Otp', payload: ''});
  };


  return (
    <View style={Styles.MainContainer}>
      <TouchableOpacity
        onPress={() => {
          goBack();
        }}
        style={{marginTop: h(10), marginLeft: w(8)}}>
        <Image
          style={{width: w(2), height: h(2), resizeMode: 'contain'}}
          source={require('./../../assets/icon/BackB.png')}
        />
      </TouchableOpacity>
      <Text style={Styles.txt}>Making Sure You’re Real</Text>
      <Text style={Styles.Txt}>Code sent at lightning speed to</Text>
      <Text style={Styles.Txt}>{contactNumber}</Text>
      <View style={Styles.OtpView}>
        <OtpInputs
          ref={otpRef}
          numberOfInputs={4}
          handleChange={(otp) => setotp(otp)}
          inputStyles={{
            height: h(6.5),
            width: h(6.5),
            borderRadius: h(20),
            borderColor: wrongOtpCheck ? 'red' : '#8C8C8C',
            borderWidth: 1,
            top: h(16),
            textAlign: 'center',
            alignSelf: 'center',
            backgroundColor: '#fff',
          }}
          
        />
      </View>
      <View>
        <TouchableOpacity
          disabled={otp == ''}
          style={{
            alignSelf: 'center',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#FE6F69',
            width: w(75),
            height: h(5),
            borderRadius: w(10),
            top: h(22),
            opacity: otp == '' ? 0.6 : 1,
          }}
          onPress={OtpCheck}>
          <Text style={Styles.AndText}>Thank You, Next</Text>
        </TouchableOpacity>
      </View>

      <Text
        style={{
          top: h(24),
          alignSelf: 'center',
          fontSize: 16,
          color: '#8c8c8c',
        }}>
        0:{seconds < 10 ? '0' + seconds : seconds} till code expires
      </Text>
      
      <TouchableOpacity
        disabled={seconds != 0 ? true : false}
        onPress={() => sendresendOtpApi()}
        style={[Styles.resendButton, {opacity: seconds != 0 ? 0.6 : 1}]}>
        <Text style={Styles.resendText}>Resend the Code Now</Text>
      </TouchableOpacity>
      {forgetLoading || resentLoading || checkUserOtpLoading ? (
        <View
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 5,
            bottom: 0,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'transparent',
          }}>
          <View>
            <ActivityIndicator size="large" color="#000" />
          </View>
        </View>
      ) : null}
    </View>
  );
};

export default OtpLogin;
