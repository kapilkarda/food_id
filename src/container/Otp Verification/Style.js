import { StyleSheet } from 'react-native';
import { h, w } from '../../utils/Dimensions';

export default StyleSheet.create({
  MainContainer: {
    flex: 1,
    backgroundColor:'#fff',
    // marginTop:h(10)
  },
  txt: {
    alignSelf: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#383B3F',
    top: h(5)

  },
  OtpInput: {
    // height: h(6.5),
    // width: h(6.5),
    // borderRadius: h(20),
    // // backgroundColor: 'red',
    // // borderColor: '#8C8C8C',
    // borderWidth: 1,
    // top: h(16),
    // textAlign: 'center',
    // alignSelf: 'center',
    // backgroundColor:wrongOtp ? '#8c8c8c' :'#fff' 
  },
  OtpView:
  {
    flexDirection: 'row',
    paddingLeft: w(18),
    paddingRight: w(18)
  },
  Txt:
  {
    alignSelf: 'center',
    fontSize: 16,
    color: '#8c8c8c',
    top: h(10)
  },
  ContactTxt:
  {
    alignSelf: 'center',
    fontSize: 16,
    color: '#8c8c8c',
    top: h(12)
  },
  buttonContainer: {
    alignSelf: 'center',
    backgroundColor: '#FE6F69',
    width: w(75),
    height: h(5),
    borderRadius: w(10),
    top: h(34),
  },
  AndText: {
    fontSize: 16,
    alignSelf: 'center',
    color: '#fff',
    // marginTop: h(1.2),
  },
  resendButton:{
  //   height:h(5),
  //   width:w(75),
  //   backgroundColor:'#FE6F69',
  //   borderRadius:20,
    marginTop:h(30),
    alignSelf:'center',
    justifyContent:'center',
    alignItems:'center'
  },
  resendText:{
    color:'#FE6F69',
    fontSize:13
  }
});
