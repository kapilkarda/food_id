import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  TouchableOpacityBase,
  BackHandler,
  Alert
} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import React from 'react';
import styles from './Styles';
import {h, w} from '../../utils/Dimensions';
import {Actions} from 'react-native-router-flux';
import { ImageBackground } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';


const slides = [
  {
    key: 1,
    title: 'Track Any and All ',
    title1: 'Ingredient Watchouts',
    text: 'Think sugar is evil? Gluten give  ',
    text1: 'you the creeps? Want to avoid',
    text2: 'dairy? Add these ingredients to',
    text3: 'your watchouts!',
    image: require('../../assets/image/gif1icon.png'),
    // imageBackground : require('./../../assets/image/Vector.png'),
    backgroundColor: '#59b2ab',
  },
  {
    key: 2,
    title: 'Scan or Search to Check',
    title1: 'for Ingredient Watchouts',
    text: 'Packaged goods are sneaky!',
    text1: 'Food ID flags ingredients ',
    text2: 'related to your watchouts when',
    text3: 'you search packaged foods or',
    text4:'scan barcodes—you ingredient',
    text5 :'finding genius, you.', 
    image: require('../../assets/image/gif3icon.png'),
    // imageBackground : require('./../../assets/image/Vector.png'),

    backgroundColor: '#febe29',
  },
  {
    key: 3,
    title: 'Learn How Different',
    title1: 'Ingredients Make You Feel',
    text: 'Dear-diary your diet. Log what',
    text1: 'you’re eating and how you feel.',
    text2: 'Learn more about yourself.',
    text3: 'Inevitably feel better. Take over',
    text4: 'the free world.',
    image: require('../../assets/image/gif2icon.png'),
    // imageBackground : require('./../../assets/image/Vector.png'),

    backgroundColor: '#22bcb5',
  }, 
];

export default class Instruction extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showRealApp: false,
      expId:'',
    };
    
  }

  getExpiredId = async () =>{
    const getExpID = await AsyncStorage.getItem('is_expired')
    if(getExpID){
      this.setState({
        expId: getExpID
      })
    }
  }

  componentWillMount() {
    this.getExpiredId();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  async handleBackButtonClick() {
  Alert.alert("Hold on!", "Are you sure you want to logout", [
    {
      text: "Cancel",
      onPress: () => null,
      style: "cancel"
    },
    { text: "YES", onPress: async () => {
      
      const Token = await AsyncStorage.getItem('token');
      console.log('Token', Token);
      const imageset = await AsyncStorage.getItem('res.path');
      console.log('imageset', imageset);
      AsyncStorage.removeItem('res.path');
      AsyncStorage.removeItem('token');
      AsyncStorage.setItem('toggle', JSON.stringify(false));
      AsyncStorage.setItem('loginStatus', '0');
      Actions.Login()
      
    } }
  ]);
 
  
    return true;
}

  
// const Token = await AsyncStorage.getItem('token');
// console.log('Token', Token);
// const imageset = await AsyncStorage.getItem('res.path');
// console.log('imageset', imageset);
// AsyncStorage.removeItem('res.path');
// AsyncStorage.removeItem('token');
// AsyncStorage.setItem('toggle', JSON.stringify(false));
// AsyncStorage.setItem('loginStatus', '0');
// Actions.Login()
  // this.props.navigation.goBack(null);



  _renderItem = ({item}) => {
    return (
      <View>
        <View style={{marginTop: h(12)}}>
          <Text style={styles.text}>
            {item.title}
          </Text>
          <Text style={styles.text}>
            {item.title1}
          </Text>
        </View> 
        {/* <ImageBackground 
        style = {{alignSelf:'center',
        width:h(30),
        height:h(30),
        // top:h(15),
        resizeMode:'contain',
        // shadowColor: '#000',
        // shadowOffset: { width: 1, height: 1 },
        // shadowOpacity: 0.5,
        }}
        source = {item.imageBackground}> */}
        <Image style={styles.iconView} source={item.image} />
        {/* </ImageBackground> */}
        <View style={{marginTop:h(5)}}>
        <Text style={styles.ptxt}>{item.text}</Text>
        <Text style={styles.ptxt}>{item.text1}</Text>
        <Text style={styles.ptxt}>{item.text2}</Text>
        <Text style={styles.ptxt}>{item.text3}</Text>
        <Text style={styles.ptxt}>{item.text4}</Text>
        <Text style={styles.ptxt}>{item.text5}</Text>
        </View>

      </View>
    );
  };

  _onDone = () => {
    console.log('this.state.expId',this.state.expId)
    // if(this.state.expId =='1') {
    //   Actions.tabbar();
    //   Actions.Dashboard();
    // }else{
      // Actions.FreeTrialPayment();
      this.props.navigation.navigate('FreeTrialPayment')

   // }
  };

  render() {
    if (this.state.showRealApp) {
      return <Instruction />;
    } else {
      return (
        <AppIntroSlider 
          
          renderDoneButton={this._renderDoneButton}
          renderNextButton={this._renderNextButton}
          renderItem={this._renderItem}
          data={slides}
          onDone={this._onDone}
          activeDotStyle={{
            backgroundColor: '#383B3F',
            bottom:h(18),
            width: w(5),
            // borderColor:''
          }}
          dotStyle={{backgroundColor: '#383B3F',bottom:h(18)}}
        />
      );
    }
  }
  _renderNextButton = () => {
    return (
      

<View
      style={{
          // position:'absolute',
          backgroundColor: '#FE6F69',
          width: w(75),
          height: h(5.5),
          // height:39,
          alignItems:'center',
          justifyContent:'center',
          borderRadius: w(10),
          // bottom: h(8),
          marginRight: w(9),
         // bottom:h(8)
        }}>
        <Text style={styles.AndText}>And</Text>
      </View>
    );
  };
  _renderDoneButton = () => {

    return (
      <View
        style={{
          backgroundColor: '#FE6F69',
          width: w(75),
          height: h(5.5),
          alignItems:'center',
          justifyContent:'center',
          // height:39,
          borderRadius: w(10),
          marginRight: w(9),
          // bottom:h(8)

       }}>
        <Text style={styles.AndText}>Got it</Text>
      </View>
      
      // <View style = {{marginRight: w(8),backgroundColor:'#000',bottom:h(4)}}>
      //   <Text>jhvhrfvuh</Text>
      //   </View>
    );
  };
}
