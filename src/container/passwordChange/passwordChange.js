import React, {useState} from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  Modal,
  Keyboard,
  TouchableWithoutFeedback,
  ActivityIndicator,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {IconAsset, Strings, UiColor} from '../../theme';
import {h, w} from '../../utils/Dimensions';
import Styles from './Style';
import {Pages} from 'react-native-pages';
import {connect} from 'react-redux';
import styles from '../Splash/styles';
import {useSelector, useDispatch} from 'react-redux';

import {ResetPasswordApi} from './../../actions/ResetPassword';
import AsyncStorage from '@react-native-community/async-storage';
import {Platform} from 'react-native';
// import Style from './Style'

const passwordChange = ({...props}) => {
  const dispatch = useDispatch();
  console.log('props', props);
  const contactNumber = props.data;
  console.log('contsctNumber' , contactNumber)
  const [password, setpassword] = useState('');
  const [confirmPassword, setconfirmPassword] = useState('');
  const [errors, seterrors] = useState('');
  const [match, setMatch] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showPasswordAgain, setShowPasswordAgain] = useState(false);

  const resetLoading = useSelector((state) => state.reset.isLoading);

  const Validation = async () => { // Validation Function
    seterrors('');
    setMatch(false);
    const user_id = await AsyncStorage.getItem('idf');
    console.log('user_id', user_id);
    var text = password;
    let reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@@#\$%\^&\*])(?=.{8,})/;
    if (password == '' || password == null) {
      seterrors('These new secret words must be the same, silly!');
    } else if (reg.test(text) === false) {
      setMatch(true);
      // setMatch(!match);
    } else if (confirmPassword == '' || confirmPassword == null) {
      seterrors('These new secret words must be the same, silly!');
    } else if (password != confirmPassword) {
      seterrors('These new secret words must be the same, silly!');
    } else {
      dispatch(ResetPasswordApi(password, confirmPassword, user_id));//Calling Api Reset password
    }
  };

//Return ui

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}>
      <View style={{backgroundColor:'#ffff', height:'100%'}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: h(15),
          }}>
          <TouchableOpacity
            onPress={() => Actions.Login()}
            style={{right: w(12)}}>
            <Image
              style={{width: 14, height: 18, resizeMode: 'contain'}}
              source={require('./../../assets/icon/BackB.png')}
            />
          </TouchableOpacity>
          <Text style={Styles.txt}>Update Your Secret Word</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            left: w(4.5),
            marginTop: h(10),
          }}>
          <TextInput
            style={[
              Styles.inputFieldContainer,
              {
                borderColor: errors != '' ? 'red' : '#000',
                alignItems: 'center',
                flexDirection: 'row',
                fontSize: 16,
              },
            ]}
            placeholderTextColor="#383B3F"
            underlineColorAndroid="transparent"
            placeholder="New Secret Word"
            autoCapitalize="none"
            underlineColorAndroid="transparent"
            secureTextEntry={!showPassword}
            onChangeText={(password) => setpassword(password)}
            value={password}
          />
          <TouchableOpacity
            onPress={() => setShowPassword(!showPassword)}
            style={Styles.passimage}>
            {!showPassword ? (
              <Image
                source={require('../../assets/icon/EyeVector.png')}
                style={{
                  resizeMode: 'contain',
                  height: 10,
                  width: 20,
                  marginLeft: w(4),
                }}
              />
            ) : (
              <Image
                source={require('../../assets/icon/openEyeVector.png')}
                style={{
                  resizeMode: 'contain',
                  height: 10,
                  width: 20,
                  marginLeft: w(4),
                }}
              />
            )} 
          </TouchableOpacity>
        </View>
        {errors != '' && match === false ? (
          <Text style={Styles.errmsg}>{errors}</Text>
        ) : null}
        {errors === '' ? (
          <Text
            style={[
              Styles.Text,
              {
                color: match ? 'red' : '#000',
                fontSize: Platform.OS === 'ios' ? 9 : 10,
              },
            ]}>
            Your secret word must be {'>'} 6 characters and contain{'\n'}an
            uppercase letter, a number, and a special character.
          </Text>
        ) : null}

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: h(7),
            left: w(3),
          }}>
          <TextInput
            style={[
              Styles.inputFieldContainer2,
              {
                borderColor: errors != '' ? 'red' : '#000',
                borderColor: errors != '' ? 'red' : '#000',

                alignItems: 'center',
                fontSize: 16,
              },
            ]}
            placeholderTextColor="#383B3F"
            underlineColorAndroid="transparent"
            placeholder=" New Secret Word Again"
            autoCapitalize="none"
            underlineColorAndroid="transparent"
            secureTextEntry={!showPasswordAgain}
            onChangeText={(confirmPassword) =>
              setconfirmPassword(confirmPassword)
            }
            value={confirmPassword}
          />
          <TouchableOpacity
            style={{right: w(12)}}
            onPress={() => setShowPasswordAgain(!showPasswordAgain)}>
            {!showPasswordAgain ? (
              <Image
                // style = {Styles.passimage}
                source={require('../../assets/icon/EyeVector.png')}
                style={{resizeMode: 'contain', height: 10, width: 20}}
              />
            ) : (
              <Image
                // style = {Styles.passimage}
                source={require('../../assets/icon/openEyeVector.png')}
                style={{resizeMode: 'contain', height: 10, width: 20}}
              />
            )}
          </TouchableOpacity>
        </View>
        {errors != '' ? <Text style={Styles.errmsg2}>{errors}</Text> : null}
        <View>
          <TouchableOpacity style={Styles.buttonContainer} onPress={Validation}>
            <Text style={Styles.AndText}>Remember Me</Text>
          </TouchableOpacity>
        </View>
        {resetLoading ? (
          <View
            style={{
              position: 'absolute',
              left: 0,
              right: 0,
              top: 5,
              bottom: 0,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: 'transparent',
            }}>
            <View>
              <ActivityIndicator size="large" color="#000" />
            </View>
          </View>
        ) : null}
      </View>
    </TouchableWithoutFeedback>
  );
};
export default passwordChange;
