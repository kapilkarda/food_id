import { StyleSheet } from 'react-native';
import { color } from 'react-native-reanimated';
import { h, w } from '../../utils/Dimensions';

export default StyleSheet.create({
  MainContainer: {
    flex: 1,
  },
  txt: {
    alignSelf: 'center',
    color: '#383B3F',
    fontSize: 20,
    fontWeight: 'bold',
  
  },
  inputFieldContainer: {
    alignSelf: 'center',
    borderColor: '#000',
    borderWidth: w(0.28),
    width: w(75),
    height: h(5),
    borderRadius: w(10),
  
    paddingLeft: w(5),
  },
  inputTextEye: {
    backgroundColor: 'green',
    width: "50%"
  },

  passimage: {
   right:w(15)
  },
  Text:
  {
    marginTop:h(1),
    alignSelf: 'center',
    color: '#8c8c8c',
    lineHeight:13,
    fontSize: 9,
    //marginRight:w(4)
    // marginLeft: w(1)
  },
  buttonContainer: {
    justifyContent:'center',
    alignItems:'center',
    alignSelf: 'center',
    backgroundColor: '#FE6F69',
    width: w(75),
    height: h(5),
    borderRadius: w(10),
    marginTop:h(8),
    left:w(0)
   
  },
  AndText: {
    fontSize: 16,
    alignSelf: 'center',
    color: '#fff',
    
  },
  socialButton: {
    flexDirection: 'row',
    top: h(48),
    alignSelf: 'center',
    marginLeft: w(8),
  },

  socialImgButton: {
    width: w(20),
    alignSelf: 'center',
  },
  img: {
    width: h(7),
    height: h(7),
    resizeMode: 'contain',
  },
  errmsg: {
    color: '#ff0000',
    top:h(1),
    fontSize: 10,
    marginLeft: w(18)
  },
  errmsg2: {
    color: '#ff0000',
    top:h(1),
    fontSize: 10,
    marginLeft: w(18)
  },
  inputFieldContainer2: {
    alignSelf: 'center',
    borderColor: '#000',
    borderWidth: w(0.28),
    width: w(75),
    height: h(5),
    borderRadius: w(10),
   
    paddingLeft: w(5),
  },

})