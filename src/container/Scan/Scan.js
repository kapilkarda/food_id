import React, {Component, useState, useEffect} from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  Image,
  Platform,
  Modal,
  SafeAreaView,
  BackHandler,
  ActivityIndicator,
} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import {View} from 'native-base';
import {Actions, navigation} from 'react-native-router-flux';
import {useSelector, useDispatch} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {BarCodeApi} from './../../actions/Barcode';
import {h, w} from '../../utils/Dimensions';
import {Alert} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import {withNavigationFocus} from 'react-navigation';
import scanonhide from '../../reducers/ReducerScanExit';
import {UserDetailApi} from '../../actions/USerProfile';
import {exitBarcode} from '../../actions/ExitBarcode';
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded,
  onFailToRecieveAd
} from 'react-native-admob';
var myarr = [];

const Scan = ({navigation}) => {
  const dispatch = useDispatch();
  const screenStatus = navigation.isFocused();

  const Scanner = useSelector((state) => console.log('Scanner', state));
  const scaneers = useSelector((state) => state.barcode.BARCODE);
  // const status = scaneers.status;
  console.log('scaneers', scaneers);

  // const name = scaneers.name;
  const getUserDetail = useSelector((state) => state.userdetail.USERDETAIL);
  const username = getUserDetail ? getUserDetail.name : '';

  let [scanEnable, setScanEnable] = useState(false);
  let [qrScanner, setQrScanner] = useState('');
  let [onHide, setOnHide] = useState(true);
  const [barcodeError, setbarcodeError] = useState(false);
  const [shareVisible, shareSetVisible] = useState(false);

  const scanOnHide = useSelector((state) => state.scanonhide.SCANONHIDEEXIT);
  const scanOnShow = useSelector((state) => state.scanonhide.SCANONSHOWXIT);
  const barcodeLoading = useSelector((state) => state.barcode.isLoading);
  const status = useSelector((state) => state.barcode.ERROR);

  useEffect(() => {
    return () => {
      setScanEnable(false);
      setbarcodeError(false);
      userDetails();
    };
  }, [screenStatus]);

  useEffect(() => {
    if (status) {
      if (status === 'fail') {
        shareSetVisible(false);
        setScanEnable(false);
        setbarcodeError(true);
      }
    }
  }, [status]);

  useEffect(() => {
    if (scaneers) {
      if (scaneers) {
        openShareModal();
      }
    }
  }, [scaneers]);

  const onSuccess = async (e) => {
    // console.log('eData',e.data)
    setbarcodeError(false);
    const token = await AsyncStorage.getItem('token');
    AsyncStorage.setItem('e.data', e.data);
    dispatch(BarCodeApi(token, e.data , navigation , 'scan'));
  };

  const userDetails = async () => {
    let token = await AsyncStorage.getItem('token');
    dispatch(UserDetailApi(token));
  };

  const openShareModal = async () => {
    const modelCheck = await AsyncStorage.getItem('Model');
    console.log('modelCheck', modelCheck);
    if (
      modelCheck === '0' ||
      modelCheck === 0 ||
      modelCheck === undefined ||
      modelCheck === null
    ) {
      shareSetVisible(true);
    }
  };

  const closeModal = () => {
      console.log('inside function');
      if (scaneers) {
        console.log('inside if');
        navigation.navigate('ScannedScreen',scaneers)
        // Actions.ScannedScreen(scaneers);
        shareSetVisible(false);
        dispatch({type: 'barCode', payload: ''});
      } else {
        console.log('inside else');
      }
  };

  const barcodescanenabal = () => {
    setScanEnable(true);
  };

    const modelClose = async () => {
    AsyncStorage.setItem('Model', '1');
   
      if (scaneers) {
        navigation.navigate('ScannedScreen',scaneers)
        shareSetVisible(false);
        dispatch({type: 'barCode', payload: ''});
      }
    
  };

  useEffect(() => {
    const backAction = () => {
      Actions.Dashboard();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => {
      backHandler.remove();
      setScanEnable(false);
    };
  }, []);
  
  const Exit = () => {
    setScanEnable(false);
    Actions.Dashboard();
  };

  const goBack=()=>{
    navigation.navigate('Dashboard')
  }

  if (scanEnable) {
    return (
      <QRCodeScanner
        ref={(node) => {
          setQrScanner(node);
        }}
        onRead={onSuccess}
        cameraStyle={{position: 'relative'}}
        reactivate={true}
        reactivateTimeout={5000}
        topContent={
          <View style={{opacity: shareVisible ? 0.6 : 1}}>
            <TouchableOpacity
              onPress={Exit}
              style={{top: h(-2), alignSelf: 'flex-end', left: w(18)}}>
              <Image
                style={{width: h(2), height: h(2), resizeMode: 'contain'}}
                source={require('./../../assets/icon/X.png')}
              />
            </TouchableOpacity>
            <Text
              style={{
                fontSize: 16,
                color: '#302F37',
                alignSelf: 'center',
                marginBottom: h(2),
              }}>
              Please Scan Barcode
              {'\n'}
            </Text>

            <Modal visible={shareVisible} transparent={true}>
              <View
                style={{
                  backgroundColor: '#000',
                  alignSelf: 'center',
                  height: h(50),
                  width: h(45),
                  marginTop: h(23),
                  borderRadius: 20,
                }}>
                <Text
                  style={{
                    color: '#fff',
                    textAlign: 'center',
                    // backgroundColor:'red',
                    fontSize: 16,
                    marginTop: h(10),
                    fontWeight: '400',
                    marginLeft: w(5),
                    marginRight: w(5),
                  }}>
                  Just a reminder that you should {'\n'} eat at your own
                  discretion. We{'\n'}
                  do our best to give the most {'\n'} accurate information but{' '}
                  {'\n'}nothing is guaranteed in life {'\n'}except death and
                  taxes.
                </Text>
                <View
                  style={{
                    alignSelf: 'center',
                    backgroundColor: '#fa8072',
                    width: w(50),
                    height: h(6),
                    borderRadius: w(10),
                    marginTop: h(4),
                  }}>
                  <TouchableOpacity onPress={closeModal}>
                    <Text
                      style={{
                        fontSize: 18,
                        textAlign: 'center',
                        // alignSelf: 'center',
                        color: '#fff',
                        marginTop: h(1.6),
                      }}>
                      Got It!
                    </Text>
                  </TouchableOpacity>
                </View>
                <TouchableOpacity onPress={modelClose}>
                  <Text
                    style={{
                      color: '#fa8072',
                      alignSelf: 'center',
                      marginTop: h(5),
                      fontSize: 18,
                    }}>
                    Don’t Show Again
                  </Text>
                </TouchableOpacity>
              </View>
              {/* </View> */}
            </Modal>
          </View>
        }
      />
    );
  } else {
    return (
      <View>
        <TouchableOpacity
          onPress={()=>{goBack()}}
          style={{alignSelf: 'flex-end', top: h(5), right: w(15)}}>
          <Image
            style={{width: h(2), height: h(2), resizeMode: 'contain'}}
            source={require('./../../assets/icon/X.png')}
          />
        </TouchableOpacity>
       
        <Text
          style={{
            fontSize: 16,
            color: '#302F37',
            alignSelf: 'center',
            marginTop: h(10),
          }}>
          Please Scan Barcode
          {'\n'}
        </Text>
        {barcodeError ? (
          <TouchableOpacity
            onPress={() =>
              Linking.openURL(
                'mailto:service@foodid.biz?subject= ' +
                  username +
                  ' has something to say',
                '&body=Description',
              )
            }
            title="support@example.com">
            <Text style={{color: '#FE6F69', alignSelf: 'center', fontSize: 10}}>
              This product can’t be found. If you think that’s{'\n'} bogus, ask
              us to add it by heading over{' '}
              <Text
                style={{
                  color: '#FE6F69',
                  textDecorationLine: 'underline',
                }}>
                here.
              </Text>
            </Text>
          </TouchableOpacity>
        ) : null}
        
        <TouchableOpacity
          style={{
            backgroundColor: '#383B3F',
            width: '100%',
            height: '65%',
            top: h(5),
          }}
          onPress={barcodescanenabal}>
          <Text
            style={{
              alignSelf: 'center',
              color: '#fff',
              fontSize: 36,
              marginTop: h(25),
            }}>
            Tap Here To Scan
          </Text>
          {/* <Text style = {{alignSelf:'center', color:'#fff', fontSize:36}}>Re-scan</Text> */}
        

        </TouchableOpacity>
        {barcodeLoading ? (
          <View
            style={{
              position: 'absolute',
              left: 0,
              right: 0,
              top: 5,
              bottom: 0,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: 'transparent',
            }}>
            <View>
              <ActivityIndicator size="large" color="#ffff" />
            </View>
          </View>
        ) : null}

{Platform.OS === 'ios' ? 
          <View style={{ alignSelf: 'center',top:h(8)}}>
                <AdMobBanner
                    adSize="Banner"
                    adUnitID="ca-app-pub-3940256099942544/2934735716"
                    // testDeviceID="CF583E54-34C6-453C-80FC-493D2468A51E"
                    didFailToReceiveAdWithError={onFailToRecieveAd}
                />
            </View>:
            <View style={{alignSelf: 'center',top:h(8)}}>
            <AdMobBanner
                adSize="Banner"
                adUnitID="ca-app-pub-3940256099942544/6300978111"
                // testDeviceID="CF583E54-34C6-453C-80FC-493D2468A51E"
                didFailToReceiveAdWithError={onFailToRecieveAd}
            />
        </View>}
      </View>
    );
  }
};
export default withNavigationFocus(Scan);
