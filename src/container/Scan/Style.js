import {StyleSheet} from 'react-native';
import {h, w} from '../../utils/Dimensions';

export default StyleSheet.create({
  MainContainer: {
    flex: 1,
  },
  txt: {
    alignSelf: 'center',
    fontSize: 30,
    fontWeight: 'bold',
    marginTop: h(9),
  },
  Otpcontainer:
  {

    
  },
  inputFieldContainer: {
    alignSelf: 'center',
    borderColor: '#000',
    borderWidth: w(1),
    width: w(85),
    height : h(8),
    borderRadius: w(10),
    marginTop: h(8),
  },
  passimage: {
    marginLeft: w(70),
    marginTop: h(-3),
  },
  forgotButton:
  {
marginLeft:w(55),
//marginRight:h(10),
marginTop:h(4)

  },
  buttonContainer: {
    alignSelf: 'center',
    backgroundColor: '#fa8072',
    width: w(85),
    height: h(8),
    borderRadius: w(10),
    marginTop: h(9),
  },
  AndText: {
    fontSize: 18,
    alignSelf: 'center',
    color: '#fff',
    marginTop: w(5),
  },
  socialButton: {
    flexDirection: 'row',
    marginTop: h(12),
    alignSelf: 'center',
  },
  socialImgButton: {
    width: w(25),
    alignSelf: 'center',
  },
  img: {
    width: w(20),
    height: h(10.52),
  },
modelView:
{
  backgroundColor:'#000',
  alignSelf:'center',
  height:h(40),
    width:w(80),
  marginTop:h(13.5),
  borderRadius:w(10),


},
modelTxT :
{
  color:'#fff',
  alignSelf:'center',
  fontSize:20,
  marginTop:h(10)
  ,fontWeight:'800'


},modelbuttonContainer: {
    alignSelf: 'center',
    backgroundColor: '#fa8072',
    width: w(50),
    height: h(6),
    borderRadius: w(10),
    marginTop: h(4),
  },
  modelAndText: {
    fontSize: 18,
    alignSelf: 'center',
    color: '#fff',
    marginTop: w(3),
  },
  ButtonJoined: {
    alignSelf: 'center',
    marginTop: h(20),
  },
  txtJoined: {
    color: '#fa8072',
  },
})