import React, { useEffect, useState } from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  Modal,
  ImageBackground,
  ScrollView,
  Keyboard,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Platform, 
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { IconAsset, Strings, UiColor } from '../../theme';
import { h, w } from '../../utils/Dimensions';
//import Styles from './Style';
import { Pages } from 'react-native-pages';

import AsyncStorage from '@react-native-community/async-storage';
import { DictionaryApi } from './../../actions/Dictionary'
import { useSelector, useDispatch } from 'react-redux';

const br = `\n`;
const DeleteBUTTON= () => {
    return ( 
<View>
    
    <Image
    style={{ height: h(2.5),
        width: h(2.5),
       
        tintColor: '#fff',
        }}
    source = {require('./../../assets/icon/dustbin.png')}
    />
        
    </View>

 
   
    )
};
export default DeleteBUTTON;
