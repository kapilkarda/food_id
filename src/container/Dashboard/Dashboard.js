import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  Modal,
  ImageBackground,
  ScrollView,
  SafeAreaView,
  BackHandler,
  Platform,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {IconAsset, Strings, UiColor} from '../../theme';
import {h, height, w} from '../../utils/Dimensions';
//import Styles from './Style';
import {Pages} from 'react-native-pages';
import {connect} from 'react-redux';
import styles from '../Splash/styles';
import Styles from './Style';
import {useSelector, useDispatch} from 'react-redux';
import {brainNomsApi} from './../../actions/BrainNoms';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import {withNavigationFocus} from 'react-navigation';
import {SearchCountApi} from './../../actions/SearchCount';
import {UserDetailApi} from './../../actions/USerProfile';
import DeviceInfo from 'react-native-device-info';
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded,
  onFailToRecieveAd
} from 'react-native-admob';
const br = `\n`;
let checkedNet = true;
let checkedDevVersion = true;
let checkedServerStatus = true;

const Dashboard = ({navigation}) => {
  const dispatch = useDispatch();
  const screenStatus = navigation.isFocused();

  const [CurrentDate, setCurrentDate] = useState('');
  const [CurrentMonth, setCurrentMonth] = useState('');
  const [shareVisible, shareSetVisible] = useState(false);
  const [shareVisibleNext, shareSetVisibleNext] = useState(false);
  const [visibleAppUpdateModal, setVisibleAppUpdateModal] = useState(false);
  const [visibleServerModal, setVisibleServerModal] = useState(false);
  const getUserDetail = useSelector((state) => state.userdetail.USERDETAIL);
  const getNetConnected = useSelector((state) => state.checkOfflineFeature);
  const getMaintainStatus = useSelector(
    (state) => state.getServerMaintainStatus,
  );

  const CurrentDay = moment().format('dddd'); //Today date Format For log Section

  useEffect(() => {
    BrainNome();
    userDetails();
  }, []);

  const getToken = async () => {
    //Get UserToken function
    const token = await AsyncStorage.getItem('token');
    if (getNetConnected) {
      if (getNetConnected.checkOfflineFeature == true && checkedNet) {
        checkedNet = false;
        if (token) {
          Actions.tabbar();
          Actions.Dashboard();
        } else {
          navigation.navigate('Login'); //Navigation to Login
        }
      } else if (getNetConnected.checkOfflineFeature == false) {
        Actions.ReachOut();
        checkedNet = true;
      }
    }
  };

  useEffect(() => {
    getToken();
  }, [getNetConnected?.checkOfflineFeature]);

  useEffect(() => {
    if (getMaintainStatus.serverStatus) {
      if (getMaintainStatus.serverStatus.data) {
        if (getMaintainStatus.serverStatus.data.status == true) {
          console.log('Server');
          if (checkedServerStatus) {
            console.log('ServerIf');
            setVisibleServerModal(true);
            checkedServerStatus = false;
          }
        } else {
          setVisibleServerModal(false);
        }
      }
    }
  }, [getMaintainStatus]);

  useEffect(() => {
    var monthNames = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var DATE = new Date().getDate();
    var MONTH = monthNames[new Date().getMonth()];
    setCurrentDate(DATE);
    setCurrentMonth(MONTH);
  }, []);

  const getNoms = useSelector((state) => state.Brainnoms.BRAINE);

  const Desciption = getNoms && getNoms[0].description;

  const BrainNome = async () => {
    const token = await AsyncStorage.getItem('token');
    dispatch(brainNomsApi(token));
  };

  const userDetails = async () => {
    //get UserDetail Function
    let token = await AsyncStorage.getItem('token');
    dispatch(UserDetailApi(token)); //UserDetail Api Calling
  };

  useEffect(() => {
    // function to redirect in payment screen
    if (getUserDetail) {
      console.log('getUserDetail.is_expired', getUserDetail.is_expired);
      if (getUserDetail.is_expired === 2) {
        Actions.FreeTrialPayment();
      }
      const deviceVersion = DeviceInfo.getVersion();
      if (Platform.OS === 'android') {
        if (checkedDevVersion) {
          if (deviceVersion != getUserDetail.android_version) {
          }
          checkedDevVersion = false;
        }
      } else if (Platform.OS === 'ios') {
        if (checkedDevVersion) {
          if (deviceVersion != getUserDetail.ios_version) {
          }
          checkedDevVersion = false;
        }
      }
    }
  }, [getUserDetail]);

  const learningscreen = async () => {
   
      navigation.navigate('Addingra')
    
  };

  const calendarlearning = async () => {
   
    navigation.navigate('Calendar')
    // Actions.Calendar()
  };

  const is_Expired = async () => {
    const Expired = await AsyncStorage.getItem('is_expired');
    console.log('Expired', Expired);
    if (Expired === true || Expired === 'true') {
      Actions.FreeTrialPayment();
    }
  };
  const backAction = () => {
    if (navigation.isFocused()) {
      Alert.alert("Food ID!", "Are you sure you want to exit the app?", [
        {
          text: "Cancel",
          onPress: () => console.log('cancel'),
        },
        { text: "YES", onPress: () => BackHandler.exitApp() }
      ]);
      return true;
    } else {
      return false;
    }
  };
  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", backAction);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", backAction);
    };
  }, [backAction]);

  const getSearchCount = async (type) => {
    const token = await AsyncStorage.getItem('token');
    dispatch(SearchCountApi(token, type, 'card', navigation)); //Api Calling for get SearchCount
  };

  const goToSearch = () => {
    getSearchCount('search');
    // Actions.SearchByname();
  };

  const goToScan = () => {
    getSearchCount('scan');
  };

  const closeModal = () => {
    setVisibleAppUpdateModal(false);
    checkedDevVersion = true;
  };

  const closeServerModal = () => {
    BackHandler.exitApp();
    setVisibleServerModal(false);
    checkedServerStatus = true;
  };
  //Ui For DashBoard
  return (
    <ScrollView
      bounces={false}
      showsVerticalScrollIndicator={false}
      style={{
        backgroundColor: shareVisible || shareVisibleNext ? '#000' : '#fff',
        opacity: shareVisible || shareVisibleNext ? 0.5 : 1,
      }}>
        
      <View style={{backgroundColor: '#fff'}}>
        <Image
          style={Styles.yellowimage}
          source={require('../../assets/icon/Vectoryellow.png')}
        />

        <Image
          style={Styles.milkimage}
          source={require('../../assets/image/MilkDashboard.png')}
        />

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            marginTop: h(12),
          }}>
          <View
            style={[
              Styles.OptionsView,
              {opacity: shareVisible || shareVisibleNext ? 0.2 : 1},
            ]}>
            <TouchableOpacity
              onPress={learningscreen}
              style={{alignSelf: 'center'}}>
              <Image
                source={require('../../assets/image/CurateWatchouts.png')}
                style={{resizeMode: 'contain', height: 30, width: 30}}
              />
            </TouchableOpacity>
            <Text
              style={[
                Styles.Optiontxt, 
                {color: shareVisible || shareVisibleNext ? '#000' : '#8C8C8C'},
              ]}>
              Curate{'\n'}Watchouts
            </Text>
            <Text
              style={[
                Styles.Optiontxt2,
                {color: shareVisible || shareVisibleNext ? '#000' : '#8C8C8C'},
              ]}>
              Add ingredients {'\n'}you’re avoiding.
            </Text>
          </View>

          <View
            style={[
              Styles.OptionsView,
              {opacity: shareVisible || shareVisibleNext ? 0.2 : 1},
            ]}>
            <TouchableOpacity
              onPress={calendarlearning}
              style={{alignSelf: 'center'}}>
              <Image
                source={require('../../assets/image/Log_Your_Food.png')}
                style={{resizeMode: 'contain', height: 30, width: 30}}
              />
            </TouchableOpacity>
            <Text
              style={[
                Styles.Optiontxt,
                {color: shareVisible || shareVisibleNext ? '#000' : '#8C8C8C'},
              ]}>
              Log Your {'\n'}Food
            </Text>
            <Text
              style={[
                Styles.Optiontxt2,
                {color: shareVisible || shareVisibleNext ? '#000' : '#8C8C8C'},
              ]}>
              {CurrentMonth} {CurrentDate} is today, and{'\n'}today is{' '}
              {CurrentDay}.{' '}
            </Text>
          </View>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            marginTop: h(3),
          }}>
          <View
            style={{
              backgroundColor: '#EBEBEB',
              justifyContent: 'center',
              width: '37%',
              padding: 12,
              height: h(10),
              opacity: shareVisible || shareVisibleNext ? 0.2 : 1,
            }}>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={Actions.Watchout}
                style={{alignSelf: 'center'}}>
                <Image
                  style={{width: 30, height: 30, resizeMode: 'contain'}}
                  source={require('./../../assets/image/ew.png')}
                />
              </TouchableOpacity>
              <Text
                style={{
                  color: '#8C8C8C',
                  fontWeight: 'bold',
                  fontSize: 14,
                  marginTop: h(0),
                  marginLeft: w(3),
                  color: shareVisible || shareVisibleNext ? '#000' : '#8C8C8C',
                }}>
                Edit Your {'\n'}Watchouts
              </Text>
            </View>
          </View>

          <View
            style={{
              backgroundColor: '#EBEBEB',
              justifyContent: 'center',
              width: '37%',
              padding: 12,
              opacity: shareVisible || shareVisibleNext ? 0.2 : 1,
            }}>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => {
                  goToScan();
                }}
                style={{
                  marginTop: h(0),
                  marginLeft: w(0),
                  alignSelf: 'center',
                }}>
                <Image
                  style={{width: 30, height: 30, resizeMode: 'contain'}}
                  source={require('../../assets/icon/Scan_Button(1).png')}
                />
              </TouchableOpacity>
              <Text
                style={[
                  Styles.Optiontxt,
                  {
                    marginTop: h(0),
                    marginLeft: w(3),
                    color:
                      shareVisible || shareVisibleNext ? '#000' : '#8C8C8C',
                  },
                ]}>
                Scan {'\n'}Barcode
              </Text>
            </View>
          </View>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            marginTop: h(3),
          }}>
          <View
            style={{
              backgroundColor: '#EBEBEB',
              justifyContent: 'center',
              width: '37%',
              height: h(10),

              padding: 12,
              borderBottomRightRadius: w(10),
              opacity: shareVisible || shareVisibleNext ? 0.2 : 1,
            }}>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => {
                  goToSearch();
                }}>
                <ImageBackground
                  style={{
                    width: 30,
                    height: 30,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  imageStyle={{resizeMode:'contain'}}
                  source={require('../../assets/icon/Ellipse22.png')}>
                  <Image
                    source={require('../../assets/icon/Right.png')}
                    style={{resizeMode: 'contain', height: 15, width: 15}}
                  />
                </ImageBackground>
              </TouchableOpacity>
              <Text
                style={{
                  color: shareVisible || shareVisibleNext ? '#000' : '#8C8C8C',
                  fontWeight: 'bold',
                  fontSize: 14,
                  marginLeft: w(2.5),
                }}>
                Search By{'\n'}Name
              </Text>
            </View>
          </View>

          <View
            style={{
              backgroundColor: '#EBEBEB',
              justifyContent: 'center',
              width: '37%',
              height: h(10),

              padding: 12,
              borderBottomRightRadius: w(10),
              opacity: shareVisible || shareVisibleNext ? 0.2 : 1,
            }}>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity onPress={Actions.Faves}>
                <Image
                  source={require('../../assets/image/Check_Your_Favorites.png')}
                  style={{resizeMode: 'contain', height: 30, width: 30}}
                />
              </TouchableOpacity>
              <Text
                style={{
                  color: shareVisible || shareVisibleNext ? '#000' : '#8C8C8C',
                  fontWeight: 'bold',
                  fontSize: 14,
                  marginLeft: w(3),
                }}>
                Check{'\n'}Faves
              </Text>
            </View>
          </View>
        </View>

        <Modal
          visible={visibleAppUpdateModal}
          transparent={true}
          animationType="slide">
          <View style={Styles.modelView}>
            <Text style={Styles.modelTxT}>
              Shiny new app update {'\n'}available. Shall we?
            </Text>

            <View style={Styles.modelbuttonContainer}>
              <TouchableOpacity
                onPress={() => {
                  closeModal();
                }}>
                <Text style={Styles.modelAndText}>Got It!</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <Modal
          visible={visibleServerModal}
          transparent={true}
          animationType="slide">
          <View
            style={{
              height: '100%',
              width: '100%',
              backgroundColor: '#00000090',
            }}>
            <View style={Styles.modelView}>
              <Text style={Styles.modelTxT}>
                Scheduled maintenance is {'\n'}happening. Like right now. {'\n'}
                We’ll try to make it quick {'\n'}and painless.
              </Text>

              <View style={Styles.modelbuttonContainer}>
                <TouchableOpacity
                  onPress={() => {
                    closeServerModal();
                  }}>
                  <Text style={Styles.modelAndText}>Got It!</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
         
      </View>
      <View style={{height: h(10), width: h(10)}} />
    </ScrollView>
  );
};
export default withNavigationFocus(Dashboard);
