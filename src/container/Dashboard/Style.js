import { StyleSheet, Dimensions } from 'react-native';
import { h, w } from '../../utils/Dimensions';

export default StyleSheet.create({
  MainContainer: {
    flex: 1,
  },
  yellowimage: {
    width: w(75),
    height: h(22),
    position: 'absolute',
    resizeMode: 'stretch',
  },
  milkimage: {
    alignSelf: 'center',
    top: h(7),
    resizeMode: 'contain',
    height: h(25),
    width: w(50)
  },
  OptionsView: {
    backgroundColor: '#EBEBEB',
    borderBottomRightRadius: w(10),
    width: '37%',
    justifyContent: 'center',
    height: h(22)
  },
  OptionsView1: {
    backgroundColor: '#EBEBEB',
    width: w(35.5),
    height: h(10.5),
    borderBottomRightRadius: w(12),
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  OptionsView2: {
    backgroundColor: '#EBEBEB',
    width: w(35.5),
    height: h(10.5),
    borderBottomRightRadius: w(12),
    flexDirection: 'row',
    alignItems: 'center',
  },
  OptionButton: {
    alignSelf: 'center',
    marginTop: h(4)
  },
  searchByNameTxt: {
    color: '#8C8C8C',
    fontWeight: 'bold',
    fontSize: 16,
    marginLeft: w(3)
  },
  Optiontxt: {
    color: '#8C8C8C',
    fontWeight: 'bold',
    fontSize: 14,
    marginLeft: w(5),
    marginTop: h(3)
  },
  Optiontxt3: {
    color: '#8C8C8C',
    fontWeight: 'bold',
    fontSize: 14,
    marginLeft: w(5),
    marginTop: h(3)
  },
  Optiontxt2: {
    color: '#8c8c8c',
    marginTop: h(1),
    marginLeft: w(5),
    fontSize: 11,
    fontWeight: '400',
    marginRight: w(2)
  },
  modelView: {
    backgroundColor: '#000',
    alignSelf: 'center',
    height: h(40),
    width: w(80),
    marginTop: h(20),
    borderRadius: w(10),
  },
  modelTxT: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 20,
    marginTop: h(10),
  },
  modelTxT2: {
    color: '#fff',
    alignSelf: 'center',
    fontSize: 20,
  },
  modelbuttonContainer: {
    alignSelf: 'center',
    backgroundColor: '#fa8072',
    width: w(50),
    height: h(6),
    borderRadius: w(10),
    marginTop: h(4),
    justifyContent: 'center'
  },
  modelAndText: {
    fontSize: 18,
    alignSelf: 'center',
    color: '#fff',
  },
  modelView: {
    backgroundColor: '#000',
    alignSelf: 'center',
    height: h(36),
    width: w(83),
    marginTop: h(20),
    borderRadius: w(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  modelTxT: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 16,
    fontWeight: '400',
  },
  modelbuttonContainer: {
    alignSelf: 'center',
    backgroundColor: '#fa8072',
    width: w(60),
    height: h(6),
    borderRadius: w(10),
    marginTop: h(4),
    justifyContent: 'center',
  },
  modelAndText: {
    fontSize: 16,
    alignSelf: 'center',
    color: '#fff',
    fontWeight: '400',
  },
});
