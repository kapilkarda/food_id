import { StyleSheet } from 'react-native';
import { h, w } from '../../utils/Dimensions';

export default StyleSheet.create({
  MainContainer: {
    flex: 1,

    // padding :h(2),

  },
  // Main:
  // {flexDirection:'row',
  // alignSelf:'center',
  // marginRight:w(4)
  // },
  // BigView:
  // {
  //   backgroundColor : '#FCBE2E',
  //   width : w(23),
  //  top:h(3.5),
  //   height:h(5),
  //   marginLeft:w(3),
  //   borderRadius:5

  // },
  CommonView:
  {
    backgroundColor: '#fcbe2e',
    width: w(18),
    // top:h(4.5),
    marginTop: h(3.5),
    height: h(5),
    marginLeft: w(3),
    borderRadius: 5
  },
  TxtColor:
  {
    color: '#fff'
  },
  ButtonView:
  {
    justifyContent:'center',
    alignItems:'center',
    // marginTop: h(1)
  },
  DrawerIcon:
  {
    width: h(2.3),
    height: h(2.3),
    left: w(-1),
    resizeMode:'contain',
    // right:h(2),
    // marginTop:h(5)
    marginTop: h(0.4),
    // position:'absolute',
    tintColor: '#383B3F'

    // marginBottom:h(10)
  },
  inputFieldContainer: {
    // alignSelf: 'center',
    borderWidth: w(0.28),
    width: w(75),
    height: h(5),
    borderRadius: w(10),
    paddingLeft: w(5),
    borderColor:'#383B3F',
    marginLeft:w(5),
    marginTop:h(-1),
    fontSize:h(1.9)
    
    // marginBottom:h(10)
    },
  
})