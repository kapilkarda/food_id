import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  Modal,
  ImageBackground,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
  Linking,
  Platform,
  BackHandler,
  ActivityIndicator,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {IconAsset, Strings, UiColor} from '../../theme';
import {h, w} from '../../utils/Dimensions';
//import Styles from './Style';
import {Pages} from 'react-native-pages';
import {connect} from 'react-redux';
import Styles from './Style';
import RadioButtonRN from 'radio-buttons-react-native';
import {SearchFood} from '../../actions/SearchFoodForWatchout';
import AsyncStorage from '@react-native-community/async-storage';
import {useSelector, useDispatch} from 'react-redux';
import {getFoodCategoryApi} from './../../actions/getFoodCategory';
import {addWatchOutApi} from './../../actions/addCuratWatchout';
import {getFoodSubCategoryApi} from './../../actions/getSubcategory';
import {SafeAreaView} from 'react-native';
import {removeCuratAPI} from './../../actions/RemoveCurat';
import {UserDetailApi} from './../../actions/USerProfile';

const br = `\n`;
var selectedSubCat = [];
var selectedSubSearchCat = [];
var arrayitem = [];

const Search = (value) => {
  // console.log('value', value);
  const dispatch = useDispatch();

  const getSearchData = useSelector((state) =>
    state.searchfood.SEARCHFOOD ? state.searchfood.SEARCHFOOD.data : null,
  );
  // console.log('searchdata', getSearchData);
  const getSearchDatastatus = useSelector((state) =>
    state.searchfood.SEARCHFOOD ? state.searchfood.SEARCHFOOD.status : null,
  );

  const getFoodCat = useSelector(
    (state) => state.getfoodcategory.GETFOODCATEGORY,
  );
  //Data Binding from reducer using useSelector
  const getSubCatData = useSelector((state) => state.subcategory.SUBCATEGORY);
  const getUserDetail = useSelector((state) => state.userdetail.USERDETAIL);
  const userDetailLoading = useSelector((state) => state.userdetail.isLoading);
  const searchFoodLoading = useSelector((state) => state.searchfood.isLoading);
  const foodCategoryLoading = useSelector(
    (state) => state.getfoodcategory.isLoading,
  );
  const subFoodCategoryLoading = useSelector(
    (state) => state.subcategory.isLoading,
  );
  const addCuratWatchoutLoading = useSelector(
    (state) => state.addcuratwatchout.isLoading,
  );
  const removeCuratWatchoutLoading = useSelector(
    (state) => state.removecurat.isLoading,
  );

  const username = getUserDetail ? getUserDetail.name : '';

  let [uiRender, setUiRender] = useState(false);
  let [search_value, setsearch_value] = useState('');
  const [showButton, setshowButton] = useState(false);
  const [selected, setselected] = useState('');
  const [catSelected, setcatSelected] = useState(false);
  const [dataArray, setdataArray] = useState([]);
  const [tokenAsync, setToken] = useState('');
  const [isremoved, setisremoved] = useState(false);
  const myTextInput = React.createRef();
  const [catadd, setcatadd] = useState('');
  const [categoryData, setcategoryData] = useState(['']);

  useEffect(() => {
    if (getFoodCat != undefined && getFoodCat.length > 0) {
      for (let item of getFoodCat) {
        item.btnSelected = false;
      }
      for (let item of getFoodCat) {
        if (item.food_category_name === search_value.toUpperCase()) {
          item.btnSelected = true;
        } else {
          item.btnSelected = false;
        }
      }
      setUiRender(!uiRender);
      //setcategoryData(getFoodCat);
    }
  }, [getFoodCat]);

  useEffect(() => {
    selectedSubCat = [];
    selectedSubSearchCat = [];
    setUiRender(!uiRender);
  }, []);

  useEffect(() => {
    userDetails();
  }, []);

  useEffect(() => {
    getToken();
  }, []);

  useEffect(() => {
    ApicallfirstTime();
  }, []);

  useEffect(() => {
    if (getSearchData != undefined) {
      if (getFoodCat != undefined) {
        for (let item of getFoodCat) {
          if (item.food_category_name === search_value.toUpperCase()) {
            item.btnSelected = true;
          } else {
            item.btnSelected = false;
          }
        }
      }
    }
    setUiRender(!uiRender);
  }, [getSearchData]);

  const ApicallfirstTime = async () => {
    //Calling Api first time when enter in screen
    if (value && value.data != undefined) {
      setsearch_value(value.data);
    }
    setUiRender(!uiRender);
    if (value.data) {
      const token = await AsyncStorage.getItem('token');
      if (value.data.length >= 3) {
        dispatch(SearchFood(token, value.data));
      } else {
        dispatch({type: 'Search_Food', payload: ''});
      }
      setUiRender(!uiRender);
    } else {
      setUiRender(!uiRender);
    }
  };

  const getToken = async () => {
    //Get user token function
    const token = await AsyncStorage.getItem('token');
    if (token) {
      setToken(token);
    }
  };

  const colorChange = (item) => {
    //Select Unselect radio button function
    if (item.type == 'category') {
      let tempArray = [...getSubCatData];
      if (item.isSelected) {
        for (let tempItem of tempArray) {
          tempItem.isSelected = false;
        }
      } else {
        for (let tempItem of tempArray) {
          tempItem.isSelected = true;
        }
      }
      if (getSearchData) {
        let tempArray1 = [...getSearchData];
        if (item.isSelected) {
          for (let tempItem of tempArray1) {
            tempItem.isSelected = false;
          }
        } else {
          for (let tempItem of tempArray1) {
            tempItem.isSelected = true;
          }
        }
        setUiRender(!uiRender);
      }
      setUiRender(!uiRender);
      dispatch({type: 'Get_Food_SubCategory', payload: tempArray});
    } else {
      let tempArray = [...getSubCatData];
      if (item.isSelected) {
        item.isSelected = false;
        if (getSubCatData) {
          if (getSubCatData.length > 0) {
            checkAllSubCat(tempArray);
          }
        }
        if (getSearchData) {
          let tempArray1 = [...getSearchData];
          checkAllSubCat(tempArray1);
        }
        if (selectedSubCat.length > 0) {
          for (let item3 of selectedSubCat) {
            if (item.id == item3.id) {
              var indexServiceId = selectedSubCat.indexOf(item3.id);
              selectedSubCat.splice(indexServiceId, 1);
            }
          }
        }
        setUiRender(!uiRender);
      } else if (item.isSelected === false) {
        item.isSelected = true;
        setUiRender(!uiRender);
        selectedSubCat.push(item);
        setUiRender(!uiRender);
        if (getSubCatData) {
          if (getSubCatData.length > 0) {
            checkAllSubCat(tempArray);
          }
        }
        if (getSearchData) {
          let tempArray1 = [...getSearchData];
          checkAllSubCat(tempArray1);
        }
      } else {
        item.isSelected = true;
        setUiRender(!uiRender);
        if (getSubCatData) {
          if (getSubCatData.length > 0) {
            checkAllSubCat(tempArray);
          }
        }
        if (getSearchData) {
          let tempArray1 = [...getSearchData];
          checkAllSubCat(tempArray1);
        }
      }
      setUiRender(!uiRender);
    }
    setselected(item.id);
    setUiRender(!uiRender);
  };

  const checkAllSubCat = (tempArray) => {
    console.log('tempArray', tempArray);
    let isAllseletced = true;
    for (let item of tempArray) {
      if (item.type == 'subcategory') {
        if (item.isSelected === false) {
          isAllseletced = false;
          break;
        }
      }
    }
    if (isAllseletced === true && tempArray[0].type === 'category') {
      tempArray[0].isSelected = true;
    } else if (isAllseletced === false && tempArray[0].type === 'category') {
      tempArray[0].isSelected = false;
    }
    setUiRender(!uiRender);
  };

  const callApiFirsttime = (value) => {
    dispatch(SearchFood(tokenAsync, value));
  };

  useEffect(() => {
    foodCat();
    setcatSelected(false);
  }, []);

  const foodCat = async () => {
    const token = await AsyncStorage.getItem('token');
    dispatch(getFoodCategoryApi(token));
  };

  const searchApiFunc = (text) => {
    //Search Food For Curat Function
    setsearch_value(text);
    if (text.length >= 3) {
      dispatch(SearchFood(tokenAsync, text)); //Api Call Fir Search Curat
    } else {
      dispatch({type: 'Search_Food', payload: ''});
    }
    // setUiRender(!uiRender);
  };

  const itemAddtoCurat = async (item) => {
    //Item Add to curat Fucntion
    setUiRender(uiRender);
    if (item.isSelected === 'true' || item.isSelected === true) {
      removecurat(item);
    } else if (item.isSelected === 'false' || item.isSelected === false) {
      const status = await AsyncStorage.getItem('status');
      let is_api = 1;
      const token = await AsyncStorage.getItem('token');
      const item_id = item.id;
      const types = item.type;
      const isremoved = item.isSelected;
      dispatch(
        addWatchOutApi(
          item_id,
          token,
          is_api,
          types,
          isremoved,
          catadd,
          search_value,
        ),
      ); //Api Call for add item in curat
    }
    colorChange(item);
  };

  const subCategory = async (item) => {
    myTextInput.current.clear();
    setsearch_value('');
    const token = await AsyncStorage.getItem('token');
    const cat_id = JSON.parse(item.food_category_id);
    setcatadd(cat_id);
    if (item.isSelected === true) {
      setisremoved(true);
    }
    dispatch(getFoodSubCategoryApi(token, cat_id, isremoved));
    CatButtoncolorChange(item);
    setcatSelected(false);
  };

  const CatButtoncolorChange = (item) => {
    //setselected(item.food_category_id);
    for (let data of getFoodCat) {
      if (data.food_category_name === item.food_category_name) {
        if (data.btnSelected === false) {
          data.btnSelected = true;
        }
      } else {
        data.btnSelected = false;
      }
    }
    setUiRender(!uiRender);
  };

  const removecurat = async (tempitem) => {   //Remove item from curat function
    const token = await AsyncStorage.getItem('token');
    const type = tempitem.type;
    const item_id = tempitem.id;
    dispatch(
      removeCuratAPI(token, type, item_id, isremoved, catadd, search_value),
    ); // Api Call for remove from curat
    setUiRender(uiRender);
    
  };

  const userDetails = async () => {
    let token = await AsyncStorage.getItem('token');
    dispatch(UserDetailApi(token));
  };

  const KeyBoardDrawer = () => {
    Keyboard.dismiss();
    Actions.drawerOpen();
  };

  useEffect(() => {
    const backAction = () => {
      Actions.Addingra();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  // console.log('getSubCatData',getSubCatData)
  const ClearData = (search_value) => {
    setsearch_value(search_value);
    dispatch({type: 'Search_Food', payload: ''});
  };
  return (
    <SafeAreaView style={{backgroundColor: '#fff'}}>
      <TouchableWithoutFeedback
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={{height: '100%'}}>
          <View style={{height: h(10)}}>
            <FlatList
              data={getFoodCat}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              // showsVerticalScrollIndicator = {false}
              renderItem={({item}) => (
                <View
                  style={{
                    backgroundColor:
                      item.btnSelected === false ? '#FCBE2E' : '#fff',
                    width:
                      item.food_category_name !== 'ADDITIVES' &&
                      item.food_category_name !== 'SHELLFISH' &&
                      item.food_category_name !== 'VEGGIES'
                        ? w(17)
                        : w(24),
                    // top:h(4.5),
                    marginTop: h(3.5),
                    height: h(5),
                    marginLeft: w(2),
                    borderRadius: 5,
                    borderWidth: w(0.1),
                    justifyContent: 'center',
                    // alignItems:'center',
                    borderColor:
                      item.btnSelected === false ? '#fff' : '#fcbe2e',
                  }}>
                  <TouchableOpacity
                    onPress={() => subCategory(item)}
                    style={Styles.ButtonView}>
                    <Text
                      style={{
                        color: item.btnSelected === false ? '#fff' : '#000',
                        fontWeight: 'bold',
                      }}>
                      {item.food_category_name}
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              marginTop: h(2.5),
            }}>
            <TouchableOpacity onPress={KeyBoardDrawer}>
              <Image
                style={Styles.DrawerIcon}
                source={require('../../assets/image/Hamburger_Nav.png')}
              />
            </TouchableOpacity>

            <TextInput
              ref={myTextInput}
              placeholderTextColor="#8c8c8c"
              style={Styles.inputFieldContainer}
              underlineColorAndroid="transparent"
              autoCapitalize="none"
              placeholder="Type Ingredient Here"
              underlineColorAndroid="transparent"
              onChangeText={(search_value) => ClearData(search_value)}
              value={search_value}
              onSubmitEditing={() => searchApiFunc(search_value)}
            />
          </View>
          {getSearchDatastatus === 'fail' &&
          search_value != undefined &&
          search_value.length != 0 ? (
            <View>
              <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                <TouchableOpacity
                  style={{top: h(3)}}
                  onPress={() =>
                    Linking.openURL(
                      'mailto:service@foodid.biz?subject= ' +
                        username +
                        ' has something to say',
                      '&body=Description',
                    )
                  }>
                  <Text
                    style={{
                      color: '#FE6F69',
                      fontSize: 10,
                      alignSelf: 'center',
                    }}>
                    This ingredient can’t be found. If you think that’s{'\n'}
                    bogus, ask us to add it by heading over{' '}
                    <Text
                      style={{
                        color: '#FE6F69',
                        textDecorationLine: 'underline',
                      }}>
                      here
                    </Text>
                    .
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          ) : null}

          {search_value !== undefined && search_value.length != 0 ? (
            <FlatList
              data={getSearchData}
              showsHorizontalScrollIndicator={false}
              style={{marginTop: h(3), marginBottom: h(7)}}
              renderItem={({item}) => (
                <TouchableOpacity onPress={() => itemAddtoCurat(item)}>
                  <View
                    style={{
                      backgroundColor:
                        item.type == 'category' ? '#fcbe2e' : '#ebebeb',
                      height: h(7),
                      borderBottomWidth: w(0.1),
                      borderColor: '#9a9a9a',
                      width: '100%',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      flexDirection: 'row',
                      marginTop: 0.2,
                      paddingLeft: w(5),
                      paddingRight: w(12),
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      {item.type == 'category' ? (
                        <View
                          style={{
                            width: h(3.5),
                            height: h(3.5),
                            backgroundColor: '#383B3F',
                            borderRadius: h(5),
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <Text
                            style={{
                              color: '#fff',
                              fontSize: h(2.2),
                              fontWeight: '500',
                            }}>
                            {item.first_name}
                          </Text>
                        </View>
                      ) : (
                        <View
                          style={{
                            width: h(3.5),
                            height: h(3.5),
                            backgroundColor: '#ebebeb',
                            borderRadius: h(5),
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <Text
                            style={{
                              color: '#fff',
                              fontSize: h(2.2),
                              fontWeight: '500',
                            }}>
                            {item.first_name}
                          </Text>
                        </View>
                      )}
                      <TouchableOpacity
                        onPress={() => itemAddtoCurat(item)}
                        style={{marginLeft: w(5), justifyContent: 'center'}}>
                        <Text
                          style={{
                            color: '#383B3F',
                            fontSize: h(1.9),
                            fontWeight: '500',
                          }}>
                          {item.name}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View
                      style={{
                        justifyContent: 'center',
                        backgroundColor: '#fff',
                        width: h(2.3),
                        height: h(2.3),
                        borderRadius: 10,
                        borderColor: '#383B3F',
                        borderWidth: w(0.1),
                        alignSelf: 'center',
                        // shadowColor: '#fff',
                        // shadowOffset: { width: 0, height: 2 },
                        // shadowOpacity: 0.5,
                      }}>
                      {item.isSelected ? (
                        <Image
                          style={{
                            height: h(2.3),
                            width: h(2.3),
                            resizeMode: 'contain',
                            alignSelf: 'center',
                          }}
                          source={require('./../../assets/image/Curate_Selection.png')}
                        />
                      ) : null}
                    </View>
                  </View>
                </TouchableOpacity>
              )}
            />
          ) : (
            <FlatList
              data={getSubCatData}
              showsHorizontalScrollIndicator={false}
              style={{marginTop: h(3), marginBottom: h(7)}}
              renderItem={({item}) => (
                <TouchableOpacity
                  onPress={() => itemAddtoCurat(item)}
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    height: h(7),
                    backgroundColor:
                      item.type == 'category' ? '#fcbe2e' : '#ebebeb',
                    //paddingHorizontal: w(12),
                    paddingLeft: w(5),
                    paddingRight: w(12),
                    borderBottomWidth: w(0.1),
                    borderColor: '#9a9a9a',
                    marginTop: 0.2,
                  }}>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    {item.type == 'category' ? (
                      <View
                        style={{
                          width: h(3.5),
                          height: h(3.5),
                          backgroundColor: '#383B3F',
                          borderRadius: h(5),
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Text
                          style={{
                            color: '#fff',
                            fontSize: h(2.2),
                            fontWeight: '500',
                          }}>
                          {item.first_name}
                        </Text>
                      </View>
                    ) : (
                      <View
                        style={{
                          width: h(3),
                          height: h(3),
                          borderRadius: h(2),
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Text
                          style={{
                            color: '#fff',
                            fontSize: h(2.5),
                            fontWeight: '500',
                          }}></Text>
                      </View>
                    )}
                    <View style={{marginLeft: 12}}>
                      <Text
                        style={{
                          color: '#383B3F',
                          fontSize: h(1.9),
                          fontWeight: '500',
                        }}>
                        {item.name}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      justifyContent: 'center',
                      backgroundColor: '#fff',
                      width: h(2.3),
                      height: h(2.3),
                      borderRadius: 10,
                      borderColor: '#383B3F',
                      borderWidth: w(0.1),
                      alignSelf: 'center',
                    }}>
                    {item.isSelected ? (
                      <Image
                        style={{
                          height: h(2.3),
                          width: h(2.3),
                          resizeMode: 'contain',
                        }}
                        source={require('./../../assets/image/Curate_Selection.png')}
                      />
                    ) : null}
                  </View>
                </TouchableOpacity>
              )}
            />
          )}
          {userDetailLoading ||
          searchFoodLoading ||
          foodCategoryLoading ||
          subFoodCategoryLoading ||
          addCuratWatchoutLoading ||
          removeCuratWatchoutLoading ? (
            <View
              style={{
                position: 'absolute',
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'transparent',
              }}>
              <View>
                <ActivityIndicator size="large" color="#000" />
              </View>
            </View>
          ) : null}
        </View>
      </TouchableWithoutFeedback>
    </SafeAreaView>
  );
};
export default Search;
