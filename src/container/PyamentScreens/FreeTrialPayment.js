import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  Modal,
  ScrollView,
  Linking,
  ActivityIndicator,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {h, w} from '../../utils/Dimensions';
// import Styles from './Style';
import {Pages} from 'react-native-pages';
import {connect} from 'react-redux';
// import Style from './Style'
import AsyncStorage from '@react-native-community/async-storage';
import {getProductList} from './../../actions/ProductList';
import {useSelector, useDispatch} from 'react-redux';
import {Platform} from 'react-native';
import {Subscription} from './.../../../../actions/subscription';
import {FreeSubscription} from './.../../../../actions/FreeSubsPlan';
import RNIap, {
  Product,
  ProductPurchase,
  PurchaseError,
  acknowledgePurchaseAndroid,
  purchaseErrorListener,
  purchaseUpdatedListener,
  requestSubscription,
  finishTransaction,
  finishTransactionIOS,
  SubscriptionPurchase,
} from 'react-native-iap';
const itemSubs = Platform.select({
  ios: ['monthly_subscription_foodid01', 'yearly_subscription_foodid'], //IOS Products
  android: ['monthly_foodid.01', 'yearly_foodid.01'], // Android Products
});
const FreeTrialPayment = ({navigation}) => {
  const dispatch = useDispatch();

  const [selected, setselected] = useState('');
  const [Products, setProducts] = useState(['']);
  const [ID, setID] = useState('');
  const [planList, setplanList] = useState(['']);
  const [uiRender, setUiRender] = useState(false);
  const [Token, setToken] = useState('');
  const [Loader, setLoader] = useState(false);
  let purchaseUpdateSubscription = null;
  let purchaseErrorSubscription = null;

  const GetPlanList = useSelector((state) => state.productlist.PRODUCTLIST);
  const subscriptionLoading = useSelector(
    (state) => state.subscription.isLoading,
  );
  const productlistLoading = useSelector(
    (state) => state.productlist.isLoading,
  );
  const FreeSubscriptionLoading = useSelector(
    (state) => state.freeSubs.isLoading,
  );
  console.log('GetPlanList', GetPlanList);

  useEffect(() => {
    ProductList();
    subscriptionstart();
    getSubscriptions();
  }, []);

  useEffect(() => {
    //Get Plan List Function
    if (GetPlanList) {
      for (let item of GetPlanList) {
        item.selected = false;
      }
      for (let data of GetPlanList) {
        if (data.id === '2') {
          data.selected = true;
          setID(data.id);
        }
      }
      setUiRender(!uiRender);
      setplanList(GetPlanList);
    }
  }, [GetPlanList]);

  const subscriptionstart = async () => {
    //Start Payment Function
    try {
      const result = await RNIap.initConnection();
      await RNIap.flushFailedPurchasesCachedAsPendingAndroid();
      console.log('result', result);
    } catch (err) {
      console.warn(err.code, err.message);
    }

    purchaseUpdateSubscription = purchaseUpdatedListener(
      async (purchase: InAppPurchase | SubscriptionPurchase) => {
        console.log('SubscriptionPurchase', SubscriptionPurchase);
        const receipt = purchase.transactionReceipt;
        console.log('receipt', receipt);
        setLoader(false);

        if (receipt) {
          setLoader(false);

          try {
            setLoader(false);

            if (Platform.OS === 'ios') {
              finishTransactionIOS(purchase.transactionId);
            } else if (Platform.OS === 'android') {
              // If consumable (can be purchased again)
              consumePurchaseAndroid(purchase.purchaseToken);
              // If not consumable
              acknowledgePurchaseAndroid(purchase.purchaseToken);
            }
            const ackResult = await finishTransaction(purchase);
            console.log('ack', ackResult);
            console.log('purchase', purchase);
            const token = await AsyncStorage.getItem('token');
            setLoader(false);

            if (Platform.OS === 'ios') {
              const product_id = purchase.productId;
              const device_type = 'ios';
              dispatch(Subscription(token, device_type, product_id , navigation));
            }
            if (Platform.OS === 'android') {
              const product_id = purchase.productId;
              const device_type = 'android';
              dispatch(Subscription(token, device_type, product_id , navigation));
            }
          } catch (ackErr) {
            console.warn('ackErr', ackErr);
          }
        }
      },
    );

    purchaseErrorSubscription = purchaseErrorListener(
      (error: PurchaseError) => {
        console.log('purchaseErrorListener', error);
        setLoader(false);
      },
    );
    setLoader(false);
  };

  const getSubscriptions = async (): void => {
    //getSubscriptions function
    try {
      const products = await RNIap.getSubscriptions(itemSubs);
      console.log('Productsgetting', products);
      setProducts(products);
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  const requestSubscriptions = async (sku: string): void => {
    try {
      setLoader(true);
      console.log('productss', Products[0].productId);
      if (ID === '2' || ID === 2) {
        RNIap.requestSubscription(Products[0].productId);
      } else if (ID === '3' || ID === 3) {
        RNIap.requestSubscription(Products[1].productId);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const SelectButton = (currentTab) => {
    console.log('currentTab', currentTab);
    const id = currentTab.id;
    setID(id);
    for (let item of planList) {
      if (item.id === currentTab.id) {
        item.selected = true;
      } else {
        item.selected = false;
      }
    }
    setUiRender(!uiRender);
  };

  const ProductList = async () => {
    const token = await AsyncStorage.getItem('token');
    console.log('token', token);
    setToken(token);
    dispatch(getProductList(token)); //Calling Api to get Product list
  };

  const inApp = () => {
    if (ID === '1' || ID === 1) {
      if (Platform.OS === 'ios') {
        dispatch(FreeSubscription(Token, 'ios', 'free_subscription_foodid' , navigation));
      } else if (Platform.OS === 'android') {
        dispatch(
          FreeSubscription(Token, 'android', 'free_subscription_foodid' , navigation),
        );
      }
    } else if (ID === '2' || ID === 2) {
      requestSubscriptions();
    } else if (ID === '3' || ID === 3) {
      requestSubscriptions();
    }
  };
  console.log('loader', Loader);
  //Ui for payment Screen
  return (
    <ScrollView bounces={false} style={{backgroundColor: '#fff'}}>
      <View style={{backgroundColor: '#fff'}}>
        <Image
          style={{
            width: h(36),
            height: h(17.5),
            resizeMode: 'contain',
            right: w(4),
          }}
          source={require('./../../assets/icon/favlistyellow.png')}
        />
        <Text
          style={{
            fontSize: 20,
            alignSelf: 'center',
            marginTop: h(6),
            color: '#383B3F',
            fontWeight: '500',
          }}>
          Choose Your Plan
        </Text>
        {/* <Text style={{ width: w(70), alignSelf: 'center', marginLeft: w(18), marginTop: h(5), fontSize: 16, color: '#8c8c8c' }}>Subscribe now for the bells and {'\n'}whistles or try a trial first. Enjoy {'\n'}scans, journal entries, and fave {'\n'}food saves. But mostly, avoid {'\n'}ingredients easily and try new {'\n'}<Text style={{ marginLeft: w(2) }}>products stress-free.</Text></Text> */}
        {planList.length !== 2 ? (
          <Text
            style={{
              color: '#8C8C8C',
              fontSize: 16,
              marginTop: h(5),
              backgroundColor: '#fff',
              textAlign: 'center',
              marginLeft: w(15),
              marginRight: w(15),
            }}>
            Subscribe now for the whole kit {'\n'}and caboodle or trial us
            first.
            {'\n'} Enjoy scans, product searches.{'\n'}food journaling, and fave
            food {'\n'} saves. Avoid ingredients and try{'\n'}packaged goods
            stress-free.
          </Text>
        ) : (
          <Text
            style={{
              color: '#8C8C8C',
              fontSize: 16,
              marginTop: h(5),
              backgroundColor: '#fff',
              textAlign: 'center',
              marginLeft: w(15),
              marginRight: w(15),
            }}>
            Your free trial got eaten up{'\n'}like a snack. Subscribe now to
            {'\n'}search and scan products{'\n'}endlessly. Journal all your
            {'\n'}foods. And save as many faves{'\n'}as you please.
          </Text>
        )}
        <View
          style={{
            marginTop: h(5),
          }}>
          <FlatList
            // scrollEnabled={false}
            data={planList}
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            style={{alignSelf: 'center'}}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() => SelectButton(item)}
                style={{
                  width: h(13.5),
                  height: item.selected ? h(21) : h(21),
                  backgroundColor: '#F7F7F7',
                  borderRadius: h(3),
                  borderColor: item.selected ? '#FE6F69' : '#8c8c8c',
                  borderWidth: item.selected ? w(0.5) : w(0),
                  marginLeft: index === 0 ? 0 : w(5),
                }}>
                <Text
                  style={{
                    fontSize: h(2.2),
                    color: item.selected ? '#383B3F' : '#8c8c8c',
                    alignSelf: 'center',
                    marginTop: h(2),
                    fontWeight: '600',
                  }}>
                  {item.plan_text}
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    alignSelf: 'center',
                    marginTop: h(1),
                  }}>
                  <Text
                    style={{
                      fontSize: 10,
                      color: item.selected ? '#383B3F' : '#8c8c8c',
                      top: h(0.3),
                    }}>
                    $
                  </Text>

                  <Text
                    style={{
                      fontSize: 20,
                      color: item.selected ? '#383B3F' : '#8c8c8c',
                      alignSelf: 'center',
                    }}>
                    {item.amount}
                  </Text>
                </View>
                <View
                  style={{
                    height: h(0.2),
                    backgroundColor: '#8c8c8c',
                    width: h(7),
                    alignSelf: 'center',
                    marginTop: h(1),
                  }}></View>
                <Text
                  style={{
                    color: item.selected ? '#383B3F' : '#8c8c8c',
                    alignSelf: 'center',
                    marginTop: h(1),
                    fontSize: 16,
                  }}>
                  {item.name}
                </Text>
                {item.selected ? (
                  <View
                    style={{
                      backgroundColor: '#FE6F69',
                      height: 35,
                      width: '100%',
                      position: 'absolute',
                      bottom: 0,
                      borderBottomRightRadius: 20,
                      borderBottomLeftRadius: 20,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Image
                      source={require('../../assets/icon/righttick-icon.png')}
                      style={{height: 15, width: 15, tintColor: '#fff'}}
                    />
                  </View>
                ) : null}
                {/* {yearlySelected ?
            <View style={{ backgroundColor: 'red', width: w(17), height: h(3), marginTop: h(1.5) }}>
            </View> : null} */}
              </TouchableOpacity>
            )}
          />
        </View>

        <TouchableOpacity
          style={{
            alignSelf: 'center',
            backgroundColor: '#FE6F69',
            width: w(75),
            height: h(5),
            borderRadius: w(10),
            marginTop: h(6),
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={inApp}
          // onPress = {getPurchases}
        >
          <Text
            style={{
              fontSize: 16,
              // alignSelf: 'center',
              color: '#fff',
              // marginTop: h(1.2),
            }}>
            Purchase Plan
          </Text>
        </TouchableOpacity>
      </View>
      <View>
        {Platform.OS === 'ios' ? (
          <Text
            style={{
              lineHeight: 16,
              alignSelf: 'center',
              width: w(65),
              fontSize: 12,
              color: '#8c8c8c',
              marginTop: h(5),
              fontWeight: 'bold',
            }}>
            Your iTunes Account will be charged at confirmation of purchase.
            Subscription will be auto-renewed. You can manage subscriptions &
            turn off in Account Settings. Read our{' '}
            <TouchableOpacity
              onPress={() =>
                Linking.openURL(
                  'https://www.apple.com/in/legal/internet-services/itunes/in/terms-en.html',
                )
              }
              style={{marginTop: h(-0.3)}}>
              <Text
                style={{color: '#6863ff', fontSize: 12, fontWeight: 'bold'}}>
                Terms of Use.
              </Text>
            </TouchableOpacity>
          </Text>
        ) : null}
        <View style={{height: 60}} />
      </View>
      {subscriptionLoading ||
      productlistLoading ||
      FreeSubscriptionLoading ||
      Loader ? (
        <View
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 5,
            bottom: 0,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'transparent',
          }}>
          <View>
            <ActivityIndicator size="large" color="#000" />
          </View>
        </View>
      ) : null}
    </ScrollView>
  );
};

export default FreeTrialPayment;
