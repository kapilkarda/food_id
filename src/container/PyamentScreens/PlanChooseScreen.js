import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  FlatList,
  Text,
  TouchableOpacity,
  ScrollView,
  Linking,
  ActivityIndicator,
  Platform,
  ImageBackground,
  BackHandler,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import {useSelector, useDispatch} from 'react-redux';
import RNIap, {
  PurchaseError,
  acknowledgePurchaseAndroid,
  purchaseErrorListener,
  purchaseUpdatedListener,
  finishTransaction,
  finishTransactionIOS,
  SubscriptionPurchase,
} from 'react-native-iap';
import {h, w} from '../../utils/Dimensions';
import {getProductList} from './../../actions/ProductList';
import {Subscription} from './.../../../../actions/subscription';

const itemSubs = Platform.select({
  ios: ['monthly_subscription_foodid01', 'yearly_subscription_foodid'],
  android: ['monthly_foodid.01', 'yearly_foodid.01'],
});

const PlanChooseScreen = ({navigation}) => {
  const dispatch = useDispatch();

  const [Products, setProducts] = useState(['']);
  const [ID, setID] = useState('');
  const [planList, setplanList] = useState(['']);
  const [uiRender, setUiRender] = useState(false);
  const [Loader, setLoader] = useState(false);

  let purchaseUpdateSubscription = null;

  const GetPlanList = useSelector((state) => state.productlist.PRODUCTLIST);
  const subscriptionLoading = useSelector(
    (state) => state.subscription.isLoading,
  );
  const productlistLoading = useSelector(
    (state) => state.productlist.isLoading,
  );

  useEffect(() => {
    ProductList();
    subscriptionstart();
    getSubscriptions();
  }, []);

  useEffect(() => {
    const backAction = () => {
      Actions.Dashboard();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    var planList = [];
    if (GetPlanList) {
      if (GetPlanList.length > 0) {
        for (let planItem of GetPlanList) {
          if (planItem.id != '1') {
            planList.push(planItem);
          }
        }
        for (let item of planList) {
          item.selected = false;
        }
        for (let data of planList) {
          if (data.id === '2') {
            data.selected = true;
            setID(data.id);
          }
        }
        setUiRender(!uiRender);
        setplanList(planList);
      }
    }
  }, [GetPlanList]);

  const subscriptionstart = async () => {
    try {
      const result = await RNIap.initConnection();
      await RNIap.flushFailedPurchasesCachedAsPendingAndroid();
    } catch (err) {
      console.warn(err.code, err.message);
    }
    purchaseUpdateSubscription = purchaseUpdatedListener(
      async (purchase: InAppPurchase | SubscriptionPurchase) => {
        console.log('SubscriptionPurchase', SubscriptionPurchase);
        const receipt = purchase.transactionReceipt;
        console.log('receipt', receipt);
        if (receipt) {
          setLoader(false);
          try {
            setLoader(false);
            if (Platform.OS === 'ios') {
              finishTransactionIOS(purchase.transactionId);
            } else if (Platform.OS === 'android') {
              // If consumable (can be purchased again)
              consumePurchaseAndroid(purchase.purchaseToken);
              // If not consumable
              acknowledgePurchaseAndroid(purchase.purchaseToken);
            }
            const ackResult = await finishTransaction(purchase);
            console.log('ack', ackResult);
            const token = await AsyncStorage.getItem('token');
            setLoader(false);
            if (Platform.OS === 'ios') {
              const product_id = purchase.productId;
              const device_type = 'ios';
              dispatch(Subscription(token, device_type, product_id));
            }
            if (Platform.OS === 'android') {
              const product_id = purchase.productId;
              const device_type = 'android';
              dispatch(Subscription(token, device_type, product_id));
            }
          } catch (ackErr) {
            console.warn('ackErr', ackErr);
          }
        }
      },
    );

    purchaseErrorSubscription = purchaseErrorListener(
      (error: PurchaseError) => {
        console.log('purchaseErrorListener', error);
        setLoader(false);

        // Alert.alert('purchase error', JSON.stringify(error));
      },
    );
    setLoader(false);

  };

  const getSubscriptions = async (): void => {
    try {
      const products = await RNIap.getSubscriptions(itemSubs);
      // console.log('Productsgetting', products);
      setProducts(products);
      // const purchases = await RNIap.getAvailablePurchases();
      // console.info('Available purchases :: ', purchases);
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  const requestSubscriptions = async (sku: string): void => {
    try {
      setLoader(true);

      console.log('productss', Products[0].productId);
      if (ID === '2' || ID === 2) {
        RNIap.requestSubscription(Products[0].productId);
      } else if (ID === '3' || ID === 3) {
        RNIap.requestSubscription(Products[1].productId);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const SelectButton = (currentTab) => {
    const id = currentTab.id;
    setID(id);
    for (let item of planList) {
      if (item.id === currentTab.id) {
        item.selected = true;
      } else {
        item.selected = false;
      }
    }
    setUiRender(!uiRender);
  };

  const ProductList = async () => {
    const token = await AsyncStorage.getItem('token');
    dispatch(getProductList(token));
  };

  const inApp = () => {
    if (ID === '1' || ID === 1) {
      AsyncStorage.setItem('toggle', JSON.stringify(true));
      Actions.TrialStart();
    } else if (ID === '2' || ID === 2) {
      requestSubscriptions();
    } else if (ID === '3' || ID === 3) {
      requestSubscriptions();
    }
  };

  return (
    <ScrollView bounces={false} style={{backgroundColor: '#fff'}}>
      <View style={{backgroundColor: '#fff'}}>
        <ImageBackground
          resizeMode="contain"
          style={{width: h(36), height: h(17.5), right: w(4)}}
          source={require('./../../assets/icon/favlistyellow.png')}>
          {/* <TouchableOpacity
            onPress={() => Actions.Dashboard()}
            style={{marginLeft: w(12), marginTop: h(5.5)}}>
            <Image
              style={{
                width: w(2),
                height: h(2),
                resizeMode: 'contain',
                tintColor: '#fff',
              }}
              source={require('./../../assets/icon/BackB.png')}
            />
          </TouchableOpacity> */}
        </ImageBackground>

        <Text
          style={{
            fontSize: 20,
            alignSelf: 'center',
            marginTop: h(6),
            color: '#383B3F',
            fontWeight: '500',
          }}>
          Choose Your Plan
        </Text>

        <Text
          style={{
            color: '#8C8C8C',
            fontSize: 16,
            marginTop: h(5),
            backgroundColor: '#fff',
            textAlign: 'center',
            marginLeft: w(15),
            marginRight: w(15),
          }}>
          Your product searches and/or{'\n'}scans are all eaten up for the{'\n'}
          day. Consider subscribing now{'\n'}for infinite access.
        </Text>

        <View style={{marginTop: h(5)}}>
          <FlatList
            // scrollEnabled={false}
            data={planList}
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            style={{alignSelf: 'center'}}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() => SelectButton(item)}
                style={{
                  width: h(13),
                  height: item.selected ? h(21) : h(21),
                  backgroundColor: '#F7F7F7',
                  borderRadius: h(3),
                  borderColor: item.selected ? '#FE6F69' : '#8c8c8c',
                  borderWidth: item.selected ? w(0.5) : w(0),
                  marginLeft: index === 0 ? 0 : w(5),
                }}>
                <Text
                  style={{
                    fontSize: 15,
                    color: item.selected ? '#383B3F' : '#8c8c8c',
                    alignSelf: 'center',
                    marginTop: h(2),
                  }}>
                  {item.plan_text}
                </Text>

                <View
                  style={{
                    flexDirection: 'row',
                    alignSelf: 'center',
                    marginTop: h(1),
                  }}>
                  <Text
                    style={{
                      fontSize: 10,
                      color: item.selected ? '#383B3F' : '#8c8c8c',
                      top: h(0.3),
                    }}>
                    $
                  </Text>
                  <Text
                    style={{
                      fontSize: 20,
                      color: item.selected ? '#383B3F' : '#8c8c8c',
                      alignSelf: 'center',
                    }}>
                    {item.amount}
                  </Text>
                </View>

                <View
                  style={{
                    height: h(0.2),
                    backgroundColor: '#8c8c8c',
                    width: h(7),
                    alignSelf: 'center',
                    marginTop: h(1),
                  }}
                />

                <Text
                  style={{
                    color: item.selected ? '#383B3F' : '#8c8c8c',
                    alignSelf: 'center',
                    marginTop: h(1),
                    fontSize: 16,
                  }}>
                  {item.name}
                </Text>

                {item.selected ? (
                  <View
                    style={{
                      backgroundColor: '#FE6F69',
                      height: 35,
                      width: '100%',
                      position: 'absolute',
                      bottom: 0,
                      borderBottomRightRadius: 20,
                      borderBottomLeftRadius: 20,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Image
                      source={require('../../assets/icon/righttick-icon.png')}
                      style={{height: 15, width: 15, tintColor: '#fff'}}
                    />
                  </View>
                ) : null}
              </TouchableOpacity>
            )}
          />
        </View>

        <TouchableOpacity
          style={{
            alignSelf: 'center',
            backgroundColor: '#FE6F69',
            width: w(75),
            height: h(5),
            borderRadius: w(10),
            marginTop: h(6),
          }}
          onPress={inApp}>
          <Text
            style={{
              fontSize: 16,
              alignSelf: 'center',
              color: '#fff',
              marginTop: h(1.2),
            }}>
            Purchase Plan
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={Actions.Dashboard}
          style={{alignSelf: 'center', marginTop: h(4)}}>
          <Text style={{color: '#FE6F69', fontSize: 16, fontWeight: '500'}}>
            Continue Trial
          </Text>
        </TouchableOpacity>
      </View>

      <View>
        {Platform.OS === 'ios' ? (
          <Text
            style={{
              lineHeight: 16,
              alignSelf: 'center',
              width: w(65),
              fontSize: 12,
              color: '#8c8c8c',
              marginTop: h(5),
              fontWeight: 'bold',
            }}>
            Your iTunes Account will be charged at confirmation of
            purchase.Subscription will be auto-renewed. You can manage
            subscriptions &turn off in Account Settings. Read our{' '}
            <TouchableOpacity
              onPress={() =>
                Linking.openURL(
                  'https://www.apple.com/in/legal/internet-services/itunes/in/terms-en.html',
                )
              }
              style={{marginTop: h(-0.3)}}>
              <Text
                style={{color: '#6863ff', fontSize: 12, fontWeight: 'bold'}}>
                Terms of Use.
              </Text>
            </TouchableOpacity>
          </Text>
        ) : null}
      </View>

      {subscriptionLoading || productlistLoading || Loader ? (
        <View
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 5,
            bottom: 0,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'transparent',
          }}>
          <View>
            <ActivityIndicator size="large" color="#000" />
          </View>
        </View>
      ) : null}
      <View style={{height: 70}} />
    </ScrollView>
  );
};

export default PlanChooseScreen;
