import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  Modal,
  ScrollView,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {h, w} from '../../utils/Dimensions';
// import Styles from './Style';
import {Pages} from 'react-native-pages';
import {connect} from 'react-redux';
// import Style from './Style'
import AsyncStorage from '@react-native-community/async-storage';

import {useSelector, useDispatch} from 'react-redux';

const TrialStart = ({navigation}) => {
  const navi = () => {
    // Actions.tabbar();
    // Actions.Dashboard();
    navigation.navigate('Dashboard')
  };
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <Image
        style={{
          width: h(36),
          height: h(17.5),
          resizeMode: 'contain',
          right: w(8),
        }}
        source={require('./../../assets/icon/Vectoryellow.png')}
      />
      <Text
        style={{
          fontSize: 20,
          alignSelf: 'center',
          marginTop: h(6),
          color: '#383B3F',
          fontWeight: '500',
        }}>
        Trial Starts Now
      </Text>
      <Text
        style={{
          alignSelf: 'center',
          marginTop: h(6),
          fontSize: 16,
          color: '#8C8C8C',
        }}>
        Yay for trying something new.{' '}
      </Text>
      <Text style={{alignSelf: 'center', fontSize: 16, color: '#8C8C8C'}}>
        Here’s the deal: you have 7 days
      </Text>
      <Text style={{alignSelf: 'center', fontSize: 16, color: '#8C8C8C'}}>
        free to try 5 scans and
      </Text>
      <Text style={{alignSelf: 'center', fontSize: 16, color: '#8C8C8C'}}>
        5 searches a day. We'll give you
      </Text>
      <Text style={{alignSelf: 'center', fontSize: 16, color: '#8C8C8C'}}>
        a heads up when your week
      </Text>
      <Text style={{alignSelf: 'center', fontSize: 16, color: '#8C8C8C'}}>
        is over.
      </Text>
     

      <View
        style={{
          alignSelf: 'center',
          backgroundColor: '#FE6F69',
          width: w(75),
          height: h(5),
          borderRadius: w(10),
          top: h(7),
        }}>
        <TouchableOpacity onPress={navi}>
          <Text
            style={{
              fontSize: 16,
              alignSelf: 'center',
              color: '#fff',
              marginTop: h(1.2),
            }}>
            Got It!
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default TrialStart;
