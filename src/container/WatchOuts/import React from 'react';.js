import React from 'react';
import {Scene, Router, Stack, Drawer} from 'react-native-router-flux';
import {Dimensions, Platform, Image} from 'react-native';
import DrawerBar from './component/Drawer/Drawer';
import Splash from './container/Splash/Splash';
import Instruction from './container/Instruction/instruction';
import join from './container/Join In/join';
import join2 from './container/Join In/join2';
import join3 from './container/Join In/join3';
import Otp from './container/Otp Verification/Otp';
import Login from './container/login/Login';
import {connect} from 'react-redux';
import passwordChange from './container/passwordChange/passwordChange';
import Dashboard from './container/Dashboard/Dashboard';
import Addingra from './container/Addingra/Addingra';
import Search from './container/Search/Search';
import Watchout from './container/WatchOuts/Watchout';
import {height} from './utils/Dimensions';
import Dictionary from './container/Dictionary/Dictionary';
import User_Details from './container/User_Details/User_Details';
import Calendar from './container/Calendar/Calendar';
import Scan from './container/Scan/Scan';
import Caseines from './container/Caseines/Caseines';
var width = Dimensions.get('window').width;

const RouterWithRedux = connect()(Router);
var image;
var tintcolor;
const TabIcon = ({selected, title, img, focused}) => {
  switch (title) {
    case 'Dashboard':
      image = focused
        ? require('./assets/icon/home.png')
        : require('./assets/icon/home.png');
      tintcolor = focused ? '#df396b' : '#000';

      break;

    case 'Addingra':
      image = focused
        ? require('./assets/icon/Curate_Outlined.png')
        : require('./assets/icon/Curate_Outlined.png');
      tintcolor = focused ? '#df396b' : '#000';
      break;

    case 'Scan':
      image = focused
        ? require('./assets/icon/Scan_Button.png')
        : require('./assets/icon/Scan_Button.png');
      tintcolor = focused ? '#df396b' : '#000';
      break;

    case 'Calendar':
      image = focused
        ? require('./assets/icon/Calendar.png')
        : require('./assets/icon/Calendar.png');
      tintcolor = focused ? '#df396b' : '#000';
      break;
    case 'User_Details':
      image = focused
        ? require('./assets/icon/Filled_Profile.png')
        : require('./assets/icon/Filled_Profile.png');
      tintcolor = focused ? '#df396b' : '#000';
      break;
  }
  return (
    <Image
      source={image}
      style={{width: h(4), height: h(4), tintColor: tintcolor}}
      resizeMode="contain"></Image>
  );
};
class Root extends React.Component {
  render() {
    return (
      <RouterWithRedux>
        <Scene key="root" hideTabBar hideNavBar>
          <Stack key="app">
            <Scene hideNavBar panHandlers={null}>
              <Scene
                initial={true}
                component={Splash}
                hideNavBar={true}
                key="Splash"
                title="Splash"
              />

              <Scene
                component={Instruction} //classname
                hideNavBar={true}
                wrap={false}
                key="Instruction"
                title="Instruction"
              />

              <Scene
                component={join} //classname
                hideNavBar={true}
                wrap={false}
                key="join"
                title="join"
              />
              <Scene
                component={join2} //classname
                hideNavBar={true}
                wrap={false}
                key="join2"
                title="join2"
              />

              <Scene
                component={join3} //classname
                hideNavBar={true}
                wrap={false}
                key="join3"
                title="join3"
              />
              <Scene
                component={Otp} //classname
                hideNavBar={true}
                wrap={false}
                key="Otp"
                title="Otp"
              />

              <Scene
                component={Login} //classname
                hideNavBar={true}
                wrap={false}
                key="Login"
                title="Login"
              />
              <Scene
                component={passwordChange} //classname
                hideNavBar={true}
                wrap={false}
                key="passwordChange"
                title="passwordChange"
              />
              <Scene
                component={Dashboard} //classname
                hideNavBar={true}
                wrap={false}
                key="Dashboard"
                title="Dashboard"
              />
              <Scene
                component={Addingra} //classname
                hideNavBar={true}
                wrap={false}
                key="Addingra"
                title="Addingra"
              />

              <Scene
                component={Search} //classname
                hideNavBar={true}
                wrap={false}
                key="Search"
                title="Search"
              />

              <Scene
                component={Watchout} //classname
                hideNavBar={true}
                wrap={false}
                key="Watchout"
                title="Watchout"
              />
              <Scene
                component={Dictionary} //classname
                hideNavBar={true}
                wrap={false}
                key="Dictionary"
                title="Dictionary"
              />
              <Scene
                component={Caseines} //classname
                hideNavBar={true}
                wrap={false}
                key="Caseines"
                title="Caseines"
              />

              <Drawer
                hideNavBar
                key="drawer"
                contentComponent={DrawerBar}
                drawerWidth={width - 100}>
                {/* <Scene
                  key="tabbar"
                  tabs
                  tabBarStyle={{
                    backgroundColor: '#fff',
                    borderBottomLeftRadius: 15,
                    borderBottomRightRadius: 15,
                    paddingVertical: 15,
                  }}> */}
                  <Scene title="Dashboard" icon={TabIcon} img={image}>
                    <Scene
                      component={Dashboard} //classname
                      hideNavBar={true}
                      wrap={false}
                      key="Dashboard"
                      title="Dashboard"
                    />
                  </Scene>
                  </Drawer>
                  <Scene title="Addingra" icon={TabIcon} img={image}>
                    <Scene
                      component={Addingra} //classname
                      hideNavBar={true}
                      wrap={false}
                      key="Addingra"
                      title="Addingra"
                    />
                  </Scene>
                  <Scene title="Scan" icon={TabIcon} img={image}>
                    <Scene
                      component={Scan} //classname
                      hideNavBar={true}
                      wrap={false}
                      key="Scan"
                      title="Scan"
                    />
                  </Scene>
                  <Scene title="Calendar" icon={TabIcon} img={image}>
                    <Scene
                      component={Calendar} //classname
                      hideNavBar={true}
                      wrap={false}
                      key="Calendar"
                      title="Calendar"
                    />
                  </Scene>
                  <Scene title="User_Details" icon={TabIcon} img={image}>
                    <Scene
                      component={User_Details} //classname
                      hideNavBar={true}
                      wrap={false}
                      key="User_Details"
                      title="User_Details"
                    />
                  </Scene>
                {/* </Scene> */}
             
            </Scene>
          </Stack>
        </Scene>
      </RouterWithRedux>
    );
  }
}

export default Root;
console.disableYellowBox = true;
