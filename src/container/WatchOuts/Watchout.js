import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  Modal,
  ImageBackground,
  ScrollView,
  SafeAreaView,
  Keyboard,
  BackHandler,
  Animated,
  ActivityIndicator,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {IconAsset, Strings, UiColor} from '../../theme';
import {h, w, width} from '../../utils/Dimensions';
//import Styles from './Style';
import {Pages} from 'react-native-pages';
import {connect} from 'react-redux';
import Styles from './Style';
import RadioButtonRN from 'radio-buttons-react-native';
import {ExpandableListView} from 'react-native-expandable-listview';
import {SwipeListView} from 'react-native-swipe-list-view';
import AsyncStorage from '@react-native-community/async-storage';
import {useSelector, useDispatch} from 'react-redux';
import {Touchable} from 'react-native';
import {curatWatchoutShowApi} from './../../actions/curatWatchOutShow';
import {removeCuratAPI} from './../../actions/RemoveCurat';
import {addWatchOutApi} from './../../actions/addCuratWatchout';
import Swipeout from 'react-native-swipeout';
import DeleteBUTTON from './../Custom Delete/CustomDelete';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import {UndoCurat} from './../../actions/UndoCurat';
import {withNavigationFocus} from 'react-navigation';
var newArray = [];

const Watchout = ({navigation}) => {
  const dispatch = useDispatch();
  const screenStatus = navigation.isFocused();

  const watchoutss = useSelector(
    (state) => state.curatewatchoutshow.CURATESHOW,
  );
  console.log('watchoutss', watchoutss);

  const RemoveCurateData = useSelector((state) =>
    state.removecurat.REMOVECURAT ? state.removecurat.REMOVECURAT.data : null,
  );

  const [swipeListShow, setswipeListShow] = useState(false);
  const [search_value, setsearch_value] = useState(null);
  const [uiRender, setuiRender] = useState(false);
  const [show, setshow] = useState(false);
  const [WatchoutList, setWatchoutList] = useState([]);
  const [oldId, setOldId] = useState([0]);
  const subCategoryItem = [];

  const curatewatchoutshowLoading = useSelector(
    (state) => state.curatewatchoutshow.isLoading,
  );
  const removeCuratWatchoutLoading = useSelector(
    (state) => state.removecurat.isLoading,
  );
  const undoCuratWatchoutLoading = useSelector(
    (state) => state.undowatchout.isLoading,
  );
  const removeCuratResData = useSelector((state) => state.removecurat);

  useEffect(() => {
    firstApi();
  }, []);

  useEffect(() => {
    if (watchoutss) {
      if (watchoutss.data) {
        newArray = [];
        for (var item of watchoutss.data) {
          if (item.type == 'category') {
            newArray.push(item);
            for (var subItem of item.subcategory) {
              newArray.push(subItem);
            }
          } else {
            newArray.push(item);
          }
        }
        if (search_value !== null && search_value.length > 0) {
          for (let item of newArray) {
            if (item.type === 'category') {
              item.Expand = true;
              let newid = [...oldId];
              newid.push(item.item_id);
              setOldId(newid);
            }
          }
        }
      }
      setuiRender(!uiRender);
    }
  }, [watchoutss]);

  const dataFunc = (varItem) => {
    let demoArray = [...newArray];
    let tempdata = [];
    for (var item of demoArray) {
      if (item.item_id === varItem.is_parent) {
        for (let data of item.subcategory) {
          if (data.name === varItem.name) {
            const index1 = item.subcategory.indexOf(data);
            item.subcategory.splice(index1, 1);
          }
        }
      }
    }
    for (let data of demoArray) {
      if (data.subcategory && data.subcategory.length > 0) {
        tempdata.push(data);
        for (let subItem of data.subcategory) {
          tempdata.push(subItem);
        }
      }
    }
    newArray = tempdata;
    setuiRender(!uiRender);
  };

  useEffect(() => {
    newArray = [];
  }, []);

  useEffect(() => {
    setWatchoutList(watchoutss);
  });

  const watchoutList = async (search_value) => {
    const token = await AsyncStorage.getItem('token');
    dispatch(curatWatchoutShowApi(token, search_value));
  };

  const SearchwatchoutList = async (search_value) => {
    if (
      search_value.length === 0 ||
      search_value === null ||
      search_value === ''
    ) {
      setOldId([0]);
    }
    setsearch_value(search_value);
    const token = await AsyncStorage.getItem('token');
    dispatch(curatWatchoutShowApi(token, search_value));
  };

  const firstApi = async (RemoveCurateData) => {
    const token = await AsyncStorage.getItem('token');
    dispatch(curatWatchoutShowApi(token, search_value, RemoveCurateData));
  };

  const subCategorys = (item, index) => {
    setuiRender(!uiRender);
    if (item.type === 'category') {
      let tempArray = [...oldId];
      for (let oldItem of tempArray) {
        if (item.item_id === oldItem) {
          const itemIndex = tempArray.indexOf(oldItem);
          tempArray.splice(itemIndex, 1);
          setOldId(tempArray);
          newArray[index].Expand = false;
        } else {
          let newid = [...oldId];
          newid.push(item.item_id);
          newArray[index].Expand = true;
          setOldId(newid);
        }
      }
    }
  };

  const removecurat = async (item, index) => {
    const token = await AsyncStorage.getItem('token');
    const type = item.type;
    const item_id = item.item_id;
    const index1 = newArray.indexOf(item);
    newArray.splice(index1, 1);
    setuiRender(!uiRender);
    dispatch(removeCuratAPI(token, type, item_id, search_value));
    setswipeListShow(false);
    dataFunc(item);

    setTimeout(() => {
      setshow(true);
    }, 1000);
    setTimeout(() => {
      setshow(false);
      setuiRender(!uiRender);
    }, 5000);
  };

  const undo = async () => {
    console.log('RemoveCurateData', RemoveCurateData);
    let tempSelectedItems = [];
    for (let item of newArray) {
      if (item.type === 'category') {
        if (item.Expand === true) {
          tempSelectedItems.push(item);
        }
      }
    }
    const token = await AsyncStorage.getItem('token');
    dispatch(
      UndoCurat(token, RemoveCurateData, search_value, tempSelectedItems),
    );

    // firstApi(RemoveCurateData);
    setshow(false);
  };

  const KeyBoardDrawer = () => {
    Keyboard.dismiss();
    Actions.drawerOpen();
  };

  useEffect(() => {
    const backAction = () => {
      Actions.Dashboard();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, [screenStatus]);

  const checkIdMatch = (item) => {
    for (let olditem of oldId) {
      if (olditem == item) {
        return true;
      }
    }
    return false;
  };

  const RightActions = (progress, dragX, item) => {
    const scale = dragX.interpolate({
      inputRange: [-100, 0],
      outputRange: [0.3, 0],
    });
    return (
      <>
        <Animated.View
          style={{
            backgroundColor: '#FE6F69',
            height: '100%',
            transform: [{translateX: scale}],
          }}>
          <Image
            style={{
              height: h(2),
              width: h(2),
              right: w(5),
              marginLeft: w(105),
              tintColor: '#fff',
              marginTop: h(2.4),
            }}
            source={require('./../../assets/icon/dustbin.png')}
          />
        </Animated.View>
      </>
    );
  };
  console.log('newArray', newArray);
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <Image
        style={Styles.yellowimage}
        source={require('../../assets/icon/Vectoryellow.png')}
      />
      <Image
        style={Styles.milkimage}
        source={require('../../assets/icon/Definition_Artichoke.png')}
      />
      <View style={{flexDirection: 'row', marginTop: h(3.5)}}>
        <TouchableOpacity onPress={KeyBoardDrawer}>
          <Image
            style={Styles.DrawerIcon}
            source={require('../../assets/image/Hamburger_Nav.png')}
          />
        </TouchableOpacity>

        <TextInput
          style={Styles.inputFieldContainer}
          placeholderTextColor="#000"
          underlineColorAndroid="transparent"
          placeholder="Search Your Watchouts"
          autoCapitalize="none"
          //keyboardType=""
          underlineColorAndroid="transparent"
          onChangeText={(search_value) => setsearch_value(search_value)}
          onSubmitEditing={() => SearchwatchoutList(search_value)}
          value={search_value}
        />
      </View>
      <ScrollView style={{marginTop: h(4), marginBottom: h(7)}}>
        <View
          style={{
            backgroundColor: '#9a9a9a',
            marginTop: h(5),
            marginBottom: h(7),
          }}>
          <FlatList
            data={newArray}
            renderItem={({item, index}) => (
              <Swipeable
                overshootRight={false}
                onSwipeableRightWillOpen={() => {
                  removecurat(item, index);
                }}
                // renderLeftActions={LeftActions}
                renderRightActions={(progress, dragX) =>
                  RightActions(progress, dragX, item)
                }>
                <View>
                  {(checkIdMatch(item.is_parent) ||
                    //.is_parent === oldCatId ||
                    item.is_parent === undefined) && (
                    <TouchableOpacity
                      activeOpacity={1}
                      onPress={() => subCategorys(item, index)}>
                      <View
                        style={{
                          flexDirection: 'row',
                          backgroundColor:
                            item.type === 'category' ? '#fcbe2e' : '#ebebeb',
                          height: h(6.8),
                          width: '110%',
                          marginTop: -3,
                          borderBottomWidth: w(0.4),
                          borderColor: '#9a9a9a',
                          borderTopRightRadius: h(8),
                          borderBottomRightRadius: h(10),
                          paddingLeft: w(3),
                        }}>
                        {item.type == 'category' && (
                          <View
                            style={{
                              width: h(3.5),
                              height: h(3.5),
                              backgroundColor: '#383B3F',
                              borderRadius: h(5),
                              justifyContent: 'center',
                              alignItems: 'center',
                              alignSelf: 'center',
                            }}>
                            <Text
                              style={{
                                color: '#fff',
                                alignSelf: 'center',
                                fontSize: h(2.2),
                                fontWeight: '500',
                              }}>
                              {item.first_name}
                            </Text>
                          </View>
                        )}
                        {item.type == 'category' && (
                          <View>
                            <TouchableOpacity
                              onPress={() => subCategorys(item, index)}
                              style={{left: w(79), top: h(2.5)}}>
                              {item.Expand ? (
                                <Image
                                  style={{width: h(2), height: h(2)}}
                                  source={require('./../../assets/image/processed.png')}
                                />
                              ) : (
                                <Image
                                  style={{width: h(2), height: h(2)}}
                                  source={require('./../../assets/icon/menu-01.png')}
                                />
                              )}
                            </TouchableOpacity>
                          </View>
                        )}
                        <Text
                          style={{
                            marginLeft:
                              item.type == 'category' ? w(-1.5) : w(11),
                            marginTop: h(2),
                            color: '#383B3F',
                            fontSize: h(2.1),
                            fontWeight: '500',
                          }}>
                          {item.name}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  )}
                </View>
              </Swipeable>
            )}
          />
        </View>
      </ScrollView>
      {show ? (
        <View
          style={{
            alignSelf: 'center',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#000',
            width: h(15),
            height: h(4),
            borderRadius: w(10),
            marginLeft: w(77),
            top: h(-4),
          }}>
          <TouchableOpacity onPress={undo}>
            <Text
              style={{
                fontSize: 14,
                alignSelf: 'center',
                color: '#EBEBEB',
                // marginTop: h(0.8),

                fontWeight: '600',
                marginRight: w(2),
              }}>
              Undo{' '}
            </Text>
          </TouchableOpacity>
        </View>
      ) : null}
      {curatewatchoutshowLoading ||
      removeCuratWatchoutLoading ||
      undoCuratWatchoutLoading ? (
        <View
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'transparent',
          }}>
          <View>
            <ActivityIndicator size="large" color="#000" />
          </View>
        </View>
      ) : null}
    </View>
  );
};
export default withNavigationFocus(Watchout);
