import {StyleSheet} from 'react-native';
import {h, w} from '../../utils/Dimensions';

export default StyleSheet.create({
  MainContainer: {
    flex: 1,
  },
  yellowimage: {
    width: w(75),
    height: h(22),
    position: 'absolute',
    resizeMode: 'stretch',
  },
  milkimage: {
    alignSelf: 'center',
    marginTop: h(4.8),
    width:h(16),
    height:h(20)
  },
  DrawerIcon:
{ 
  width: h(2.3),
  height: h(2.3),
  marginLeft : w(5),
  resizeMode:'contain',
  // right:h(2),
  // marginTop:h(5)
  marginTop:h(5.1),
  // position:'absolute',
  tintColor:'#383B3F'

},
inputFieldContainer: {
  // alignSelf: 'center',
  borderWidth: w(0.28),
  width: w(75),
  height: h(5),
  borderRadius: w(10),
  marginTop: h(3.5),
  paddingLeft: w(5),
  borderColor:'#383B3F',
  marginLeft:w(7)
  },
  
})
