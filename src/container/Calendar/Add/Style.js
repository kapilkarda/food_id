import {StyleSheet} from 'react-native';
import {h, w} from '../../../utils/Dimensions';

export default StyleSheet.create({
  MainContainer: {
    flex: 1,
  },
  HeaderStyle:
      
        {
            // flexDirection:'row',
            marginLeft:w(10),
            marginTop:h(10),
            justifyContent:'space-between'
        },
        
      milkimage: {
        marginTop: h(9),
        alignSelf:'center',
        height:h(25),
    width:w(50),
    resizeMode:'contain'
      },
      buttonContainer: {
        alignSelf: 'center',
        backgroundColor: '#fe6f69',
        width: w(75),
        height: h(5),
        borderRadius: w(10),
        marginTop: h(2),
      },
      AndText: {
        fontSize: 16,
        alignSelf: 'center',
        color: '#FBFAFA',
        marginTop: h(1.2),
        fontWeight:'500'
      },
})