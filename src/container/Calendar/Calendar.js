import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Animated,
  FlatList,
  Modal,
  BackHandler,
  Platform,
  ActivityIndicator,
} from 'react-native';
import CalendarPicker from './CalendarPicker/index';
import {ScrollView} from 'react-native-gesture-handler';
import {Actions} from 'react-native-router-flux';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {h, height, w, width} from '../../utils/Dimensions';
import {connect} from 'react-redux';
import {Item} from 'native-base';
import moment, {locale} from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import CalendarStrip from './CalendarStrip/CalendarStrip';
import {color} from 'react-native-reanimated';
import {getExperianceApi} from '../../actions/ExperianceShow';
import {Calendar} from 'react-native-calendars';
import {SafeAreaView} from 'react-native';
import {useBackHandler} from '@react-native-community/hooks';

const local = {
  name: 'fr',
  config: {
    weekdays: 'dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi'.split('_'),
  },
};
// const local = {
//   name: 'fr',
//   config: {
//     weekdays : 'dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi'.split('_')
//   }
// }

class Calendars extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedStartDate: null,
      uiRender: false,
      shouldShow: false,
      todayDate: '', 
      DataArray: [],
      manageHeight: false,
      search_date: '',
      changeDate: null,
      disableButton: false,
      shareVisible: false,
      DateSelectSame: moment().toDate(),
      learningVisible: false,
    };
    this.onDateChange = this.onDateChange.bind(this);
  }

  componentDidMount = async () => {
    this.setState({
      changeDate: moment().format('YYYY[-]MM[-]DD'),
      // selectedStartDate:moment().format('dddd[,] MMMM D')
    });
    const todayFORMAT = moment().format('YYYY[-]MM[-]DD');
    // console.log('todayFORMAT', todayFORMAT);
    this.setState({
      todayDate: todayFORMAT,
    });
  };

  componentWillMount = async () => {
    const learning = await AsyncStorage.getItem('learningscreen');
    if (learning === '0') {
      this.setState({
        learningVisible: true,

        // todayDate: todayFORMAT,
      });
      // Actions.CalendarAdd();
    }
    let token = await AsyncStorage.getItem('token');
    const format = moment().format('dddd[,] MMM D');
    this.setState({
      token: token,
      format: format,
    });
    this.props.experianceshowCall(token, format);
    this.setState({
      changeDate: moment().format('YYYY[-]MM[-]DD'),
    });
  };

  componentWillReceiveProps = async (nextProps) => {
    // console.log('all Data', nextProps);
    const experianceData = nextProps.experianceshow.EXPERIANCESHOW;
    // console.log('experianceData', experianceData);
    if (experianceData) {
      this.setState(
        {
          DataArray: experianceData,
        },
        () => {},
      );
    }
  };

  onDateChange = async (date) => {
    //On date Change Function
    this.setState({
      uiRender: !this.state.uiRender,
    });
    const newFormat = moment(date).format('YYYY[-]MM[-]DD');
    this.setState({
      changeDate: newFormat,
    });
    const format = moment(date).format('dddd[,] MMM D');

    const newSelectformat = moment(date).format('dddd[,] MMM D YYYY');
    await AsyncStorage.setItem('format', format);
    const gatDate = moment().format('D');
    AsyncStorage.setItem('date', gatDate);
    let token = await AsyncStorage.getItem('token');
    this.setState({
      token: token,
      format: format,
    });
    this.setState(
      {
        selectedStartDate: format,
        DateSelectSame: newSelectformat,
      },
      () => {
        // console.log('format', format);
      },
    );
    this.props.experianceshowCall(token, format);
  };

  Onheight = () => {
    //Function convert calendar to week into month
    if (this.state.shouldShow === false) {
      this.setState({
        shouldShow: true,
      });
    } else {
      this.setState({
        shouldShow: false,
      });
    }
  };

  height = (item, index) => {
    this.state.DataArray[index].Expand = !this.state.DataArray[index].Expand;
    this.setState({
      uiRender: !this.state.uiRender,
    });
  };

  onPressAddIcon = () => {
    // function for navigate to entry data with date
    const startDate = this.state.selectedStartDate
      ? this.state.selectedStartDate.toString()
      : moment().format('dddd[,] MMM D');
    const todayFORMAT = moment().format('YYYY-MM-DD');
    if (this.state.changeDate == null) {
      this.setState({
        changeDate: moment().format('YYYY[-]MM[-]DD'),
      });
    }
    console.log('this.state.changeDate', this.state.changeDate);
    console.log('todayFormat', todayFORMAT);
    if (this.state.changeDate <= todayFORMAT) {
      Actions.SelectedDate(startDate);
    } else {
      this.setState({
        shareVisible: true,
      });
    }
  };

  closeModal = () => {
    //for closing model function
    this.setState({
      shareVisible: false,
    });
  };

  componentDidMount = async () => {
    BackHandler.addEventListener('hardwareBackPress', () =>
      this.hardwareBackPress(),
    );
  };

  componentWillUnmount() {
    // console.log('props', this.props);
    BackHandler.removeEventListener('hardwareBackPress', () =>
      this.hardwareBackPress(),
    );
  }

  async hardwareBackPress() {
    Actions.pop();
  }
  learning = () => {
    AsyncStorage.setItem('learningscreen', '1');
    this.setState({
      learningVisible: false,

      // todayDate: todayFORMAT,
    });
    // Actions.Calendar()
  };
 Nav = () =>
 {
  this.props.navigation.navigate('Dashboard')
 }
  render() {
    // console.log('this.state.selectedStartDate', this.state.DateSelectSame);
    const {selectedStartDate} = this.state;
    const startDate = selectedStartDate
      ? selectedStartDate.toString()
      : moment().format('dddd[,] MMM D');
    if (this.state.learningVisible === false) {
      return (
        <SafeAreaView style={{flex: 1, backgroundColor: '#383B3F'}}>
          <View style={{backgroundColor: '#ffff', height: '100%'}}>
            <ScrollView
              bounces={false}
              style={{
                backgroundColor: this.state.shareVisible ? '#fff' : '#fff',
                opacity: this.state.shareVisible ? 0.5 : 1,
                marginBottom: h(7),
              }}>
              <View style={styles.container}>
                {this.state.shouldShow ? (
                  <View
                    style={{
                      backgroundColor: '#383B3F',
                      borderBottomLeftRadius: h(4),
                    }}>
                    <CalendarPicker
                      startFromMonday={true}
                      weekdays={['M', 'T', 'W', 'T', 'F', 'S', 'S']}
                      selectedDayStyle={{
                        backgroundColor: '#fa8072',
                        width: h(3),
                        height: h(5),
                      }}
                      todayBackgroundColor="#fff"
                      months={[
                        'January',
                        'February',
                        'March',
                        'April',
                        'May',
                        'June',
                        'July',
                        'August',
                        'September',
                        'October',
                        'November',
                        'December',
                      ]}
                      todayTextStyle={{color: '#ffff'}}
                      previousTitle="Previous"
                      nextTitle="next"
                      selectedDayColor="#fa8072"
                      selectedDayTextColor="#fff"
                      scaleFactor={375}
                      textStyle={{
                        color: '#fff',
                        fontWeight: 'bold',
                      }}
                      onDateChange={this.onDateChange}
                      onMonthChange={this.onMonthChange}
                      // maxDate={maxDate}
                      maxDate={moment().toDate()}
                      selectedDate={this.state.DateSelectSame}
                      selectedStartDate={this.state.DateSelectSame}
                      // selectedDate={moment().toDate()}
                    />

                    <View>
                      {/* <Text>SELECTED DATE:{ startDate }</Text> */}
                    </View>
                  </View>
                ) : (
                  <CalendarStrip
                    style={{
                      backgroundColor: '#383B3F',
                      borderBottomLeftRadius: h(3),
                      height: h(20),
                      paddingHorizontal: 13,
                    }}
                    calendarHeaderStyle={{
                      color: '#fff',
                    }}
                    // calendarHeaderFormat={moment().format('Mo')}
                    dateNameStyle={{
                      color: '#fff',
                      fontWeight: 'bold',
                      fontSize: 12,
                    }}
                    dateNumberStyle={{
                      color: '#fff',
                      marginTop: h(2),
                      backgroundColor: '#383B3F',
                      borderRadius: 20,
                      paddingBottom: 5,
                      paddingTop: 5,
                      color: '#fff',
                      type: 'border',
                      borderWidth: 1,
                      borderHighlightColor: '#383B3F',
                      borderColor: '#383B3F',
                    }}
                    onDateSelected={this.onDateChange}
                    highlightDateNameStyle={{
                      color: '#fff',
                      fontWeight: 'bold',
                      marginTop: h(0.6),
                      fontSize: 12,
                    }}
                    highlightDateNumberContainerStyle={{
                      backgroundColor: '#fa8072',
                      borderRadius: 20,
                      marginTop: h(1.1),
                      paddingHorizontal: 2,
                    }}
                    highlightDateNumberStyle={{
                      width: w(5),
                      paddingVertical: 11,
                      color: '#fff',
                      borderRadius: 20,
                      type: 'border',
                      borderWidth: 1,
                      borderHighlightColor: '#fa8072',
                      borderColor: '#fa8072',
                      alignSelf: 'center',
                      marginRight: 1,
                    }}
                    iconLeftStyle={{
                      tintColor: '#fff',
                      marginLeft: w(3),
                      width: 15,
                      height: 15,
                    }}
                    iconRightStyle={{
                      marginLeft: w(10),
                      width: 15,
                      height: 15,
                    }}
                    maxDate={moment().toDate()}
                    selectedDate={this.state.DateSelectSame}
                    updateWeek
                  />
                )}

                <TouchableOpacity onPress={() => this.Onheight()}>
                  {this.state.shouldShow ? (
                    <Image
                      style={{
                        width: h(2),
                        height: h(2),
                        marginTop: h(3.5),
                        marginLeft: w(84),
                        resizeMode: 'contain',
                      }}
                      source={require('./../../assets/icon/Condensed_Arrow_Icon.png')}
                    />
                  ) : (
                    <Image
                      style={{
                        width: h(2),
                        height: h(2),
                        marginTop: h(3.5),
                        marginLeft: w(84),
                        resizeMode: 'contain',
                      }}
                      source={require('./../../assets/icon/Expand_Arrow_IconExpand_Carrot_Icon.png')}
                    />
                  )}
                </TouchableOpacity>

                <View style={{width: w(65), alignSelf: 'center'}}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      width: w(63),
                      alignSelf: 'flex-start',
                      marginTop: h(3),
                    }}>
                    <Text
                      style={{
                        color: '#8C8C8C',
                        fontSize: 16,
                        marginLeft: w(2.5),
                      }}>
                      {startDate}
                    </Text>
                    {this.state.DataArray.length <= 3 &&
                    this.state.DataArray.length > 0 ? (
                      <TouchableOpacity
                        onPress={() => Actions.SelectedDate(startDate)}>
                        <Image
                          style={{
                            width: h(3),
                            height: h(3),
                            resizeMode: 'contain',
                          }}
                          source={require('./../../assets/icon/plus2.png')}
                        />
                      </TouchableOpacity>
                    ) : null}
                    <Text
                      style={{
                        fontSize: 16,
                        color: '#8c8c8c',
                      }}>
                      <Text style={{color: '#FE6F69'}}>
                        {this.state.DataArray.length}
                      </Text>{' '}
                      of 4
                    </Text>
                  </View>
                  <View
                    style={{
                      width: '98%',
                      backgroundColor: '#9a9a9a',
                      // alignSelf: 'flex-start',
                      // alignSelf:'center',
                      marginLeft: w(2.5),
                      height: 1.5,
                      marginTop: h(1.3),
                    }}></View>
                </View>

                {this.state.DataArray.length == 0 ? (
                  <Text
                    style={{
                      color: '#FE6F69',
                      fontSize: 16,
                      marginLeft: w(20),
                      marginTop: h(2),
                    }}>
                    Let’s talk foods and feelings.
                  </Text>
                ) : null}
                <TouchableOpacity
                  style={{alignSelf: 'center', marginTop: h(8)}}
                  onPress={this.onPressAddIcon}>
                  {this.state.DataArray.length == 0 ? (
                    <Image
                      style={{
                        width: h(5),
                        alignSelf: 'center',
                        resizeMode: 'contain',
                      }}
                      source={require('./../../assets/icon/plus2.png')}
                    />
                  ) : null}
                </TouchableOpacity>

                <FlatList
                  data={this.state.DataArray}
                  style={{marginTop: h(-5)}}
                  renderItem={({item, index}) => (
                    <View style={{marginTop: h(3)}}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          borderBottomColor: '#8c8c8c',
                          marginTop: 10,
                          width: w(87),
                        }}>
                        <View style={{flexDirection: 'row'}}>
                          <View
                            style={{
                              width: Platform.OS === 'ios' ? h(7) : h(6),
                              height: h(1),
                              backgroundColor: '#FE6F69',
                              borderRadius: h(1),
                              alignSelf: 'center',
                              marginLeft: Platform.OS === 'ios' ? -10 : 0,
                              marginTop: h(-1),
                            }}></View>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                              width: w(68),
                              paddingBottom: 10,
                              borderBottomWidth: h(0.2),
                              borderColor: '#8C8C8C',
                              marginLeft: w(8),
                            }}>
                            <Text
                              style={{
                                color: '#FE6F69',
                                fontWeight: 'bold',
                                fontSize: 16,
                                alignSelf: 'center',
                              }}>
                              Feeling {item.feeling_description}
                            </Text>

                            <TouchableOpacity
                              onPress={() => Actions.SelectedDate(item)}
                              style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                              }}>
                              <Image
                                style={{width: h(3), height: h(3)}}
                                source={require('./../../assets/icon/Log.png')}
                              />
                            </TouchableOpacity>
                          </View>
                        </View>
                      </View>
                      {/* <View
                      style={{
                        width: w(67),
                        height: h(0.2),
                        backgroundColor: '#8C8C8C',
                        // marginLeft: w(7),
                        alignSelf: 'center',
                        marginTop: h(1.5),
                      }}></View> */}
                      <View style={{flexDirection: 'row'}}>
                        <View
                          style={{
                            width: Platform.OS === 'ios' ? h(7) : h(6),
                            height: h(1),
                            backgroundColor: '#fff',
                            borderRadius: h(1),
                            alignSelf: 'center',
                            marginLeft: Platform.OS === 'ios' ? -10 : 0,
                          }}></View>
                        <View
                          style={{
                            borderBottomWidth: 1,
                            width: w(68),
                            height: item.Expand ? h(13) : h(7),
                            alignSelf: 'center',
                            marginLeft: w(8),
                            flexDirection: 'row',
                          }}>
                          <Text
                            style={{
                              marginTop: h(1),
                              color: '#8c8c8c',
                              // alignSelf: 'center',
                              fontSize: 16,
                              width: w(64),
                            }}>
                            {item.Expand ? (
                              <>{item.food_items}</>
                            ) : (
                              <>{item.food_items.slice(0, 80)}</>
                            )}
                          </Text>
                          <TouchableOpacity
                            onPress={() => this.height(item, index)}
                            style={{
                              height: 18,
                              width: 18,
                              alignItems: 'center',
                              justifyContent: 'center',
                              marginTop: 5,
                              alignSelf: 'flex-end',
                            }}>
                            {item.Expand ? (
                              <Image
                                style={{height: h(2.5), width: h(0.8)}}
                                source={require('./../../assets/image/processed.png')}
                              />
                            ) : (
                              <Image
                                style={{height: h(2), width: h(2)}}
                                source={require('./../../assets/icon/menu-01.png')}
                              />
                            )}
                          </TouchableOpacity>
                        </View>
                      </View>
                      {/* <View
                      style={{
                        width: w(67),
                        height: h(0.2),
                        backgroundColor: '#8C8C8C',
                        marginLeft: w(20),
                        marginTop: item.Expand ? 15 : 10,
                      }}></View> */}
                    </View>
                  )}
                />
              </View>
              <Modal
                visible={this.state.shareVisible}
                transparent={true}
                animationType="slide">
                <View style={styles.modelView}>
                  <Text style={styles.modelTxT}>
                    Since eating food in the future{'\n'} is impossible, so is
                    logging it.
                  </Text>

                  <View style={styles.modelbuttonContainer}>
                    <TouchableOpacity onPress={this.closeModal}>
                      <Text style={styles.modelAndText}>Got It!</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </Modal>
              <View style={{height: h(10), width: h(10)}}></View>
            </ScrollView>
            {this.props.experianceShowLoading ? (
              <View
                style={{
                  position: 'absolute',
                  left: 0,
                  right: 0,
                  top: 5,
                  bottom: 0,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: 'transparent',
                }}>
                <View>
                  <ActivityIndicator size="large" color="#000" />
                </View>
              </View>
            ) : null}
          </View>
        </SafeAreaView>
      );
    } else {
      return (
        <ScrollView style={{backgroundColor: '#ffff'}}>
          <View style={{backgroundColor: '#ffff'}}>
            <View
              style={{
                marginLeft: w(10),
                marginTop: h(10),
                justifyContent: 'space-between',
              }}>
              <TouchableOpacity onPress={this.Nav}>
                <Image
                  style={{width: w(2), height: h(2), marginTop: h(1)}}
                  source = {require('./../../assets/icon/BackB.png')}
                />
              </TouchableOpacity>
            </View>
            <Image
              style={{
                marginTop: h(9),
                alignSelf: 'center',
                height: h(25),
                width: w(50),
                resizeMode: 'contain',
              }}
              source={require('./../../assets/image/MilkDashboard.png')}
              // source={require('../../../assets/image/MilkDashboard.png')}
            />
            <Text
              style={{
                alignSelf: 'center',
                color: '#383B3F',
                fontSize: 16,
                marginTop: h(6),
              }}>
              Jot down ingredients and how {'\n'}
            </Text>
            <Text
              style={{
                alignSelf: 'center',
                color: '#383B3F',
                fontSize: 16,
                top: h(-2),
              }}>
              they make you feel. You’ll find {'\n'}
            </Text>
            <Text
              style={{
                alignSelf: 'center',
                color: '#383B3F',
                fontSize: 16,
                top: h(-4),
              }}>
              digest regrets in no time!
            </Text>
            <TouchableOpacity
              style={{
                alignSelf: 'center',
                backgroundColor: '#fe6f69',
                width: w(75),
                height: h(5),
                borderRadius: w(10),
                marginTop: h(2),
              }}
              onPress={this.learning}>
              <Text
                style={{
                  fontSize: 16,
                  alignSelf: 'center',
                  color: '#FBFAFA',
                  marginTop: h(1.2),
                  fontWeight: '500',
                }}>
                Get After It
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    experianceshow: state.experianceshow,
    experianceShowLoading: state.experianceshow.isLoading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    experianceshowCall: (token, format) =>
      dispatch(getExperianceApi(token, format)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Calendars);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  modelView: {
    backgroundColor: '#000',
    alignSelf: 'center',
    height: h(36),
    width: w(83),
    marginTop: h(20),
    borderRadius: w(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  modelTxT: {
    color: '#fff',
    // alignSelf: 'center',
    textAlign: 'center',
    fontSize: 19,
    // marginTop: h(15),
    fontWeight: '400',
  },
  modelTxT2: {
    color: '#fff',
    alignSelf: 'center',
    fontSize: 20,
    // marginTop:h(5),
    fontWeight: '800',
  },
  modelbuttonContainer: {
    alignSelf: 'center',
    backgroundColor: '#fa8072',
    width: w(60),
    height: h(6),
    borderRadius: w(10),
    marginTop: h(4),
    justifyContent: 'center',
  },
  modelAndText: {
    fontSize: 18,
    alignSelf: 'center',
    color: '#fff',
    fontWeight: 'bold',
    // marginTop: w(3),
  },
});
