import React from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import PropTypes from 'prop-types';
import { h } from '../../../utils/Dimensions';

export default function Controls(props) {

  const {
    styles,
    textStyles,
    label,
    component,
    onPressControl,
    disabled,
    nextImage,
  } = props;

  return (
    <TouchableOpacity
      onPress={() => onPressControl()}
      style={styles}
      disabled={disabled}
      hitSlop={{ top: 20, bottom: 20 }}>
      <View style={{ opacity: disabled ? 0 : 1, marginTop: 10}}>
        {/* {component || <Text style={[textStyles]}>{label}</Text>} */}
        {nextImage == 'previousMonthImage' && (
          // <View style={{ paddingLeft: 25, alignItems: 'center' }}>
            <Image
              style={{
                width: 15,
              height: 15,
              alignSelf: 'center',
              marginBottom: 10,
              tintColor: '#fff',
              marginLeft: 25,
              }}
              source={require('./../../../assets/icon/BackB.png')}
              resizeMode="contain"
            />
          // </View>
        )}
        {nextImage == 'nextMonthImage' && (
          // <View style={{ marginRight: 145, width: '50%', alignContent: 'flex-start' }}>
            <Image
              style={{
                width: 15,
                height: 15,
                alignSelf: 'center',
                marginBottom: 10,
                marginLeft: 40,
              }}
              source={require('./../../../assets/icon/Forward_Button.png')}
              resizeMode="contain"
            />
          // </View>
        )}
      </View>
    </TouchableOpacity>
  );
}

Controls.propTypes = {
  styles: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  label: PropTypes.string,
  onPressControl: PropTypes.func.isRequired,
};
