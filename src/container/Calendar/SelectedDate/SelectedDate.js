import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Alert,
  Modal,
  ScrollView,
  ActivityIndicator,
  TouchableWithoutFeedback,
  Keyboard,
  BackHandler,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {h, w, width} from '../../../utils/Dimensions';
// import Styles from './Style';
import {Pages} from 'react-native-pages';
import {connect} from 'react-redux';
// import Style from './Style'
import Slider from 'react-native-sliders';
import AsyncStorage from '@react-native-community/async-storage';
import {SearchFood} from './../../../actions/SearchFoodForWatchout';
import {useSelector, useDispatch} from 'react-redux';
import {addExpApi} from './../../../actions/AddExperiance';
import {editexpApi} from './../../../actions//editexp';
import moment from 'moment';

const flatListArray = [];

const SelectedDate = ({navigation, ...props}) => {
  const dispatch = useDispatch();
  const EditDate = props.date;
  const Loading = useSelector((state) => state.addexperiance.isLoadingData);
  const editExpData = useSelector((state) => state.editexp.isLoadingData);

  const month = moment().format('MMMM');
  let [search_value, setsearch_value] = useState('');
  let [index, setIndex] = useState('');
  let [valueChange, setvalueChange] = useState(51);
  let [text, settext] = useState('Good');
  let [dates, setdates] = useState('');
  const [uiRender, setuiRender] = useState(false);
  const [colorChange, setcolorChange] = useState(false);
  const [shareVisible, shareSetVisible] = useState(false);

  const Date = props?props.data:'';
  const headerDate = moment(Date).format('D');
  const HeaderMonth = moment(Date).format('MMMM');
  const ID = props.id;
  console.log('ID', ID);

  const tempArray = [];
  const searchDataArray = [];
  const myTextInput = React.createRef();
  console.log('date', Date);
  const TodayDate = moment().format('D');
  console.log('TodayDate', TodayDate);

  useEffect(() => {
    flatListArray.splice(0, flatListArray.length);
  }, []);

  useEffect(() => {
    feeling();
    setuiRender(uiRender);
  }, []);

  useEffect(() => {
    getDates();
    EditElements();
  }, []);

  const getDates = async () => {
    const data = await AsyncStorage.getItem('date');
    setdates(data);
  };

  const EditElements = () => {
    // funtion to show item at edit time
    if (ID === undefined || ID === 'undefined') {
      flatListArray.splice(0, flatListArray.length);
    } else {
      const products = props.food_items.split(',');
      for (let item of products) {
        flatListArray.push(item);
      }
    }
  };

  const changeSliderValue = (value) => {
    // change slider value function
    console.log('value', Math.round(value));
    const slideProgress = value;
    setvalueChange(slideProgress);
    if (value >= 1 && value < 20) {
      settext('Horrible');
    } else if (value >= 20 && value < 40) {
      settext('Eh');
    } else if (value >= 40 && value < 55) {
      settext('Good');
    } else if (value > 55) {
      settext('Great');
    }
  };

  const elementPush = async (text) => {
    setsearch_value(text);
  };

  const clearTextField = () => {
    if (search_value != '') {
      if (flatListArray.length <= 9) {
        flatListArray.push(search_value);
        myTextInput.current.clear();
        setuiRender(!uiRender);
        setsearch_value('');
      }
    } else {
    }
  };

  const ExperianceAdd = async () => {
    //Add Experiance function / Save Entry
    if (ID === undefined || ID === 'undefined') {
      const token = await AsyncStorage.getItem('token');
      const feeling_description = text;
      const feeling_status = 1;
      const items = flatListArray;
      if (flatListArray.length != 0) {
        dispatch(
          addExpApi(token, Date, feeling_description, feeling_status, items,navigation),
        ); //Calling Add Exp Api
        if (colorChange == false) {
          setcolorChange(true);
        } else {
          setcolorChange(false);
        }
      } else {
        shareSetVisible(true);
      }
    } else if (ID != undefined || ID != 'undefined') {
      const token = await AsyncStorage.getItem('token');
      const id = props.id;
      const date = EditDate;
      const feeling_description = text;
      const feeling_status = 1;
      const items = flatListArray;
      if (flatListArray.length != 0) {
        dispatch(
          editexpApi(
            token,
            date,
            feeling_description,
            feeling_status,
            items,
            id,
          ),
        ); // Calling Edit Api
        if (colorChange == false) {
          setcolorChange(true);
        } else {
          setcolorChange(false);
        }
      } else {
        shareSetVisible(true);
      }
    }
  };

  const removeItem = (item, index) => {
    setIndex(index);
    flatListArray.splice(index, 1);
    setuiRender(!uiRender);
  };

  const feeling = () => {
    //Set Slider Value Function
    if (ID != undefined || ID != 'undefined') {
      const descri = props.feeling_description;
      if (descri === 'Horrible') {
        setvalueChange(1);
        settext('Horrible');
      } else if (descri === 'Eh') {
        setvalueChange(26);
       settext('Eh');
      } else if (descri === 'Good') {
        setvalueChange(51);
        settext('Good');
      } else if (descri === 'Great') {
        setvalueChange(75);
        settext('Great');
      }
    }
  };

  const closeModal = () => {
    shareSetVisible(false);
  };
  useEffect(() => {
    const backAction = () => {
      Actions.Calendar();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}>
      <View
        style={{
          backgroundColor: shareVisible ? '#000' : '#fff',
          opacity: shareVisible ? 0.5 : 1,
        }}>
        <Modal visible={shareVisible} transparent={true} animationType="slide">
          <View
            style={{
              backgroundColor: '#121619',
              alignSelf: 'center',
              height: h(38),
              width: w(83),
              marginTop: h(25),
              borderRadius: w(10),
            }}>
            <Text
              style={{
                color: '#fff',
                textAlign: 'center',

                fontSize: 18,
                marginTop: h(9),
                fontWeight: '400',
              }}>
              Have at least one ingredient{'\n'}entered to save this entry. But
              {'\n'}feel free to add more than that.
            </Text>

            <TouchableOpacity
              style={{
                alignSelf: 'center',
                backgroundColor: '#fa8072',
                width: w(60),
                height: h(5.5),
                borderRadius: w(10),
                marginTop: h(6),
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={closeModal}>
              <Text
                style={{
                  fontSize: 18,

                  color: '#fff',
                }}>
                Got It!
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
        <View
          style={{
            backgroundColor: '#383b3f',
            height: h(19),
            borderBottomLeftRadius: h(5),
          }}>
          <View style={{flexDirection: 'row', top: h(1)}}>
            <Text
              style={{
                color: '#fff',
                fontSize: 25,
                marginTop: h(8),
                marginLeft: w(65),
              }}>
              {HeaderMonth}
            </Text>
            <View
              style={{
                width: h(3.5),
                height: h(4.5),
                backgroundColor: '#fe726d',
                marginTop: h(8),
                borderRadius: h(2),
                left: h(1),
                justifyContent: 'center',
                alignContent: 'center',
              }}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 16,

                  alignSelf: 'center',
                }}>
                {headerDate}
              </Text>
            </View>
          </View>
          <TouchableOpacity
            style={{
              width: w(6),
              height: h(6),
              top: h(-2.5),
              marginLeft: w(10),
            }}
            onPress={Actions.Calendar}>
            <Image
              style={{
                width: w(2),
                height: h(2),
                tintColor: '#fff',
                alignSelf: 'center',
              }}
              source={require('../../../assets/icon/BackB.png')}
            />
          </TouchableOpacity>
        </View>
        <View style={{marginTop: h(7), marginLeft: w(11)}}>
          {ID === undefined ? (
            <Text
              style={{
                height: h(5),
                color: '#8c8c8c',
                fontSize: 16,
                marginLeft: w(9),
                marginTop: h(3),
              }}>
              {Date}
            </Text>
          ) : (
            <Text
              style={{
                height: h(5),
                color: '#8c8c8c',
                fontSize: 16,
                marginLeft: h(5),
                marginTop: h(3),
              }}>
              {EditDate}
            </Text>
          )}
          <TouchableOpacity
            style={{
              height: h(3.05),
              width: h(3.1),
              // marginLeft: w(65),
              // justifyContent:'flex-start',
              alignSelf: 'flex-end',
              right: w(8),
              marginTop: h(-4),
            }}
            disabled={Loading ? true : false || editExpData ? true : false}
            onPress={ExperianceAdd}>
            {colorChange ? (
              <Image
                style={{
                  height: h(2.5),
                  width: h(2.5),
                  marginTop: h(-1.5),
                  resizeMode: 'contain',
                }}
                source={require('./../../../assets/icon/FinishJornalEntryGreen.png')}
              />
            ) : (
              <Image
                style={{
                  height: h(2.5),
                  width: h(2.5),
                  marginTop: h(-1.5),
                  resizeMode: 'contain',
                }}
                source={require('./../../../assets/icon/right252.png')}
              />
            )}
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: '60%',
            height: h(0.2),
            backgroundColor: '#8C8C8C',
            top: h(0),
            // marginLeft: w(20),
            alignSelf: 'center',
          }}></View>
        <TextInput
          ref={myTextInput}
          placeholderTextColor="#9a9a9a"
          style={[
            {
              padding: h(1),
              width: w(65),
              backgroundColor: flatListArray.length >= 10 ? '#d9d9d9' : '#fff',
              // marginLeft: w(20),
              marginTop: h(5),
              fontSize: 16,
              color: '#9a9a9a',
              borderWidth: w(0.2),
              alignSelf: 'center',
              borderRadius: w(3),
            },
          ]}
          autoFocus={true}
          underlineColorAndroid="transparent"
          maxLength={15}
          placeholder="I ate ..."
          autoCapitalize="none"
          underlineColorAndroid="transparent"
          onChangeText={(text) => elementPush(text)}
          value={search_value}
          onSubmitEditing={() => clearTextField()}
          editable={flatListArray.length >= 10 ? false : true}
        />

        <View
          style={{
            borderBottomWidth: w(0.1),
            height: h(22),
            width: w(81),
            alignSelf: 'center',
            marginTop: h(1),
          }}>
          <ScrollView
            style={{width: w(65), alignSelf: 'center', marginBottom: h(1)}}>
            <TouchableOpacity
              activeOpacity={1}
              style={{flexDirection: 'row', flexWrap: 'wrap'}}>
              <Modal visible={Loading} transparent={true}>
                <View
                  style={{
                    alignSelf: 'center',
                    height: h(40),
                    width: w(80),
                    marginTop: h(50),
                  }}>
                  <ActivityIndicator
                    visible={Loading}
                    size="large"
                    color="#000"
                  />
                </View>
              </Modal>
              <Modal visible={editExpData} transparent={true}>
                <View
                  style={{
                    alignSelf: 'center',
                    height: h(40),
                    width: w(80),
                    marginTop: h(50),
                  }}>
                  <ActivityIndicator
                    visible={editExpData}
                    size="large"
                    color="#000"
                  />
                </View>
              </Modal>

              {flatListArray.map((item, flatIndex) => (
                <View
                  style={{
                    height: h(3),
                    backgroundColor: '#C4C4C4',
                    marginLeft: w(3),
                    borderRadius: h(1),
                    flexDirection: 'row',
                    marginTop: h(1),
                  }}>
                  <Text
                    style={{
                      color: '#fff',
                      alignSelf: 'center',
                      marginLeft: w(-4),
                      textAlign: 'center',
                      paddingHorizontal: h(3),
                    }}>
                    {item}
                  </Text>

                  <TouchableOpacity onPress={() => removeItem(item, flatIndex)}>
                    <Image
                      style={{height: h(3), width: h(3), borderRadius: h(1)}}
                      source={require('./../../../assets/icon/Delete_modular.png')}
                    />
                  </TouchableOpacity>
                </View>
              ))}
            </TouchableOpacity>
          </ScrollView>
        </View>

        <Text
          style={{
            fontSize: 16,
            color: '#8c8c8c',
            marginLeft: w(11),
            marginTop: h(5),
          }}>
          I felt ...
        </Text>

        <Slider
          style={{
            width: w(81),
            alignSelf: 'center',
            height: h(4),
            marginTop: h(2),
          }}
          minimumValue={1}
          maximumValue={75}
          minimumTrackTintColor="#fe726d"
          maximumTrackTintColor="#9a9a9a"
          trackStyle={{height: h(0.5)}}
          thumbStyle={{
            height: 8,
            width: 8,
            alignItems: 'center',
            justifyContent: 'center',
            paddingTop: 3,
          }}
          thumbImage={require('./../../../assets/icon/thumb.png')}
          value={valueChange}
          onValueChange={(value) => changeSliderValue(value)}
          step={25}
        />
        <View style={{flexDirection: 'row', marginTop: h(-1)}}>
          <Text style={{color: '#9a9a9a', marginLeft: w(10)}}>Horrible</Text>
          <Text style={{color: '#9a9a9a', left: w(13.5)}}>Eh</Text>
          <Text style={{color: '#9a9a9a', left: w(32)}}>Good</Text>
          <Text style={{color: '#9a9a9a', left: w(46)}}>Great</Text>
        </View>
        <View
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: '#fff',
          }}></View>
      </View>
    </TouchableWithoutFeedback>
  );
};
export default SelectedDate;
