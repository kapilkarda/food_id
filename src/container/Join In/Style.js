import { StyleSheet } from 'react-native';
import { h, w } from '../../utils/Dimensions';

export default StyleSheet.create({
  MainContainer: {
    flex: 1,
    backgroundColor: '#fff'
  },
  txt: {
    alignSelf: 'center',
    fontSize: 30,
    fontWeight: 'bold',
    marginTop: h(3),
    color: '#383B3F',
    // fontFamily:'Roboto'
  },
  inputFieldContainer: {
    // alignSelf: 'center',
    borderWidth: w(0.28),
    width: w(75),
    height: h(5),
    borderRadius: w(10),
    top: h(30),
    paddingLeft: w(5),
    left: w(3)
  },
  inputViewSecretWord: {
    width: w(75),
    height: h(5),
    borderRadius: w(10),
    borderWidth: w(0.28),
    alignSelf: 'center',
    marginTop: 55,
    flexDirection: 'row',
    alignItems: 'center'
  },
  inputFieldContainerSecretWord: {
    backgroundColor: '#fff',
    marginLeft: w(6),
    width: '75%',
  },
  eyeIcon: {
    marginLeft: w(2),
  },
  inputFieldContainerName: {
    // alignSelf: 'center',
    borderWidth: w(0.28),
    width: w(75),
    height: h(5),
    borderRadius: w(10),
    marginTop: h(6.5),
    paddingLeft: w(5),
    left: w(12.5),
    borderColor: '#383B3F'
    // left:w(3)
  },
  inputFieldContainerEmail: {
    borderWidth: w(0.28),
    width: w(75),
    height: h(5),
    borderRadius: w(10),
    marginTop: h(7),
    paddingLeft: w(5),
    left: w(12.5)
  },
  inputFieldMobile: {
    borderWidth: w(0.28),
    width: w(75),
    height: h(5),
    borderRadius: w(10),
    top: h(8),
    paddingLeft: w(5),
    left: w(12.5)
  },
  inputFieldPhone: {
    // alignSelf: 'center',
    borderWidth: w(0.28),
    width: w(75),
    height: h(5),
    borderRadius: w(10),
    top: h(38),
    paddingLeft: w(5),
    left: w(12.5)
    // left:w(3)
  },
  inputFieldContainer2: {
    borderWidth: w(0.28),
    width: w(75),
    height: h(5),
    borderRadius: w(10),
    top: h(38),
    paddingLeft: w(5),
    left: w(3)
  },
  buttonContainer: {
    alignSelf: 'center',
    backgroundColor: '#FE6F69',
    width: w(75),
    height: h(5),
    borderRadius: w(10),
    top: h(45),
  },
  buttonContainer2: {
    alignSelf: 'center',
    backgroundColor: '#FE6F69',
    width: w(75),
    height: h(5),
    borderRadius: w(10),
    top: h(42),
  },
  AndText: {
    fontSize: 16,
    alignSelf: 'center',
    color: '#fff',
    // marginTop: h(1.2),
  },
  socialButton: {
    flexDirection: 'row',
    top: h(24),
    justifyContent: 'space-evenly',
    alignItems: 'center',
    // marginLeft: w(9),
  },
  socialButton2: {
    flexDirection: 'row',
    top: h(10),
    justifyContent: 'space-evenly',
    alignItems: 'center',
    // alignSelf: 'center',
    // marginLeft: w(8),
  },
  socialButtonJoinpassword: {
    flexDirection: 'row',
    marginTop: h(10),
    justifyContent: 'space-evenly',
    alignItems: 'center',
    // alignSelf: 'center',
    marginLeft: w(5),
  },
  socialImgButton: {
    // width: w(20),
    // alignSelf: 'center',
  },
  img: {
    width: h(7),
    height: h(7.2),
    resizeMode: 'contain',
    // marginLeft:w(26)
  },
  googleimg: {
    width: h(7),
    height: h(7),
    resizeMode: 'contain',
  },
  ButtonJoined: {
    alignSelf: 'center',
    top: h(53),
  },
  ButtonJoined2: {
    alignSelf: 'center',
    top: h(15),
    marginLeft: w(5)
  },
  ButtonJoinedpass: {
    alignSelf: 'center',
    marginTop: h(2),
    marginLeft: w(3)
  },
  ButtonJoined3: {
    alignSelf: 'center',
    top: h(30),
    marginLeft: w(5)
  },
  txtJoined: {
    color: '#FE6F69',
  },
  txtJoinedjoin2: {
    color: '#FE6F69',
    marginTop: h(-4),
    alignSelf: 'center',
    marginLeft: w(-6)
  },
  errmsg: {
    color: 'red',
    marginTop: 8,
    marginLeft: w(16),
    fontSize: 10,
  },
  errmsgJoinname: {
    color: 'red',
    top: h(1),
    marginLeft: w(16),
    fontSize: 10,
  },
  errormsg: {
    color: 'red',
    marginTop: 8,
    marginLeft: w(20),
    fontSize: 10,
  },
  errmsg2: {
    color: '#ff0000',
    marginTop: h(9),
    marginLeft: w(16),
    fontSize: 10,
  },
  joinInText: {
    fontSize: 30,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginTop: h(-1),
    //  marginTop:h(-2),
    color: '#383B3F'
  },
  subHeadingText: {
    alignSelf: 'center',
    fontSize: 16,
    color: '#8C8C8C',
    marginTop: h(8)
  },
  subHeadingText1: {
    alignSelf: 'center',
    fontSize: 16,
    color: '#8C8C8C',
  },
  introtxt: {
    alignSelf: 'center',
    fontSize: 16,
    color: '#8C8C8C',
    // top: h(25),
  },
  introtxt2: {
    alignSelf: 'center',
    fontSize: 16,
    color: '#8C8C8C',
    // top: h(23),
  },
  passimage: {
    top: h(32),
    width: h(5),
    right: w(7),
    height: h(5)
  },
  passimage2: {
    top: h(40),
    width: h(5),
    height: h(5),
    right: w(8),
  },
  passwordtxt: {
    alignSelf: 'center',
    color: '#8c8c8c',
    top: h(31),
    fontSize: 10,
    marginLeft: w(-2),
  },
  passwordHintTxt: {
    // alignSelf: 'center',
    // color: '#8c8c8c',
    marginTop: h(1),
    fontSize: 9,
    marginLeft: w(19),
    lineHeight: 13
  },
  socialButton3: {
    flexDirection: 'row',
    top: h(48),
    justifyContent: 'space-evenly',
    alignItems: 'center',
    left: w(0)
    // alignSelf: 'center',
    // marginLeft: w(7),
  },
  buttonContainer3: {
    alignSelf: 'center',
    backgroundColor: '#FE6F69',
    width: w(75),
    height: h(5),
    borderRadius: w(10),
    top: h(42),
    opacity: 0.6
  },
  modelView: {
    backgroundColor: '#000',
    alignSelf: 'center',
    height: h(36),
    width: w(83),
    marginTop: h(20),
    borderRadius: w(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  modelTxT: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 16,
    fontWeight: '400',
  },
  modelbuttonContainer: {
    alignSelf: 'center',
    backgroundColor: '#fa8072',
    width: w(60),
    height: h(6),
    borderRadius: w(10),
    marginTop: h(4),
    justifyContent: 'center',
  },
  modelAndText: {
    fontSize: 16,
    alignSelf: 'center',
    color: '#fff',
    fontWeight: '400',
  },

});
