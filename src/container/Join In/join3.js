import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
  Linking,
  Platform,
  BackHandler,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {IconAsset, Strings, UiColor} from '../../theme';
import {h, w} from '../../utils/Dimensions';
import Styles from './Style';
import {useSelector, useDispatch} from 'react-redux';
import {OtpApi} from '../../actions/Otp';
import AsyncStorage from '@react-native-community/async-storage';
import {SocialLoginApi} from '../../actions/SocialLogin';
import {
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager,
} from 'react-native-fbsdk';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from 'react-native-google-signin';
import styles from '../Splash/styles';
import {
  AppleButton,
  appleAuth,
} from '@invertase/react-native-apple-authentication';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const join3 = ({navigation, ...props}) => {
  const dispatch = useDispatch();
  const Login = useSelector((state) => console.log('otp', state));
  const singUpLogin = useSelector((state) => state.otp.OTP);
  const socialLoading = useSelector((state) => state.social.isLoading);
  const otpLoading = useSelector((state) => state.otp.isLoading);
  console.log('singUpLogin', singUpLogin);
  AsyncStorage.setItem('singUpLogin', JSON.stringify(singUpLogin));

  console.log('join3Props', props);
  const email = props.email;
  const mobile = props.mobile;
  const name = props.username;

  const [password, setpassword] = useState('');
  const [confirmPassword, setconfirmPassword] = useState('');
  const [hidePassword, sethidePassword] = useState(true);
  const [uiRender, setuiRender] = useState(false);
  const [errors, seterrors] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const [showPasswordagain, setShowPasswordagain] = useState(false);

  const [match, setMatch] = useState(false);

  const Validation = () => { //Validation Function
    seterrors('');
    setMatch(false);
    var text = password;
    let reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@@#\$%\^&\*])(?=.{8,})/;
    if (reg.test(text) === false) {
      setMatch(true);
      // seterrors('Your  word must contain an  uppercase letter, \n a number, and a special character.');
    } else if (confirmPassword == '' || confirmPassword == null) {
      seterrors('These new secret words must be the same, silly! ');
    } else if (password != confirmPassword) {
      seterrors('These new secret words must be the same, silly! ');
    } else {
      dispatch(OtpApi(email, mobile, password, confirmPassword, name)); // Calling api to Get otp 
    }
  };
  const url = () => { // Linking Url for term and condtion redirect page
    Linking.openURL('https://foodid.squarespace.com/terms-and-conditions');
  };
  const configureGoogle = () => {
    return GoogleSignin.configure({
      iosClientId:
        '911162193902-t727o372o94l32j9gh3q0c73alj5ah73.apps.googleusercontent.com',
      webClientId:
        '911162193902-q1a6lul636onqmhkd19im1kc84of8m64.apps.googleusercontent.com',
      offlineAccess: true,
    });
  };
  const googleLogin = async () => { // Google login
    configureGoogle();
    signOutGoogle();
    // console.log('gfygs')
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();

      let user = userInfo.user;

      let data = {
        name: user.name,
        socialId: user.id,
        email: user.email,
        deviceToken: null,
        loginType: null,
        userType: null,
        social_token: 'fsdsfs',
        mobile: '',
        provider: 'google',
      };
      dispatch(SocialLoginApi(data,  navigation)); //calling social login Api
    } catch (error) {
      console.log('error', error);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        //signin cancelled
      } else if (error.code === statusCodes.IN_PROGRESS) {
        //signin inprogress
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        //playservice not available
      }
    }
  };
  const signOutGoogle = async () => {
    console.log('google signout called.');

    console.log('before revokeaccess');
    await GoogleSignin.revokeAccess()
      .then((res) => console.log('revoke access->response->', res))
      .catch((err) => console.log('revoke access error -> ', err));
    console.log('revoked');
    await GoogleSignin.signOut()
      .then((res) => console.log('sign out response->', res))
      .catch((err) => console.log('signout error ->', err));
    console.log('signed out.');
  };
  const fbLogin = async () => {
    if (Platform.OS === 'android') {
      LoginManager.setLoginBehavior('web_only');
    }

    const result = await LoginManager.logInWithPermissions([
      'public_profile',
      'email',
    ]);

    console.log('result', result);
    if (result.isCancelled) {
      // throw errorConsts.cancelledLogin;
    } else {
      const {accessToken} = await AccessToken.getCurrentAccessToken();
      if (accessToken) {
        const user = await fetchFbProfile(accessToken);
        console.log('user fb', user);
        try {
          const currentAccessToken = await AccessToken.getCurrentAccessToken();

          const graphRequest = new GraphRequest(
            '/me',
            {
              accessToken: currentAccessToken.accessToken,
              parameters: {
                fields: {
                  string: 'picture.type(large)',
                },
              },
            },
            (error, result) => {
              if (error) {
                console.log(error);
              } else {
                console.log('result fblogin', result);
                let data = {
                  name: user.name,
                  socialId: user.id,
                  email: user.email,
                  mobile: null,
                  provider: 'facebook',
                  
                };
               
                dispatch(SocialLoginApi(data, navigation));
              }
            },
          );

          new GraphRequestManager().addRequest(graphRequest).start();
        } catch (error) {
          console.error(error);
        }
      }
    }
  };
  const fetchFbProfile = async (accessToken) => { //Facebook By login
    try {
      const response = await fetch(
        `https://graph.facebook.com/v2.5/me?fields=email,first_name,last_name,name,picture&access_token=${accessToken}`,
      );
      return response.json();
    } catch (error) {
      console.log('Unable to login, please retry.');
    }
  };
  async function onAppleButtonPress(updateCredentialStateForUser) {
    //   const appleAuthRequestResponse = await appleAuth.performRequest({
    //     requestedOperation: AppleAuthRequestOperation.LOGOUT
    //   });
    console.warn('Beginning Apple Authentication');
    appleAuth.Operation.LOGOUT;

    // start a login request
    try {
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
        requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
      });

      console.log('appleAuthRequestResponse', appleAuthRequestResponse);

      if (
        appleAuthRequestResponse.fullName.givenName !== null &&
        appleAuthRequestResponse.email !== null
      ) {
        let data = {
          name: appleAuthRequestResponse.fullName.givenName,
          socialId: ' ',
          email: appleAuthRequestResponse.email,
          mobile: '',
          deviceToken: appleAuthRequestResponse.identityToken,
          loginType: null,
          userType: null,
          provider: 'apple',
        };
        dispatch(SocialLoginApi(data, navigation));
      } else {
        let data = {
          name: '',
          socialId: ' ',
          email: '',
          mobile: '',
          deviceToken: appleAuthRequestResponse.identityToken,
          provider: 'apple',
        };
        dispatch(SocialLoginApi(data ,navigation));
      }
     
      console.warn(`Apple Authentication Completed, ${user}, ${email}`);
    } catch (error) {
      if (error.code === appleAuth.Error.CANCELED) {
        console.warn('User canceled Apple Sign in.');
      } else {
        console.error(error);
      }
    }
  }
  useEffect(() => {
    const backAction = () => {
      Actions.join2();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
    //return password and confirm password ui
  }, []);

  const urlPrivicy = () =>
  {
    Linking.openURL('https://foodid.squarespace.com/privacy-policy');
  }
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}>
      <ScrollView style={Styles.MainContainer}>
        <TouchableOpacity
          onPress={() => navigation.navigate('join2')}
          style={{marginTop: h(9), marginLeft: w(10),top:h(2)}}>
          <Image
            style={{width: w(2), height: h(2), resizeMode: 'contain'}}
            source={require('./../../assets/icon/BackB.png')}
          />
        </TouchableOpacity>
      
        <Text style={Styles.joinInText}>Join In</Text>
        <View style={{backgroundColor: '#fff', marginTop: h(7)}}>
          <Text style={Styles.introtxt}>
            Get creative enough you’ll remember
          </Text>
          <Text style={Styles.introtxt}>and no one else will guess.</Text>
        </View>
        <View
          style={{
            width: w(75),
            height: h(5),
            borderRadius: w(10),
            borderWidth: w(0.28),
            alignSelf: 'center',
            marginTop: 45,
            flexDirection: 'row',
            borderColor: errors != '' ? 'red' : '#000',
            alignItems: 'center',
          }}>
          <TextInput
            style={[
              Styles.inputFieldContainerSecretWord,
              {
                borderColor:
                  password.length == 0
                    ? '#000'
                    : '#000' && password == confirmPassword
                    ? '#5283C8'
                    : '#000' && errors != ''
                    ? 'red'
                    : '#000',
                fontSize: 16,
              },
            ]}
            placeholderTextColor="#383B3F"
            underlineColorAndroid="transparent"
            placeholder="Secret Word"
            autoCapitalize="none"
            underlineColorAndroid="transparent"
            secureTextEntry={!showPassword}
            onChangeText={(password) => setpassword(password)}
            value={password}
          />
          <TouchableOpacity
            onPress={() => setShowPassword(!showPassword)}
            style={Styles.eyeIcon}>
            {!showPassword ? (
              <Image
                source={require('../../assets/icon/EyeVector.png')}
                style={{resizeMode: 'contain', height: 10, width: 20}}
              />
            ) : (
              <Image
                source={require('../../assets/icon/openEyeVector.png')}
                style={{resizeMode: 'contain', height: 10, width: 20}}
              />
            )}
          </TouchableOpacity>
        </View>
        {errors != '' ? <Text style={Styles.errormsg}>{errors}</Text> : null}
        {errors === '' ? (
          <Text
            style={[
              Styles.passwordHintTxt,
              {color: match && errors == '' ? 'red' : '#8c8c8c'},
            ]}>
            Your secret word must be {'>'} 6 characters and contain{'\n'}an
            uppercase letter, a number, and a special character.
          </Text>
        ) : null}
        
        <View
          style={{
            width: w(75),
            height: h(5),
            borderRadius: w(10),
            borderWidth: w(0.28),
            alignSelf: 'center',
            marginTop: 55,
            flexDirection: 'row',
            borderColor: errors != '' ? 'red' : '#000',
            alignItems: 'center',
          }}>
          <TextInput
            style={[
              Styles.inputFieldContainerSecretWord,
              {
                borderColor:
                  password.length == 0
                    ? '#000'
                    : '#000' && password == confirmPassword
                    ? '#5283C8'
                    : '#000' && errors != ''
                    ? 'red'
                    : '#000',
                fontSize: 16,
              },
            ]}
            placeholderTextColor="#383B3F"
            underlineColorAndroid="transparent"
            placeholder="Secret Word Again"
            autoCapitalize="none"
            underlineColorAndroid="transparent"
            secureTextEntry={!showPasswordagain}
            onChangeText={(confirmPassword) =>
              setconfirmPassword(confirmPassword)
            }
            value={confirmPassword}
          />
          <TouchableOpacity
            onPress={() => setShowPasswordagain(!showPasswordagain)}
            style={Styles.eyeIcon}>
            {!showPasswordagain ? (
              <Image
                source={require('../../assets/icon/EyeVector.png')}
                style={{resizeMode: 'contain', height: 10, width: 20}}
              />
            ) : (
              <Image
                source={require('../../assets/icon/openEyeVector.png')}
                style={{resizeMode: 'contain', height: 10, width: 20}}
              />
            )}
          </TouchableOpacity>
        </View>

        
        <View style={{marginLeft: w(19)}}>
          <Text
            style={{
              color: '#9a9a9a',
              marginTop: h(1),
              lineHeight: 15,
              fontSize: 10,
            }}>
            By clicking ‘Get After It’ you agree to the following{'\n'}
            <Text
              onPress={url}
              style={{
                color: '#add8e6',
                fontSize: 10,
                lineHeight: 15,
                textDecorationLine:'underline'
              }}>
              Terms and Conditions{' '}
            </Text>
            <Text
              onPress={url}
              style={{
                color: '#9a9a9a',
                lineHeight: 15,
                fontSize: 10,
              }}>
              and{' '}
            </Text>
            <Text
              onPress={urlPrivicy}
              style={{
                color: '#add8e6',
                fontSize: 10,
                lineHeight: 15,
                textDecorationLine:'underline'
              }}>
              Privacy Policy 
            </Text>
            <Text>
              {' '}
              without{'\n'}reservation.(Probs don't read them anyways.)
            </Text>
          </Text>
        </View>

        <TouchableOpacity
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            backgroundColor: '#FE6F69',
            width: w(75),
            height: h(5),
            borderRadius: w(10),
            top: h(5),
            opacity: password.length == 0 ? 0.6 : 1,
          }}
          disabled={password.length == 0}
          onPress={Validation}>
          <Text style={Styles.AndText}>Get After It</Text>
        </TouchableOpacity>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignSelf: 'center',
            marginTop: h(12),
          }}>
          <TouchableOpacity
            onPress={fbLogin}
            style={{
              height: height / 14.5,
              width: height / 14.5,
              borderRadius: h(14.5),
              backgroundColor: '#FE6F69',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              style={{
                width: h(2.8),
                height: h(2.8),
                resizeMode: 'contain',
              }}
              source={require('../../assets/icon/Facebook-glass.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={googleLogin}
            style={{
              height: height / 14.5,
              width: height / 14.5,
              borderRadius: h(14.5),
              backgroundColor: '#FE6F69',
              alignItems: 'center',
              justifyContent: 'center',
              marginLeft: w(5),
            }}>
            <Image
              style={{
                width: h(2.8),
                height: h(2.8),
                resizeMode: 'contain',
              }}
              source={require('../../assets/icon/google-glass-logo.png')}
            />
          </TouchableOpacity>
          {Platform.OS === 'ios' ? (
            <TouchableOpacity
              onPress={onAppleButtonPress}
              style={{
                height: height / 14.5,
                width: height / 14.5,
                borderRadius: h(14.5),
                backgroundColor: '#FE6F69',
                alignItems: 'center',
                justifyContent: 'center',
                marginLeft: w(5),
              }}>
              <Image
                style={{
                  width: h(3),
                  height: h(3),
                  resizeMode: 'contain',
                  tintColor: '#fff',
                }}
                source={require('../../assets/icon/apple-glass-logo.png')}
              />
            </TouchableOpacity>
          ) : null}
        </View>

        <TouchableOpacity
          onPress={Actions.Login}
          style={Styles.ButtonJoinedpass}>
          <Text style={Styles.txtJoined}>Already Joined</Text>
        </TouchableOpacity>
        {socialLoading || otpLoading ? (
          <View
            style={{
              position: 'absolute',
              left: 0,
              right: 0,
              top: 5,
              bottom: 0,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: 'transparent',
            }}>
            <View>
              <ActivityIndicator size="large" color="#000" />
            </View>
          </View>
        ) : null}
        <View style={{height: 50}} />
      </ScrollView>
    </TouchableWithoutFeedback>
  );
};
export default join3;
