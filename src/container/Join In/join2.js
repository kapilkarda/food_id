import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Keyboard,
  TouchableWithoutFeedback,
  ScrollView,
  StyleSheet,
  Platform,
  BackHandler,
  Modal,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {IconAsset, Strings, UiColor} from '../../theme';
import {h, w} from '../../utils/Dimensions';
import Styles from './Style';
import AsyncStorage from '@react-native-community/async-storage';
import {Left} from 'native-base';
import {SocialLoginApi} from '../../actions/SocialLogin';
import {useSelector, useDispatch} from 'react-redux';
import {
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager,
} from 'react-native-fbsdk';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from 'react-native-google-signin';
import {RgistrationCheckApi} from '../../actions/RagistrationCheck';
import {
  AppleButton,
  appleAuth,
} from '@invertase/react-native-apple-authentication';
const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
const join2 = ({navigation, ...props}) => {
  const dispatch = useDispatch();
  const errordata = useSelector((state) => console.log('error', state));
  const checkError = useSelector((state) => state.usercheck.CHECK);
  const checkLoading = useSelector((state) => state.usercheck.isLoading);
  const socialLoading = useSelector((state) => state.social.isLoading);
  console.log('checkError', checkError);
  console.log('data1', props.data);
  const [email, setemail] = useState('');
  const [number, setnumber] = useState('');
  const [errors, seterrors] = useState('');
  const [errors2, seterrors2] = useState('');
  const [erroralreadyexist, seterroralreadyexist] = useState('');
  const [loginError, setLoginError] = useState('');
  const [emailError, setEmailError] = useState('');
  const [shareVisible, shareSetVisible] = useState(false);
  const [borderError, setborderError] = useState(false);
  const [borderEmail, setborderEmail] = useState(false);
  const [blank, setblank] = useState(false);
  const [blank2, setblank2] = useState(false);
// Data Object 
  let data = {
    userName: props.data,
    email: email,
    number: number,
  };
  AsyncStorage.setItem('number', number);

  console.log('dataget', data);

  const Validation = () => {  //Validation 
    setblank(false);
    setblank2(false);
    seterrors('');
    seterrors2('');
    var text = email;
    // let phoneReg =
    //   /^[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (((email == '' || email == null) && number === '') || number == null) {
      setblank(true);
      setblank2(true);
    } else if (reg.test(text) === false) {
      seterrors('Spam’s not our style. Email please.');
      seterrors2('Call you never. Phone number please.');
    } else if (email == '' || email == null) {
      setblank(true);
    } else if (reg.test(text) === false) {
      seterrors('Spam’s not our style. Email please.');
    } else if (number === '' || number == null) {
      setblank2(true);
    } else if (number.length < 10) {
      seterrors2('Call you never. Phone number please.');
    }else {
      check();
    }
  };
 
  const check = async () => {  //Check function if email and number already exist
    shareSetVisible(false);
    setborderEmail(false);
    setborderError(false);
    setLoginError('');
    setEmailError('');

    const status = await AsyncStorage.getItem('status');
    const username = props.data;
    console.log('username', username);
    dispatch(RgistrationCheckApi(email, number, username)); //Calling Api 
    setTimeout(async () => {
      const errorMessage = await AsyncStorage.getItem('message');
      console.log('errorMessage', errorMessage);
      if (errorMessage === 'Mobile Number Already Exist') {
        setborderError(true);
        setLoginError('This number’s been here before. Try ‘Already Joined’.');
      } else if (errorMessage === 'Email Used in Social Login') {
        shareSetVisible(true);
      }
      // else if (errorMessage === '')
      else if (errorMessage === 'Email Already Exist.') {
        setborderEmail(true);
        setEmailError('This email’s been here before. Try ‘Already Joined’.');
      }
    }, 2000);
  };
  const configureGoogle = () => { //LoginBy Google
    return GoogleSignin.configure({
      iosClientId:
        '911162193902-t727o372o94l32j9gh3q0c73alj5ah73.apps.googleusercontent.com',
      webClientId:
        '911162193902-q1a6lul636onqmhkd19im1kc84of8m64.apps.googleusercontent.com',
      offlineAccess: true,
    });
  };
  const googleLogin = async () => {
    configureGoogle();
    signOutGoogle();
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();

      let user = userInfo.user;

      let data = {
        name: user.name,
        socialId: user.id,
        email: user.email,
        deviceToken: null,
        loginType: null,
        userType: null,
        social_token: 'fsdsfs',
        mobile: '',
        provider: 'google',
      };
      console.log('data', data);
      dispatch(SocialLoginApi(data, navigation));
    } catch (error) {
      console.log('error', error);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        //signin cancelled
      } else if (error.code === statusCodes.IN_PROGRESS) {
        //signin inprogress
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        //playservice not available
      }
    }
  };
  const signOutGoogle = async () => {
    console.log('google signout called.');
    console.log('before revokeaccess');
    await GoogleSignin.revokeAccess()
      .then((res) => console.log('revoke access->response->', res))
      .catch((err) => console.log('revoke access error -> ', err));
    console.log('revoked');
    await GoogleSignin.signOut()
      .then((res) => console.log('sign out response->', res))
      .catch((err) => console.log('signout error ->', err));
    console.log('signed out.');
  };

  const fbLogin = async () => { //LoginBy Facebook
    if (Platform.OS === 'android') {
      LoginManager.setLoginBehavior('web_only');
    }

    const result = await LoginManager.logInWithPermissions([
      'public_profile',
      'email',
    ]);

    console.log('result', result);
    if (result.isCancelled) {
      // throw errorConsts.cancelledLogin;
    } else {
      const {accessToken} = await AccessToken.getCurrentAccessToken();
      if (accessToken) {
        const user = await fetchFbProfile(accessToken);
        console.log('user fb', user);
        try {
          const currentAccessToken = await AccessToken.getCurrentAccessToken();

          const graphRequest = new GraphRequest(
            '/me',
            {
              accessToken: currentAccessToken.accessToken,
              parameters: {
                fields: {
                  string: 'picture.type(large)',
                },
              },
            },
            (error, result) => {
              if (error) {
                console.log(error);
              } else {
                console.log('result fblogin', result);
                let data = {
                  name: user.name,
                  socialId: user.id,
                  email: user.email,
                  mobile: null,
                  provider: 'facebook',
                };

                dispatch(SocialLoginApi(data, navigation)); // Calling Social Login Api
              }
            },
          );

          new GraphRequestManager().addRequest(graphRequest).start();
        } catch (error) {
          console.error(error);
        }
      }
    }
  };
  const fetchFbProfile = async (accessToken) => {
    try {
      const response = await fetch(
        `https://graph.facebook.com/v2.5/me?fields=email,first_name,last_name,name,picture&access_token=${accessToken}`,
      );
      return response.json();
    } catch (error) {
      console.log('Unable to login, please retry.');
    }
  };
  async function onAppleButtonPress(updateCredentialStateForUser) { // Calling Apple login
   
    console.warn('Beginning Apple Authentication');
    appleAuth.Operation.LOGOUT;

    // start a login request
    try {
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
        requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
      });

      console.log('appleAuthRequestResponse', appleAuthRequestResponse);

      if (
        appleAuthRequestResponse.fullName.givenName !== null &&
        appleAuthRequestResponse.email !== null
      ) {
        let data = {
          name: appleAuthRequestResponse.fullName.givenName,
          socialId: ' ',
          email: appleAuthRequestResponse.email,
          mobile: '',
          deviceToken: appleAuthRequestResponse.identityToken,
          loginType: null,
          userType: null,
          provider: 'apple',
        };
        dispatch(SocialLoginApi(data, navigation));
      } else {
        let data = {
          name: '',
          socialId: ' ',
          email: '',
          mobile: '',
          deviceToken: appleAuthRequestResponse.identityToken,
          provider: 'apple',
        };
        dispatch(SocialLoginApi(data,  navigation));
      }

      console.warn(`Apple Authentication Completed, ${user}, ${email}`);
    } catch (error) {
      if (error.code === appleAuth.Error.CANCELED) {
        console.warn('User canceled Apple Sign in.');
      } else {
        console.error(error);
      }
    }
  }
  useEffect(() => {  //Back Hundler For Android Devices
    const backAction = () => {
      Actions.join();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  const closeModal = () => {
    shareSetVisible(false);
  };
  // Ui For join with email and number
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}>
      <View
        style={{
          flex: 1,
          backgroundColor: shareVisible ? '#000':'#fff',
          opacity: shareVisible ? 0.5 : 1,
        }}>
        <TouchableOpacity
          onPress={() => navigation.navigate('join')}
          style={{top: h(11), marginLeft: w(10), width: w(5), height: h(5)}}>
          <Image
            style={{width: w(2), height: h(2), resizeMode: 'contain'}}
            source={require('./../../assets/icon/BackB.png')}
          />
        </TouchableOpacity>
        <Text style={styles.joinInText}>Join In</Text>
        <View style={{marginTop: h(8)}}>
          <Text style={styles.subHeadText}>
            Just your standard contact info
          </Text>
          <Text style={styles.subHeadText}>fill-in-the-blanks.</Text>
        </View>
        <View
          style={[
            styles.emailContainer,
            {
              borderColor:
                errors != '' || borderEmail || blank ? 'red' : '#383B3F',
            },
          ]}>
          <TextInput
            style={{
              height: h(5),
              marginLeft: w(5),
              width: '90%',
              color: '#000',
              fontSize: 16,
            }}
            placeholderTextColor="#383B3F"
            underlineColorAndroid="transparent"
            placeholder="Email"
            keyboardType="email-address"
            underlineColorAndroid="transparent"
            onChangeText={(email) => setemail(email)}
            value={email}
          />
        </View>
        {errors != '' && emailError == '' ? (
          <Text style={styles.errmsg}>{errors}</Text>
        ) : null}
        {emailError != '' ? (
          <Text style={styles.errmsg}>{emailError}</Text>
        ) : null}
        <View
          style={[
            styles.emailContainer,
            {
              marginTop: h(8),
              borderColor:
                errors2 != '' || borderError || blank2 ? 'red' : '#383B3F',
            },
          ]}>
          <TextInput
            style={{
              height: h(5),
              marginLeft: w(5),
              width: '90%',
              color: '#000',
              fontSize: 16,
              borderColor: errors2 !== '' ? 'red' : '#383B3F',
            }}
            placeholderTextColor="#383B3F"
            underlineColorAndroid="transparent"
            placeholder="Phone"
            autoCapitalize="none"
            keyboardType="name-phone-pad"
            maxLength={14}
            underlineColorAndroid="transparent"
            onChangeText={(number) => setnumber(number)}
            value={number}
          />
        </View>
        {errors2 != '' && loginError == '' ? (
          <Text style={styles.errMesg2}>{errors2}</Text>
        ) : null}
        {loginError != '' ? (
          <Text style={styles.errMesg2}>{loginError}</Text>
        ) : null}
        <TouchableOpacity
          style={{
            height: h(5),
            width: w(75),
            backgroundColor: '#FE6F69',
            borderRadius: 40,
            marginTop: h(8),
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
          }}
          onPress={Validation}>
          <Text style={{color: '#fff', fontSize: 16, fontWeight: '500'}}>
            Next
          </Text>
        </TouchableOpacity>
        <View
          style={{
            flexDirection: 'row',
            marginTop: h(6),
            justifyContent: 'center',
            alignSelf: 'center',
          }}>
          <TouchableOpacity
            onPress={fbLogin}
            style={{
              height: height / 14.5,
              width: height / 14.5,
              borderRadius: h(14.5),
              backgroundColor: '#FE6F69',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              style={{
                width: h(2.8),
                height: h(2.8),
                resizeMode: 'contain',
              }}
              source={require('../../assets/icon/Facebook-glass.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={googleLogin}
            style={{
              height: height / 14.5,
              width: height / 14.5,
              borderRadius: h(14.5),
              backgroundColor: '#FE6F69',
              alignItems: 'center',
              justifyContent: 'center',
              marginLeft: w(5),
            }}>
            <Image
              style={{
                width: h(2.8),
                height: h(2.8),
                resizeMode: 'contain',
              }}
              source={require('../../assets/icon/google-glass-logo.png')}
            />
          </TouchableOpacity>
          {Platform.OS === 'ios' ? (
            <TouchableOpacity
              onPress={onAppleButtonPress}
              style={{
                height: height / 14.5,
                width: height / 14.5,
                borderRadius: h(14.5),
                backgroundColor: '#FE6F69',
                alignItems: 'center',
                justifyContent: 'center',
                marginLeft: w(5),
              }}>
              <Image
                style={{
                  width: h(3),
                  height: h(3),
                  resizeMode: 'contain',
                  tintColor: '#fff',
                }}
                source={require('../../assets/icon/apple-glass-logo.png')}
              />
            </TouchableOpacity>
          ) : null}
        </View>
        <TouchableOpacity
          style={{alignSelf: 'center', marginTop: h(5)}}
          onPress={Actions.Login}>
          <Text style={{color: '#FE6F69', fontSize: 13}}>Already Joined</Text>
        </TouchableOpacity>
        <Modal visible={shareVisible} transparent={true} animationType="slide">
          <View
            style={{
              backgroundColor: '#121619',
              alignSelf: 'center',
              height: h(40),
              width: w(80),
              marginTop: h(25),
              borderRadius: w(10),
            }}>
            <Text
              style={{
                color: '#fff',
                textAlign: 'center',

                fontSize: 20,
                marginTop: h(10),
                fontWeight: '400',
              }}>
              This email has signed in using social before. Try logging in{'\n'}
              that way instead.
            </Text>

            <TouchableOpacity
              style={{
                alignSelf: 'center',
                backgroundColor: '#fa8072',
                width: w(50),
                height: h(6),
                borderRadius: w(10),
                marginTop: h(4),
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={closeModal}>
              <Text
                style={{
                  fontSize: 18,

                  color: '#fff',
                }}>
                Got It!
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
        {socialLoading || checkLoading ? (
          <View
            style={{
              position: 'absolute',
              left: 0,
              right: 0,
              top: 5,
              bottom: 0,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: 'transparent',
            }}>
            <View>
              <ActivityIndicator size="large" color="#000" />
            </View>
          </View>
        ) : null}
      </View>
    </TouchableWithoutFeedback>
  );
};
export default join2;
const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    backgroundColor: '#fff',
  },
  joinInText: {
    color: '#383B3F',
    fontWeight: 'bold',
    fontSize: 30,
    alignSelf: 'center',
    marginTop: h(5),
  },
  subHeadText: {
    color: '#8C8C8C',
    // fontWeight: 'bold',
    fontSize: 16,
    alignSelf: 'center',
  },
  emailContainer: {
    height: h(5),
    width: w(75),
    borderColor: '#383B3F',
    borderWidth: 1,
    borderRadius: 40,
    alignSelf: 'center',
    marginTop: h(6),
    alignItems: 'center',
    justifyContent: 'center',
  },
  nextButton: {
    height: h(5),
    width: w(75),
    backgroundColor: '#FE6F69',
    borderRadius: 40,
    marginTop: h(8),
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  fbimage: {
    height: h(7.1),
    width: h(7),
    resizeMode: 'contain',
    // left: Platform.OS === 'android' ? w(1) : null,
  },
  googleImage: {
    height: h(7.1),
    width: h(7),
    resizeMode: 'contain',
  },
  errMesg2: {
    color: '#ff0000',
    fontSize: 10,
    marginLeft: w(18),
    marginTop: h(1),
  },
  errmsg: {
    color: 'red',
    marginTop: 8,
    marginLeft: w(18),
    fontSize: 10,
  },
});
