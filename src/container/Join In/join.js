import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  Keyboard,
  TouchableWithoutFeedback,
  BackHandler,
  Alert,
  ActivityIndicator,
  Modal,
  Dimensions,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {IconAsset, Strings, UiColor} from '../../theme';
import {h, w} from '../../utils/Dimensions';
import Styles from './Style';
import {SocialLoginApi} from './../../actions/SocialLogin';
import {useSelector, useDispatch} from 'react-redux';
import {
  AppleButton,
  appleAuth,
} from '@invertase/react-native-apple-authentication';

import {
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager,
} from 'react-native-fbsdk';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from 'react-native-google-signin';
import AsyncStorage from '@react-native-community/async-storage';
import {Platform} from 'react-native';

let checkedServerStatus = true;
const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
const join = ({navigation}) => {
  const dispatch = useDispatch();

  const socialLoading = useSelector((state) => state.social.isLoading);
  const getMaintainStatus = useSelector(
    (state) => state.getServerMaintainStatus,
  );

  const [name, setname] = useState('');
  const [errors, seterrors] = useState('');
  const [disabled, setDisabled] = useState(false);
  const [visibleServerModal, setVisibleServerModal] = useState(false);
  const userInfo = {};

  useEffect(() => {
    if (getMaintainStatus.serverStatus) {
      if (getMaintainStatus.serverStatus.data) {
        if (getMaintainStatus.serverStatus.data.status == true) {
          if (checkedServerStatus) {
            setVisibleServerModal(true);
            checkedServerStatus = false;
          }
        } else {
          setVisibleServerModal(false);
        }
      }
    }
  }, [getMaintainStatus]);
// Validation 
  const error = () => {
    console.log('hsfhs');
    var text = name;
    let reg = /^[a-zA-Z]{2,40}( [a-zA-Z]{2,40})+/; // Email Regex

    if (name == '' || name == null) {
      // errors.push("Name can't be empty");
      seterrors('Don’t be shy. Full name please.');
      console.log('name empty');
    } else if (name !== '' || name !== null) {
      if (reg.test(text) === false) {
        console.log('name  is not Correct');
        seterrors('Don’t be shy. Full name please.');
        // return false;
      } else {
        Actions.join2(name); //Navigation
      }
    }

    return errors;
  };
  const configureGoogle = () => {
    return GoogleSignin.configure({
      iosClientId:
        '911162193902-t727o372o94l32j9gh3q0c73alj5ah73.apps.googleusercontent.com',
      webClientId:
        '911162193902-q1a6lul636onqmhkd19im1kc84of8m64.apps.googleusercontent.com',
      offlineAccess: true,
    });
  };
  const googleLogin = async () => {  // Login By Google
    configureGoogle();
    signOutGoogle();
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();

      let user = userInfo.user;

      let data = {
        name: user.name,
        socialId: user.id,
        email: user.email,
        deviceToken: null,
        loginType: null,
        userType: null,
        social_token: 'fsdsfs',
        mobile: '',
        provider: 'google',
      };
      console.log('data', data);
      dispatch(SocialLoginApi(data,  navigation));
    } catch (error) {
      console.log('error', error);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        //signin cancelled
      } else if (error.code === statusCodes.IN_PROGRESS) {
        //signin inprogress
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        //playservice not available
      }
    }
  };
  const signOutGoogle = async () => {
    console.log('google signout called.');

    console.log('before revokeaccess');
    await GoogleSignin.revokeAccess()
      .then((res) => console.log('revoke access->response->', res))
      .catch((err) => console.log('revoke access error -> ', err));
    console.log('revoked');
    await GoogleSignin.signOut()
      .then((res) => console.log('sign out response->', res))
      .catch((err) => console.log('signout error ->', err));
    console.log('signed out.');
    AsyncStorage.setItem('SignUp', 'SignUp');
  };
  const fbLogin = async () => {   //Login BY Facebook
    if (Platform.OS === 'android') {
      LoginManager.setLoginBehavior('web_only');
    }

    const result = await LoginManager.logInWithPermissions([
      'public_profile',
      'email',
    ]);

    console.log('result', result);
    if (result.isCancelled) {
      // throw errorConsts.cancelledLogin;
    } else {
      const {accessToken} = await AccessToken.getCurrentAccessToken();
      if (accessToken) {
        const user = await fetchFbProfile(accessToken);
        console.log('user fb', user);
        try {
          const currentAccessToken = await AccessToken.getCurrentAccessToken();

          const graphRequest = new GraphRequest(
            '/me',
            {
              accessToken: currentAccessToken.accessToken,
              parameters: {
                fields: {
                  string: 'picture.type(large)',
                },
              },
            },
            (error, result) => {
              if (error) {
                console.log(error);
              } else {
                console.log('result fblogin', result);
                let data = {
                  name: user.name,
                  socialId: user.id,
                  email: user.email,
                  mobile: null,
                  provider: 'facebook',
                  social_token: user.id,
                };
                dispatch(SocialLoginApi(data,  navigation));
              }
            },
          );

          new GraphRequestManager().addRequest(graphRequest).start();
        } catch (error) {
          console.error(error);
        }
      }
    }
  };
  const fetchFbProfile = async (accessToken) => {
    try {
      const response = await fetch(
        `https://graph.facebook.com/v2.5/me?fields=email,first_name,last_name,name,picture&access_token=${accessToken}`,
      );
      return response.json();
    } catch (error) {
      console.log('Unable to login, please retry.');
    }
  };
  async function onAppleButtonPress(updateCredentialStateForUser) { //Login By Apple
    console.warn('Beginning Apple Authentication');
    appleAuth.Operation.LOGOUT;

    // start a login request
    try {
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
        requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
      });

      console.log('appleAuthRequestResponse', appleAuthRequestResponse);

      if (
        appleAuthRequestResponse.fullName.givenName !== null &&
        appleAuthRequestResponse.email !== null
      ) {
        let data = {
          name: appleAuthRequestResponse.fullName.givenName,
          socialId: ' ',
          email: appleAuthRequestResponse.email,
          mobile: '',
          deviceToken: appleAuthRequestResponse.identityToken,
          loginType: null,
          userType: null,
          provider: 'apple',
        };
        dispatch(SocialLoginApi(data, navigation));
      } else {
        let data = {
          name: '',
          socialId: ' ',
          email: '',
          mobile: '',
          deviceToken: appleAuthRequestResponse.identityToken,
          provider: 'apple',
        };
        dispatch(SocialLoginApi(data , navigation)); // Calling Social Login Api
      }

      console.warn(`Apple Authentication Completed, ${user}, ${email}`);
    } catch (error) {
      if (error.code === appleAuth.Error.CANCELED) {
        console.warn('User canceled Apple Sign in.');
      } else {
        console.error(error);
      }
    }
  }

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Hold on!', 'Are you sure you want to Exit the App', [
        {
          text: 'Cancel',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'YES', onPress: () => BackHandler.exitApp()},
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  const closeServerModal = () => {
    BackHandler.exitApp();
    setVisibleServerModal(false);
    checkedServerStatus = true;
  };
// Ui For join with Name
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}>
      <View style={Styles.MainContainer}>
       
        <View
          style={{flex: 0.85, alignItems: 'center', justifyContent: 'center'}}>
          <Text style={Styles.txt}>Join In</Text>
        </View>
        <View style={{flex: 1.3}}>
          <Text
            style={{
              alignSelf: 'center',
              fontSize: 16,
              color: '#8C8C8C',
              // top: h(20),
            }}>
            Let’s play the name game. You go first.
          </Text>
          <View>
            <TextInput
              style={[
                Styles.inputFieldContainerName,
                {borderColor: errors != '' ? 'red' : '#000', fontSize: 16},
              ]}
              placeholderTextColor="#383B3F"
              underlineColorAndroid="transparent"
              placeholder="Nom De Plume"
              autoCapitalize="none"
              //keyboardType=""
              underlineColorAndroid="transparent"
              onChangeText={(name) => setname(name)}
              value={name}
            />
          </View>
          {errors != '' ? (
            <Text style={Styles.errmsgJoinname}>{errors}</Text>
          ) : null}

          <TouchableOpacity
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'center',
              backgroundColor: '#FE6F69',
              width: w(75),
              height: h(5),
              borderRadius: w(10),
              marginTop: errors != '' ? h(5) : h(6.5),
              opacity: name.length != 0 ? 1 : errors != '' ? 0.6 : 1,
            }}
            disabled={name.length == 0 && errors != ''}
            onPress={error}>
            <Text style={Styles.AndText}>Next</Text>
          </TouchableOpacity>
        </View>

        <View style={{flex: 2}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignSelf: 'center',
              padding: 5,
            }}>
            <TouchableOpacity
              onPress={fbLogin}
              style={{
                height: height / 14.5,
                width: height / 14.5,
                borderRadius: h(14.5),
                backgroundColor: '#FE6F69',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Image
                style={{
                  width: h(2.8),
                  height: h(2.8),
                  resizeMode: 'contain',
                }}
                source={require('../../assets/icon/Facebook-glass.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={googleLogin}
              style={{
                height: height / 14.5,
                width: height / 14.5,
                borderRadius: h(14.5),
                backgroundColor: '#FE6F69',
                alignItems: 'center',
                justifyContent: 'center',
                marginLeft: w(5),
              }}>
              <Image
                style={{
                  width: h(2.8),
                  height: h(2.8),
                  resizeMode: 'contain',
                }}
                source={require('../../assets/icon/google-glass-logo.png')}
              />
            </TouchableOpacity>
            {Platform.OS === 'ios' ? (
              <TouchableOpacity
                onPress={onAppleButtonPress}
                style={{
                  height: height / 14.5,
                  width: height / 14.5,
                  borderRadius: h(14.5),
                  backgroundColor: '#FE6F69',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginLeft: w(5),
                }}>
                <Image
                  style={{
                    width: h(3),
                    height: h(3),
                    resizeMode: 'contain',
                    tintColor: '#fff',
                  }}
                  source={require('../../assets/icon/apple-glass-logo.png')}
                />
              </TouchableOpacity>
            ) : null}
          </View>
          <TouchableOpacity
            onPress={Actions.Login}
            style={{
              alignSelf: 'center',
              marginTop: h(7),
              marginLeft: Platform.OS === 'android' ? w(-2) : null,
            }}>
            <Text style={Styles.txtJoined}>Already Joined</Text>
          </TouchableOpacity>
        </View>
        {socialLoading ? (
          <View
            style={{
              position: 'absolute',
              left: 0,
              right: 0,
              top: 5,
              bottom: 0,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: 'transparent',
            }}>
            <View>
              <ActivityIndicator size="large" color="#000" />
            </View>
          </View>
        ) : null}
        <Modal
          visible={visibleServerModal}
          transparent={true}
          animationType="slide">
          <View
            style={{
              height: '100%',
              width: '100%',
              backgroundColor: '#00000090',
            }}>
            <View style={Styles.modelView}>
              <Text style={Styles.modelTxT}>
                Scheduled maintenance is {'\n'}happening. Like right now. {'\n'}
                We’ll try to make it quick {'\n'}and painless.
              </Text>

              <View style={Styles.modelbuttonContainer}>
                <TouchableOpacity
                  onPress={() => {
                    closeServerModal();
                  }}>
                  <Text style={Styles.modelAndText}>Got It!</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    </TouchableWithoutFeedback>
  );
};
export default join;
