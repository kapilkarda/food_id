const initialState = {
  SOCIAL: '',
  isLoading: false,
}

const social = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Social':
      return { isLoading: true };
    case 'Social':
      return { ...state, SOCIAL: action.payload, isLoading: false };
    case 'Failure_Social':
      return { isLoading: false };
    default:
      return state;
  }
};

export default social;
