const initialState = {
  RAGISTRATION: '',
  isLoading: false,
}

const ragistration = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Ragistration':
      return { isLoading: true };
    case 'Ragistration':
      return { ...state, RAGISTRATION: action.payload, isLoading: false };
    case 'Failure_Ragistration':
      return { isLoading: false };
    default:
      return state;
  }
};

export default ragistration;
