const initialState = {
  FORGET: '',
  isLoading: false,
}

const forget = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Forget':
      return { isLoading: true };
    case 'Forget':
      return { ...state, FORGET: action.payload, isLoading: false };
    case 'Failure_Forget':
      return { isLoading: false };
    default:
      return state;
  }
};

export default forget;
