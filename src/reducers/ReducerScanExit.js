const initialState = {
    SEARCHONHIDEEXIT:'',
    SCANONSHOWXIT:''
}
  
const scanonhide = (state = initialState, action) => {
  switch (action.type) {
      case 'ScannerHideOnExit':
        return{...state, SCANONHIDEEXIT : action.payload , loading:false};
        case 'ScannerOpenOnExit':
          return{...state, SCANONSHOWXIT : action.payload , loading:false};
    default:
      return state;
  }
};

export default scanonhide;  
  