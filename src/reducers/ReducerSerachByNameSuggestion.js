
const initialState = {
  SEARCHBYNAMESUGGESTION: '',
  isLoading: false,
}

const searchByNameSuggestion = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Suggestion_By_Name':
      return { isLoading: true };
    case 'Suggestion_By_Name':
      return { ...state, SEARCHBYNAMESUGGESTION: action.payload, isLoading: false };
    case 'Failure_Suggestion_By_Name':
      return { isLoading: false };
    default:
      return state;
  }
};

export default searchByNameSuggestion;
