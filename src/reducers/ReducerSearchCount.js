const initialState = {
  SEARCHCOUNT: '',
  isLoading: false,
};

const SearchCount = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Search_Count':
      return {isLoading: true};
    case 'Search_Count':
      return {...state, SEARCHCOUNT: action.payload, isLoading: false};
    case 'Failure_Search_Count':
      return {isLoading: false};
    default:
      return state;
  }
};

export default SearchCount;
