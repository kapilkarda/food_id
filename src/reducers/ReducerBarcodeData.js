const initialState = {
    BARCODEDATAS:'',
    ERROR:'',
    isLoading: false
  }
  
  const barcodeData = (state = initialState, action) => {
    switch (action.type) {
      
      case 'Request_BarcodeData':
          return{ isLoading: true };
      case 'BarcodeData':
          return{...state, BARCODEDATAS : action.payload , isLoading: false};
      case 'ErrorBarcode':
          return { ...state, ERROR: action.payload, loading: false };
      case 'Failure_BarcodeData':
          return { isLoading: false };
      default:
        return state;
    }
  };
  
  
  // const initialState = { isLoading: true, message: "" };
  
  // const userdetail = (state = initialState, action) => {
  //   return Object.assign({}, state, action.payload);
  // };
  
   export default barcodeData;
  
  