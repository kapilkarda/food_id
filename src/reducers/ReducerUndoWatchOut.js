const initialState = {
  UNDOWATCHOUT: '',
  isLoading: false,
}

const undowatchout = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Undo_Curat':
      return { isLoading: true };
    case 'Undo_Curat':
      return { ...state, UNDOWATCHOUT: action.payload, isLoading: false };
    case 'Failure_Undo_Curat':
      return { isLoading: false };
    default:
      return state;
  }
};

export default undowatchout;
