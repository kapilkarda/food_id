const initialState = {
  USERDETAIL: '',
  isLoading: false,
}

const userdetail = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_User_Detail':
      return { isLoading: true };
    case 'User_Detail':
      return { ...state, USERDETAIL: action.payload, isLoading: false };
    case 'Failure_User_Detail':
      return { isLoading: false };
    default:
      return state;
  }
};

export default userdetail;

