const initialState = {
  REMOVEFAV: '',
  isLoading: false,
}

const removefav = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Remove_Fav':
      return { isLoading: true };
    case 'Remove_Fav':
      return { ...state, REMOVEFAV: action.payload, isLoading: false };
    case 'Failure_Remove_Fav':
      return { isLoading: false };
    default:
      return state;
  }
};

export default removefav;
