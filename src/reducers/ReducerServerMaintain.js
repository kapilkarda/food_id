const initialState = {
    serverStatus: null,
    isLoading: false,
}

const getServerMaintainStatus = (state = initialState, action) => {
    switch (action.type) {
        case 'Request_SERVER_STATUS':
            return { isLoading: true };
        case 'SERVER_STATUS':
            return { ...state, serverStatus: action.payload, isLoading: false };
        case 'Failure_SERVER_STATUS':
            return { isLoading: false };
        default:
            return state;
    }
};

export default getServerMaintainStatus;
