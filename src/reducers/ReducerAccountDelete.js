const initialState = {
  DELETEACCOUNT: '',
  isLoading: false,
}

const deleteaccount = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Account_Delete':
      return { isLoading: true };
    case 'Account_Delete':
      return { ...state, DELETEACCOUNT: action.payload, isLoading: false };
    case 'Failure_Account_Delete':
      return { isLoading: false };
    default:
      return state;
  }
};

export default deleteaccount;

