const initialState = {
  EXPERIANCESHOW: '',
  isLoading: false,
}

const experianceshow = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_experiance_show':
      return { isLoading: true };
    case 'experiance_show':
      return { ...state, EXPERIANCESHOW: action.payload, isLoading: false };
    case 'Failure_experiance_show':
      return { isLoading: false };
    default:
      return state;
  }
};

export default experianceshow;
