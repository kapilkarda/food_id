const initialState = {
  FAVLIST: '',
  isLoading: false,
}

const favlist = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Fav_List':
      return { isLoading: true };
    case 'Fav_List':
      return { ...state, FAVLIST: action.payload, isLoading: false };
    case 'Failure_Fav_List':
      return { isLoading: false };
    default:
      return state;
  }
};

export default favlist
