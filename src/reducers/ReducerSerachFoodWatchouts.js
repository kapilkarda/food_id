const initialState = {
  SEARCHFOOD: '',
  isLoading: false,
}

const searchfood = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Search_Food':
      return { isLoading: true };
    case 'Search_Food':
      return { ...state, SEARCHFOOD: action.payload, isLoading: false };
    case 'Failure_Search_Food':
      return { isLoading: false };
    default:
      return state;
  }
};

export default searchfood;
