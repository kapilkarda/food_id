const initialState = {
  PRODUCTLIST: '',
  isLoading: false,
}

const productlist = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_ProductList':
      return { isLoading: true };
    case 'ProductList':
      return { ...state, PRODUCTLIST: action.payload, isLoading: false };
    case 'Failure_ProductList':
      return { isLoading: false };
    default:
      return state;
  }
};

export default productlist;
