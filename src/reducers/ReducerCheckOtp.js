const initialState = {
  otpCheck: '',
  isLoading: false,
};

const userOtpCheck = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Check_Otp':
      return {isLoading: true};
    case 'Check_Otp':
      return {...state, otpCheck: action.payload, isLoading: false};
    case 'Failure_Check_Otp':
      return {isLoading: false};
    default:
      return state;
  }
};

export default userOtpCheck;
