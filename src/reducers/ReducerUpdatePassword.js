const initialState = {
  PASSWORDUPDATE: '',
  isLoading: false,
}

const passwordupdate = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Password_Change':
      return { isLoading: true };
    case 'Password_Change':
      return { ...state, PASSWORDUPDATE: action.payload, isLoading: false };
    case 'Failure_Password_Change':
      return { isLoading: false };
    default:
      return state;
  }
};

export default passwordupdate;
