
const initialState = {
  SUBSCRIPTION: '',
  isLoading: false,
}

const subscription = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_subcriptionPlan':
      return { isLoading: true };
    case 'subcriptionPlan':
      return { ...state, SUBSCRIPTION: action.payload, isLoading: false };
    case 'Failure_subcriptionPlan':
      return { isLoading: false };
    default:
      return state;
  }
};

export default subscription;
