const initialState = {
  SCREENSTATUS : '',
};

const screenstatus = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_SCREEN_STATUS':
      return {...state, SCREENSTATUS: action.payload, isLoading: false};
    default:
      return state;
  }
};

export default screenstatus;
