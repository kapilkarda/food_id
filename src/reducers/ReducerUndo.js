
const initialState = {
  UNDO: '',
  isLoading: false,
}

const undo = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Undo_Food':
      return { isLoading: true };
    case 'Undo_Food':
      return { ...state, UNDO: action.payload, isLoading: false };
    case 'Failure_Undo_Food':
      return { isLoading: false };
    default:
      return state;
  }
};

export default undo;
