const initialState = {
  BARCODE: '',
  ERRORERROR: '',
  isLoading: false,
}

const barcode = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_barCode':
      return { isLoading: true };
    case 'barCode':
      return { ...state, BARCODE: action.payload, isLoading: false };
    case 'ErrorBarcode':
      return { ...state, ERROR: action.payload, isLoading: false };
      case 'Failure_barCode':
      return { isLoading: false }
    default:
      return state;
  }
};

export default barcode;
