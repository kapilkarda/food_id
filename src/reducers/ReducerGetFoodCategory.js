const initialState = {
  GETFOODCATEGORY: '',
  isLoading: false,
}

const getfoodcategory = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Get_Food_Category':
      return { isLoading: true };
    case 'Get_Food_Category':
      return { ...state, GETFOODCATEGORY: action.payload, isLoading: false };
    case 'Failure_Get_Food_Category':
      return { isLoading: false };
    default:
      return state;
  }
};

export default getfoodcategory;

