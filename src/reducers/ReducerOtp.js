const initialState = {
  OTP: '',
  isLoading: false,
}

const otp = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Otp':
      return { isLoading: true };
    case 'Otp':
      return { ...state, OTP: action.payload, isLoading: false };
    case 'Failure_Otp':
      return { isLoading: false }
    default:
      return state;
  }
};

export default otp;
