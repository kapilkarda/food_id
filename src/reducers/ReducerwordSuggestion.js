const initialState = {
  WORDS: '',
  isLoading: false,
}

const wordsuggestion = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Suggestion':
      return { isLoading: true };
    case 'Suggestion':
      return { ...state, WORDS: action.payload, isLoading: false };
    case 'Failure_Suggestion':
      return { isLoading: false };
    default:
      return state;
  }
};

export default wordsuggestion;
