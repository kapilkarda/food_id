const initialState = {
  ADDCURATWATCHOUT: '',
  isLoading: false,
}

const addcuratwatchout = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Add_Curat_Watchout':
      return { isLoading: true };
    case 'Add_Curat_Watchout':
      return { ...state, ADDCURATWATCHOUT: action.payload, isLoading: false };
    case 'Failure_Add_Curat_Watchout':
      return { isLoading: false };
    default:
      return state;
  }
};

export default addcuratwatchout;
