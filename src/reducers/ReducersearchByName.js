const initialState = {
  SEARCHBYNAME: '',
  isLoading: false,
}

const searchByname = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_SearchByName':
      return { isLoading: true };
    case 'SearchByName':
      return { ...state, SEARCHBYNAME: action.payload, isLoading: false };
    case 'Failure_SearchByName':
      return { isLoading: false };
    default:
      return state;
  }
};

export default searchByname;
