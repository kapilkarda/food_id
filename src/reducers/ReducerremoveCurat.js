const initialState = {
  REMOVECURAT: '',
  isLoading: false,
}

const removecurat = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Remove_Curat':
      return { isLoading: true };
    case 'Remove_Curat':
      return { ...state, REMOVECURAT: action.payload, isLoading: false };
    case 'Failure_Remove_Curat':
      return { isLoading: false };
    default:
      return state;
  }
};

export default removecurat;
