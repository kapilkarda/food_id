const initialState = {
  RESET: '',
  isLoading: false,
}

const reset = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Reset':
      return { isLoading: true };
    case 'Reset':
      return { ...state, RESET: action.payload, isLoading: false };
    case 'Failure_Reset':
      return { isLoading: false };
    default:
      return state;
  }
};

export default reset;
