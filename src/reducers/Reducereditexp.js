const initialState = {
    EDITEXP:'',
    isLoadingData: false
  }
  
  const editexp = (state = initialState, action) => {
    switch (action.type) {
      case 'Edit_Exp_Request': 
         return{ isLoadingData: true };
      case 'Editexp': 
         return{...state, EDITEXP : action.payload , isLoadingData: false};
      case 'Edit_Exp_Failure': 
        return{ isLoadingData: false };
      default:
        return state;
    }
  };
  
  
  // const initialState = { isLoading: true, message: "" };
  
  // const userdetail = (state = initialState, action) => {
  //   return Object.assign({}, state, action.payload);
  // };
  
   export default editexp;
  
  