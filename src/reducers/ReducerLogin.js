const initialState = {
  ERROR: '',
  isLoading: false,
}

const Login = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Login':
      return { isLoading: true };
    case 'Error':
      return { ...state, ERROR: action.payload, isLoading: false };
    case 'Success_Login':
      return { isLoading: false };
    case 'Failure_Login':
      return { isLoading: false };
    default:
      return state;
  }
};

export default Login;
