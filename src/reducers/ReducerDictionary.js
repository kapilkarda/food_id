const initialState = {
  DICTIONARY: '',
  isLoading: false,
}

const dictionary = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Dictionary':
      return { isLoading: true };
    case 'Dictionary':
      return { ...state, DICTIONARY: action.payload, isLoading: false };
    case 'Failure_Dictionary':
      return { isLoading: false };
    default:
      return state;
  }
};

export default dictionary;
