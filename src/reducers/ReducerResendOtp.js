const initialState = {
  resent: '',
  isLoading: false,
};

const userResendOtp = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Resend_Otp':
      return {isLoading: true};
    case 'Resend_Otp':
      return {...state, resent: action.payload, isLoading: false};
    case 'Failure_Resend_Otp':
      return {isLoading: false};
    default:
      return state;
  }
};

export default userResendOtp;
