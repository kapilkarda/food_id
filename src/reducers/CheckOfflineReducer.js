const initialState = {
  checkOfflineFeature: null,
  isLoading: false,
}

const CheckOfflineReducer = (state = initialState, action) => {

  switch (action.type) {
    case 'CHECK_OFFLINE':
      return { ...state, checkOfflineFeature: action.payload, isLoading: false };
    default:
      return state;
  }
};

export default CheckOfflineReducer;
