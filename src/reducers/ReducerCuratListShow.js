const initialState = {
  CURATESHOW: '',
  isLoading: false,
}

const curatewatchoutshow = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Curat_List_Show':
      return { isLoading: true };
    case 'Curat_List_Show':
      return { ...state, CURATESHOW: action.payload, isLoading: false };
    case 'Failure_Curat_List_Show':
      return { isLoading: false };
    default:
      return state;
  }
};

export default curatewatchoutshow;
