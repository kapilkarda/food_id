const initialState = {
    FREESUBSCRIPTION: '',
    isLoading: false,
}

const FreeSubscription = (state = initialState, action) => {
    switch (action.type) {
        case 'Request_Free_SubcriptionPlan':
            return { isLoading: true };
        case 'Free_SubcriptionPlan':
            return { ...state, FREESUBSCRIPTION: action.payload, isLoading: false };
        case 'Failure_Free_SubcriptionPlan':
            return { isLoading: false };
        default:
            return state;
    }
};

export default FreeSubscription;
