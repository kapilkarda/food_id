const initialState = {
  CHECK: '',
  isLoading: false,
}

const usercheck = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Check':
      return { isLoading: true };
    case 'Check':
      return { ...state, CHECK: action.payload, isLoading: false };
    case 'Failure_Check':
      return { isLoading: false };
    default:
      return state;
  }
};

export default usercheck;