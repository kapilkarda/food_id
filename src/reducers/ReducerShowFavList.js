const initialState = {
  SHOWFAVLIST: '',
  isLoading: false,
}

const showfavlist = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Show_Fav_List':
      return { isLoading: true };
    case 'Show_Fav_List':
      return { ...state, SHOWFAVLIST: action.payload, isLoading: false };
    case 'Failure_Show_Fav_List':
      return { isLoading: false };
    default:
      return state;
  }
};

export default showfavlist;
