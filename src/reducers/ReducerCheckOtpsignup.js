const initialState = {
    otpCheckSignUp: '',
    isLoading: false,
  };
  
  const userOtpChecksignup = (state = initialState, action) => {
    switch (action.type) {
      case 'Request_Check_OtpSignup':
        return {isLoading: true};
      case 'Check_OtpSignup':
        return {...state, otpCheckSignUp: action.payload, isLoading: false};
      case 'Failure_Check_OtpSignup':
        return {isLoading: false};
      default:
        return state;
    }
  };
  
  export default userOtpChecksignup;
  