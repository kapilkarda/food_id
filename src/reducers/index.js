import { combineReducers } from "redux";
import ReducerLogin from "./ReducerLogin";
import ReducerRagistration from "./ReducerRagistration";
import ReducerOtp from './ReducerOtp';
import ReducerSocialLogin from './ReducerSocialLogin';
import ReducerForget from './ReducerForget';
import ReducerReset from './ReducerReset';
import ReducerUserProfile from './ReducerUserProfile';
import ReducerChangeInfo from './ReducerChangeInfo';
import ReducerUpdatePassword from './ReducerUpdatePassword';
import ReducerFavList from './ReducerFavList';
import ReducerSearchFoodWatchouts from './ReducerSerachFoodWatchouts';
import ReducerShowFavList from './ReducerShowFavList';
import ReducerRemoveFav from './ReducerRemoveFav';
import ReducerGetFoodCategory from './ReducerGetFoodCategory';
import ReducerBrainNome from './ReducerBrainNome';
import ReducerBarCode from './ReducerBarCode';
import ReducerAddCuratWatchOut from './ReducerAddCuratWatchOut';
import ReducerCuratListShow from './ReducerCuratListShow';
import ReducerDictionary from './ReducerDictionary';
import ReducerwordSuggestion from './ReducerwordSuggestion';
import ReducerPhotoUpload from './ReducerPhotoUpload';
import ReducersearchByName from './ReducersearchByName';
import ReducerSubcategory from './ReducerSubCategory';
import ReducerAddExp from './ReducerAddExp';
import RedeucerExperiance from './ReducerExperiance';
import ReducerremoveCurat from './ReducerremoveCurat';
import Reducereditexp from './Reducereditexp';
import ReducerUndo from './ReducerUndo';
import ReducerSerachByNameSuggestion from './ReducerSerachByNameSuggestion';
import ReducerBarcodeData from './ReducerBarcodeData';
import ReducerCheck from './ReducerCheck';
import ReducerAccountDelete from './ReducerAccountDelete';
import ReducerScanExit from './ReducerScanExit';
import ReducerProductList from './ReducerProductList';
import ReducerUndoWatchOut from './ReducerUndoWatchOut';
import ReducerSubscriptionPlan from './ReducerSubscriptionPlan';
import ReducerSearchCount from './ReducerSearchCount';
import ReducerFreeSubsPlan from './ReducerFreeSubsPlan';
import CheckOfflineReducer from "./CheckOfflineReducer";
import ReducerServerMaintain from './ReducerServerMaintain';
import ReducerCheckOtp from './ReducerCheckOtp';
import ReducerResendOtp from './ReducerResendOtp';
import ReducerCheckOtpSignUp from './ReducerCheckOtpsignup'
import ReducerTermAndCondition from './ReducerTermAndCondition'

// combine all reducers
const rootReducer = combineReducers({
    Login: ReducerLogin,
    ragistration: ReducerRagistration,
    otp: ReducerOtp,
    social: ReducerSocialLogin,
    forget: ReducerForget,
    reset: ReducerReset,
    userdetail: ReducerUserProfile,
    chnageuserdetail: ReducerChangeInfo,
    passwordupdate: ReducerUpdatePassword,
    favlist: ReducerFavList,
    searchfood: ReducerSearchFoodWatchouts,
    showfavlist: ReducerShowFavList,
    removefav: ReducerRemoveFav,
    getfoodcategory: ReducerGetFoodCategory,
    Brainnoms: ReducerBrainNome,
    barcode: ReducerBarCode,
    addcuratwatchout: ReducerAddCuratWatchOut,
    curatewatchoutshow: ReducerCuratListShow,
    dictionary: ReducerDictionary,
    wordsuggestion: ReducerwordSuggestion,
    photoUpload: ReducerPhotoUpload,
    searchByname: ReducersearchByName,
    subcategory: ReducerSubcategory,
    addexperiance: ReducerAddExp,
    experianceshow: RedeucerExperiance,
    removecurat: ReducerremoveCurat,
    editexp: Reducereditexp,
    undo: ReducerUndo,
    searchByNameSuggestion: ReducerSerachByNameSuggestion,
    barcodeData: ReducerBarcodeData,
    usercheck: ReducerCheck,
    deleteaccount: ReducerAccountDelete,
    scanonhide: ReducerScanExit,
    productlist: ReducerProductList,
    undowatchout: ReducerUndoWatchOut,
    subscription: ReducerSubscriptionPlan,
    searchCount: ReducerSearchCount,
    freeSubs: ReducerFreeSubsPlan,
    checkOfflineFeature: CheckOfflineReducer,
    getServerMaintainStatus: ReducerServerMaintain,
    checkUserOtp: ReducerCheckOtp,
    resent: ReducerResendOtp,
    userOtpChecksignup:ReducerCheckOtpSignUp,
    termandcondition:ReducerTermAndCondition
});

export default rootReducer;
