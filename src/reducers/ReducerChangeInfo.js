const initialState = {
  CHANGEUSERDETAIL: '',
  isLoading: false,
}

const chnageuserdetail = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_User_Detail_Change':
      return { isLoading: true };
    case 'User_Detail_Change':
      return { ...state, CHANGEUSERDETAIL: action.payload, isLoading: false };
    case 'Failure_User_Detail_Change':
      return { isLoading: false };
    default:
      return state;
  }
};

export default chnageuserdetail;

