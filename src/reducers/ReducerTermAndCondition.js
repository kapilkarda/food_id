
const initialState = {
    TERMANDCONDITION: '',
    isLoading: false,
  }
  
  const termandcondition = (state = initialState, action) => {
    switch (action.type) {
      case 'TermAndCondition':
        return { isLoading: false };
      case 'TermAndCondition':
        return { ...state, TERMANDCONDITION: action.payload, isLoading: false };
      case 'FailTermAndCondition':
        return { isLoading: false };
      default:
        return state;
    }
  };
  
  export default termandcondition;
  