
const initialState = {
  SUBCATEGORY: '',
  isLoading: false,
}

const subcategory = (state = initialState, action) => {
  switch (action.type) {
    case 'Request_Get_Food_SubCategory':
      return { isLoading: true };
    case 'Get_Food_SubCategory':
      return { ...state, SUBCATEGORY: action.payload, isLoading: false };
    case 'Failure_Get_Food_SubCategory':
      return { isLoading: false };
    default:
      return state;
  }
};

export default subcategory;
